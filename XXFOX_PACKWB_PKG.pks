CREATE OR REPLACE PACKAGE APPS.XXFOX_PACKWB_PKG
AS
/* $Header: XXFOX_PACKWB_PKG.pks 1.0 2015/08/14 18:14:22PST Development  $
REM +==========================================================================+
REM |     Copyright (c) 2006 DAZ Systems, Inc .                                |
REM |                  El Segundo, CA 90505                                    |
REM |                All rights reserved                                       |
REM +==========================================================================+
REM
REM NAME
REM  XXFOX_PACKWB_PKG.pks
REM
REM PROGRAM TYPE
REM  PL/SQL Package
REM
REM PURPOSE
REM  The purpose of this package is to contain the custom procedures and
REM  functions used by DAZSI' Oracle Applications Customizations.
REM
REM  This package is primarily used for Custom Packing Workbench
REM HISTORY
REM ===========================================================================
REM  08/14/2015   Kranti K           Created
REM ===========================================================================*/

   PROCEDURE initialize_temp (
      p_trip_id            IN  NUMBER,
      p_delivery_id        IN  NUMBER,
      p_header_id          IN  NUMBER,
      p_delivery_status    IN  VARCHAR2,
      p_customer_id        IN  NUMBER,
      p_pack_slip_no       IN  NUMBER,
      p_order_type_id      IN  NUMBER,
      p_item_categ         IN  VARCHAR2,
      p_ship_method_code   IN  VARCHAR2,
      p_ship_date_from     IN  DATE,
      p_ship_date_to       IN  DATE,
      p_inventory_item_id  IN  NUMBER,
      p_organization_id    IN  NUMBER,
      p_percent_reserved   IN  NUMBER,
      p_delay_reason       IN  VARCHAR2,
      p_stag_loc_id        IN  NUMBER,
      x_session_id         IN  OUT NOCOPY NUMBER,
      x_batch_id           OUT NOCOPY NUMBER,
      x_return_status      OUT NOCOPY VARCHAR2,
      x_msg_data           OUT NOCOPY VARCHAR2
   );
   --
   FUNCTION get_delivery_status (p_delivery_id    IN NUMBER,
                                 p_status_code    IN VARCHAR2,
                                 p_planned_flag   IN VARCHAR2,
                                 p_audited_flag   IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION get_trip_status (p_trip_id        IN NUMBER, --kp
                                 p_status_code    IN VARCHAR2,
                                 p_planned_flag   IN VARCHAR2,
                                 p_audited_flag   IN VARCHAR2)
      RETURN VARCHAR2;
   --
   FUNCTION get_rsv_det (
     p_delivery_id     IN   NUMBER,
     p_trip_id         IN   NUMBER,
     p_inv_item_id     IN   NUMBER,
     p_inv_org_id      IN   NUMBER
   ) RETURN VARCHAR2;
   --

   PROCEDURE print_doc_set (
      p_trip_id             IN  NUMBER,
      p_delivery_id         IN  NUMBER,
      p_report_set_name     IN  VARCHAR2,
      x_return_status       OUT VARCHAR2,
      x_msg_data            OUT VARCHAR2
   );
   --
   PROCEDURE ship_confirm (
      p_trip_id        IN  VARCHAR2,
      x_return_status  OUT VARCHAR2,
      x_msg_data       OUT VARCHAR2
   );
   --
   PROCEDURE print_pack_slip (
      p_org_id            IN  NUMBER,
      p_delivery_id       IN  NUMBER,
      p_print_cust_item   IN  VARCHAR2 DEFAULT 'N',
      p_item_display      IN  VARCHAR2 DEFAULT 'D',
      p_print_mode        IN  VARCHAR2 DEFAULT 'DRAFT',
      p_sort              IN  VARCHAR2 DEFAULT 'INV',
      p_display_unshipped IN  VARCHAR2 DEFAULT 'Y',
      p_printer           IN  VARCHAR2,
      x_request_id        OUT NUMBER,
      x_msg_count         OUT NUMBER,
      x_return_status     OUT VARCHAR2,
      x_return_msg        OUT VARCHAR2
   );
   --
   PROCEDURE print_bol (
      p_org_id        IN  NUMBER,
      p_delivery_id   IN  NUMBER,
      p_trip_id       IN  VARCHAR2,
      p_printer       IN  VARCHAR2,
      x_request_id    OUT NUMBER,
      x_msg_count     OUT NUMBER,
      x_return_status OUT VARCHAR2,
      x_return_msg    OUT VARCHAR2
   );
   --
   PROCEDURE pick_release (
      x_return_status         OUT  VARCHAR2,
      x_msg_data              OUT  VARCHAR2,
      p_delivery_id           IN   NUMBER,
      p_trip_id             IN     NUMBER,  --KP
      p_stage_locator         IN   VARCHAR2,
      p_printer_name          IN   VARCHAR2,
      p_organization_id       IN   NUMBER,
      p_exclude_fg            IN   VARCHAR2,
      x_request_id            OUT  NUMBER,
      x_part_label_req_id     OUT  NUMBER
   );
   --
   PROCEDURE complete_delivery (
      x_return_status         OUT  VARCHAR2,
      x_msg_data              OUT  VARCHAR2,
      p_delivery_id           IN   NUMBER, -- raj N
      p_trip_id               IN NUMBER
   );
   --
   PROCEDURE print_pick_slip (
      p_organization_id   IN  NUMBER,
      p_delivery_id       IN  NUMBER,
      p_printer           IN  VARCHAR2,
      x_request_id        OUT NUMBER,
      x_msg_count         OUT NUMBER,
      x_return_status     OUT VARCHAR2,
      x_return_msg        OUT VARCHAR2
   );
   --

   PROCEDURE print_packing_slip (p_organization_id       IN     NUMBER,
                                 p_trip_id               IN     NUMBER,
                                 x_request_id           OUT     NUMBER,
                                 x_return_status        OUT     VARCHAR2,
                                 x_return_msg           OUT     VARCHAR2);

PROCEDURE update_ship_method
    (p_delivery_id      IN     NUMBER,
     p_ship_method_code IN     VARCHAR2,
     x_msg_count        OUT    NUMBER,
     x_return_status    OUT    VARCHAR2,
     x_return_msg       OUT    VARCHAR2
    );
	
PROCEDURE update_trip_ship_method
    (p_trip_id          IN     NUMBER,
     p_ship_method_code IN     VARCHAR2,
     x_msg_count        OUT    NUMBER,
     x_return_status    OUT    VARCHAR2,
     x_return_msg       OUT    VARCHAR2
    );	

   PROCEDURE create_update_freight1 (
      x_return_status1          OUT     VARCHAR2,
      x_msg_data1               OUT     VARCHAR2,
      p_trip_id1                 IN     NUMBER,  --KP
      p_freight_cost_id1         IN     NUMBER,
      p_freight_cost_type_id1    IN     NUMBER,
      p_unit_amount1             IN     NUMBER,
      p_action                   IN     VARCHAR2
   );
   FUNCTION xxfox_find_part_number  (p_item_id IN NUMBER) RETURN VARCHAR2;
   FUNCTION xxfox_sscc_label_data(p_trip NUMBER, p_val VARCHAR2) RETURN VARCHAR2;
   FUNCTION xxfox_find_part_number1  (p_item_id IN NUMBER) RETURN VARCHAR2;
END XXFOX_PACKWB_PKG;
/
EXIT;