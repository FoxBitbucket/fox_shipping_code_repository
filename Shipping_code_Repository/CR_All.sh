#!/bin/ksh
CRNO=CR_Test10001
INSTALL_PATH=$PWD
echo "$INSTALL_PATH"
DATE_TIME=`date | awk {' print $1"_"$2"_"$3"_"$6"_"$4 '}`
LOG_FILE=$CRNO"$DATE_TIME".log
echo $LOG_FILE
echo
echo "***Current Date and Time****" > $LOG_FILE
echo $DATE_TIME >> $LOG_FILE
echo "----------------------------" >> $LOG_FILE
echo >> $LOG_FILE

echo "Enter Global User Name (Apps).......: "
read apps_user

echo "Enter Global User Password.(Apps)...: "
read -s APPS_PSWD
APPSID=APPS/$APPS_PSWD

echo "Enter SID...........................: "
read dbsid
echo "Enter Port number...: "
read DB_PORT
echo "Enter db host address...: "
read HOST
# --------------------------------------------------------
#  Check if database SID is entered else prompt to get it
# --------------------------------------------------------
while [ -z "$dbsid" ];
do
    echo "Please enter Database SID : "
    read dbsid
done
cd $INSTALL_PATH
#Change permissions for all files
chmod 777 *
cd $INSTALL_PATH


# -----------  TABLES (6) ----------------
# XXFOX_AUDIT_LINES
# XXFOX_AUDIT_REFERENCES
# XXFOX_PACKING_DELIVERY_LINES_TBL
# XXFOX_TRIP_AUDIT_DETAILS_TBL
# XXFOX_AUDIT_DATA_FOR_EDI_TBL
# XXFOX_PACKING_DELIVERIES_TBL
# Alter Table for Audit workbench - XXFOX_AUDIT_LINES 
echo "Alter Table for Audit workbench - XXFOX_AUDIT_LINES "
echo
if sqlplus -s $APPSID @XXFOX_AUDIT_LINES_TBL.sql exit
then
    echo "Alter table for Audit workbench - XXFOX_AUDIT_LINES successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Alter table for Audit workbench - XXFOX_AUDIT_LINES unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# Alter Table for Audit workbench - XXFOX_AUDIT_REFERENCES 
echo "Alter Table for Audit workbench XXFOX_AUDIT_REFERENCES "
echo
if sqlplus -s $APPSID @XXFOX_AUDIT_REFERENCES_TBL.sql exit
then
    echo "Alter Table for Audit workbench - XXFOX_AUDIT_REFERENCES successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Alter Table for Audit workbench - XXFOX_AUDIT_REFERENCES unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# Alter Table for Packing workbench - XXFOX_PACKING_DELIVERY_LINES_TBL 
echo "Alter Table for Packing workbench XXFOX_PACKING_DELIVERY_LINES_TBL"
echo
if sqlplus -s $APPSID @XXFOX_PACKING_DELIVERY_LINES_TBL.sql exit
then
    echo "Alter Table for Packing workbench - XXFOX_PACKING_DELIVERY_LINES_TBL successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Alter Table for Packing workbench - XXFOX_PACKING_DELIVERY_LINES_TBL unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# Table for Audit workbench - XXFOX_TRIP_AUDIT_DETAILS_TBL 
echo " Table for Audit workbench XXFOX_TRIP_AUDIT_DETAILS_TBL"
echo
if sqlplus -s $APPSID @XXFOX_TRIP_AUDIT_DETAILS_TBL.sql exit
then
    echo "Table for Audit workbench - XXFOX_TRIP_AUDIT_DETAILS_TBL successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Table for Audit workbench - XXFOX_TRIP_AUDIT_DETAILS_TBL unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# Table for Packing workbench - XXFOX_AUDIT_DATA_FOR_EDI_TBL 
echo " Table for Packing workbench XXFOX_AUDIT_DATA_FOR_EDI_TBL"
echo
if sqlplus -s $APPSID @XXFOX_AUDIT_DATA_FOR_EDI_TBL.sql exit
then
    echo "Table for Packing workbench - XXFOX_AUDIT_DATA_FOR_EDI_TBL successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Table for Packing workbench - XXFOX_AUDIT_DATA_FOR_EDI_TBL unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# Alter Table for Packing workbench - XXFOX_PACKING_DELIVERIES_TBL 
echo "Alter Table for Packing workbench XXFOX_PACKING_DELIVERIES_TBL"
echo
if sqlplus -s $APPSID @XXFOX_PACKING_DELIVERIES_TBL.sql exit
then
    echo "Alter Table for Packing workbench - XXFOX_PACKING_DELIVERIES_TBL successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Alter Table for Packing workbench - XXFOX_PACKING_DELIVERIES_TBL unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# -----------  VIEWS (13) ----------------
# XXFOX_AUDIT_LINES_MEASURES_V
# XXFOX_AUDIT_LINES_V
# XXFOX_PACKING_DELIVERY_LINES_V
# XXFOX_PRINT_SSCC_LABEL_V
# XXFOX_LABEL_ISO_PICK_RELEASE_V
# XXFOX_WIP_JOBTRAVEL_LABEL_V
# XXFOX_LABEL_PARTS_V
# XXFOX_LABEL_BOX_V
# XXFOX_LABEL_CUST_PART_V
# XXFOX_LABEL_PALLET_V
# XXFOX_LABEL_SHIPMENT_V
# XXFOX_FEDEX_FREIGHT_CARRIER_V (Last Section)
# XXFOX_UPS_FREIGHT_CARRIER_V (Last Section)
# View Creation for Audit workbench - XXFOX_AUDIT_LINES_MEASURES_V 
echo "View creation for Audit workbench - XXFOX_AUDIT_LINES_MEASURES_V "
echo
if sqlplus -s $APPSID @XXFOX_AUDIT_LINES_MEASURES_V.sql exit
then
    echo "View Creation for Audit workbench - XXFOX_AUDIT_LINES_MEASURES_V successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "View Creation for Audit workbench - XXFOX_AUDIT_LINES_MEASURES_V unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# View Creation for Audit workbench - XXFOX_AUDIT_LINES_V
echo "View creation for Audit workbench - XXFOX_AUDIT_LINES_V "
echo
if sqlplus -s $APPSID @XXFOX_AUDIT_LINES_V.sql exit
then
    echo "View Creation for Audit workbench - XXFOX_AUDIT_LINES_V successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "View Creation for Audit workbench -  XXFOX_AUDIT_LINES_V unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# View Creation for Packing workbench - XXFOX_PACKING_DELIVERY_LINES_V
echo "View creation  - XXFOX_PACKING_DELIVERY_LINES_V "
echo
if sqlplus -s $APPSID @XXFOX_PACKING_DELIVERY_LINES_V.sql exit
then
    echo "View Creation for Packing workbench XXFOX_PACKING_DELIVERY_LINES_V successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "View Creation for Packing workbench XXFOX_PACKING_DELIVERY_LINES_V unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# View Creation for Packing workbench - XXFOX_PRINT_SSCC_LABEL_V
echo "View Creation for Packing workbench - XXFOX_PRINT_SSCC_LABEL_V "
echo
if sqlplus -s $APPSID @XXFOX_PRINT_SSCC_LABEL_V.sql exit
then
    echo "View Creation for Packing workbench XXFOX_PRINT_SSCC_LABEL_V successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "View Creation for Packing workbench XXFOX_PRINT_SSCC_LABEL_V unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi

# M
# View Creation for Packing workbench - XXFOX_LABEL_ISO_PICK_RELEASE_V
echo "View Creation for Packing workbench - XXXFOX_LABEL_ISO_PICK_RELEASE_V"
echo
if sqlplus -s $APPSID @XXFOX_LABEL_ISO_PICK_RELEASE_V.sql exit
then
    echo "View Creation for Packing workbench XXFOX_LABEL_ISO_PICK_RELEASE_V successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "View Creation for Packing workbench XXFOX_LABEL_ISO_PICK_RELEASE_V unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# View Creation  - XXFOX_WIP_JOBTRAVEL_LABEL_V
echo "View Creation for Packing workbench - XXFOX_WIP_JOBTRAVEL_LABEL_V"
echo
if sqlplus -s $APPSID @XXFOX_WIP_JOBTRAVEL_LABEL_V.sql exit
then
    echo "View Creation XXFOX_WIP_JOBTRAVEL_LABEL_V successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "View Creation for XXFOX_WIP_JOBTRAVEL_LABEL_V unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# View Creation for part label - XXFOX_LABEL_PARTS_V
echo "View Creation for Packing workbench Part Label - XXFOX_LABEL_PARTS_V "
echo
if sqlplus -s $APPSID @XXFOX_LABEL_PARTS_V.sql exit
then
    echo "View Creation for packing workbench part label XXFOX_LABEL_PARTS_V  successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "View Creation for packing workbench part label XXFOX_LABEL_PARTS_V unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi

# View Creation for Shipping Labels - XXFOX_LABEL_BOX_V 
echo "View creation for Shipping Labels - XXFOX_LABEL_BOX_V "
echo
if sqlplus -s $APPSID @XXFOX_LABEL_BOX_V.sql exit
then
    echo "View Creation for Shipping Labels  - XXFOX_LABEL_BOX_V successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "View Creation for Shipping Labels - XXFOX_LABEL_BOX_V unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# View Creation  - XXFOX_LABEL_CUST_PART_V
echo "View creation for Shipping Labels - XXFOX_LABEL_CUST_PART_V "
echo
if sqlplus -s $APPSID @XXFOX_LABEL_CUST_PART_V.sql exit
then
    echo "View Creation for Shipping Labels - XXFOX_LABEL_CUST_PART_V successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "View Creation for Shipping Labels -  XXFOX_LABEL_CUST_PART_V unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# View Creation for Shipping Labels - XXFOX_LABEL_PALLET_V
echo "View creation for Shipping Labels  - XXFOX_LABEL_PALLET_V"
echo
if sqlplus -s $APPSID @XXFOX_LABEL_PALLET_V.sql exit
then
    echo "View Creation for Shipping Labels XXFOX_LABEL_PALLET_V successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "View Creation for Shipping Labels XXFOX_LABEL_PALLET_V unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# View Creation for Shipping Labels - XXFOX_LABEL_SHIPMENT_V
echo "View Creation for Shipping Labels - XXFOX_LABEL_SHIPMENT_V "
echo
if sqlplus -s $APPSID @XXFOX_LABEL_SHIPMENT_V.sql exit
then
    echo "View Creation for Shipping Labels XXFOX_LABEL_SHIPMENT_V successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "View Creation for Shipping Labels XXFOX_LABEL_SHIPMENT_V unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi



# -----------  SEQUENCE (2) ----------------
# XXFOX_SSCC_CODE_S
# XXFOX_EDI_ASN_FILENAME_S
# Sequence Creation for Packing workbench - XXFOX_SSCC_CODE_S
echo "Sequence Creation for Packing workbench - XXFOX_SSCC_CODE_S "
echo
if sqlplus -s $APPSID @XXFOX_SSCC_CODE_S.sql exit
then
    echo "Sequence Creation for Packing workbench - XXFOX_SSCC_CODE_S successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Sequence Creation for Packing workbench - XXFOX_SSCC_CODE_S unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi

# Sequence Creation for EDI - XXFOX_EDI_ASN_FILENAME_S
echo "Sequence Creation for Packing workbench - XXFOX_EDI_ASN_FILENAME_S "
echo
if sqlplus -s $APPSID @XXFOX_EDI_ASN_FILENAME_S.sql exit
then
    echo "Sequence Creation for EDI - XXFOX_EDI_ASN_FILENAME_S successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Sequence Creation for EDI - XXFOX_EDI_ASN_FILENAME_S unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# -----------  PROCEDURE (1) ----------------
# XXFOX_TRIP_AUDIT_DETAILS_PRC
# Procedure Creation for Packing workbench - XXFOX_TRIP_AUDIT_DETAILS_PRC
echo "Procedure Creation for Packing workbench - XXFOX_TRIP_AUDIT_DETAILS_PRC "
echo
if sqlplus -s $APPSID @XXFOX_TRIP_AUDIT_DETAILS_PRC.sql exit
then
    echo "Procedure Creation for Packing workbench - XXFOX_TRIP_AUDIT_DETAILS_PRC successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Procedure Creation for Packing workbench - XXFOX_TRIP_AUDIT_DETAILS_PRC unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi



#-----------  PACKAGES(5)=10----------------
# XXFOX_PACKWB_PKG
# XXFOX_LABEL_UTIL_PKG
# XXFOX_WSHRDPAK_XMLP_PKG
# XXFOX_WSH_EDI_ASN_OUTBOUND
# XXFOX_SHIPPING_INTERFACE_PKG
# XXFOX_SHIPPING_INTERFACE_PKG (Last Section)
#Package Spec compiling - XXFOX_PACKWB_PKG.pks
if sqlplus -s $APPSID @XXFOX_PACKWB_PKG.pks exit
then
    echo "Compilation of package spec XXFOX_PACKWB_PKG.pks successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Compilation of package spec XXFOX_PACKWB_PKG.pks unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


#Package Body compiling - XXFOX_PACKWB_PKG.pkb
if sqlplus -s $APPSID @XXFOX_PACKWB_PKG.pkb exit
then
    echo "Compilation of Package Body XXFOX_PACKWB_PKG.pkb successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Compilation of Package Body XXFOX_PACKWB_PKG.pkb unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


#Package Spec compiling - XXFOX_LABEL_UTIL_PKG.pks
if sqlplus -s $APPSID @XXFOX_LABEL_UTIL_PKG.pks exit
then
    echo "Compilation of package spec XXFOX_LABEL_UTIL_PKG.pks successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Compilation of package spec XXFOX_LABEL_UTIL_PKG.pks unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


#Package Body compiling - XXFOX_LABEL_UTIL_PKG.pkb
if sqlplus -s $APPSID @XXFOX_LABEL_UTIL_PKG.pkb exit
then
    echo "Compilation of Package Body XXFOX_LABEL_UTIL_PKG.pkb successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Compilation of Package Body XXFOX_LABEL_UTIL_PKG.pkb unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


#Package Spec for FOX Packing Slip Report  -  XXFOX_WSHRDPAK_XMLP_PKG
if sqlplus -s $APPSID @XXFOX_WSHRDPAK_XMLP_PKG.pks exit
then
    echo "Compilation of package spec for FOX Packing Slip Report XXFOX_WSHRDPAK_XMLP_PKG.pks successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Compilation of package spec for FOX Packing Slip Report XXFOX_WSHRDPAK_XMLP_PKG.pks unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


#Package Body for FOX Packing Slip Report - XXFOX_WSHRDPAK_XMLP_PKG
if sqlplus -s $APPSID @XXFOX_WSHRDPAK_XMLP_PKG.pkb exit
then
    echo "Compilation of Package Body for FOX Packing Slip Report - XXFOX_WSHRDPAK_XMLP_PKG.pkb successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Compilation of Package Body for FOX Packing Slip Report - XXFOX_WSHRDPAK_XMLP_PKG.pkb unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# M
#Package Spec compiling - XXFOX_WSH_EDI_ASN_OUTBOUND.pks
if sqlplus -s $APPSID @XXFOX_WSH_EDI_ASN_OUTBOUND.pks exit
then
    echo "Compilation of package spec XXFOX_WSH_EDI_ASN_OUTBOUND.pks successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Compilation of package spec XXFOX_WSH_EDI_ASN_OUTBOUND.pks unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# M
#Package Body compiling - XXFOX_WSH_EDI_ASN_OUTBOUND.pkb
if sqlplus -s $APPSID @XXFOX_WSH_EDI_ASN_OUTBOUND.pkb exit
then
    echo "Compilation of Package Body XXFOX_WSH_EDI_ASN_OUTBOUND.pkb successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Compilation of Package Body XXFOX_WSH_EDI_ASN_OUTBOUND.pkb unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi



#-----------  Anonymos Block (1) ----------------
# M
#Anonymos Block compiling - XX_PICK_RLS_LABEL.sql
if sqlplus -s $APPSID @XX_PICK_RLS_LABEL.sql exit
then
    echo "Anonymos Block XX_PICK_RLS_LABEL.sql successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Anonymos Block XX_PICK_RLS_LABEL.sql unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi



# -----------  REPORTS (10)----------------
# FOX Packing Slip Report
# FOX Shipment Audit Details Report
# FOX Pick Release Report
# Move Order Pick Slip Report
# FOX Pick Slip Report
# FOX Packing Workbench Part Label
# FOX EDI ASN Outbound
# FOX Pallet Label Document
# FOX Pick Release Shipping Label
# FOX RTV Shipping Report
# --------------------------------------------------------
#  FOX Packing Slip Report (5)
# --------------------------------------------------------

#Concurrent Program
echo "XXFOX_PACK_REP Concurrent Program"
echo "XXFOX_PACK_REP Concurrent Program" >> $LOG_FILE
echo
$FND_TOP/bin/FNDLOAD $APPSID 0 Y UPLOAD $FND_TOP/patch/115/import/afcpprog.lct XXFOX_PACK_REP.ldt - WARNING=YES UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE 
if [ "$?" = "0" ]
then
   echo "XXFOX_PACK_REP Concurrent Program uploaded successfully"
   echo "XXFOX_PACK_REP Concurrent Program uploaded successfully" >> $LOG_FILE
else
   echo "Failed to upload XXFOX_PACK_REP Concurrent Program"
   echo "Failed to upload XXFOX_PACK_REP Concurrent Program" >> $LOG_FILE
fi


#Request Group
echo "Uploading  XXFOX_PACK_REP_RG Request Group"
echo "Uploading  XXFOX_PACK_REP_RG Request Group" >> $LOG_FILE
echo
$FND_TOP/bin/FNDLOAD $APPSID 0 Y UPLOAD $FND_TOP/patch/115/import/afcpreqg.lct XXFOX_PACK_REP_RG.ldt UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE
if [ "$?" = "0" ]
then
   echo "XXFOX_PACK_REP_RG Request Group uploaded successfully"
   echo "XXFOX_PACK_REP_RG Request Group uploaded successfully" >> $LOG_FILE
else
   echo "Failed to upload  XXFOX_PACK_REP_RG Request Group"
   echo "Failed to upload  XXFOX_PACK_REP_RG Request Group" >> $LOG_FILE
fi


#Data Definition
echo "Uploading data definition and template definition: XXFOX_PACK_REP_DD.ldt "
echo "Uploading data definition and template definition: XXFOX_PACK_REP_DD.ldt " >> $LOG_FILE
echo
$FND_TOP/bin/FNDLOAD $APPSID O Y UPLOAD $XDO_TOP/patch/115/import/xdotmpl.lct XXFOX_PACK_REP_DD.ldt CUSTOM_MODE=FORCE
if [ "$?" = "0" ]
then
   echo "Uploading data definition and template definition successful: XXFOX_PACK_REP_DD.ldt"
   echo "Uploading data definition and template definition successful: XXFOX_PACK_REP_DD.ldt" >> $LOG_FILE
else
   echo "Failed to Upload data definition and template definition: XXFOX_PACK_REP_DD.ldt"
   echo "Failed to Upload data definition and template definition: XXFOX_PACK_REP_DD.ldt" >> $LOG_FILE
   exit 1
fi


#Data Templet
echo "Uploading xml Data template XXFOX_PACK_REP_DT.xml"
echo "Uploading xml Data template XXFOX_PACK_REP_DT.xml" >> $LOG_FILE
java oracle.apps.xdo.oa.util.XDOLoader UPLOAD -DB_USERNAME $apps_user -DB_PASSWORD $APPS_PSWD -JDBC_CONNECTION $HOST:$DB_PORT:$dbsid -APPS_SHORT_NAME XXFOX -LOB_TYPE DATA_TEMPLATE   -LOB_CODE XXFOX_PACK_REP -LANGUAGE en -TERRITORY US -XDO_FILE_TYPE XML-DATA-TEMPLATE -FILE_NAME  XXFOX_PACK_REP_DT.xml -NLS_LANG ENGLISH_UNITED STATES.WE8ISO8859P1 -CUSTOM_MODE FORCE
if [ "$?" = "0" ]
then
   echo "Uploading xml Data template successful: XXFOX_PACK_REP_DT.xml"
   echo "Uploading xml Data template successful: XXFOX_PACK_REP_DT.xml" >> $LOG_FILE
else
   echo "Failed to Upload xml Data template : XXFOX_PACK_REP_DT.xml"
   echo "Failed to Upload xml Data template : XXFOX_PACK_REP_DT.xml" >> $LOG_FILE
   exit 1
fi


#Templet
echo "Uploading template XXFOX_PACK_REP_RTF.rtf"
echo "Uploading template XXFOX_PACK_REP_RTF.rtf" >> $LOG_FILE
java oracle.apps.xdo.oa.util.XDOLoader UPLOAD -DB_USERNAME $apps_user -DB_PASSWORD $APPS_PSWD -JDBC_CONNECTION $HOST:$DB_PORT:$dbsid -APPS_SHORT_NAME XXFOX -LOB_TYPE TEMPLATE_SOURCE -LOB_CODE XXFOX_PACK_REP -LANGUAGE en -TERRITORY US -XDO_FILE_TYPE RTF -FILE_CONTENT_TYPE ’application/rtf’ -FILE_NAME  XXFOX_PACK_REP_RTF.rtf  -NLS_LANG ENGLISH_UNITED STATES.WE8ISO8859P1 -CUSTOM_MODE FORCE
if [ "$?" = "0" ]
then
   echo "Uploading template successful: XXFOX_PACK_REP_RTF.rtf"
   echo "Uploading template successful: XXFOX_PACK_REP_RTF.rtf" >> $LOG_FILE
else
   echo "Failed to Upload template: XXFOX_PACK_REP_RTF.rtf"
   echo "Failed to Upload xml Data template: XXFOX_PACK_REP_RTF.rtf" >> $LOG_FILE
   exit 1
fi

# --------------------------------------------------------
#  FOX Shipment Audit Details Report (5)
# --------------------------------------------------------
#Concurrent Program
echo "XXFOX_WSH_AUD_DTL Concurrent Program"
echo "XXFOX_WSH_AUD_DTL Concurrent Program" >> $LOG_FILE
echo
$FND_TOP/bin/FNDLOAD $APPSID 0 Y UPLOAD $FND_TOP/patch/115/import/afcpprog.lct XXFOX_WSH_AUD_DTL.ldt UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE 
if [ "$?" = "0" ]
then
   echo "XXFOX_WSH_AUD_DTL Concurrent Program uploaded successfully"
   echo "XXFOX_WSH_AUD_DTL Concurrent Program uploaded successfully" >> $LOG_FILE
else
   echo "Failed to upload XXFOX_WSH_AUD_DTL Concurrent Program"
   echo "Failed to upload XXFOX_WSH_AUD_DTL Concurrent Program" >> $LOG_FILE
fi


#Request Group
echo "Uploading  XXFOX_WSH_AUD_DTL_RG Request Group"
echo "Uploading  XXFOX_WSH_AUD_DTL_RG Request Group" >> $LOG_FILE
echo
$FND_TOP/bin/FNDLOAD $APPSID 0 Y UPLOAD $FND_TOP/patch/115/import/afcpreqg.lct XXFOX_WSH_AUD_DTL_RG.ldt UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE
if [ "$?" = "0" ]
then
   echo "XXFOX_WSH_AUD_DTL_RG Request Group uploaded successfully"
   echo "XXFOX_WSH_AUD_DTL_RG Request Group uploaded successfully" >> $LOG_FILE
else
   echo "Failed to upload  XXFOX_WSH_AUD_DTL_RG Request Group"
   echo "Failed to upload  XXFOX_WSH_AUD_DTL_RG Request Group" >> $LOG_FILE
fi


#Data Definition
echo "Uploading data definition and template definition: XXFOX_WSH_AUD_DTL_DD.ldt "
echo "Uploading data definition and template definition: XXFOX_WSH_AUD_DTL_DD.ldt " >> $LOG_FILE
echo
$FND_TOP/bin/FNDLOAD $APPSID O Y UPLOAD $XDO_TOP/patch/115/import/xdotmpl.lct XXFOX_WSH_AUD_DTL_DD.ldt CUSTOM_MODE=FORCE
if [ "$?" = "0" ]
then
   echo "Uploading data definition and template definition successful: XXFOX_WSH_AUD_DTL_DD.ldt"
   echo "Uploading data definition and template definition successful: XXFOX_WSH_AUD_DTL_DD.ldt" >> $LOG_FILE
else
   echo "Failed to Upload data definition and template definition: XXFOX_WSH_AUD_DTL_DD.ldt"
   echo "Failed to Upload data definition and template definition: XXFOX_WSH_AUD_DTL_DD.ldt" >> $LOG_FILE
   exit 1
fi


#Data Templet
echo "Uploading xml Data template XXFOX_WSH_AUD_DTL_DT.xml"
echo "Uploading xml Data template XXFOX_WSH_AUD_DTL_DT.xml" >> $LOG_FILE
java oracle.apps.xdo.oa.util.XDOLoader UPLOAD -DB_USERNAME $apps_user -DB_PASSWORD $APPS_PSWD -JDBC_CONNECTION $HOST:$DB_PORT:$dbsid -APPS_SHORT_NAME XXFOX -LOB_TYPE DATA_TEMPLATE   -LOB_CODE XXFOX_WSH_AUD_DTL -LANGUAGE en -TERRITORY US -XDO_FILE_TYPE XML-DATA-TEMPLATE -FILE_NAME XXFOX_WSH_AUD_DTL_DT.xml -NLS_LANG ENGLISH_UNITED STATES.WE8ISO8859P1 -CUSTOM_MODE FORCE
if [ "$?" = "0" ]
then
   echo "Uploading xml Data template successful: XXFOX_WSH_AUD_DTL_DT.xml"
   echo "Uploading xml Data template successful: XXFOX_WSH_AUD_DTL_DT.xml" >> $LOG_FILE
else
   echo "Failed to Upload xml Data template : XXFOX_WSH_AUD_DTL_DT.xml"
   echo "Failed to Upload xml Data template : XXFOX_WSH_AUD_DTL_DT.xml" >> $LOG_FILE
   exit 1
fi


#Templet
echo "Uploading template XXFOX_WSH_AUD_DTL_RTF.rtf"
echo "Uploading template XXFOX_WSH_AUD_DTL_RTF.rtf" >> $LOG_FILE
java oracle.apps.xdo.oa.util.XDOLoader UPLOAD -DB_USERNAME $apps_user -DB_PASSWORD $APPS_PSWD -JDBC_CONNECTION $HOST:$DB_PORT:$dbsid -APPS_SHORT_NAME XXFOX -LOB_TYPE TEMPLATE_SOURCE -LOB_CODE XXFOX_WSH_AUD_DTL -LANGUAGE en -TERRITORY 00  -XDO_FILE_TYPE RTF -FILE_CONTENT_TYPE ’application/rtf’ -FILE_NAME  XXFOX_WSH_AUD_DTL_RTF.rtf  -NLS_LANG ENGLISH_UNITED STATES.WE8ISO8859P1 -CUSTOM_MODE FORCE
if [ "$?" = "0" ]
then
   echo "Uploading template successful: XXFOX_WSH_AUD_DTL_RTF.rtf"
   echo "Uploading template successful: XXFOX_WSH_AUD_DTL_RTF.rtf" >> $LOG_FILE
else
   echo "Failed to Upload template: XXFOX_WSH_AUD_DTL_RTF.rtf"
   echo "Failed to Upload xml Data template: XXFOX_WSH_AUD_DTL_RTF.rtf" >> $LOG_FILE
   exit 1
fi


# --------------------------------------------------------
#  FOX Pick Release Report (2)
# --------------------------------------------------------
# M
#Concurrent Program
echo "XXFOXWSHRDPIKPRTLBL Concurrent Program"
echo "XXFOXWSHRDPIKPRTLBL Concurrent Program" >> $LOG_FILE
echo
$FND_TOP/bin/FNDLOAD $APPSID 0 Y UPLOAD $FND_TOP/patch/115/import/afcpprog.lct XXFOXWSHRDPIKPRTLBL.ldt UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE 
if [ "$?" = "0" ]
then
   echo "XXFOXWSHRDPIKPRTLBL Concurrent Program uploaded successfully"
   echo "XXFOXWSHRDPIKPRTLBL Concurrent Program uploaded successfully" >> $LOG_FILE
else
   echo "Failed to upload XXFOXWSHRDPIKPRTLBL Concurrent Program"
   echo "Failed to upload XXFOXWSHRDPIKPRTLBL Concurrent Program" >> $LOG_FILE
fi


# M
#Data Templet
echo "Uploading xml Data template XXFOXWSHRDPIKPRTLBL.xml"
echo "Uploading xml Data template XXFOXWSHRDPIKPRTLBL.xml" >> $LOG_FILE
java oracle.apps.xdo.oa.util.XDOLoader UPLOAD -DB_USERNAME $apps_user -DB_PASSWORD $APPS_PSWD -JDBC_CONNECTION $HOST:$DB_PORT:$dbsid -APPS_SHORT_NAME XXFOX -LOB_TYPE DATA_TEMPLATE   -LOB_CODE XXFOXWSHRDPIKPRTLBL -LANGUAGE en -TERRITORY US -XDO_FILE_TYPE XML-DATA-TEMPLATE -FILE_NAME XXFOXWSHRDPIKPRTLBL.xml -NLS_LANG ENGLISH_UNITED STATES.WE8ISO8859P1 -CUSTOM_MODE FORCE
if [ "$?" = "0" ]
then
   echo "Uploading xml Data template successful: XXFOXWSHRDPIKPRTLBL.xml"
   echo "Uploading xml Data template successful: XXFOXWSHRDPIKPRTLBL.xml" >> $LOG_FILE
else
   echo "Failed to Upload xml Data template : XXFOXWSHRDPIKPRTLBL.xml"
   echo "Failed to Upload xml Data template : XXFOXWSHRDPIKPRTLBL.xml" >> $LOG_FILE
   exit 1
fi

# --------------------------------------------------------
#  Move Order Pick Slip Report (1) 
# --------------------------------------------------------

#Templet
echo "Uploading template XXFOX_INVTOPKL_RTF.rtf"
echo "Uploading template XXFOX_INVTOPKL_RTF.rtf" >> $LOG_FILE
java oracle.apps.xdo.oa.util.XDOLoader UPLOAD -DB_USERNAME $apps_user -DB_PASSWORD $APPS_PSWD -JDBC_CONNECTION $HOST:$DB_PORT:$dbsid -APPS_SHORT_NAME XXFOX -LOB_TYPE TEMPLATE_SOURCE -LOB_CODE XXFOX_INVTOPKL -LANGUAGE en -TERRITORY 00  -XDO_FILE_TYPE RTF -FILE_CONTENT_TYPE ’application/rtf’ -FILE_NAME  XXFOX_INVTOPKL_RTF.rtf  -NLS_LANG ENGLISH_UNITED STATES.WE8ISO8859P1 -CUSTOM_MODE FORCE
if [ "$?" = "0" ]
then
   echo "Uploading template successful: XXFOX_INVTOPKL_RTF.rtf"
   echo "Uploading template successful: XXFOX_INVTOPKL_RTF.rtf" >> $LOG_FILE
else
   echo "Failed to Upload template: XXFOX_INVTOPKL_RTF.rtf"
   echo "Failed to Upload xml Data template: XXFOX_INVTOPKL_RTF.rtf" >> $LOG_FILE
   exit 1
fi


# --------------------------------------------------------
#  FOX Pick Slip Report (2)
# --------------------------------------------------------
# .rdf moving 
cp XXFOX_WSHRDPIK.rdf $XXFOX_TOP/reports/US/XXFOX_WSHRDPIK.rdf
if [ $? -ne 0 ]
then
    echo "Error copying XXFOX_WSHRDPIK.rdf to $XXFOX_TOP/reports/US/XXFOX_WSHRDPIK.rdf" >>$LOG_FILE
    echo " ">>$LOG_FILE
    exit 1
else
    echo "Successfully coppied XXFOX_WSHRDPIK.rdf to $XXFOX_TOP/reports/US/XXFOX_WSHRDPIK.rdf">>$LOG_FILE
    echo " ">>$LOG_FILE
fi

#Templet
echo "Uploading template XXFOX_WSHRDPIK_RTF.rtf"
echo "Uploading template XXFOX_WSHRDPIK_RTF.rtf" >> $LOG_FILE
java oracle.apps.xdo.oa.util.XDOLoader UPLOAD -DB_USERNAME $apps_user -DB_PASSWORD $APPS_PSWD -JDBC_CONNECTION $HOST:$DB_PORT:$dbsid -APPS_SHORT_NAME XXFOX -LOB_TYPE TEMPLATE_SOURCE -LOB_CODE XXFOX_WSHRDPIK -LANGUAGE en -TERRITORY US -XDO_FILE_TYPE RTF -FILE_CONTENT_TYPE ’application/rtf’ -FILE_NAME  XXFOX_WSHRDPIK_RTF.rtf  -NLS_LANG ENGLISH_UNITED STATES.WE8ISO8859P1 -CUSTOM_MODE FORCE
if [ "$?" = "0" ]
then
   echo "Uploading template successful: XXFOX_WSHRDPIK_RTF.rtf"
   echo "Uploading template successful: XXFOX_WSHRDPIK_RTF.rtf" >> $LOG_FILE
else
   echo "Failed to Upload template: XXFOX_WSHRDPIK_RTF.rtf"
   echo "Failed to Upload xml Data template: XXFOX_WSHRDPIK_RTF.rtf" >> $LOG_FILE
   exit 1
fi

# --------------------------------------------------------
#  FOX Packing Workbench Part Label - Program (1)
# --------------------------------------------------------
#Concurrent Program
echo "XXFOXPICKPARTLBL Concurrent Program"
echo "XXFOXPICKPARTLBL Concurrent Program" >> $LOG_FILE
echo
$FND_TOP/bin/FNDLOAD $APPSID 0 Y UPLOAD $FND_TOP/patch/115/import/afcpprog.lct XXFOXPICKPARTLBL.ldt UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE 
if [ "$?" = "0" ]
then
   echo "XXFOXPICKPARTLBL Concurrent Program uploaded successfully"
   echo "XXFOXPICKPARTLBL Concurrent Program uploaded successfully" >> $LOG_FILE
else
   echo "Failed to upload XXFOXPICKPARTLBL Concurrent Program"
   echo "Failed to upload XXFOXPICKPARTLBL Concurrent Program" >> $LOG_FILE
fi


# --------------------------------------------------------
#  FOX EDI ASN Outbound (1)
# --------------------------------------------------------
#Concurrent Program
echo "XXFOX_WSH_EDI_ASN_OUTBOUND_CP Concurrent Program"
echo "XXFOX_WSH_EDI_ASN_OUTBOUND_CP Concurrent Program" >> $LOG_FILE
echo
$FND_TOP/bin/FNDLOAD $APPSID 0 Y UPLOAD $FND_TOP/patch/115/import/afcpprog.lct XXFOX_WSH_EDI_ASN_OUTBOUND_CP.ldt UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE 
if [ "$?" = "0" ]
then
   echo "XXFOX_WSH_EDI_ASN_OUTBOUND_CP Concurrent Program uploaded successfully"
   echo "XXFOX_WSH_EDI_ASN_OUTBOUND_CP Concurrent Program uploaded successfully" >> $LOG_FILE
else
   echo "Failed to upload XXFOX_WSH_EDI_ASN_OUTBOUND_CP Concurrent Program"
   echo "Failed to upload XXFOX_WSH_EDI_ASN_OUTBOUND_CP Concurrent Program" >> $LOG_FILE
fi

# --------------------------------------------------------
#  FOX Pallet Label Document (3)
# --------------------------------------------------------
#Concurrent Program
echo "XXFOX_PALLET_LABEL Concurrent Program"
echo "XXFOX_PALLET_LABEL Concurrent Program" >> $LOG_FILE
echo
$FND_TOP/bin/FNDLOAD $APPSID 0 Y UPLOAD $FND_TOP/patch/115/import/afcpprog.lct XXFOX_PALLET_LABEL.ldt - WARNING=YES UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE 
if [ "$?" = "0" ]
then
   echo "XXFOX_PALLET_LABEL Concurrent Program uploaded successfully"
   echo "XXFOX_PALLET_LABEL Concurrent Program uploaded successfully" >> $LOG_FILE
else
   echo "Failed to upload XXFOX_PALLET_LABEL Concurrent Program"
   echo "Failed to upload XXFOX_PALLET_LABEL Concurrent Program" >> $LOG_FILE
fi


#Data Templet
echo "Uploading xml Data template XXFOX_PALLET_LABEL.xml"
echo "Uploading xml Data template XXFOX_PALLET_LABEL.xml" >> $LOG_FILE
java oracle.apps.xdo.oa.util.XDOLoader UPLOAD -DB_USERNAME $apps_user -DB_PASSWORD $APPS_PSWD -JDBC_CONNECTION $HOST:$DB_PORT:$dbsid -APPS_SHORT_NAME XXFOX -LOB_TYPE DATA_TEMPLATE   -LOB_CODE XXFOX_PALLET_LABEL -LANGUAGE en -TERRITORY US -XDO_FILE_TYPE XML-DATA-TEMPLATE -FILE_NAME  XXFOX_PALLET_LABEL.xml -NLS_LANG ENGLISH_UNITED STATES.WE8ISO8859P1 -CUSTOM_MODE FORCE
if [ "$?" = "0" ]
then
   echo "Uploading xml Data template successful: XXFOX_PALLET_LABEL.xml"
   echo "Uploading xml Data template successful: XXFOX_PALLET_LABEL.xml" >> $LOG_FILE
else
   echo "Failed to Upload xml Data template : XXFOX_PALLET_LABEL.xml"
   echo "Failed to Upload xml Data template : XXFOX_PALLET_LABEL.xml" >> $LOG_FILE
   exit 1
fi


#Templet
echo "Uploading template XXFOX_PALLET_LABEL.rtf"
echo "Uploading template XXFOX_PALLET_LABEL.rtf" >> $LOG_FILE
java oracle.apps.xdo.oa.util.XDOLoader UPLOAD -DB_USERNAME $apps_user -DB_PASSWORD $APPS_PSWD -JDBC_CONNECTION $HOST:$DB_PORT:$dbsid -APPS_SHORT_NAME XXFOX -LOB_TYPE TEMPLATE_SOURCE -LOB_CODE XXFOX_PALLET_LABEL -LANGUAGE en -TERRITORY 00  -XDO_FILE_TYPE RTF -FILE_CONTENT_TYPE ’application/rtf’ -FILE_NAME  XXFOX_PALLET_LABEL.rtf  -NLS_LANG ENGLISH_UNITED STATES.WE8ISO8859P1 -CUSTOM_MODE FORCE
if [ "$?" = "0" ]
then
   echo "Uploading template successful: XXFOX_PALLET_LABEL.rtf"
   echo "Uploading template successful: XXFOX_PALLET_LABEL.rtf" >> $LOG_FILE
else
   echo "Failed to Upload template: XXFOX_PALLET_LABEL.rtf"
   echo "Failed to Upload xml Data template: XXFOX_PALLET_LABEL.rtf" >> $LOG_FILE
   exit 1
fi

# --------------------------------------------------------
#  FOX Pick Release Shipping Label (2)
# --------------------------------------------------------
#Concurrent Program
echo "XXFOXWSHRDPIKSHPLBL Concurrent Program"
echo "XXFOXWSHRDPIKSHPLBL Concurrent Program" >> $LOG_FILE
echo
$FND_TOP/bin/FNDLOAD $APPSID 0 Y UPLOAD $FND_TOP/patch/115/import/afcpprog.lct XXFOXWSHRDPIKSHPLBL.ldt UPLOAD_MODE=REPLACE CUSTOM_MODE=FORCE 
if [ "$?" = "0" ]
then
   echo "XXFOXWSHRDPIKSHPLBL Concurrent Program uploaded successfully"
   echo "XXFOXWSHRDPIKSHPLBL Concurrent Program uploaded successfully" >> $LOG_FILE
else
   echo "Failed to upload XXFOXWSHRDPIKSHPLBL Concurrent Program"
   echo "Failed to upload XXFOXWSHRDPIKSHPLBL Concurrent Program" >> $LOG_FILE
fi



#Data Templet
echo "Uploading xml Data template XXFOXWSHRDPIKSHPLBL.xml"
echo "Uploading xml Data template XXFOXWSHRDPIKSHPLBL.xml" >> $LOG_FILE
java oracle.apps.xdo.oa.util.XDOLoader UPLOAD -DB_USERNAME $apps_user -DB_PASSWORD $APPS_PSWD -JDBC_CONNECTION $HOST:$DB_PORT:$dbsid -APPS_SHORT_NAME XXFOX -LOB_TYPE DATA_TEMPLATE   -LOB_CODE XXFOXWSHRDPIKSHPLBL -LANGUAGE en -TERRITORY US -XDO_FILE_TYPE XML-DATA-TEMPLATE -FILE_NAME XXFOXWSHRDPIKSHPLBL.xml -NLS_LANG ENGLISH_UNITED STATES.WE8ISO8859P1 -CUSTOM_MODE FORCE
if [ "$?" = "0" ]
then
   echo "Uploading xml Data template successful: XXFOXWSHRDPIKSHPLBL.xml"
   echo "Uploading xml Data template successful: XXFOXWSHRDPIKSHPLBL.xml" >> $LOG_FILE
else
   echo "Failed to Upload xml Data template : XXFOXWSHRDPIKSHPLBL.xml"
   echo "Failed to Upload xml Data template : XXFOXWSHRDPIKSHPLBL.xml" >> $LOG_FILE
   exit 1
fi

# --------------------------------------------------------
#  FOX RTV Shipping Report (1)
# --------------------------------------------------------
#Data Templet
echo "Uploading xml Data template XXFOX_RCV_WSH_REP_DDN.xml"
echo "Uploading xml Data template XXFOX_RCV_WSH_REP_DDN.xml" >> $LOG_FILE
java oracle.apps.xdo.oa.util.XDOLoader UPLOAD -DB_USERNAME $apps_user -DB_PASSWORD $APPS_PSWD -JDBC_CONNECTION $HOST:$DB_PORT:$dbsid -APPS_SHORT_NAME XXFOX -LOB_TYPE DATA_TEMPLATE   -LOB_CODE XXFOXRCVSHIP -LANGUAGE en -TERRITORY US -XDO_FILE_TYPE XML-DATA-TEMPLATE -FILE_NAME XXFOX_RCV_WSH_REP_DDN.xml -NLS_LANG ENGLISH_UNITED STATES.WE8ISO8859P1 -CUSTOM_MODE FORCE
if [ "$?" = "0" ]
then
   echo "Uploading xml Data template successful: XXFOX_RCV_WSH_REP_DDN.xml"
   echo "Uploading xml Data template successful: XXFOX_RCV_WSH_REP_DDN.xml" >> $LOG_FILE
else
   echo "Failed to Upload xml Data template : XXFOX_RCV_WSH_REP_DDN.xml"
   echo "Failed to Upload xml Data template : XXFOX_RCV_WSH_REP_DDN.xml" >> $LOG_FILE
   exit 1
fi


#-----------  FORMS (2)----------------
# XXFOX_WSHPACWB.fmb
# XXFOX_WSHAUDWB.fmb
# Renaming the existing forms 
echo "Renaming the existing forms"
cp $XXFOX_TOP/forms/US/XXFOX_WSHPACWB.fmb $XXFOX_TOP/forms/US/XXFOX_WSHPACWB.fmb$CRNO
cp $XXFOX_TOP/forms/US/XXFOX_WSHAUDWB.fmb $XXFOX_TOP/forms/US/XXFOX_WSHAUDWB.fmb$CRNO
# Copying the forms to AU_TOP
echo "Copying the forms to AU_TOP"
cp XXFOX_WSHPACWB.fmb $AU_TOP/forms/US/XXFOX_WSHPACWB.fmb
cp XXFOX_WSHAUDWB.fmb $AU_TOP/forms/US/XXFOX_WSHAUDWB.fmb
#Changing the current directory to AU_TOP and compiling the form
echo "Changing the current directory to AU_TOP"
cd $AU_TOP/forms/US
#Change permissions 
chmod 777 XXFOX_WSHPACWB.fmb
chmod 777 XXFOX_WSHAUDWB.fmb


# Compiling the form XXFOX_WSHPACWB.fmb
echo "Compiling the form XXFOX_WSHPACWB.fmb"
frmcmp_batch module=$AU_TOP/forms/US/XXFOX_WSHPACWB.fmb userid=$APPSID output_file=$AU_TOP/forms/US/XXFOX_WSHPACWB.fmx module_type=form batch=no compile_all=yes
echo "Moving the XXFOX_WSHPACWB.fmx to custom top $XXFOX_TOP/forms/US "
mv $AU_TOP/forms/US/XXFOX_WSHPACWB.fmx $XXFOX_TOP/forms/US
#Error log generation
echo "Error file generation  at XXFOX_TOP/forms/US/XXFOX_WSHPACWB.err"
cat $LOG_FILE >> $INSTALL_PATH/XXFOX_WSHPACWB$CRNO.log
cat $INSTALL_PATH/XXFOX_WSHPACWB$CRNO.log $XXFOX_TOP/forms/US/XXFOX_WSHPACWB.err >>$LOG_FILE
rm $INSTALL_PATH/XXFOX_WSHPACWB$CRNO.log

# Compiling the form XXFOX_WSHAUDWB.fmb
echo "Compiling the form XXFOX_WSHAUDWB.fmb"
frmcmp_batch module=$AU_TOP/forms/US/XXFOX_WSHAUDWB.fmb userid=$APPSID output_file=$AU_TOP/forms/US/XXFOX_WSHAUDWB.fmx module_type=form batch=no compile_all=yes
echo "Moving the XXFOX_WSHAUDWB.fmx to custom top $XXFOX_TOP/forms/US "
mv $AU_TOP/forms/US/XXFOX_WSHAUDWB.fmx $XXFOX_TOP/forms/US
#Error log generation
echo "Error file generation  at XXFOX_TOP/forms/US/XXFOX_WSHAUDWB.err"
cat $LOG_FILE >> $INSTALL_PATH/XXFOX_WSHAUDWB$CRNO.log
cat $INSTALL_PATH/XXFOX_WSHAUDWB$CRNO.log $XXFOX_TOP/forms/US/XXFOX_WSHAUDWB.err >>$LOG_FILE
rm $INSTALL_PATH/XXFOX_WSHAUDWB$CRNO.log


# From Srini
#Package Spec compiling - XXFOX_SHIPPING_INTERFACE_PKG.pks
if sqlplus -s $APPSID @XXFOX_SHIPPING_INTERFACE_PKG.pks exit
then
    echo "Compilation of package spec XXFOX_SHIPPING_INTERFACE_PKG.pks successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Compilation of package spec XXFOX_SHIPPING_INTERFACE_PKG.pks unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# From Srini
#Package Body compiling - XXFOX_SHIPPING_INTERFACE_PKG.pkb
if sqlplus -s $APPSID @XXFOX_SHIPPING_INTERFACE_PKG.pkb exit
then
    echo "Compilation of Package Body XXFOX_SHIPPING_INTERFACE_PKG.pkb successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Compilation of Package Body XXFOX_SHIPPING_INTERFACE_PKG.pkb unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi

# From Srini
# View Creation for Freight carriers - XXFOX_FEDEX_FREIGHT_CARRIER_V
echo "View Creation for Freight carriers - XXFOX_FEDEX_FREIGHT_CARRIER_V "
echo
if sqlplus -s $APPSID @XXFOX_FEDEX_FREIGHT_CARRIER_V.sql exit
then
    echo "View Creation for Freight carriers XXFOX_FEDEX_FREIGHT_CARRIER_V successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "View Creation for Freight carriers XXFOX_FEDEX_FREIGHT_CARRIER_V unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


# From srini
# View Creation for Freight carriers - XXFOX_UPS_FREIGHT_CARRIER_V
echo "View Creation for Freight carriers - XXFOX_UPS_FREIGHT_CARRIER_V "
echo
if sqlplus -s $APPSID @XXFOX_UPS_FREIGHT_CARRIER_V.sql exit
then
    echo "View Creation for Freight carriers XXFOX_UPS_FREIGHT_CARRIER_V successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "View Creation for Freight carriers XXFOX_UPS_FREIGHT_CARRIER_V unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


cd $INSTALL_PATH

echo " "
echo "Installation Completed..."
echo "Please check installation log file : "
echo "Installation Completed..."
echo " "

exit 0
