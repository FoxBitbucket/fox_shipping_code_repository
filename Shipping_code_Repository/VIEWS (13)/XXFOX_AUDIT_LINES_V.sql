
CREATE OR REPLACE FORCE VIEW APPS.XXFOX_AUDIT_LINES_V
AS
     SELECT   wts.trip_id,                                                --kp
              --wda.delivery_id,
              wdd.inventory_item_id,
              msi.segment1 item_number,
              msi.description part_description,
              (SELECT   NVL (SUM (audit_quantity), 0)
                 FROM   xxfox_audit_lines
                WHERE   trip_id = wts.TRIP_ID                             --kp
                        AND inventory_item_id = wdd.inventory_item_id)
                 audit_quantity,
              (SELECT   MAX (creation_date)
                 FROM   xxfox_audit_lines
                WHERE   trip_id = wts.trip_id
                        AND inventory_item_id = wdd.inventory_item_id)
                 creation_date,
              MAX (wdd.created_by) created_by,
              MAX (wdd.last_update_date) last_update_date,
              MAX (wdd.last_updated_by) last_updated_by,
              NVL (SUM (requested_quantity), 0) requested_qty,
              NVL (SUM (picked_quantity), 0) picked_qty,
              NVL (SUM (shipped_quantity), 0) shipped_qty,
              MAX (wdd.attribute1) puller
       FROM   wsh_delivery_details wdd,
              wsh_delivery_assignments wda,
              wsh_delivery_legs wdl,                                      --kp
              wsh_trip_stops wts,                                         --kp
              mtl_system_items_b msi
      WHERE       wdd.source_code = 'OE'
              AND wdd.delivery_detail_id = wda.delivery_detail_id
              AND wda.delivery_id = wdl.delivery_id(+)                    --kp
              AND wdl.pick_up_stop_id = wts.stop_id(+)                    --kp
              AND wdd.inventory_item_id = msi.inventory_item_id
              AND wdd.organization_id = msi.organization_id
   GROUP BY   wts.trip_id,
              --wda.delivery_id,
              wdd.inventory_item_id,
              msi.segment1,
              msi.description

/

exit;