CREATE OR REPLACE FORCE VIEW APPS.XXFOX_LABEL_SHIPMENT_V
(
   SHIPMENT_NUM,
   TRIP_ID,
   TRIP_NAME,
   ORGANIZATION_ID,
   ORDER_NUMBER,
   CUSTOMER_NUMBER,
   CUSTOMER_NAME,
   ADDRESS1,
   ADDRESS2,
   ADDRESS3,
   ADDRESS4,
   ORIG_ADDRESS4,
   CITY,
   STATE,
   PROVINCE,
   POSTAL_CODE,
   COUNTY,
   COUNTRY,
   CONCAT_ADDRESS,
   SYS_DATE,
   INVENTORY_ITEM_ID,
   SUBINVENTORY_CODE,
   LOCATOR_ID,
   LOT_NUMBER,
   REVISION,
   SERIAL_NUMBER,
   LPN_ID,
   DELIVERY_ID,
   DELIVERY_DETAIL_ID,
   SALES_ORDER_HEADER_ID,
   SALES_ORDER_LINE_ID
)
AS
   SELECT   DISTINCT                                        --wnd.delivery_id,
            --wnd.name delivery_number,
            DECODE (WT.TRIP_ID, NULL, WND.DELIVERY_ID, wt.name) shipment_num,
            WT.TRIP_ID,
            WT.NAME TRIP_NAME,
            wnd.organization_id,
            --wdd.source_header_number order_number,
            NULL order_number,
            --,xal.pallet_number, xal.pallet_wt_uom, xal.pallet_net_wt, xal.pallet_gross_wt
            --,xal.pallet_dim_uom, xal.pallet_dimension,count( distinct box_number) total_boxes
            hca.account_number customer_number,
            NVL (hl.address4, hp.party_name) customer_name,
            hl.address1,
            hl.address2,
            hl.address3,
            '' address4,
            hl.address4 orig_address4,
            hl.city,
            hl.state,
            hl.province,
            hl.postal_code,
            hl.county,
            NVL (ftv.territory_short_name, hl.country) country,
               hl.address1
            || DECODE (hl.address2, NULL, NULL, CHR (13) || hl.address2)
            || DECODE (hl.address3, NULL, NULL, CHR (13) || hl.address3)
            || CHR (13)
            || hl.city
            || ', '
            || hl.state
            || ' '
            || hl.postal_code
            || CHR (13)
            || NVL (ftv.territory_short_name, hl.country)
               CONCAT_ADDRESS,
            TO_CHAR (SYSDATE, 'MM/DD/YYYY') sys_date,
            TO_NUMBER (NULL) INVENTORY_ITEM_ID,
            TO_CHAR (NULL) SUBINVENTORY_CODE,
            TO_NUMBER (NULL) LOCATOR_ID,
            TO_CHAR (NULL) LOT_NUMBER,
            TO_CHAR (NULL) REVISION,
            TO_CHAR (NULL) SERIAL_NUMBER,
            TO_NUMBER (NULL) LPN_ID,
            --TO_NUMBER (NULL) DELIVERY_ID,
            DECODE (WT.TRIP_ID, NULL, WND.DELIVERY_ID, NULL) DELIVERY_ID,
            TO_NUMBER (NULL) DELIVERY_DETAIL_ID,
            TO_NUMBER (NULL) SALES_ORDER_HEADER_ID,
            --wdd.source_header_id SALES_ORDER_HEADER_ID,
            TO_NUMBER (NULL) SALES_ORDER_LINE_ID
     FROM   wsh_new_deliveries wnd,
            --wsh_delivery_assignments wda,
            --wsh_delivery_details wdd,
            wsh_trips wt,
            wsh_trip_stops wts,
            wsh_delivery_legs wdl,
            hz_cust_accounts hca,
            hz_parties hp,
            hz_locations hl,
            fnd_territories_vl ftv
    WHERE       1 = 1
            AND wts.stop_id(+) = wdl.pick_up_stop_id
            AND wdl.delivery_id(+) = wnd.delivery_id
            AND wts.trip_id = wt.trip_id(+)
            --AND wnd.delivery_id = wda.delivery_id
            --      AND wda.delivery_detail_id = wdd.delivery_detail_id
            --      AND wdd.source_code = 'OE'
            AND wnd.customer_id = hca.cust_account_id
            AND hca.party_id = hp.party_id
            AND NVL (wnd.intmed_ship_to_location_id,
                     wnd.ultimate_dropoff_location_id) = hl.location_id
            AND hl.country = ftv.territory_code(+);


EXIT
/