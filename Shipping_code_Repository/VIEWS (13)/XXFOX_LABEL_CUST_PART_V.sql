CREATE OR REPLACE FORCE VIEW APPS.XXFOX_LABEL_CUST_PART_V
(
   TRIP_ID,
   TRIP_NAME,
   DELIVERY_ID,
   DELIVERY_NUMBER,
   ORGANIZATION_ID,
   SO_NUMBER,
   SO_LINE,
   SO_SHIP_LINE,
   SO_LINE_REL_REF,
   PALLET_NUMBER,
   AUDIT_DATE,
   BOX_NUMBER,
   ORDERED_ITEM,
   BAR_ORDERED_ITEM,
   INTERNAL_ITEM,
   HTS_CODE,
   QUANTITY,
   BAR_QUANTITY,
   UOM,
   CUST_PO_NUMBER,
   BAR_CUST_PO_NUMBER,
   SHIP_DATE,
   COUNTRY_OF_ORIGIN,
   ITEM_DESCRIPTION,
   CUSTOMER_NUMBER,
   CUSTOMER_NAME,
   ADDRESS1,
   ADDRESS2,
   ADDRESS3,
   ADDRESS4,
   ADDRESS4_ORIG,
   CITY,
   STATE,
   PROVINCE,
   POSTAL_CODE,
   COUNTY,
   COUNTRY,
   CONCAT_ADDRESS,
   SYS_DATE,
   INVENTORY_ITEM_ID,
   SUBINVENTORY_CODE,
   LOCATOR_ID,
   LOT_NUMBER,
   REVISION,
   SERIAL_NUMBER,
   LPN_ID,
   DELIVERY_DETAIL_ID,
   SALES_ORDER_HEADER_ID,
   SALES_ORDER_LINE_ID,
   COMP_NAME,
   FROM_ADDRESS1,
   FROM_CITY,
   FROM_COUNTRY,
   FROM_STATE,
   FROM_POSTAL_CODE,
   CONCAT_FROM_ADDRESS,
   FROM_PHONE_NO,
   ITEM_REVISION
)
AS
     SELECT   wt.trip_id,
              wt.name,
              wnd.delivery_id,
              wnd.name delivery_number,
              wdd.organization_id,
              wdd.source_header_number so_number,
              ol.line_number so_line,
              ol.shipment_number so_ship_line,
                 wdd.source_header_number
              || ' / '
              || ol.line_number
              || ' / '
              || ol.shipment_number
                 so_line_rel_ref,
              xal.pallet_number,
              TRUNC (MAX (xal.audit_date)) audit_date,
              xal.box_number,
              ol.ordered_item ordered_item,
              --'P' || SUBSTR (REPLACE (ol.ordered_item, '-', ''), 1, 15)
              (SELECT   mci.customer_item_number
                 FROM   mtl_customer_item_xrefs mcix, mtl_customer_items mci
                WHERE       1 = 1
                        AND mcix.inventory_item_id = wdd.inventory_item_id  --
                        AND mcix.master_organization_id = 83
                        AND mcix.customer_item_id = mci.customer_item_id
                        AND mcix.inactive_flag = 'N'
                        AND mcix.preference_number =
                              (SELECT   MIN (a.preference_number)
                                 FROM   mtl_customer_item_xrefs a
                                WHERE   a.customer_item_id =
                                           mcix.customer_item_id)
                        AND mci.customer_id = wnd.customer_id               --
                                                             )
                 bar_ordered_item,
              msi.segment1 internal_item,
              mmsi.attribute9 HTS_CODE,
              SUM (wdd.requested_quantity) quantity,
              'Q' || SUM (wdd.requested_quantity) bar_quantity,
              wdd.requested_quantity_uom UOM,
              ol.cust_po_number,
              DECODE (ol.cust_po_number,
                      NULL, NULL,
                      'K' || SUBSTR (ol.cust_po_number, 1, 10))
                 bar_cust_po_number,
              TO_CHAR (wnd.ultimate_dropoff_date, 'MM/DD/YYYY') SHIP_DATE,
              (SELECT   NVL (cftv.territory_short_name, mcr.cross_reference)
                 FROM   mtl_cross_references_b mcr, fnd_territories_vl cftv
                WHERE       mcr.inventory_item_id = msi.inventory_item_id
                        AND mcr.organization_id = msi.organization_id
                        AND mcr.cross_reference_type = 'COO'
                        AND TRUNC (SYSDATE) BETWEEN TRUNC(NVL (
                                                             mcr.start_date_active,
                                                             SYSDATE
                                                          ))
                                                AND  TRUNC(NVL (
                                                              mcr.end_date_active,
                                                              SYSDATE
                                                           ))
                        AND mcr.cross_reference = cftv.territory_code(+)
                        AND ROWNUM = 1)
                 country_of_origin,
              SUBSTR (NVL (ol.user_item_description, msi.description), 1, 60)
                 item_description,
              hca.account_number customer_number,
              NVL (hl.address4, hp.party_name) customer_name,
              hl.address1,
              hl.address2,
              hl.address3,
              '' address4,
              hl.address4 address4_orig,
              hl.city,
              hl.state,
              hl.province,
              hl.postal_code,
              hl.county,
              NVL (hlftv.territory_short_name, hl.country) country,
                 hl.address1
              || DECODE (hl.address2, NULL, NULL, CHR (13) || hl.address2)
              || DECODE (hl.address3, NULL, NULL, CHR (13) || hl.address3)
              || CHR (13)
              || hl.city
              || ', '
              || hl.state
              || ' '
              || hl.postal_code
              || CHR (13)
              || NVL (hlftv.territory_short_name, hl.country)
                 CONCAT_ADDRESS,
              TO_CHAR (SYSDATE, 'MM/DD/YYYY') sys_date,
              TO_NUMBER (NULL) INVENTORY_ITEM_ID,
              TO_CHAR (NULL) SUBINVENTORY_CODE,
              TO_NUMBER (NULL) LOCATOR_ID,
              TO_CHAR (NULL) LOT_NUMBER,
              TO_CHAR (NULL) REVISION,
              TO_CHAR (NULL) SERIAL_NUMBER,
              TO_NUMBER (NULL) LPN_ID,
              TO_NUMBER (NULL) DELIVERY_DETAIL_ID,
              TO_NUMBER (NULL) SALES_ORDER_HEADER_ID,
              TO_NUMBER (NULL) SALES_ORDER_LINE_ID,
              xep.name COMP_NAME,
              hol.address_line_1 from_address1,
              hol.town_or_city from_city,
              NVL (ftv.territory_short_name, hol.region_2) from_country,
              hol.region_2 from_state,
              hol.postal_code from_postal_code,
              hol.address_line_1
              || DECODE (hol.address_line_2,
                         NULL, NULL,
                         CHR (13) || hol.address_line_2)
              || DECODE (hol.address_line_3,
                         NULL, NULL,
                         CHR (13) || hol.address_line_3)
              || CHR (13)
              || hol.town_or_city
              || ', '
              || hol.region_2
              || ' '
              || hol.postal_code
              || CHR (13)
              || NVL (ftv.territory_short_name, hol.region_2)
                 CONCAT_FROM_ADDRESS,
              hol.telephone_number_1 from_phone_no,
              (SELECT   mir.revision revision
                 FROM   mtl_item_revisions mir
                WHERE       mir.effectivity_date <= SYSDATE
                        AND mir.inventory_item_id = msi.inventory_item_id
                        AND mir.organization_id = msi.organization_id
                        AND mir.revision_id IN
                                 (SELECT   MAX (mir1.revision_id)
                                    FROM   mtl_item_revisions mir1
                                   WHERE   mir1.effectivity_date <= SYSDATE
                                           AND mir1.inventory_item_id =
                                                 msi.inventory_item_id
                                           AND mir1.organization_id =
                                                 msi.organization_id))
                 ITEM_REVISION
       FROM   xxfox_audit_lines xal,
              wsh_trips wt,
              wsh_trip_stops wts,
              wsh_delivery_legs wdl,
              wsh_new_deliveries wnd,
              wsh_delivery_assignments wda,
              wsh_delivery_details wdd,
              oe_order_lines_all ol,
              mtl_system_items msi             --, mtl_customer_items_all_v ci
                                  ,
              hz_cust_accounts hca,
              hz_parties hp,
              hz_locations hl,
              fnd_territories_vl hlftv,
              hr_organization_units hou,
              hr_locations hol,
              fnd_territories_vl ftv,
              mtl_parameters mp,
              mtl_system_items mmsi,
              org_organization_definitions ood,
              xle_entity_profiles xep
      WHERE       1 = 1
              --and xal.delivery_id = wnd.delivery_id
              AND xal.trip_id = wt.trip_id
              AND wts.stop_id = wdl.pick_up_stop_id
              AND wdl.delivery_id = wnd.delivery_id
              AND wts.trip_id = wt.trip_id
              AND xal.inventory_item_id = wdd.inventory_item_id
              AND wnd.delivery_id = wda.delivery_id
              AND wda.delivery_detail_id = wdd.delivery_detail_id
              AND wdd.source_line_id = ol.line_id
              AND ol.inventory_item_id = msi.inventory_item_id
              AND ol.ship_from_org_id = msi.organization_id
              --AND    ol.ordered_item_id    = ci.customer_item_id (+)
              AND wnd.customer_id = hca.cust_account_id
              AND hca.party_id = hp.party_id
              AND NVL (wnd.intmed_ship_to_location_id,
                       wnd.ultimate_dropoff_location_id) = hl.location_id
              AND hl.country = hlftv.territory_code(+)
              AND wnd.organization_id = hou.organization_id
              AND hou.location_id = hol.location_id
              AND hol.country = ftv.territory_code(+)
              AND msi.organization_id = mp.organization_id
              AND mp.master_organization_id = mmsi.organization_id
              AND msi.inventory_item_id = mmsi.inventory_item_id
              AND wdd.organization_id = ood.organization_id
              AND ood.legal_entity = legal_entity_id
   GROUP BY   wt.trip_id,
              wt.name,
              wnd.delivery_id,
              wdd.inventory_item_id,
              wnd.customer_id,
              wnd.name,
              wdd.organization_id,
              wdd.source_header_number,
              ol.line_number,
              ol.shipment_number,
              xal.pallet_number,
              xal.box_number,
              ol.ordered_item,
              msi.segment1,
              msi.inventory_item_id,
              msi.organization_id,
              mmsi.attribute9,
              NVL (ol.user_item_description, msi.description),
              wdd.requested_quantity_uom,
              ol.cust_po_number,
              wnd.ultimate_dropoff_date,
              hca.account_number,
              hp.party_name,
              hl.address1,
              hl.address2,
              hl.address3,
              hl.address4,
              hl.city,
              hl.state,
              hl.province,
              hl.postal_code,
              hl.county,
              hl.country,
              hlftv.territory_short_name,
              hol.address_line_1,
              hol.address_line_2,
              hol.address_line_3,
              hol.town_or_city,
              ftv.territory_short_name,
              hol.region_2,
              hol.postal_code,
              hol.telephone_number_1,
              xep.name
   ORDER BY   xal.pallet_number, xal.box_number
/
EXIT
/