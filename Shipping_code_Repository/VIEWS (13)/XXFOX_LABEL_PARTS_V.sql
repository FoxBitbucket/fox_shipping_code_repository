CREATE OR REPLACE FORCE VIEW APPS.XXFOX_LABEL_PARTS_V
(
   DELIVERY_ID,
   DELIVERY_NUMBER,
   ORGANIZATION_ID,
   QTY,
   ORDER_NUMBER,
   SOLINREL,
   SHIPMENT_NUMBER,
   ITEM_DESCRIPTION,
   ITEM_NUMBER,
   INVENTORY_ITEM_ID,
   CUST_PO_NUMBER,
   CUSTOMER_NUMBER,
   CUSTOMER_NAME,
   ADDRESS1,
   ADDRESS2,
   ADDRESS3,
   ADDRESS4,
   ADDRESS4_ORIG,
   CITY,
   STATE,
   PROVINCE,
   POSTAL_CODE,
   COUNTY,
   COUNTRY,
   CONCAT_ADDRESS,
   COO,
   HTS,
   SYS_DATE,
   SUBINVENTORY_CODE,
   LOCATOR_ID,
   LOT_NUMBER,
   REVISION,
   SERIAL_NUMBER,
   LPN_ID,
   DELIVERY_DETAIL_ID,
   SALES_ORDER_HEADER_ID,
   SALES_ORDER_LINE_ID,
   RELEASED_STATUS,
   FG_YN,
   ITEM_REVISION
)
AS
     SELECT wnd.delivery_id,
            wnd.name delivery_number,
            wdd.organization_id,
            SUM (wdd.requested_quantity) qty,
            oh.order_number,
            ol.line_number solinrel,
            ol.shipment_number,
            msi.description item_description,
            msi.segment1 Item_number,
            ol.inventory_item_id,
            oh.cust_po_number,
            hca.account_number customer_number,
            NVL (hl.address4, hp.party_name) customer_name,
            hl.address1,
            hl.address2,
            hl.address3,
            '' address4,
            hl.address4 address4_orig,
            hl.city,
            hl.state,
            hl.province,
            hl.postal_code,
            hl.county,
            NVL (ftv.territory_short_name, hl.country) country,
               hl.address1
            || DECODE (hl.address2, NULL, NULL, CHR (13) || hl.address2)
            || DECODE (hl.address3, NULL, NULL, CHR (13) || hl.address3)
            || CHR (13)
            || hl.city
            || ', '
            || hl.state
            || ' '
            || hl.postal_code
            || CHR (13)
            || NVL (ftv.territory_short_name, hl.country)
               CONCAT_ADDRESS,
            NULL COO,
            NULL HTS,
            TO_CHAR (SYSDATE, 'MM/DD/YYYY') sys_date,
            TO_CHAR (NULL) SUBINVENTORY_CODE,
            TO_NUMBER (NULL) LOCATOR_ID,
            TO_CHAR (NULL) LOT_NUMBER,
            TO_CHAR (NULL) REVISION,
            TO_CHAR (NULL) SERIAL_NUMBER,
            TO_NUMBER (NULL) LPN_ID,
            TO_NUMBER (NULL) DELIVERY_DETAIL_ID,
            oh.header_id SALES_ORDER_HEADER_ID,
            ol.line_id SALES_ORDER_LINE_ID,
            wdd.released_status,
            DECODE (msi.item_type,
                    'FG', 'Y',
                    'FFG', 'Y',
                    'FATO', 'Y',
                    'FFGS', 'Y',
                    'FFGSM', 'Y',
                    'N')
               FG_YN,
            (SELECT mir.revision
               FROM mtl_item_revisions mir
              WHERE     mir.effectivity_date <= SYSDATE
                    AND mir.inventory_item_id = msi.inventory_item_id
                    AND mir.organization_id = msi.organization_id
                    AND mir.revision_id IN
                           (SELECT MAX (mir1.revision_id)
                              FROM mtl_item_revisions mir1
                             WHERE     mir1.effectivity_date <= SYSDATE
                                   AND mir1.inventory_item_id =
                                          msi.inventory_item_id
                                   AND mir1.organization_id =
                                          msi.organization_id))
               ITEM_REVISION
       FROM wsh_new_deliveries wnd,
            wsh_delivery_assignments wda,
            wsh_delivery_details wdd,
            mtl_system_items msi,
            oe_order_lines_all ol,
            oe_order_headers_all oh,
            hz_cust_accounts hca,
            hz_parties hp,
            hz_locations hl,
            fnd_territories_vl ftv
      WHERE     1 = 1                           --wnd.name = p_delivery_number
            AND wnd.delivery_id = wda.delivery_id
            AND wda.delivery_detail_id = wdd.delivery_detail_id
            AND wdd.inventory_item_id = msi.inventory_item_id
            AND wdd.organization_id = msi.organization_id
            AND wdd.source_line_id = ol.line_id
            --AND    ol.ordered_item = NVL(p_part_number,ol.ordered_item)
            AND ol.header_id = oh.header_id
            AND wnd.customer_id = hca.cust_account_id
            AND hca.party_id = hp.party_id
            AND wnd.ultimate_dropoff_location_id = hl.location_id
            AND hl.country = ftv.territory_code(+)
   GROUP BY wnd.delivery_id,
            wnd.name,
            wdd.organization_id,
            oh.order_number,
            ol.line_number,
            ol.shipment_number,
            wdd.item_description,
            msi.segment1,
            msi.description,
            ol.inventory_item_id,
            oh.cust_po_number,
            hca.account_number,
            hp.party_name,
            hl.address1,
            hl.address2,
            hl.address3,
            hl.address4,
            hl.city,
            hl.state,
            hl.province,
            hl.postal_code,
            hl.county,
            hl.country,
            ftv.territory_short_name,
            oh.header_id,
            ol.line_id,
            wdd.released_status,
            msi.item_type,
            msi.inventory_item_id,
            msi.organization_id
/
EXIT
/