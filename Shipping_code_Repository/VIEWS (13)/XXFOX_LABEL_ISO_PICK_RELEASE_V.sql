CREATE OR REPLACE FORCE VIEW "APPS"."XXFOX_LABEL_ISO_PICK_RELEASE_V" 
AS 
  SELECT --wnd.name delivery_number,wnd.delivery_id,--commented by Awaiz on 15.FEB.2018
   wt.name trip_number
  ,wt.trip_id
  ,(SELECT   mci.customer_item_number                  
      FROM   mtl_customer_item_xrefs mcix,
             mtl_customer_items mci
     WHERE  1 = 1
       AND mcix.inventory_item_id = wdd.inventory_item_id--
       AND mcix.master_organization_id = 83
       AND mcix.customer_item_id = mci.customer_item_id
       AND mcix.inactive_flag = 'N'
       AND mcix.preference_number = (SELECT MIN (a.preference_number)
                                           FROM mtl_customer_item_xrefs a
                                          WHERE a.customer_item_id = mcix.customer_item_id)
       AND mci.customer_id = wnd.customer_id --
    ) CUSTOMER_PART_NUMBER --Added by Awaiz on 16.FEB.2018
  ,pap.full_name requestor
  ,msi.segment1 item_number
  ,msi.description item_description
  ,mtrl.organization_id
  ,wdd.inventory_item_id
  ,TO_CHAR(prh.approved_date, 'MM/DD/YYYY') request_date
  ,TO_CHAR(prh.approved_date, 'HH:MI:SS AM') request_time
  ,prl.destination_subinventory subinventory_code, sum(wdd.requested_quantity) quantiity
  ,(SELECT concatenated_segments
      FROM   MTL_ITEM_LOCATIONS_kfv
     WHERE  inventory_location_id = decode(msi.reservable_type,2,wdd.locator_id,wpsv.locator_id)
    ) from_locator,wpsv.locator_id
   ,wpsv.pick_slip_number
   ,wdd.released_status
   ,wdd.source_header_type_id SALES_ORDER_HEADER_ID
   ,wdd.source_header_number order_number
   ,source_header_type_id order_type_id
   ,to_char(sysdate,'MM/DD/YYYY') sys_date
   ,TO_CHAR(NULL) LOT_NUMBER
   ,TO_CHAR(NULL) REVISION
   ,TO_CHAR(NULL) SERIAL_NUMBER
   ,TO_NUMBER(NULL) LPN_ID
   ,TO_NUMBER(NULL) DELIVERY_DETAIL_ID
   ,TO_NUMBER(NULL) SALES_ORDER_LINE_ID
  ,(SELECT 
                                                  mir.revision revision
                                                FROM mtl_item_revisions mir
                                                WHERE mir.effectivity_date   <= SYSDATE
                                                AND mir.inventory_item_id = msi.inventory_item_id
                                                AND mir.organization_id   = msi.organization_id
                                                AND mir.revision_id      IN
                                                  (SELECT MAX (mir1.revision_id)
                                                  FROM mtl_item_revisions mir1
                                                  WHERE mir1.effectivity_date    <= SYSDATE
                                                  AND mir1.inventory_item_id = msi.inventory_item_id
                                                  AND mir1.organization_id   = msi.organization_id
                                                  )) ITEM_REVISION
    FROM    oe_order_headers_all oh 
           ,oe_order_lines_all ol 
           ,mtl_txn_request_lines mtrl 
           ,mtl_txn_request_headers mtrh 
           ,mtl_material_transactions_temp wpsv
           ,wsh_delivery_details wdd
           ,wsh_delivery_assignments_v wda
           ,wsh_new_deliveries wnd
           ,po_requisition_lines_all prl
           ,po_requisition_headers_all prh
           ,per_all_people_f pap
           ,mtl_system_items_vl msi
           ,wsh_delivery_legs wdl --added by awaiz on 15.FEB.2018
           ,wsh_trip_stops wtsp--added by awaiz on 15.FEB.2018
           ,wsh_trip_stops wtsd--added by awaiz on 15.FEB.2018
           ,wsh_trips wt --added by awaiz on 15.FEB.2018
    WHERE  oh.source_document_type_id = 10
    AND    oh.header_id= ol.header_id
    AND    wpsv.move_order_line_id = mtrl.line_id
    AND    wpsv.move_order_line_id = mtrl.line_id
    AND    mtrl.header_id = mtrh.header_id
    AND    mtrl.line_id = wdd.move_order_line_id
    AND    wdd.delivery_detail_id = wda.delivery_detail_id
    AND    wda.delivery_id = wnd.delivery_id(+)
    AND    (wnd.delivery_type IS NULL OR wnd.delivery_type = 'STANDARD')
    AND    wdd.source_line_id = ol.line_id
    AND    wdd.source_header_id = ol.header_id
    AND    wdd.released_status='S'
    AND    wdd.source_code = 'OE'
    AND    wdd.container_flag IN ('N', 'Y')
    AND    ol.source_document_type_id = 10
    AND    ol.source_document_line_id = prl.requisition_line_id
    AND    ol.source_document_id = prh.requisition_header_id
    AND    prh.preparer_id = pap.person_id
    AND    SYSDATE BETWEEN pap.effective_start_date ANd pap.effective_end_date
    AND    wdd.inventory_item_id = msi.inventory_item_id
    AND    wdd.organization_id = msi.organization_id
    AND    wdl.delivery_id (+) = wnd.delivery_id  --added by awaiz on 15.FEB.2018
    AND    wtsp.stop_id (+)    = wdl.pick_up_stop_id --added by awaiz on 15.FEB.2018
    AND    wtsd.stop_id (+) = wdl.drop_off_stop_id --added by awaiz on 15.FEB.2018
    AND    wt.trip_id (+)      = wtsp.trip_id --added by awaiz on 15.FEB.2018
    --AND    (p_pick_status = 'A' OR (p_pick_status ='U' AND wdd.released_status = 'S' ) OR (p_pick_status='P' AND wdd.released_status <> 'S' ))
    --AND    wpsv.pick_slip_number BETWEEN NVL(p_pick_slip_num_low,wpsv.pick_slip_number) AND NVL(p_pick_slip_num_high,wpsv.pick_slip_number)
    --AND    wdd.source_header_type_id  = NVL(p_order_type_id,wdd.source_header_type_id)
    --AND    to_number(wdd.source_header_number) BETWEEN  NVL(p_order_num_low,to_number(wdd.source_header_number)) AND NVL(p_order_num_high,to_number(wdd.source_header_number))
    --AND    mtrh.request_number BETWEEN NVL(p_move_order_low,mtrh.request_number) AND NVL(p_move_order_high,mtrh.request_number)
    --AND    NVL(wdd.ship_method_code,'X') = NVL(p_freight_code,NVL(wdd.ship_method_code,'X'))
    --AND    mtrl.organization_id = NVL(p_organization_id,mtrl.organization_id)
    --AND    wdd.customer_id = NVL(p_customer_id,wdd.customer_id)
    --AND    mtrl.pick_slip_date BETWEEN NVL(l_detail_date_low,mtrl.pick_slip_date)  AND NVL (l_detail_date_high,mtrl.pick_slip_date)
    GROUP BY 
            --wnd.name,wnd.delivery_id,--Added by Awaiz on 15.FEB.2018
            wt.name,wt.trip_id --Commented by Awaiz on 15.FEB.2018
           ,wdd.inventory_item_id,wnd.customer_id,pap.full_name, msi.segment1, msi.description, mtrl.organization_id, wdd.inventory_item_id, prh.approved_date, prl.destination_subinventory
           ,msi.reservable_type,wdd.locator_id,wpsv.locator_id
           ,wpsv.pick_slip_number,wdd.released_status,wdd.source_header_type_id,wdd.source_header_number, source_header_type_id,msi.inventory_item_id,msi.organization_id
		   /
		   EXIT;
