 CREATE OR REPLACE FORCE VIEW APPS.XXFOX_FEDEX_FREIGHT_CARRIER_V ("DELIVERY_NUMBER", "CUSTOMER_NUMBER", "CUSTOMER_NAME", "SHIP_METHOD", "DELIVERY_WEIGHT_IN_LB", "CONTACT_NAME", "CONTACT_PHONE_NUMBER", "CONTACT_EMAIL_ID", "CUST_FREIGHT_CARRIER_ACCT_NO", "CUSTOMER_ADDRESS1", "CUSTOMER_ADDRESS2", "CUSTOMER_ADDRESS3", "CUSTOMER_ADDRESS4", "CITY", "STATE", "PROVINCE", "POSTAL_CODE", "COUNTY", "COUNTRY", "STATUS_CODE", "CUST_UPS_ACCOUNT", "BILL_FREIGHT_TO") AS 
   SELECT wnd.name DELIVERY_NUMBER,
          cust_acct.account_number CUSTOMER_NUMBER,
          party.party_name CUSTOMER_NAME,
          fsl.meaning ship_method,
          CASE
             WHEN wnd.weight_uom_code = 'LG'
             THEN
                NVL (wnd.gross_weight, wnd.net_weight)
             WHEN     wnd.weight_uom_code IS NOT NULL
                  AND wnd.weight_uom_code != 'LG'
                  AND NVL (wnd.gross_weight, wnd.net_weight) IS NOT NULL
             THEN
                0.453592 * NVL (wnd.gross_weight, wnd.net_weight)
             ELSE
                NULL
          END
             DELIVERY_WEIGHT_IN_LB,
          cont.contact_name contact_name,
          cont.contact_phone_number CONTACT_PHONE_NUMBER,
          cont.contact_email_id CONTACT_EMAIL_ID,
          XXFOX_SHIPPING_INTERFACE_PKG.get_CUST_acct_FEDEX_ACCOUNT (
             wnd.customer_id,wnd.delivery_id,1) cust_freight_carrier_acct_no,
          wl.address1 CUSTOMER_ADDRESS1,
          wl.address2 CUSTOMER_ADDRESS2,
          wl.address3 CUSTOMER_ADDRESS3,
          wl.address4 CUSTOMER_ADDRESS4,
          wl.city,
          wl.state,
          wl.province,
          wl.postal_code,
          wl.county,
          wl.country,
          wnd.status_code,
          XXFOX_SHIPPING_INTERFACE_PKG.get_CUST_acct_FEDEX_ACCOUNT (
             wnd.customer_id,wnd.delivery_id,2)
             CUST_UPS_ACCOUNT,
          CASE
             WHEN XXFOX_SHIPPING_INTERFACE_PKG.get_CUST_acct_FEDEX_ACCOUNT (
                     wnd.customer_id,wnd.delivery_id,2)
                     IS NOT NULL
             THEN
                'Receiver'
             ELSE
                'Shipper'
          END
             BILL_FREIGHT_TO
     FROM wsh_new_deliveries wnd,
          hz_cust_accounts cust_acct,
          hz_parties party,
          wsh_carrier_services_v wcs,
          fnd_lookup_values_vl fsl,
          wsh_locations wl,
          (SELECT wnd.delivery_id,
                  cp.party_name contact_name,
                  phn.raw_phone_number CONTACT_PHONE_NUMBER,
                  email.email_address CONTACT_EMAIL_ID,
                  ROW_NUMBER ()
                  OVER (PARTITION BY wnd.delivery_id
                        ORDER BY wdd.delivery_detail_id ASC)
                     AS rn
             FROM wsh_new_deliveries wnd,
                  wsh_delivery_assignments wda,
                  wsh_delivery_details wdd,
                  hz_cust_account_roles hcar,
                  hz_relationships hr,
                  hz_parties cp,
                  hz_contact_points phn,
                  hz_contact_points email
            WHERE     wnd.delivery_id = wda.delivery_id
                  AND wda.delivery_detail_id = wdd.delivery_detail_id
                  AND NVL (wdd.ship_to_contact_id, wdd.sold_to_contact_id) =
                         hcar.cust_account_role_id
                  AND hcar.party_id = hr.party_id
                  AND hr.relationship_code = 'CONTACT_OF'
                  AND hr.subject_id = cp.party_id
                  AND hr.party_id = phn.owner_table_id
                  AND phn.owner_table_name = 'HZ_PARTIES'
                  AND phn.contact_point_type = 'PHONE'
                  AND phn.phone_line_type = 'GEN'
                  AND phn.primary_flag = 'Y'
                  AND phn.status = 'A'
                  AND hr.party_id = email.owner_table_id
                  AND email.owner_table_name = 'HZ_PARTIES'
                  AND email.contact_point_type = 'EMAIL'
                  AND email.primary_flag = 'Y'
                  AND email.status = 'A') cont
    WHERE     1 = 1
          AND wnd.customer_id = cust_acct.cust_account_id
          AND cust_acct.party_id = party.party_id
          AND wnd.ship_method_code = wcs.ship_method_code
          AND UPPER (wcs.ship_method_code) LIKE ('%FEDEX%')
          AND wcs.service_level = fsl.lookup_code
          AND fsl.lookup_type = 'WSH_SERVICE_LEVELS'
          AND source_location_id =
                 NVL (intmed_ship_to_location_id,
                      ultimate_dropoff_location_id)
          AND wnd.delivery_id = cont.delivery_id(+)
          AND cont.rn(+) = 1
          AND EXISTS
                 (SELECT 1
                    FROM wsh_delivery_assignments wda,
                         wsh_delivery_details wdd
                   WHERE     wnd.delivery_id = wda.delivery_id
                         AND wda.delivery_detail_id = wdd.delivery_detail_id
                         AND wdd.released_status != 'C' -- eliminate shipped deliveries
                         AND wdd.released_status != 'B')
          AND NOT EXISTS
                     (SELECT DELIVERY_NUMBER
                        FROM XXFOX_SHIPPING_INTERFACE_STG
                       WHERE     NVL (interface_status, 'P') IN
                                    ('P', 'E', 'S')
                             AND delivery_number = wnd.name)
          and not exists (select 1 from WSH_TRIP_DELIVERIES_V                   
                             where delivery_id = wnd.delivery_id)
   union all                          
   SELECT to_char(trip_Data.trip_id) DELIVERY_NUMBER,
          cust_acct.account_number CUSTOMER_NUMBER,
          party.party_name CUSTOMER_NAME,
          fsl.meaning ship_method,
          xxfox_shipping_interface_pkg.get_trip_weight(trip_data.trip_id)
             DELIVERY_WEIGHT_IN_LB,
          cont.contact_name contact_name,
          cont.contact_phone_number CONTACT_PHONE_NUMBER,
          cont.contact_email_id CONTACT_EMAIL_ID,
          XXFOX_SHIPPING_INTERFACE_PKG.get_CUST_acct_FEDEX_ACCOUNT (
             wnd.customer_id,wnd.delivery_id,1) cust_freight_carrier_acct_no,
          wl.address1 CUSTOMER_ADDRESS1,
          wl.address2 CUSTOMER_ADDRESS2,
          wl.address3 CUSTOMER_ADDRESS3,
          wl.address4 CUSTOMER_ADDRESS4,
          wl.city,
          wl.state,
          wl.province,
          wl.postal_code,
          wl.county,
          wl.country,
          wnd.status_code,
          XXFOX_SHIPPING_INTERFACE_PKG.get_CUST_acct_FEDEX_ACCOUNT (
             wnd.customer_id,wnd.delivery_id,2)
             CUST_UPS_ACCOUNT,
          CASE
             WHEN XXFOX_SHIPPING_INTERFACE_PKG.get_CUST_acct_FEDEX_ACCOUNT (
                     wnd.customer_id,wnd.delivery_id,2)
                     IS NOT NULL
             THEN
                'Receiver'
             ELSE
                'Shipper'
          END
             BILL_FREIGHT_TO
     FROM (SELECT trip_id, delivery_id, ROW_NUMBER ()
                  OVER (PARTITION BY trip_id
                        ORDER BY delivery_id ASC)
                     AS rn
                      from WSH_TRIP_DELIVERIES_V) trip_Data, wsh_new_deliveries wnd,
          hz_cust_accounts cust_acct,
          hz_parties party,
          wsh_carrier_services_v wcs,
          fnd_lookup_values_vl fsl,
          wsh_locations wl,
          (SELECT wnd.delivery_id,
                  cp.party_name contact_name,
                  phn.raw_phone_number CONTACT_PHONE_NUMBER,
                  email.email_address CONTACT_EMAIL_ID,
                  ROW_NUMBER ()
                  OVER (PARTITION BY wnd.delivery_id
                        ORDER BY wdd.delivery_detail_id ASC)
                     AS rn
             FROM wsh_new_deliveries wnd,
                  wsh_delivery_assignments wda,
                  wsh_delivery_details wdd,
                  hz_cust_account_roles hcar,
                  hz_relationships hr,
                  hz_parties cp,
                  hz_contact_points phn,
                  hz_contact_points email
            WHERE     wnd.delivery_id = wda.delivery_id
                  AND wda.delivery_detail_id = wdd.delivery_detail_id
                  AND NVL (wdd.ship_to_contact_id, wdd.sold_to_contact_id) =
                         hcar.cust_account_role_id
                  AND hcar.party_id = hr.party_id
                  AND hr.relationship_code = 'CONTACT_OF'
                  AND hr.subject_id = cp.party_id
                  AND hr.party_id = phn.owner_table_id
                  AND phn.owner_table_name = 'HZ_PARTIES'
                  AND phn.contact_point_type = 'PHONE'
                  AND phn.phone_line_type = 'GEN'
                  AND phn.primary_flag = 'Y'
                  AND phn.status = 'A'
                  AND hr.party_id = email.owner_table_id
                  AND email.owner_table_name = 'HZ_PARTIES'
                  AND email.contact_point_type = 'EMAIL'
                  AND email.primary_flag = 'Y'
                  AND email.status = 'A') cont
    WHERE     1 = 1
    and wnd.delivery_id = trip_Data.delivery_id
    and trip_Data.rn = 1
          AND wnd.customer_id = cust_acct.cust_account_id
          AND cust_acct.party_id = party.party_id
          AND wnd.ship_method_code = wcs.ship_method_code
          AND UPPER (wcs.ship_method_code) LIKE ('%FEDEX%')
          AND wcs.service_level = fsl.lookup_code
          AND fsl.lookup_type = 'WSH_SERVICE_LEVELS'
          AND source_location_id =
                 NVL (intmed_ship_to_location_id,
                      ultimate_dropoff_location_id)
          AND wnd.delivery_id = cont.delivery_id(+)
          AND cont.rn(+) = 1
          AND EXISTS
                 (SELECT 1
                    FROM wsh_delivery_assignments wda,
                         wsh_delivery_details wdd
                   WHERE     wnd.delivery_id = wda.delivery_id
                         AND wda.delivery_detail_id = wdd.delivery_detail_id
                         AND wdd.released_status != 'C' -- eliminate shipped deliveries
                         AND wdd.released_status != 'B')
          AND NOT EXISTS
                     (SELECT DELIVERY_NUMBER
                        FROM XXFOX_SHIPPING_INTERFACE_STG
                       WHERE     NVL (interface_status, 'P') IN
                                    ('P', 'E', 'S')
                             AND delivery_number = to_char(trip_data.trip_id))
                             ;

EXIT
/							 