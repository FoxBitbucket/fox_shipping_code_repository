CREATE OR REPLACE FORCE VIEW APPS.XXFOX_LABEL_BOX_V
(
   TRIP_ID,
   TRIP_NAME,
   ORGANIZATION_ID,
   PALLET_NUMBER,
   AUDIT_DATE,
   BOX_NUMBER,
   TOTAL_NO_OF_BOXES,
   CUSTOMER_NUMBER,
   CUSTOMER_NAME,
   ADDRESS1,
   ADDRESS2,
   ADDRESS3,
   ADDRESS4,
   ADDRESS4_ORIG,
   CITY,
   STATE,
   PROVINCE,
   POSTAL_CODE,
   COUNTY,
   COUNTRY,
   CONCAT_ADDRESS,
   COMP_NAME,
   FROM_ADDRESS1,
   FROM_CITY,
   FROM_COUNTRY,
   FROM_STATE,
   FROM_POSTAL_CODE,
   CONCAT_FROM_ADDRESS,
   FROM_PHONE_NO,
   SYS_DATE,
   INVENTORY_ITEM_ID,
   SUBINVENTORY_CODE,
   LOCATOR_ID,
   LOT_NUMBER,
   REVISION,
   SERIAL_NUMBER,
   LPN_ID,
   DELIVERY_ID,
   DELIVERY_DETAIL_ID,
   SALES_ORDER_HEADER_ID,
   SALES_ORDER_LINE_ID
)
AS
     SELECT                                                 --wnd.delivery_id,
              --wnd.name delivery_number,
              x.trip_id,
              x.trip_name trip_name,
              x.organization_id,
              xal.pallet_number,
              TRUNC (MAX (xal.audit_date)) audit_date,
              xal.box_number,
              (SELECT   COUNT (DISTINCT  --pallet_number || ':' || box_number)
                                       box_number)
                 FROM   xxfox_audit_lines
                WHERE   1 = 1             --and delivery_id = wnd.delivery_id)
                             AND trip_id = x.trip_id)
                 TOTAL_NO_OF_BOXES,
              x.account_number customer_number,
              NVL (hl.address4, x.party_name) customer_name,
              hl.address1,
              hl.address2,
              hl.address3,
              '' address4,
              hl.address4 address4_orig,
              hl.city,
              hl.state,
              hl.province,
              hl.postal_code,
              hl.county,
              NVL (ftv.territory_short_name, hl.country) country,
                 hl.address1
              || DECODE (hl.address2, NULL, NULL, CHR (13) || hl.address2)
              || DECODE (hl.address3, NULL, NULL, CHR (13) || hl.address3)
              || CHR (13)
              || hl.city
              || ', '
              || hl.state
              || ' '
              || hl.postal_code
              || CHR (13)
              || NVL (ftv.territory_short_name, hl.country)
                 CONCAT_ADDRESS,
              xep.name COMP_NAME,
              hol.address_line_1 from_address1,
              hol.town_or_city from_city,
              NVL (fftv.territory_short_name, hol.region_2) from_country,
              hol.region_2 from_state,
              hol.postal_code from_postal_code,
              hol.address_line_1
              || DECODE (hol.address_line_2,
                         NULL, NULL,
                         CHR (13) || hol.address_line_2)
              || DECODE (hol.address_line_3,
                         NULL, NULL,
                         CHR (13) || hol.address_line_3)
              || CHR (13)
              || hol.town_or_city
              || ', '
              || hol.region_2
              || ' '
              || hol.postal_code
              || CHR (13)
              || NVL (fftv.territory_short_name, hol.region_2)
                 CONCAT_FROM_ADDRESS,
              hol.telephone_number_1 from_phone_no,
              TO_CHAR (SYSDATE, 'MM/DD/YYYY') sys_date,
              TO_NUMBER (NULL) INVENTORY_ITEM_ID,
              TO_CHAR (NULL) SUBINVENTORY_CODE,
              TO_NUMBER (NULL) LOCATOR_ID,
              TO_CHAR (NULL) LOT_NUMBER,
              TO_CHAR (NULL) REVISION,
              TO_CHAR (NULL) SERIAL_NUMBER,
              TO_NUMBER (NULL) LPN_ID,
              TO_NUMBER (NULL) DELIVERY_ID,
              TO_NUMBER (NULL) DELIVERY_DETAIL_ID,
              TO_NUMBER (NULL) SALES_ORDER_HEADER_ID,
              TO_NUMBER (NULL) SALES_ORDER_LINE_ID
       FROM   xxfox_audit_lines xal,
              --wsh_new_deliveries wnd,
              --wsh_delivery_assignments wda,
              --wsh_delivery_details wdd,
              (SELECT   DISTINCT wts.trip_id,
                                 wt.name trip_name,
                                 wnd.delivery_id,
                                 wnd.organization_id,
                                 hca.cust_account_id,
                                 hca.account_number,
                                 hp.party_name,
                                 wnd.intmed_ship_to_location_id,
                                 wnd.ultimate_dropoff_location_id
                 FROM   wsh_new_deliveries wnd,
                        wsh_delivery_legs wdl,
                        wsh_trip_stops wts,
                        wsh_trips wt,
                        hz_cust_accounts hca,
                        hz_parties hp
                WHERE       1 = 1
                        AND wts.stop_id = wdl.pick_up_stop_id
                        AND wdl.delivery_id = wnd.delivery_id
                        AND wts.trip_id = wt.trip_id
                        AND wnd.customer_id = hca.cust_account_id
                        AND hca.party_id = hp.party_id) x,
              --hz_cust_accounts hca,
              --hz_parties hp,
              hz_locations hl,
              fnd_territories_vl ftv,
              hr_organization_units hou,
              hr_locations hol,
              fnd_territories_vl fftv,
              org_organization_definitions ood,
              xle_entity_profiles xep
      WHERE       xal.trip_id = x.trip_id
              --AND xal.inventory_item_id = wdd.inventory_item_id
              --AND wnd.delivery_id = wda.delivery_id
              --AND wda.delivery_detail_id = wdd.delivery_detail_id
              --AND x.cust_account_id = hca.cust_account_id
              --AND hca.party_id = hp.party_id
              AND NVL (x.intmed_ship_to_location_id,
                       x.ultimate_dropoff_location_id) = hl.location_id
              AND hl.country = ftv.territory_code(+)
              AND x.organization_id = hou.organization_id
              AND hou.location_id = hol.location_id
              AND hol.country = fftv.territory_code(+)
              --AND wnd.organization_id = ood.organization_id
              AND x.organization_id = ood.organization_id
              AND ood.legal_entity = xep.legal_entity_id
   GROUP BY   x.trip_id,
              x.TRIP_NAME,
              --wnd.delivery_id,
              --wnd.name,
              x.organization_id,
              xal.pallet_number,
              xal.box_number,
              x.account_number,
              x.party_name,
              hl.address1,
              hl.address2,
              hl.address3,
              hl.address4,
              hl.city,
              hl.state,
              hl.province,
              hl.postal_code,
              hl.county,
              hl.country,
              ftv.territory_short_name,
              xep.name,
              hol.address_line_1,
              hol.address_line_2,
              hol.address_line_3,
              hol.town_or_city,
              NVL (fftv.territory_short_name, hol.region_2),
              hol.region_2,
              hol.postal_code,
              hol.telephone_number_1
   ORDER BY   x.trip_id
/
EXIT
/