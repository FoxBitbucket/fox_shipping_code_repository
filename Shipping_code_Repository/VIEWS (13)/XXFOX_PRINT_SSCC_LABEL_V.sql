
CREATE OR REPLACE FORCE VIEW APPS.XXFOX_PRINT_SSCC_LABEL_V
AS
     SELECT                                                  --xal.box_number,
              --xal.pallet_number,
              --xal.inventory_item_id,
              xal.trip_id "TRIP_ID",
              SUM (xal.audit_quantity) "audit_quantity",
              XXFOX_PACKWB_PKG.XXFOX_SSCC_LABEL_DATA (xal.trip_id,         --xal.pallet_number,
                                                      --null,--xal.box_number,
               'SF') "Ship_From",
              XXFOX_PACKWB_PKG.XXFOX_SSCC_LABEL_DATA (xal.trip_id,         --xal.pallet_number,
                                                      --null,--xal.box_number,
               'ST') "Ship_to",
              XXFOX_PACKWB_PKG.XXFOX_SSCC_LABEL_DATA (xal.trip_id,         --xal.pallet_number,
                                                      --null,--xal.box_number,
               'PC') "Ship_To_Postal_Code",
              XXFOX_PACKWB_PKG.XXFOX_SSCC_LABEL_DATA (xal.trip_id,         --xal.pallet_number,
                                                      --null,--xal.box_number,
               'SM') "Shipping_Method",
              --apps.xxfox_sscc_label_data (xal.trip_id,
              --                            xal.pallet_number,
              --                            xal.box_number,
              --                           'PO')
              xal.po_number "PO",
              DECODE (
                 XXFOX_PACKWB_PKG.XXFOX_SSCC_LABEL_DATA (xal.trip_id, 'PCARTON'),
                 NULL,
                 NULL,
                 xal.pallet_number
                 || XXFOX_PACKWB_PKG.XXFOX_SSCC_LABEL_DATA (xal.trip_id, 'PCARTON')
              )   "carton_number",
              XXFOX_PACKWB_PKG.XXFOX_SSCC_LABEL_DATA (xal.trip_id,         --xal.pallet_number,
                                                      --null,--xal.box_number,
               'BOL') "BOL",
              xal.sscc_code "SSCC_CODE",
              'Pallet' sscc_type,
              NULL subinventory_code,
	                  NULL locator_id,
	                  NULL lot_number,
	                  NULL revision,
	                  NULL serial_number,
	                  NULL lpn_id,
	                  NULL delivery_id,
	                  NULL delivery_detail_id,
	                  NULL sales_order_header_id,
            NULL sales_order_line_id
       FROM   xxfox_audit_lines xal
      WHERE   xal.sscc_code IS NOT NULL
              AND ( (pallet_number IS NOT NULL)
                   OR (pallet_number IS NULL AND box_number IS NOT NULL))
   GROUP BY   xal.trip_id,
              xal.sscc_code,
              xal.pallet_number,
              xal.po_number
   UNION
     SELECT                                                  --xal.box_number,
              --xal.pallet_number,
              --xal.inventory_item_id,
              xal.trip_id "TRIP_ID",
              SUM (xal.audit_quantity) "audit_quantity",
              XXFOX_PACKWB_PKG.XXFOX_SSCC_LABEL_DATA (xal.trip_id, 'SF') "Ship_From",
              XXFOX_PACKWB_PKG.XXFOX_SSCC_LABEL_DATA (xal.trip_id, 'ST') "Ship_to",
              XXFOX_PACKWB_PKG.XXFOX_SSCC_LABEL_DATA (xal.trip_id, 'PC') "Ship_To_Postal_Code",
              XXFOX_PACKWB_PKG.XXFOX_SSCC_LABEL_DATA (xal.trip_id, 'SM') "Shipping_Method",
              xal.po_number "PO",
              DECODE (
                 XXFOX_PACKWB_PKG.XXFOX_SSCC_LABEL_DATA (xal.trip_id, 'CCARTON'),
                 NULL,
                 NULL,
                 xal.box_number
                 || XXFOX_PACKWB_PKG.XXFOX_SSCC_LABEL_DATA (xal.trip_id, 'CCARTON')
              )
                 "carton_number",
              XXFOX_PACKWB_PKG.XXFOX_SSCC_LABEL_DATA (xal.trip_id, 'BOL') "BOL",
              xal.sscc_code "SSCC_CODE",
              'Box' sscc_type,
              NULL subinventory_code,
	                  NULL locator_id,
	                  NULL lot_number,
	                  NULL revision,
	                  NULL serial_number,
	                  NULL lpn_id,
	                  NULL delivery_id,
	                  NULL delivery_detail_id,
	                  NULL sales_order_header_id,
            NULL sales_order_line_id
       FROM   xxfox_audit_lines xal
      WHERE   xal.sscc_code IS NOT NULL        --and pallet_number IS NOT NULL
              AND (pallet_number IS NULL AND box_number IS NOT NULL)
   GROUP BY   xal.trip_id,
              xal.sscc_code,
              xal.box_number,
              xal.po_number
/
EXIT;