CREATE OR REPLACE FORCE VIEW APPS.XXFOX_AUDIT_LINES_MEASURES_V
AS
     SELECT   trip_id,
              delivery_id,
              pallet_number pallet,
              box_number box,
              pallet_dimension dim,
              pallet_dim_uom dim_uom,
              pallet_gross_wt gross_weight,
              pallet_net_wt net_weight,
              pallet_wt_uom uom
       FROM   XXFOX_AUDIT_LINES
   GROUP BY   trip_id,
              delivery_id,
              pallet_number,
              box_number,
              pallet_dimension,
              pallet_dim_uom,
              pallet_gross_wt,
              pallet_net_wt,
              pallet_wt_uom

/

exit;