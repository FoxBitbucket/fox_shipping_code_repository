
CREATE OR REPLACE FORCE VIEW APPS.XXFOX_WIP_JOBTRAVEL_LABEL_V
AS
   SELECT   we.wip_entity_id,
            we.wip_entity_name,
            TO_CHAR (wdj.scheduled_start_date, 'MM/DD/YYYY')
               scheduled_start_date,
            wdj.start_quantity,
            msi.segment1 item_number,
            msi.description item_description,
            (  SELECT   LISTAGG (fds.short_text, CHR (10))
                           WITHIN GROUP (ORDER BY fad.pk1_value)
                 FROM   fnd_attached_documents fad,
                        fnd_documents fd,
                        fnd_documents_short_text fds,
                        fnd_document_datatypes fdd,
                        fnd_document_categories_tl fdct
                WHERE       fad.document_id = fd.document_id
                        AND fd.media_id = fds.media_id
                        AND fd.datatype_id = fdd.datatype_id
                        AND fd.category_id = fdct.category_id
                        AND fdd.user_name = 'Short Text'
                        AND fad.entity_name = 'WIP_DISCRETE_JOBS'
                        AND fdct.language = USERENV ('LANG')
                        AND fad.pk1_value = we.wip_entity_id
                        AND fad.pk2_value = we.organization_id
             GROUP BY   fad.pk1_value)
               wo_notes,
            msi.planner_code planner_code,
            we.primary_item_id inventory_item_id,
            we.organization_id,
            NULL subinventory_code,
            NULL locator_id,
            NULL lot_number,
            NULL revision,
            NULL serial_number,
            NULL lpn_id,
            NULL delivery_id,
            NULL delivery_detail_id,
            NULL sales_order_header_id,
            NULL sales_order_line_id
     FROM   wip_entities we, wip_discrete_jobs wdj, mtl_system_items_b msi
    WHERE       1 = 1
            AND we.wip_entity_id = wdj.wip_entity_id
            AND msi.inventory_item_id = we.primary_item_id
            AND msi.organization_id = we.organization_id;


--DROP SYNONYM XXFOX.XXFOX_WIP_JOBTRAVEL_LABEL_V;

--CREATE SYNONYM XXFOX.XXFOX_WIP_JOBTRAVEL_LABEL_V FOR APPS.XXFOX_WIP_JOBTRAVEL_LABEL_V;


--DROP SYNONYM XX_APEX.XXFOX_WIP_JOBTRAVEL_LABEL_V;

--CREATE SYNONYM XX_APEX.XXFOX_WIP_JOBTRAVEL_LABEL_V FOR APPS.XXFOX_WIP_JOBTRAVEL_LABEL_V;

exit;