CREATE OR REPLACE PACKAGE APPS.XXFOX_SHIPPING_INTERFACE_PKG AUTHID CURRENT_USER AS


TYPE track_rec IS RECORD
   (delivery_number         VARCHAR2(60),
    tracking_number         VARCHAR2(240),
    lead_track_number       VARCHAR2(240),
    package_id              NUMBER,
    weight                  NUMBER,
    freight_cost            NUMBER,
    void_indicator          VARCHAR2(5),
    viod_reason             VARCHAR2(240)
   );

PROCEDURE update_delivery_api
   (p_track_rec          IN     track_rec,
    x_return_status      IN OUT VARCHAR2,
    x_msg_data           IN OUT VARCHAR2
   );

PROCEDURE UPDATE_DELIVERY
   (errbuf          OUT VARCHAR2,
    retcode         OUT NUMBER
   );


FUNCTION get_CUST_UPS_ACCOUNT
   (p_site_use_id    IN    NUMBER
   ) RETURN VARCHAR2;


FUNCTION get_CUST_acct_UPS_ACCOUNT
   (p_cust_acct_id    IN    NUMBER
   ) RETURN VARCHAR2;

FUNCTION get_CUST_acct_FEDEX_ACCOUNT
   (p_cust_acct_id    IN    NUMBER,p_delivery_id IN NUMBER,  p_number in NUMBER
   ) RETURN VARCHAR2;

FUNCTION get_trip_weight
   (p_trip_id    IN    NUMBER
   ) RETURN number;

END XXFOX_SHIPPING_INTERFACE_PKG;

EXIT
/
