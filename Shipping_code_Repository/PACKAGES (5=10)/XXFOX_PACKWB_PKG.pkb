CREATE OR REPLACE PACKAGE BODY APPS.xxfox_packwb_pkg
AS
   /* $Header: XXFOX_PACKWB_PKG.pkb 1.0 2015/08/14 18:40:24PST Development  $
   REM +==========================================================================+
   REM |     Copyright (c) 2006 DAZ Systems, Inc .                                |
   REM |                  El Segundo, CA 90505                                    |
   REM |                All rights reserved                                       |
   REM +==========================================================================+
   REM
   REM NAME
   REM  XXFOX_PACKWB_PKG.pkb
   REM
   REM PROGRAM TYPE
   REM  PL/SQL Package
   REM
   REM PURPOSE
   REM  The purpose of this package is to contain the custom procedures and
   REM  functions used by DAZSI' Oracle Applications Customizations.
   REM
   REM  This package is primarily used for Custom Packing Workbench
   REM HISTORY
   REM ===========================================================================
   REM  08/14/2015   Kranti K           Created
   REM  03/17/2016   Sriram Natarajan   Fixed Delivery Status derivation logic
   REM                                  search for tag SN03172016
   REM  02/11/2017   Raj N              Added proc validate_edi_trip,update_serial_in_stg,
   REM                                  update_delivery_api for PCN#154
   REM ===========================================================================
   REM ===========================================================================*/
   PROCEDURE print_debug (p_message IN VARCHAR2)
   AS
   BEGIN
      -- dbms_output.put_line (p_message);
      apps.fnd_file.put_line (fnd_file.LOG, TO_CHAR (SYSDATE, 'MM/DD/YYYY HH24:MI:SS') || ' => ' || p_message);
   EXCEPTION
      WHEN OTHERS THEN
         fnd_file.put_line (fnd_file.LOG, 'Error logging debug mesgs: ' || SQLERRM);
   END print_debug;

   --
   PROCEDURE initialize_temp (p_trip_id             IN            NUMBER,
                              p_delivery_id         IN            NUMBER,
                              p_header_id           IN            NUMBER,
                              p_delivery_status     IN            VARCHAR2,
                              p_customer_id         IN            NUMBER,
                              p_pack_slip_no        IN            NUMBER,
                              p_order_type_id       IN            NUMBER,
                              p_item_categ          IN            VARCHAR2,
                              p_ship_method_code    IN            VARCHAR2,
                              p_ship_date_from      IN            DATE,
                              p_ship_date_to        IN            DATE,
                              p_inventory_item_id   IN            NUMBER,
                              p_organization_id     IN            NUMBER,
                              p_percent_reserved    IN            NUMBER,
                              p_delay_reason        IN            VARCHAR2,
                              p_stag_loc_id         IN            NUMBER,
                              x_session_id          IN OUT NOCOPY NUMBER,
                              x_batch_id               OUT NOCOPY NUMBER,
                              x_return_status          OUT NOCOPY VARCHAR2,
                              x_msg_data               OUT NOCOPY VARCHAR2)
   AS
      v_debug_loc       NUMBER;
      v_session_id      NUMBER;
      v_batch_id        NUMBER;
      v_query1          VARCHAR2 (32700);
      v_where1          VARCHAR2 (10000) := NULL;
      v_group_by1       VARCHAR2 (10000) := NULL;
      v_query2          VARCHAR2 (32700);
      v_where2          VARCHAR2 (10000) := NULL;
      v_where3          VARCHAR2 (20000) := NULL; --kp
      v_group_by2       VARCHAR2 (10000) := NULL;
      v_group_by3       VARCHAR2 (10000) := NULL;
      v_query_handle1   NUMBER;
      v_query_handle2   NUMBER;
      v_query_count1    NUMBER;
      v_query_count2    NUMBER;
      v_update_str      VARCHAR2 (4000);
      v_delete_str      VARCHAR2 (4000);
      v_delivery_id1     VARCHAR(2000) := NULL;
      v_tripid_temp     NUMBER; --kp
      v_cnt             NUMBER:=0;
      
      CURSOR get_del (p_headid NUMBER) IS
      SELECT wnd.delivery_id
        FROM wsh_delivery_details wdd, 
             wsh_delivery_assignments wda,
             wsh_new_deliveries wnd
       WHERE wdd.source_code        = 'OE'
         AND wdd.delivery_detail_id = wda.delivery_detail_id
         AND wda.delivery_id        = wnd.delivery_id
         AND wdd.source_header_id   = p_headid;
        

   --
   BEGIN
      x_return_status := 'S';
      v_debug_loc := 10;
      v_session_id := x_session_id;

      SELECT xxfox_packing_del_batch_s.NEXTVAL INTO v_batch_id FROM DUAL;

      x_batch_id := v_batch_id;
      v_debug_loc := 20;
      v_query1 :=
            'INSERT INTO xxfox_packing_deliveries '
         || 'SELECT '
         || v_batch_id
         || ' , '
         || v_session_id
         || ' , wts.trip_id
         , wnd.delivery_id
         , MIN(TRUNC(wnd.initial_pickup_date)) date_scheduled
         , COUNT(1) no_of_lines
         , SUM(wdd.requested_quantity) requested_quantity
         , hp.party_name customer_name
         , hca.account_number customer_number
         , wdd.source_header_type_name order_type
         , xxfox_packwb_pkg.get_delivery_status (wnd.delivery_id, wnd.status_code, wnd.planned_flag, NVL(wnd.attribute4, ''N'')) delivery_status
         , MAX(mil.concatenated_segments) staging_location
         , MAX((SELECT fd.file_name
                FROM fnd_attached_documents fad, fnd_documents fd, fnd_documents_tl fdt
                WHERE fad.document_id = fd.document_id
                AND fd.document_id = fdt.document_id
                AND fdt.language = ''US''
                AND fad.entity_name = ''OE_ORDER_HEADERS''
                AND fad.pk1_value = TO_CHAR(wdd.source_header_id)
                AND fdt.title LIKE ''604%'')) check_list
         , NULL
         , wnd.attribute2 delay_reason
         , (select address1||case when address2 is null then null else chr(32)||address2 end||case when address3 is null then null else chr(32)||address3 end||chr(32)||city||chr(32)||state||chr(32)||postal_code||chr(32)||COUNTRY
            from hz_locations a
            where location_id = wnd.ultimate_dropoff_location_id
           ) ship_to_address
         , xxfox_packwb_pkg.get_trip_status (wts.trip_id, wnd.status_code, wnd.planned_flag, NVL(wnd.attribute4, ''N'')) trip_status
     FROM wsh_delivery_details wdd
       , wsh_delivery_assignments wda
       , mtl_txn_request_lines mtrl
       , wsh_new_deliveries wnd
       , wsh_delivery_legs wdl
       , wsh_document_instances wdi
       , wsh_trip_stops wts
       , hz_cust_accounts hca
       , hz_parties hp
       , mtl_item_locations_kfv mil';
      v_where1 := ' WHERE wdd.source_code = ''OE''
                  AND wdd.delivery_detail_id = wda.delivery_detail_id
                  AND wdd.move_order_line_id = mtrl.line_id(+)
                  AND wda.delivery_id = wnd.delivery_id
                  AND wnd.delivery_id = wdl.delivery_id(+)
                  AND wdi.entity_id(+) = wdl.delivery_id
                  AND wdi.entity_name(+) = ''WSH_NEW_DELIVERIES''
                  AND wdi.document_type(+) = ''PACK_TYPE''
                  AND wdi.status(+) <> ''CANCELLED''
                  AND wdl.pick_up_stop_id = wts.stop_id(+)
                  AND wdd.customer_id = hca.cust_account_id
                  AND hca.party_id = hp.party_id
                  AND mtrl.to_locator_id = mil.inventory_location_id(+)
                  AND mtrl.organization_id = mil.organization_id(+)';

      IF p_trip_id IS NOT NULL THEN
         v_where1 := v_where1 || ' AND wts.trip_id = :p_trip_id';
      END IF;

      IF p_delivery_id IS NOT NULL THEN
         v_where1 := v_where1 || ' AND wnd.delivery_id = :p_delivery_id';
      END IF;


      IF p_header_id IS NOT NULL THEN
        v_delivery_id1 := NULL;        
        FOR get_deliveries IN get_del(p_header_id) LOOP        
            IF v_delivery_id1 IS NULL  THEN
                v_delivery_id1 := get_deliveries.delivery_id;
            ELSE
                v_delivery_id1 := v_delivery_id1||','||get_deliveries.delivery_id;            
            END IF;            
        END LOOP;              
         IF v_delivery_id1 IS NOT NULL THEN
             v_where1 := v_where1 || ' AND wnd.delivery_id IN ( '||v_delivery_id1||')';
         ELSE
             v_where1 := v_where1 || ' AND wnd.delivery_id = 0 ';
         END IF;
      END IF;


      /*IF p_header_id IS NOT NULL THEN
        v_where1 := v_where1 || ' AND wdd.source_header_id = :p_header_id';
      END IF;*/
      IF p_customer_id IS NOT NULL THEN
         v_where1 := v_where1 || ' AND wdd.customer_id = :p_customer_id';
      END IF;

      IF p_pack_slip_no IS NOT NULL THEN
         v_where1 := v_where1 || ' AND wdi.sequence_number = :p_pack_slip_no';
      END IF;

      IF p_order_type_id IS NOT NULL THEN
         v_where1 := v_where1 || ' AND wdd.source_header_type_id = :p_order_type_id';
      END IF;

      IF p_ship_method_code IS NOT NULL THEN
         v_where1 := v_where1 || ' AND wdd.ship_method_code = :p_ship_method_code';
      END IF;

      IF p_ship_date_from IS NOT NULL THEN
         IF p_ship_date_to IS NOT NULL THEN
            v_where1 :=
               v_where1 || ' AND TRUNC(wnd.initial_pickup_date) BETWEEN :p_ship_date_from AND :p_ship_date_to';
         ELSE
            v_where1 := v_where1 || ' AND TRUNC(wnd.initial_pickup_date) >= :p_ship_date_from';
         END IF;
      ELSE
         IF p_ship_date_to IS NOT NULL THEN
            v_where1 := v_where1 || ' AND TRUNC(wnd.initial_pickup_date) <= :p_ship_date_to';
         END IF;
      END IF;

      IF p_inventory_item_id IS NOT NULL THEN
         v_where1 := v_where1 || ' AND wdd.inventory_item_id = :p_inventory_item_id';
      END IF;

      IF p_item_categ IS NOT NULL THEN
         v_where1 :=
               v_where1
            || ' AND EXISTS (SELECT 1
                                             FROM mtl_item_categories mic
                                               , mtl_categories_kfv mc
                                             WHERE mic.category_id = mc.category_id
                                             AND mic.category_set_id = 1
                                             AND mic.inventory_item_id = wdd.inventory_item_id
                                             AND mic.organization_id = wdd.organization_id
                                             AND mc.concatenated_segments = :p_item_categ)';
      END IF;

      IF p_delivery_status <> 'Shipped' THEN
         v_where1 := v_where1 || ' AND wdd.released_status IN (''R'', ''S'', ''B'', ''Y'')';
      END IF;

      IF p_delay_reason IS NOT NULL THEN
         v_where1 := v_where1 || ' AND wnd.attribute2 = :p_delay_reason';
      END IF;

      IF p_organization_id IS NOT NULL THEN
         v_where1 := v_where1 || ' AND wnd.organization_id = :p_organization_id';
      END IF;

      IF p_stag_loc_id IS NOT NULL THEN
         v_where1 := v_where1 || ' AND mtrl.to_locator_id = :p_stag_loc_id';
      END IF;

      v_group_by1 := ' GROUP BY wts.trip_id
                       , wnd.delivery_id
                       , hca.account_number
                       , hp.party_name
                       , wdd.source_header_type_name
                       , wnd.status_code
                       , wnd.planned_flag
                       , wnd.attribute4
                       , wnd.attribute2
                       , wnd.ultimate_dropoff_location_id';
      v_debug_loc := 30;
      v_query1 := v_query1 || v_where1 || v_group_by1;

      DELETE FROM xxfox_packing_deliveries
            WHERE session_id = v_session_id;

      v_query_handle1 := dbms_sql.open_cursor;
      dbms_sql.parse (v_query_handle1, v_query1, dbms_sql.native);

      IF p_trip_id IS NOT NULL THEN
         dbms_sql.bind_variable (v_query_handle1, 'p_trip_id', p_trip_id);
      END IF;

      IF p_delivery_id IS NOT NULL THEN
         dbms_sql.bind_variable (v_query_handle1, 'p_delivery_id', p_delivery_id);
      END IF;
/*
      IF v_delivery_id1 IS NOT NULL THEN
         dbms_sql.bind_variable (v_query_handle1, 'v_delivery_id1', v_delivery_id1);
      END IF;
*/
      /*IF p_header_id IS NOT NULL THEN
        DBMS_SQL.BIND_VARIABLE(v_query_handle1, 'p_header_id', p_header_id);
      END IF;*/
      IF p_customer_id IS NOT NULL THEN
         dbms_sql.bind_variable (v_query_handle1, 'p_customer_id', p_customer_id);
      END IF;

      IF p_pack_slip_no IS NOT NULL THEN
         dbms_sql.bind_variable (v_query_handle1, 'p_pack_slip_no', p_pack_slip_no);
      END IF;

      IF p_order_type_id IS NOT NULL THEN
         dbms_sql.bind_variable (v_query_handle1, 'p_order_type_id', p_order_type_id);
      END IF;

      IF p_ship_method_code IS NOT NULL THEN
         dbms_sql.bind_variable (v_query_handle1, 'p_ship_method_code', p_ship_method_code);
      END IF;

      IF p_ship_date_from IS NOT NULL THEN
         dbms_sql.bind_variable (v_query_handle1, 'p_ship_date_from', p_ship_date_from);
      END IF;

      IF p_ship_date_to IS NOT NULL THEN
         dbms_sql.bind_variable (v_query_handle1, 'p_ship_date_to', p_ship_date_to);
      END IF;

      IF p_inventory_item_id IS NOT NULL THEN
         dbms_sql.bind_variable (v_query_handle1, 'p_inventory_item_id', p_inventory_item_id);
      END IF;

      IF p_item_categ IS NOT NULL THEN
         dbms_sql.bind_variable (v_query_handle1, 'p_item_categ', p_item_categ);
      END IF;

      IF p_delay_reason IS NOT NULL THEN
         dbms_sql.bind_variable (v_query_handle1, 'p_delay_reason', p_delay_reason);
      END IF;

      IF p_organization_id IS NOT NULL THEN
         dbms_sql.bind_variable (v_query_handle1, 'p_organization_id', p_organization_id);
      END IF;

      IF p_stag_loc_id IS NOT NULL THEN
         dbms_sql.bind_variable (v_query_handle1, 'p_stag_loc_id', p_stag_loc_id);
      END IF;

      v_debug_loc := 40;
      v_query_count1 := dbms_sql.execute (v_query_handle1);
      v_debug_loc := 50;
      dbms_sql.close_cursor (v_query_handle1);

      IF p_delivery_status <> 'Not Shipped' THEN
         DELETE FROM xxfox_packing_deliveries
               WHERE session_id = v_session_id AND batch_id = v_batch_id AND  TRIP_STATUS <> p_delivery_status; --kp
      END IF;


      v_debug_loc := 60;
      v_query2 :=
            'INSERT INTO xxfox_packing_delivery_lines '
         || 'SELECT '
         || v_batch_id
         || ' , '
         || v_session_id
         || ', delivery_id
         , date_scheduled
         , inventory_item_id
         , segment1
         , concatenated_segments
         , no_of_lines
         , req_qty
         , SUBSTR(rsv_det, 1, INSTR(rsv_det, ''|'', 1, 1) -1) rsv_qty
         , customer_item_number
         , SUBSTR(rsv_det, INSTR(rsv_det,''|'', 1, 1) + 1, INSTR(rsv_det,''|'', 1, 2) - INSTR(rsv_det,''|'', 1, 1)-1) pick_subinventory
         , SUBSTR(rsv_det, INSTR(rsv_det,''|'', 1, 2) + 1, INSTR(rsv_det,''|'', 1, 3) - INSTR(rsv_det,''|'', 1, 2)-1) pick_locator
         , attribute1
         , description
         , PICKED_QUANTITY
         , AUDITED_QUANTITY
         , trip_id
       FROM (SELECT wts.trip_id,wda.delivery_id
               , MIN(TRUNC(wdd.date_scheduled)) date_scheduled
               , wdd.inventory_item_id
               , msi.segment1
               , mc.concatenated_segments
               , COUNT(1) no_of_lines
               , SUM(wdd.requested_quantity) req_qty
               , xxfox_packwb_pkg.get_rsv_det (wda.delivery_id,NULL,wdd.inventory_item_id, wdd.organization_id) rsv_det
               , NULL customer_item_number
               , wdd.attribute1
               , msi.description
               , SUM(wdd.PICKED_QUANTITY) PICKED_QUANTITY
               , (select SUM(xal.audit_quantity) from xxfox_audit_lines xal
                             where DECODE(TRIP_ID,NULL,delivery_id,trip_id)= DECODE(TRIP_id,NULL,wda.delivery_id,wts.TRIP_ID)  -- KP
                             and xal.inventory_item_id = wdd.inventory_item_id) AUDITED_QUANTITY
           FROM wsh_delivery_details wdd
             , wsh_delivery_assignments wda
             , wsh_delivery_legs wdl ---------------kp
             , wsh_trip_stops wts  -----------------kp
             , mtl_system_items_b msi
             , mtl_item_categories mic
             , mtl_categories_kfv mc';
      v_where2 :=
         ' WHERE wdd.source_code = ''OE''
                         AND wdd.delivery_detail_id = wda.delivery_detail_id
                         AND wdd.inventory_item_id = msi.inventory_item_id
                         AND wda.delivery_id = wdl.delivery_id(+)  ------------KP
                         AND wdl.pick_up_stop_id = wts.stop_id(+)  ------------KP
                         AND wdd.organization_id = msi.organization_id
                         AND wdd.inventory_item_id = mic.inventory_item_id(+)
                         AND wdd.organization_id = mic.organization_id(+)
                         AND mic.category_id = mc.category_id(+)
                         AND mic.category_set_id(+) = 1';


BEGIN --------KP
      BEGIN
        SELECT NVL(trip_id,0)
          INTO v_tripid_temp
          FROM xxfox_packing_deliveries
         WHERE session_id = v_session_id
           AND batch_id   = v_batch_id ;
      EXCEPTION
          WHEN OTHERS THEN
             v_tripid_temp := 0 ;
      END;

      IF v_tripid_temp != 0 THEN
           v_where3 := 'AND wts.trip_id IN (SELECT trip_id FROM xxfox_packing_deliveries WHERE session_id = :v_session_id AND batch_id = :v_batch_id)';
      ELSE
           v_where3 := 'AND wda.delivery_id IN (SELECT delivery_id FROM xxfox_packing_deliveries WHERE session_id = :v_session_id AND batch_id = :v_batch_id)';
      END IF;
EXCEPTION
    WHEN OTHERS THEN
          NULL;
END; ----------------------KP

      v_group_by2 := ' GROUP BY wts.trip_id,wda.delivery_id
                              , wdd.inventory_item_id
                              , wdd.organization_id
                              , msi.segment1
                              , mc.concatenated_segments
                              , wdd.attribute1
                              , msi.description)';

      v_debug_loc := 70;
      v_query2 := v_query2 || v_where2 ||v_where3||v_group_by2; ------KP

      DELETE FROM xxfox_packing_delivery_lines
            WHERE session_id = v_session_id;

      v_query_handle2 := dbms_sql.open_cursor;
      v_debug_loc := 80;
      dbms_sql.parse (v_query_handle2, v_query2, dbms_sql.native);
      dbms_sql.bind_variable (v_query_handle2, 'v_session_id', v_session_id);
      dbms_sql.bind_variable (v_query_handle2, 'v_batch_id', v_batch_id);
      v_debug_loc := 90;
      v_query_count2 := dbms_sql.execute (v_query_handle2);
      dbms_sql.close_cursor (v_query_handle2);
      v_update_str :=
         'UPDATE xxfox_packing_deliveries xpd
                      SET percent_reserved = (SELECT ROUND((NVL(SUM(reserved_quantity), 0)/xpd.requested_quantity) * 100, 2)
                      FROM xxfox_packing_delivery_lines
                      WHERE delivery_id = xpd.delivery_id
                      AND batch_id = xpd.batch_id
                      AND session_id = xpd.session_id)';

      EXECUTE IMMEDIATE v_update_str;

      IF p_percent_reserved IS NOT NULL THEN
         v_delete_str := 'DELETE FROM xxfox_packing_deliveries
                        WHERE percent_reserved < :p_percent_reserved
                        AND batch_id = :v_batch_id
                        AND session_id = :v_session_id';

         EXECUTE IMMEDIATE v_delete_str USING p_percent_reserved, v_batch_id, v_session_id;

         v_delete_str := 'DELETE FROM xxfox_packing_delivery_lines xpdl
                        WHERE NOT EXISTS (SELECT 1
                                          FROM xxfox_packing_deliveries xpd
                                          WHERE xpdl.delivery_id = xpd.delivery_id
                                          AND xpdl.batch_id = xpd.batch_id
                                          AND xpdl.session_id = xpd.session_id)
                        AND batch_id = :v_batch_id
                        AND session_id = :v_session_id';

         EXECUTE IMMEDIATE v_delete_str USING v_batch_id, v_session_id;
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         x_return_status := 'E';
         x_msg_data := SQLERRM || ' Error loc: ' || v_debug_loc;

         IF dbms_sql.is_open (v_query_handle1) THEN
            dbms_sql.close_cursor (v_query_handle1);
         END IF;

         IF dbms_sql.is_open (v_query_handle2) THEN
            dbms_sql.close_cursor (v_query_handle2);
         END IF;
   END initialize_temp;

   --
FUNCTION get_delivery_status (p_delivery_id    IN NUMBER,
                                 p_status_code    IN VARCHAR2,
                                 p_planned_flag   IN VARCHAR2,
                                 p_audited_flag   IN VARCHAR2)
      RETURN VARCHAR2
   AS
      v_rtr_ct              NUMBER := 0;
      v_rtw_ct              NUMBER := 0;
      v_bo_ct               NUMBER := 0;
      v_staged_ct           NUMBER := 0;
      v_shipped_ct          NUMBER := 0;
      v_delivery_status     VARCHAR2 (240);
      v_pick_tkt_print_ct   NUMBER;
      v_pick_tkt_print_dt   DATE;
      v_audit_nc_ct         NUMBER;
   BEGIN
      FOR rec_glb1
         IN (  SELECT wdd.released_status,
                      COUNT (DISTINCT NVL (wdd.attribute2, '@')) pick_tkt_print_ct,
                      MAX (wdd.attribute2) pick_tkt_print_dt,
                      COUNT (*) ct
                 FROM wsh_delivery_details wdd, wsh_delivery_assignments wda
                WHERE     wdd.source_code = 'OE'
                      AND wdd.delivery_detail_id = wda.delivery_detail_id
                      AND wda.delivery_id = p_delivery_id
             GROUP BY wdd.released_status) LOOP
         IF rec_glb1.released_status = 'R' THEN
            v_rtr_ct := rec_glb1.ct;
            v_pick_tkt_print_ct := rec_glb1.pick_tkt_print_ct;
            v_pick_tkt_print_dt := rec_glb1.pick_tkt_print_dt;
         ELSIF rec_glb1.released_status = 'S' THEN
            v_rtw_ct := rec_glb1.ct;
            v_pick_tkt_print_ct := rec_glb1.pick_tkt_print_ct;
            v_pick_tkt_print_dt := rec_glb1.pick_tkt_print_dt;
         ELSIF rec_glb1.released_status = 'B' THEN
            v_bo_ct := rec_glb1.ct;
            v_pick_tkt_print_ct := rec_glb1.pick_tkt_print_ct;
            v_pick_tkt_print_dt := rec_glb1.pick_tkt_print_dt;
         ELSIF rec_glb1.released_status = 'Y' THEN
            v_staged_ct := rec_glb1.ct;
            v_pick_tkt_print_ct := rec_glb1.pick_tkt_print_ct;
            v_pick_tkt_print_dt := rec_glb1.pick_tkt_print_dt;
         ELSIF rec_glb1.released_status = 'C' THEN
            v_shipped_ct := rec_glb1.ct;
            v_pick_tkt_print_ct := rec_glb1.pick_tkt_print_ct;
            v_pick_tkt_print_dt := rec_glb1.pick_tkt_print_dt;
         END IF;
      END LOOP;

      IF (v_rtw_ct > 0 OR v_bo_ct > 0) AND (v_pick_tkt_print_ct = 1 AND v_pick_tkt_print_dt IS NULL) THEN
         v_delivery_status := 'Not Printed';
         RETURN v_delivery_status;
      ELSIF v_rtw_ct > 0 AND ( (v_bo_ct > 0 OR v_rtr_ct > 0) AND v_staged_ct = 0 AND v_pick_tkt_print_dt IS NOT NULL) THEN
         v_delivery_status := 'Picking';
         RETURN v_delivery_status;
      ELSIF v_rtw_ct > 0 AND (v_bo_ct = 0 AND v_rtr_ct = 0 AND v_staged_ct = 0 AND v_pick_tkt_print_dt IS NOT NULL) THEN --SN03172016: Fixed by Sriram on 17-Mar-2016
         v_delivery_status := 'Picking';
         RETURN v_delivery_status;
      ELSIF v_staged_ct > 0 AND ( (v_rtr_ct > 0 OR v_rtw_ct > 0 OR v_bo_ct > 0) AND v_pick_tkt_print_dt IS NOT NULL) THEN --SN03172016: Fixed by Sriram on 17-Mar-2016
         v_delivery_status := 'Picking';
         RETURN v_delivery_status;
      ELSIF v_rtw_ct = 0 AND v_bo_ct = 0 AND v_staged_ct > 0 AND p_status_code = 'OP' THEN
         IF p_planned_flag = 'Y' THEN
            v_delivery_status := 'Ready to Ship';
            RETURN v_delivery_status;
         ELSE
           /* SELECT COUNT (1)
              INTO v_audit_nc_ct
              FROM (  SELECT wdd.inventory_item_id,
                             SUM (requested_quantity) req_qty,
                             MAX (NVL ( (audit_qty), 0)) audit_qty
                        FROM wsh_delivery_details wdd,
                             wsh_delivery_assignments wda,
                             (  SELECT delivery_id, inventory_item_id, NVL (SUM (audit_quantity), 0) audit_qty
                                  FROM xxfox_audit_lines
                                 WHERE delivery_id = p_delivery_id
                              GROUP BY delivery_id, inventory_item_id) audit_lines
                       WHERE     wdd.source_code = 'OE'
                             AND wdd.delivery_detail_id = wda.delivery_detail_id
                             AND wdd.inventory_item_id = audit_lines.inventory_item_id(+)
                             AND wda.delivery_id = p_delivery_id
                             AND wdd.released_status = 'Y'
                    GROUP BY wdd.inventory_item_id)
             WHERE req_qty <> audit_qty;

            IF v_audit_nc_ct = 0 THEN
               v_delivery_status := 'Audited';
               RETURN v_delivery_status;
            ELSE */
               v_delivery_status := 'Pulled';
               RETURN v_delivery_status;
           --END IF;
         END IF;
      ELSIF v_shipped_ct > 0 THEN
         v_delivery_status := 'Shipped';
         RETURN v_delivery_status;
      ELSE
         v_delivery_status := 'Not Printed';
         RETURN v_delivery_status;
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         RETURN NULL;
   END get_delivery_status;

   FUNCTION get_trip_status  (p_trip_id        IN NUMBER, --kp
                                 p_status_code    IN VARCHAR2,
                                 p_planned_flag   IN VARCHAR2,
                                 p_audited_flag   IN VARCHAR2)
      RETURN VARCHAR2
   AS
      v_rtr_ct              NUMBER := 0;
      v_rtw_ct              NUMBER := 0;
      v_bo_ct               NUMBER := 0;
      v_staged_ct           NUMBER := 0;
      v_shipped_ct          NUMBER := 0;
      v_delivery_status     VARCHAR2 (240);
      v_trip_status         VARCHAR2 (240);
      v_pick_tkt_print_ct   NUMBER;
      v_pick_tkt_print_dt   DATE;
      v_audit_nc_ct         NUMBER;
   BEGIN
      FOR rec_glb1
         IN (  SELECT wts.trip_id,wda.delivery_id,wdd.released_status,
                      COUNT (DISTINCT NVL (wdd.attribute2, '@')) pick_tkt_print_ct,
                      MAX (wdd.attribute2) pick_tkt_print_dt,
                      COUNT (*) ct
                 FROM wsh_delivery_details wdd,
                      wsh_delivery_assignments wda
                      , wsh_delivery_legs wdl --kp
                       , wsh_trip_stops wts--kp
                WHERE     wdd.source_code = 'OE'
                      AND wdd.delivery_detail_id = wda.delivery_detail_id
                      AND wda.delivery_id = wdl.delivery_id(+)--kp
                      AND wdl.pick_up_stop_id = wts.stop_id(+) --kp
                      AND wts.trip_id   =  p_trip_id--kp
             GROUP BY wts.trip_id,wda.delivery_id,wdd.released_status) LOOP
         IF rec_glb1.released_status = 'R' THEN
            v_rtr_ct := rec_glb1.ct;
            v_pick_tkt_print_ct := rec_glb1.pick_tkt_print_ct;
            v_pick_tkt_print_dt := rec_glb1.pick_tkt_print_dt;
         ELSIF rec_glb1.released_status = 'S' THEN
            v_rtw_ct := rec_glb1.ct;
            v_pick_tkt_print_ct := rec_glb1.pick_tkt_print_ct;
            v_pick_tkt_print_dt := rec_glb1.pick_tkt_print_dt;
         ELSIF rec_glb1.released_status = 'B' THEN
            v_bo_ct := rec_glb1.ct;
            v_pick_tkt_print_ct := rec_glb1.pick_tkt_print_ct;
            v_pick_tkt_print_dt := rec_glb1.pick_tkt_print_dt;
         ELSIF rec_glb1.released_status = 'Y' THEN
            v_staged_ct := rec_glb1.ct;
            v_pick_tkt_print_ct := rec_glb1.pick_tkt_print_ct;
            v_pick_tkt_print_dt := rec_glb1.pick_tkt_print_dt;
         ELSIF rec_glb1.released_status = 'C' THEN
            v_shipped_ct := rec_glb1.ct;
            v_pick_tkt_print_ct := rec_glb1.pick_tkt_print_ct;
            v_pick_tkt_print_dt := rec_glb1.pick_tkt_print_dt;
         END IF;
      END LOOP;

      IF (v_rtw_ct > 0 OR v_bo_ct > 0) AND (v_pick_tkt_print_ct = 1 AND v_pick_tkt_print_dt IS NULL) THEN
         v_delivery_status := 'Not Printed';
         RETURN v_delivery_status;
      ELSIF v_rtw_ct > 0 AND ( (v_bo_ct > 0 OR v_rtr_ct > 0) AND v_staged_ct = 0 AND v_pick_tkt_print_dt IS NOT NULL) THEN
         v_delivery_status := 'Picking';
         RETURN v_delivery_status;
      ELSIF v_rtw_ct > 0 AND (v_bo_ct = 0 AND v_rtr_ct = 0 AND v_staged_ct = 0 AND v_pick_tkt_print_dt IS NOT NULL) THEN --SN03172016: Fixed by Sriram on 17-Mar-2016
         v_delivery_status := 'Picking';
         RETURN v_delivery_status;
      ELSIF v_staged_ct > 0 AND ( (v_rtr_ct > 0 OR v_rtw_ct > 0 OR v_bo_ct > 0) AND v_pick_tkt_print_dt IS NOT NULL) THEN --SN03172016: Fixed by Sriram on 17-Mar-2016
         v_delivery_status := 'Picking';
         RETURN v_delivery_status;
      ELSIF v_rtw_ct = 0 AND v_bo_ct = 0 AND v_staged_ct > 0 AND p_status_code = 'OP' THEN
         IF p_planned_flag = 'Y' THEN
            v_delivery_status := 'Ready to Ship';
            RETURN v_delivery_status;
         ELSE
            SELECT COUNT (1)
              INTO v_audit_nc_ct
              FROM (  SELECT wdd.inventory_item_id,
                             SUM (requested_quantity) req_qty,
                             MAX (NVL ( (audit_qty), 0)) audit_qty
                        FROM wsh_delivery_details wdd,
                             wsh_delivery_assignments wda,
                              wsh_delivery_legs wdl  --kp
                               , wsh_trip_stops wts,--kp
                             (  SELECT trip_id,inventory_item_id, NVL (SUM (audit_quantity), 0) audit_qty
                                  FROM xxfox_audit_lines
                                 WHERE trip_id = p_trip_id --kp
                              GROUP BY trip_id, inventory_item_id) audit_lines
                       WHERE     wdd.source_code = 'OE'
                             AND wdd.delivery_detail_id = wda.delivery_detail_id
                             AND wda.delivery_id = wdl.delivery_id(+)  --kp
                             AND wdl.pick_up_stop_id = wts.stop_id(+)  --kp
                             AND wdd.inventory_item_id = audit_lines.inventory_item_id(+)
                             AND wts.trip_id = p_trip_id --kp
                             AND wdd.released_status = 'Y'
                    GROUP BY wdd.inventory_item_id)
             WHERE req_qty <> audit_qty;

            IF v_audit_nc_ct = 0 THEN
               v_delivery_status := 'Audited';
               RETURN v_delivery_status;
            ELSE
               v_delivery_status := 'Pulled';
               RETURN v_delivery_status;
            END IF;
         END IF;
      ELSIF v_shipped_ct > 0 THEN
         v_delivery_status := 'Shipped';
         RETURN v_delivery_status;
      ELSE
         v_delivery_status := 'Not Printed';
         RETURN v_delivery_status;
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         RETURN NULL;
   END get_trip_status;


   --
   FUNCTION get_rsv_det (p_delivery_id IN NUMBER,p_trip_id IN NUMBER, p_inv_item_id IN NUMBER, p_inv_org_id IN NUMBER)
      RETURN VARCHAR2
   AS
      v_query          VARCHAR2 (10000);
      v_rsv_det        VARCHAR2 (500);
      v_rsv_det2       VARCHAR2 (500);
      v_detailed_qty   NUMBER;
      v_rsv_qty        NUMBER:=0;
      v_rsv_qty_tot    NUMBER:=0;
      CURSOR C_del(trip NUMBER) IS
          SELECT DISTINCT wda.delivery_id
            FROM wsh_delivery_details wdd
               , wsh_delivery_assignments wda
               , wsh_delivery_legs wdl
               , wsh_trip_stops wts
            WHERE wdd.source_code = 'OE'
              AND wdd.delivery_detail_id = wda.delivery_detail_id
              AND wda.delivery_id = wdl.delivery_id(+)
              AND wdl.pick_up_stop_id = wts.stop_id(+)
              AND wdd.organization_id = p_inv_org_id
              AND wts.trip_id = trip
              ORDER BY 1;



   BEGIN


      IF p_trip_id IS NULL AND p_delivery_id IS NOT NULL THEN
       BEGIN
           v_query :=
            'SELECT SUM(mr.reservation_quantity)||''|''||MAX(CASE WHEN mr.staged_flag = ''Y'' THEN NULL ELSE mr.subinventory_code END)||''|''||MAX(CASE WHEN mr.staged_flag = ''Y'' THEN NULL ELSE mil.concatenated_segments END)||''|'''
         || CHR (10)
         || '  , SUM(mr.detailed_quantity)'
         || CHR (10)
         || 'FROM mtl_reservations mr'
         || CHR (10)
         || '  , mtl_item_locations_kfv mil'
         || CHR (10)
         || 'WHERE mr.locator_id = mil.inventory_location_id(+)'
         || CHR (10)
         || 'AND mr.organization_id = mil.organization_id(+)'
         || CHR (10)
         || 'AND mr.demand_source_line_id IN (SELECT wdd.source_line_id FROM wsh_delivery_details wdd, wsh_delivery_assignments wda'
         || CHR (10)
         || '                                 WHERE source_code = ''OE'' AND wdd.delivery_detail_id = wda.delivery_detail_id'
         || CHR (10)
         || '                                 AND wda.delivery_id = :p_delivery_id AND wdd.inventory_item_id = :p_inventory_item_id)'
         || CHR (10)
         || 'AND mr.organization_id = :p_organization_id'
         || CHR (10)
         || 'AND mr.demand_source_type_id IN (2, 8)'
         || CHR (10)
         || 'AND mr.supply_source_type_id = 13';

      EXECUTE IMMEDIATE v_query INTO v_rsv_det, v_detailed_qty USING p_delivery_id, p_inv_item_id, p_inv_org_id;

        IF v_rsv_det LIKE '%|||%' AND NVL (v_detailed_qty, 0) > 0 THEN
         v_query :=
               'SELECT MAX(mmtt.subinventory_code)||''|''||MAX(mil.concatenated_segments)||''|'''
            || CHR (10)
            || 'FROM mtl_material_transactions_temp mmtt'
            || CHR (10)
            || '  , mtl_item_locations_kfv mil'
            || CHR (10)
            || 'WHERE mmtt.locator_id = mil.inventory_location_id(+)'
            || CHR (10)
            || 'AND mmtt.organization_id = mil.organization_id(+)'
            || CHR (10)
            || 'AND mmtt.move_order_line_id IN (SELECT wdd.move_order_line_id FROM wsh_delivery_details wdd, wsh_delivery_assignments wda'
            || CHR (10)
            || '                                WHERE source_code = ''OE'' AND wdd.delivery_detail_id = wda.delivery_detail_id'
            || CHR (10)
            || '                                AND wda.delivery_id = :p_delivery_id AND wdd.inventory_item_id = :p_inventory_item_id)'
            || CHR (10)
            || 'AND mmtt.organization_id = :p_organization_id'
            || CHR (10)
            || 'AND mmtt.transaction_type_id IN (52, 53)';

         EXECUTE IMMEDIATE v_query INTO v_rsv_det2 USING p_delivery_id, p_inv_item_id, p_inv_org_id;

         v_rsv_det :=
               SUBSTR (v_rsv_det,
                       1,
                         INSTR (v_rsv_det,
                                '|',
                                1,
                                1)
                       - 1)
            || '|'
            || v_rsv_det2;
        END IF;
     END;
     --v_rsv_qty_tot :=
    RETURN v_rsv_det; --v_rsv_det;
  ELSIF p_trip_id IS NOT NULL THEN
    BEGIN
      v_rsv_qty := 0;
      FOR C_del_rec IN C_del(p_trip_id)
      LOOP
        BEGIN
          v_query :=
            'SELECT SUM(mr.reservation_quantity)||''|''||MAX(CASE WHEN mr.staged_flag = ''Y'' THEN NULL ELSE mr.subinventory_code END)||''|''||MAX(CASE WHEN mr.staged_flag = ''Y'' THEN NULL ELSE mil.concatenated_segments END)||''|'''
         || CHR (10)
         || '  , SUM(mr.detailed_quantity)'
         || CHR (10)
         || 'FROM mtl_reservations mr'
         || CHR (10)
         || '  , mtl_item_locations_kfv mil'
         || CHR (10)
         || 'WHERE mr.locator_id = mil.inventory_location_id(+)'
         || CHR (10)
         || 'AND mr.organization_id = mil.organization_id(+)'
         || CHR (10)
         || 'AND mr.demand_source_line_id IN (SELECT wdd.source_line_id FROM wsh_delivery_details wdd, wsh_delivery_assignments wda'
         || CHR (10)
         || '                                 WHERE source_code = ''OE'' AND wdd.delivery_detail_id = wda.delivery_detail_id'
         || CHR (10)
         || '                                 AND wda.delivery_id = '||C_del_rec.delivery_id||' AND wdd.inventory_item_id = :p_inventory_item_id)'
         || CHR (10)
         || 'AND mr.organization_id = :p_organization_id'
         || CHR (10)
         || 'AND mr.demand_source_type_id IN (2, 8)'
         || CHR (10)
         || 'AND mr.supply_source_type_id = 13';

         EXECUTE IMMEDIATE v_query INTO v_rsv_det, v_detailed_qty USING p_inv_item_id, p_inv_org_id;

         IF v_rsv_det LIKE '%|||%' AND NVL (v_detailed_qty, 0) > 0 THEN
            v_query :=
               'SELECT MAX(mmtt.subinventory_code)||''|''||MAX(mil.concatenated_segments)||''|'''
            || CHR (10)
            || 'FROM mtl_material_transactions_temp mmtt'
            || CHR (10)
            || '  , mtl_item_locations_kfv mil'
            || CHR (10)
            || 'WHERE mmtt.locator_id = mil.inventory_location_id(+)'
            || CHR (10)
            || 'AND mmtt.organization_id = mil.organization_id(+)'
            || CHR (10)
            || 'AND mmtt.move_order_line_id IN (SELECT wdd.move_order_line_id FROM wsh_delivery_details wdd, wsh_delivery_assignments wda'
            || CHR (10)
            || '                                WHERE source_code = ''OE'' AND wdd.delivery_detail_id = wda.delivery_detail_id'
            || CHR (10)
            || '                                AND wda.delivery_id = '
            ||C_del_rec.delivery_id
            ||' AND wdd.inventory_item_id = :p_inventory_item_id)'
            || CHR (10)
            || 'AND mmtt.organization_id = :p_organization_id'
            || CHR (10)
            || 'AND mmtt.transaction_type_id IN (52, 53)';

            EXECUTE IMMEDIATE v_query INTO v_rsv_det2 USING p_inv_item_id, p_inv_org_id;

           v_rsv_det :=
               SUBSTR (v_rsv_det,
                       1,
                         INSTR (v_rsv_det,
                                '|',
                                1,
                                1)
                       - 1)
            || '|'
            || v_rsv_det2;
         END IF;

        END;
       v_rsv_qty := SUBSTR(v_rsv_det, 1, INSTR(v_rsv_det, '|', 1, 1) -1);   --v_rsv_qty  v_rsv_det;
       v_rsv_qty_tot := v_rsv_qty_tot + v_rsv_qty ;
      END LOOP;
      RETURN v_rsv_qty_tot;

   END;
  END IF;



 EXCEPTION
      WHEN OTHERS THEN
         RETURN sqlerrm;
 END get_rsv_det;

   --
   PROCEDURE print_doc_set (p_trip_id           IN     NUMBER,
                            p_delivery_id       IN     NUMBER,
                            p_report_set_name   IN     VARCHAR2,
                            x_return_status        OUT VARCHAR2,
                            x_msg_data             OUT VARCHAR2)
   AS
      v_debug_loc          NUMBER;
      x_msg_count          NUMBER;
      l_msg                VARCHAR2 (4000);
      x_trip_id            NUMBER;
      x_trip_name          VARCHAR2 (30);
      v_pack_slip_no       VARCHAR2 (50);
      v_pickup_loc_id      NUMBER;
      v_inv_org_id         NUMBER;
      v_set_of_books_id    NUMBER;
      x_document_number    NUMBER;
      v_bol_no             VARCHAR2 (50);
      v_delivery_leg_id    NUMBER;
      v_ship_method_code   VARCHAR2 (30);
      v_printer            VARCHAR2 (250) := NVL (fnd_profile.VALUE ('PRINTER'), 'noprint');
   BEGIN
      v_debug_loc := 10;

      IF p_delivery_id IS NOT NULL THEN
         v_debug_loc := 20;

         SELECT wdi.sequence_number, wnd.initial_pickup_location_id, wnd.organization_id
           INTO v_pack_slip_no, v_pickup_loc_id, v_inv_org_id
           FROM wsh_new_deliveries wnd, wsh_document_instances wdi
          WHERE     wnd.delivery_id = wdi.entity_id(+)
                AND wdi.entity_name(+) = 'WSH_NEW_DELIVERIES'
                AND wdi.document_type(+) = 'PACK_TYPE'
                AND wnd.delivery_id = p_delivery_id;

         IF v_pack_slip_no IS NULL THEN
            v_debug_loc := 30;

            -- derive set_of_books_id
             SELECT hoi.org_information1
              INTO v_set_of_books_id
              FROM hr_organization_information hoi
             WHERE hoi.organization_id = v_inv_org_id AND hoi.org_information_context = 'Accounting Information';

             v_debug_loc := 40;
             wsh_document_pvt.create_document (p_api_version          => 1.0,
                                              p_init_msg_list        => 'F',
                                              p_commit               => NULL,
                                              p_validation_level     => NULL,
                                              x_return_status        => x_return_status,
                                              x_msg_count            => x_msg_count,
                                              x_msg_data             => x_msg_data,
                                              p_entity_name          => 'WSH_NEW_DELIVERIES',
                                              p_entity_id            => p_delivery_id,
                                              p_application_id       => 665,
                                              p_location_id          => v_pickup_loc_id,
                                              p_document_type        => 'PACK_TYPE',
                                              p_document_sub_type    => 'SALES_ORDER',
                                              p_ledger_id            => v_set_of_books_id,
                                              p_consolidate_option   => 'BOTH',
                                              x_document_number      => x_document_number);

             IF x_return_status <> fnd_api.g_ret_sts_success THEN
               v_debug_loc := 50;
               x_msg_data := '';

               FOR i IN 1 .. x_msg_count LOOP
                  l_msg := apps.fnd_msg_pub.get (p_msg_index => i, p_encoded => apps.fnd_api.g_false);
                  x_msg_data := x_msg_data || 'Msg ' || i || ': ' || l_msg || CHR (10);
               END LOOP;

               x_msg_data :=
                     'Error generating pack slip no for Delivery: '
                  || p_delivery_id
                  || ' '
                  || SUBSTR (x_msg_data, 1, 100);
               ROLLBACK;
               RETURN;
             END IF;
         END IF;

         --
         v_debug_loc := 60;

         SELECT wdi.sequence_number,
                wnd.initial_pickup_location_id,
                wnd.organization_id,
                wdl.delivery_leg_id,
                wnd.ship_method_code
           INTO v_bol_no,
                v_pickup_loc_id,
                v_inv_org_id,
                v_delivery_leg_id,
                v_ship_method_code
           FROM wsh_new_deliveries wnd, wsh_delivery_legs wdl, wsh_document_instances wdi
          WHERE     wnd.delivery_id = wdl.delivery_id
                AND wdl.delivery_leg_id = wdi.entity_id(+)
                AND wdi.entity_name(+) = 'WSH_DELIVERY_LEGS'
                AND wdi.document_type(+) = 'BOL'
                AND wnd.delivery_id = p_delivery_id;

         IF v_bol_no IS NULL THEN
            v_debug_loc := 70;

            -- derive set_of_books_id
            SELECT hoi.org_information1
              INTO v_set_of_books_id
              FROM hr_organization_information hoi
             WHERE hoi.organization_id = v_inv_org_id AND hoi.org_information_context = 'Accounting Information';

            v_debug_loc := 80;
            wsh_document_pvt.create_document (p_api_version          => 1.0,
                                              p_init_msg_list        => 'F',
                                              p_commit               => NULL,
                                              p_validation_level     => NULL,
                                              x_return_status        => x_return_status,
                                              x_msg_count            => x_msg_count,
                                              x_msg_data             => x_msg_data,
                                              p_entity_name          => 'WSH_DELIVERY_LEGS',
                                              p_entity_id            => v_delivery_leg_id,
                                              p_application_id       => 665,
                                              p_location_id          => v_pickup_loc_id,
                                              p_document_type        => 'BOL',
                                              p_document_sub_type    => v_ship_method_code,
                                              p_ledger_id            => v_set_of_books_id,
                                              p_consolidate_option   => 'BOTH',
                                              x_document_number      => x_document_number);

            IF x_return_status <> fnd_api.g_ret_sts_success THEN
               v_debug_loc := 90;
               x_msg_data := '';

               FOR i IN 1 .. x_msg_count LOOP
                  l_msg := apps.fnd_msg_pub.get (p_msg_index => i, p_encoded => apps.fnd_api.g_false);
                  x_msg_data := x_msg_data || 'Msg ' || i || ': ' || l_msg || CHR (10);
               END LOOP;

               x_msg_data :=
                  'Error generating bol for Delivery: ' || p_delivery_id || ' ' || SUBSTR (x_msg_data, 1, 100);
               ROLLBACK;
               RETURN;
            END IF;
         END IF;
      END IF;

      /*IF p_trip_id IS NOT NULL THEN
        wsh_trips_pub.trip_action(p_api_version_number  => 1.0
                                 ,p_init_msg_list       => fnd_api.g_true
                                 ,x_return_status       => x_return_status
                                 ,x_msg_count           => x_msg_count
                                 ,x_msg_data            => x_msg_data
                                 ,p_action_code         => 'PRINT-DOC-SETS'
                                 ,p_trip_id             => p_trip_id
                                 ,p_report_set_name     => p_report_set_name);
        --
        IF x_return_status <> fnd_api.g_ret_sts_success THEN
          x_msg_data := '';
          FOR i IN 1 .. x_msg_count
          LOOP
            l_msg := apps.fnd_msg_pub.get (p_msg_index => i, p_encoded => apps.fnd_api.g_false);
            x_msg_data := x_msg_data || 'Msg ' || i || ': ' || l_msg || CHR(10);
          END LOOP;
          x_msg_data := 'Error printing document set for Trip: ' || p_trip_id || ' ' || x_msg_data;
          ROLLBACK;
          RETURN;
        ELSE
          COMMIT;
        END IF; --x_return_status = 'E'
      ELSE*/
      IF p_delivery_id IS NOT NULL THEN
         v_debug_loc := 100;

         IF fnd_request.set_print_options (printer   => v_printer,
                                           copies    => CASE WHEN v_printer = 'noprint' THEN 0 ELSE 1 END) THEN
            wsh_deliveries_pub.delivery_action (p_api_version_number   => 1.0,
                                                p_init_msg_list        => fnd_api.g_true,
                                                x_return_status        => x_return_status,
                                                x_msg_count            => x_msg_count,
                                                x_msg_data             => x_msg_data,
                                                p_action_code          => 'PRINT-DOC-SETS',
                                                p_delivery_id          => p_delivery_id,
                                                p_asg_trip_id          => p_trip_id,  --kp
                                                p_sc_report_set_name   => p_report_set_name,
                                                x_trip_id              => x_trip_id,
                                                x_trip_name            => x_trip_name);

            --
            IF x_return_status <> fnd_api.g_ret_sts_success THEN
               IF x_msg_data > 1 THEN
                  x_msg_data := '';

                  FOR i IN 1 .. x_msg_count LOOP
                     l_msg := apps.fnd_msg_pub.get (p_msg_index => i, p_encoded => apps.fnd_api.g_false);
                     x_msg_data := x_msg_data || 'Msg ' || i || ': ' || l_msg || CHR (10);
                  END LOOP;
               END IF;

               x_msg_data := 'Error printing document set for Delivery: ' || p_delivery_id || ' ' || x_msg_data;
               ROLLBACK;
               RETURN;
            ELSE
               COMMIT;
            END IF;                                                                            --x_return_status = 'E'
         END IF;                           --IF fnd_request.set_print_options (printer => v_printer, copies => 1) THEN
      END IF;                                                                      --IF p_delivery_id IS NOT NULL THEN
   /*END IF; --IF p_trip_id IS NOT NULL THEN*/
   EXCEPTION
      WHEN OTHERS THEN
         x_return_status := 'E';
         x_msg_data := 'Error printing document set: ' || SQLERRM || ' error loc: ' || v_debug_loc;
   END print_doc_set;

   --
   PROCEDURE validate_edi_trip (p_trip_id IN NUMBER, x_return_status IN OUT VARCHAR2, x_msg_data IN OUT VARCHAR2)
   IS
      CURSOR c_dly (p_trip_id IN NUMBER)
      IS
         SELECT wnd.delivery_id, wnd.ship_method_code
           FROM wsh_trip_stops wts, wsh_delivery_legs wdl, wsh_new_deliveries wnd
          WHERE wnd.delivery_id = wdl.delivery_id AND wdl.pick_up_stop_id = wts.stop_id AND wts.trip_id = p_trip_id;

      l_ece_tp_location_code   VARCHAR2 (40);
      l_edi_delivery           VARCHAR2 (1) := 'N';
      l_dly_err                VARCHAR2 (4000);
      l_gross_weight           NUMBER;
      l_net_weight             NUMBER;
      l_ship_method_code       VARCHAR2 (30);
      l_scac_code              VARCHAR2 (4);
      l_edi_trip               VARCHAR2 (1) := 'N';
      l_vehicle_number         VARCHAR2 (35);
      v_pwt                    NUMBER := 0;
      v_pgw                    NUMBER := 0;

l_v_scac varchar2(1) := 'N';
l_v_vn varchar2(1) := 'N';
l_v_weights varchar2(1) := 'N';

   BEGIN
      x_return_status := 'S';
      x_msg_data := NULL;

begin
select distinct flv.attribute1, flv.attribute2, flv.attribute3, scac_code, wt.vehicle_number
into l_v_scac, l_v_vn, l_v_weights, l_scac_code, l_vehicle_number 
from fnd_lookup_values flv, wsh_trip_stops wts, wsh_delivery_legs wdl, wsh_new_deliveries wnd, hz_cust_accounts hca
, wsh_trips wt
, wsh_carriers wc
where 1=1
and wnd.delivery_id = wdl.delivery_id 
AND wdl.pick_up_stop_id = wts.stop_id 
AND wts.trip_id = wt.trip_id
and wt.trip_id = p_trip_id
and wnd.customer_id = hca.cust_account_id
and hca.account_number = flv.lookup_code
and flv.lookup_type = 'XXFOX_SHIP_CONF_VALIDATION'
and wc.carrier_id = wt.carrier_id;
EXCEPTION
when others then
l_v_scac := 'N';
l_v_vn :='N';
l_v_weights := 'N';
end;



if(l_v_weights = 'Y') then

      BEGIN
         SELECT NVL(SUM(PALLET_NET_WT),0),
                NVL(SUM(PALLET_GROSS_WT),0)
           INTO v_pwt,
                v_pgw
          FROM XXFOX_AUDIT_LINES
         WHERE TRIP_ID = p_trip_id;
      EXCEPTION
         WHEN OTHERS THEN
            v_pwt := 0;
            v_pgw := 0;
      END;

      IF (v_pwt = 0) AND (v_pgw = 0) THEN
          x_return_status := 'E';
          l_dly_err := l_dly_err || 'Error: Trip weight is Null for Trip#' || p_trip_id || '. ';
      END IF;
end if;


if(l_v_scac = 'Y') then

               IF l_scac_code IS NULL THEN
                  l_dly_err := l_dly_err || 'SCAC Code doesnot exists for Trip: ' || P_trip_id || '. ';
                  x_return_status := 'E';
               END IF;


end if;

if(l_v_vn = 'Y') then
         IF l_vehicle_number IS NULL THEN
            l_dly_err := l_dly_err || 'Vehical number does not exists for trip :' || p_trip_id || '. ';
            x_return_status := 'E';
         END IF;
end if;

/*
      FOR r_dly IN c_dly (p_trip_id) LOOP
      
      
         l_edi_delivery := 'N';
         l_ece_tp_location_code := NULL;
         l_gross_weight := NULL;
         l_net_weight := NULL;
         l_ship_method_code := NULL;
         l_scac_code := NULL;

         BEGIN
            SELECT hcas.ece_tp_location_code,
                   gross_weight,
                   net_weight,
                   ship_method_code
              INTO l_ece_tp_location_code,
                   l_gross_weight,
                   l_net_weight,
                   l_ship_method_code
              FROM wsh_new_deliveries wnd,
                   hz_party_sites hps,
                   hz_cust_acct_sites_all hcas,
                   ece_tp_headers eth,
                   ece_tp_details etd
             WHERE     wnd.delivery_id = r_dly.delivery_id
                   AND hps.location_id = wnd.ultimate_dropoff_location_id
                   AND hps.party_site_id = hcas.party_site_id
                   AND hcas.tp_header_id = eth.tp_header_id
                   AND etd.tp_header_id = eth.tp_header_id
                   AND etd.document_type = 'DSNO';

            IF l_ece_tp_location_code IS NOT NULL THEN
               l_edi_delivery := 'Y';
               l_edi_trip := 'Y';
            END IF;
         EXCEPTION
            WHEN OTHERS THEN
               l_edi_delivery := 'N';
         END;

         IF l_edi_delivery = 'Y' THEN
      
      
            -- Validating carrier scac_code
            BEGIN
               SELECT scac_code
                 INTO l_scac_code
                 FROM wsh_carriers wc, wsh_carrier_services wcs
                WHERE wc.carrier_id = wcs.carrier_id AND wcs.ship_method_code = l_ship_method_code;

               IF l_scac_code IS NULL THEN
                  l_dly_err := l_dly_err || 'SCAC Code doesnot exists for delivery: ' || r_dly.delivery_id || '. ';
                  x_return_status := 'E';
               END IF;


            EXCEPTION
               WHEN OTHERS THEN
                  l_scac_code := NULL;
            END;
         END IF;
      END LOOP;

      IF l_edi_trip = 'Y' THEN
         -- Validating trip vehicle number
         BEGIN
            SELECT vehicle_number
              INTO l_vehicle_number
              FROM wsh_trips
             WHERE trip_id = p_trip_id;
         EXCEPTION
            WHEN OTHERS THEN
               l_vehicle_number := NULL;
         END;

         IF l_vehicle_number IS NULL THEN
            l_dly_err := l_dly_err || 'Vehical number does not exists for trip :' || p_trip_id || '. ';
            x_return_status := 'E';
         END IF;
      END IF;
*/

      IF x_return_status = 'E' THEN
         x_msg_data := l_dly_err;
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         x_return_status := 'E';
         x_msg_data := 'Error in procedure validate_edi_trip :' || SQLERRM;
   END validate_edi_trip;

   --
   PROCEDURE ship_confirm (p_trip_id IN VARCHAR2, x_return_status OUT VARCHAR2, x_msg_data OUT VARCHAR2)
   AS
      x_action_prms    wsh_trips_pub.action_param_rectype;
      x_msg_count      NUMBER;
      l_msg            VARCHAR2 (4000);
      x_request_id     NUMBER;

      e_err_edi_trip   EXCEPTION;
   BEGIN
      validate_edi_trip (p_trip_id, x_return_status, x_msg_data);

      IF x_return_status = 'E' THEN
         RAISE e_err_edi_trip;
      END IF;

      x_action_prms.action_code := 'TRIP-CONFIRM';
      wsh_trips_pub.trip_action (p_api_version_number   => 1.0,
                                 p_init_msg_list        => fnd_api.g_true,
                                 p_commit               => fnd_api.g_false,
                                 x_return_status        => x_return_status,
                                 x_msg_count            => x_msg_count,
                                 x_msg_data             => x_msg_data,
                                 p_action_param_rec     => x_action_prms,
                                 p_trip_id              => p_trip_id,
                                 p_trip_name            => TO_CHAR (p_trip_id));
      COMMIT;


 /*   Calling from the form
     IF (x_return_status = 'S') THEN
         BEGIN
         x_request_id := FND_REQUEST.submit_request(
                                           application   => 'XXFOX',
                                           program       => 'XXFOX_WSH_EDI_ASN_OUTBOUND',
                                           description   => NULL,
                                           start_time    => NULL,
                                           sub_request   => FALSE,
                                           argument1     => p_trip_id
                                                    );
          print_debug ('Ship Confirmed and EDI Program Submitted, Request ID -  ' || x_request_id);
         EXCEPTION
            WHEN OTHERS THEN
                 print_debug ('EDI Program Submitted submission error..  ' || SUBSTR(SQLERRM,1,100));
                 x_request_id := 0;
         END;

      END IF; */


      --
      IF x_return_status <> fnd_api.g_ret_sts_success THEN
         x_msg_data := '';

         FOR i IN 1 .. x_msg_count LOOP
            l_msg := apps.fnd_msg_pub.get (p_msg_index => i, p_encoded => apps.fnd_api.g_false);
            x_msg_data := x_msg_data || 'Msg ' || i || ': ' || l_msg || CHR (10);
         END LOOP;

         RETURN;
      END IF;                                                                                  --x_return_status = 'E'
   EXCEPTION
      WHEN e_err_edi_trip THEN
         x_msg_data := 'Error validating edi trip: ' || x_msg_data;
      WHEN OTHERS THEN
         x_return_status := 'E';
         x_msg_data := 'Error ship confirming trip: ' || p_trip_id || ' ' || SQLERRM;
   END ship_confirm;

   --
   PROCEDURE print_pack_slip (p_org_id              IN     NUMBER,
                              p_delivery_id         IN     NUMBER,
                              p_print_cust_item     IN     VARCHAR2 DEFAULT 'N',
                              p_item_display        IN     VARCHAR2 DEFAULT 'D',
                              p_print_mode          IN     VARCHAR2 DEFAULT 'DRAFT',
                              p_sort                IN     VARCHAR2 DEFAULT 'INV',
                              p_display_unshipped   IN     VARCHAR2 DEFAULT 'Y',
                              p_printer             IN     VARCHAR2,
                              x_request_id             OUT NUMBER,
                              x_msg_count              OUT NUMBER,
                              x_return_status          OUT VARCHAR2,
                              x_return_msg             OUT VARCHAR2)
   AS
      xml_layout             BOOLEAN;
      l_pickup_location_id   NUMBER;
      l_org_id               NUMBER;
      l_set_of_books_id      NUMBER;
      l_document_number      VARCHAR2 (100);
   BEGIN
      print_debug ('print_pack_slip p_delivery_id = ' || p_delivery_id);

      -- derive l_pickup_location_id
      SELECT dl.initial_pickup_location_id, dl.organization_id
        INTO l_pickup_location_id, l_org_id
        FROM wsh_new_deliveries dl
       WHERE dl.delivery_id = p_delivery_id;

      print_debug ('print_pack_slip l_pickup_location_id = ' || l_pickup_location_id);
      print_debug ('print_pack_slip l_org_id = ' || l_org_id);

      -- derive set_of_books_id
      SELECT hoi.org_information1
        INTO l_set_of_books_id
        FROM hr_organization_information hoi
       WHERE hoi.organization_id = l_org_id AND hoi.org_information_context = 'Accounting Information';

      --
      print_debug ('print_pack_slip l_set_of_books_id = ' || l_set_of_books_id);
      -- first call wsh_document_pvt.create_document
      wsh_document_pvt.create_document (p_api_version          => 1.0,
                                        p_init_msg_list        => 'F',
                                        p_commit               => NULL,
                                        p_validation_level     => NULL,
                                        x_return_status        => x_return_status,
                                        x_msg_count            => x_msg_count,
                                        x_msg_data             => x_return_msg,
                                        p_entity_name          => 'WSH_NEW_DELIVERIES',
                                        p_entity_id            => p_delivery_id,
                                        p_application_id       => 665,
                                        p_location_id          => l_pickup_location_id,
                                        p_document_type        => 'PACK_TYPE',
                                        p_document_sub_type    => 'SALES_ORDER',
                                        p_ledger_id            => l_set_of_books_id,
                                        p_consolidate_option   => 'BOTH',
                                        x_document_number      => l_document_number);
      print_debug ('print_pack_slip wsh_document_pvt.create_document x_return_status = ' || x_return_status);
      print_debug ('print_pack_slip wsh_document_pvt.create_document x_msg_count = ' || x_msg_count);
      print_debug ('print_pack_slip wsh_document_pvt.create_document x_return_msg = ' || x_return_msg);
      print_debug ('print_pack_slip l_document_number = ' || l_document_number);

      IF (x_return_status = 'S') THEN
         IF fnd_request.set_print_options (printer => p_printer, copies => 1) THEN
            xml_layout :=
               fnd_request.add_layout ('WSH',
                                       'WSHRDPAK',
                                       'en',
                                       'US',
                                       'PDF');
            x_request_id :=
               fnd_request.submit_request (application   => 'WSH',
                                           program       => 'WSHRDPAK',
                                           description   => NULL,
                                           start_time    => NULL,
                                           sub_request   => FALSE,
                                           argument1     => p_org_id,
                                           argument2     => p_delivery_id,
                                           argument3     => NVL (p_print_cust_item, 'N'),
                                           argument4     => NVL (p_item_display, 'D'),
                                           argument5     => NVL (p_print_mode, 'DRAFT'),
                                           argument6     => NVL (p_sort, 'INV'),
                                           argument7     => NULL,
                                           argument8     => NULL,
                                           argument9     => NULL,
                                           argument10    => fnd_profile.VALUE ('REPORT_QUANTITY_PRECISION'),
                                           argument11    => NVL (p_display_unshipped, 'Y'));
            print_debug ('print_pack_slip request_id = ' || x_request_id);
            COMMIT;
            x_return_status := 'S';
         ELSE
            print_debug ('print_pack_slip error setting printer options');
            x_request_id := 0;
            x_return_status := 'E';
            x_return_msg := 'Error setting printer options.';
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         print_debug ('print_pack_slip error ' || SQLERRM);
         x_return_status := 'E';
         x_return_msg := SQLERRM;
         x_msg_count := 1;
         x_request_id := 0;
   END print_pack_slip;

   --
   PROCEDURE print_bol (p_org_id          IN     NUMBER,
                        p_delivery_id     IN     NUMBER,
                        p_trip_id         IN     VARCHAR2,
                        p_printer         IN     VARCHAR2,
                        x_request_id         OUT NUMBER,
                        x_msg_count          OUT NUMBER,
                        x_return_status      OUT VARCHAR2,
                        x_return_msg         OUT VARCHAR2)
   AS
      xml_layout             BOOLEAN;
      v_delivery_leg_id      NUMBER;
      v_ship_method_code     VARCHAR2 (30);
      v_pickup_location_id   NUMBER;
      l_document_number      VARCHAR2 (100);
      v_org_id               NUMBER;
      v_set_of_books_id      NUMBER;
   BEGIN
      print_debug ('print_bol p_delivery_id = ' || p_delivery_id);

      BEGIN
         SELECT wdl.delivery_leg_id,
                wnd.ship_method_code,
                wnd.initial_pickup_location_id,
                wnd.organization_id
           INTO v_delivery_leg_id,
                v_ship_method_code,
                v_pickup_location_id,
                v_org_id
           FROM wsh_delivery_legs wdl, wsh_new_deliveries wnd
          WHERE wdl.delivery_id = wnd.delivery_id AND wnd.delivery_id = p_delivery_id;
      EXCEPTION
         WHEN OTHERS THEN
            print_debug ('Error deriving delivey_leg_id, delivery: ' || p_delivery_id || ' ' || SQLERRM);
            x_return_status := 'E';
            x_return_msg := 'Error deriving delivey_leg_id, delivery: ' || p_delivery_id || ' ' || SQLERRM;
            x_msg_count := 1;
            x_request_id := 0;
            RETURN;
      END;

      -- derive set_of_books_id
      SELECT hoi.org_information1
        INTO v_set_of_books_id
        FROM hr_organization_information hoi
       WHERE hoi.organization_id = v_org_id AND hoi.org_information_context = 'Accounting Information';

      --
      print_debug ('print_pack_slip l_set_of_books_id = ' || v_set_of_books_id);
      -- first call wsh_document_pvt.create_document
      wsh_document_pvt.create_document (p_api_version          => 1.0,
                                        p_init_msg_list        => 'F',
                                        p_commit               => NULL,
                                        p_validation_level     => NULL,
                                        x_return_status        => x_return_status,
                                        x_msg_count            => x_msg_count,
                                        x_msg_data             => x_return_msg,
                                        p_entity_name          => 'WSH_DELIVERY_LEGS',
                                        p_entity_id            => v_delivery_leg_id,
                                        p_application_id       => 665,
                                        p_location_id          => v_pickup_location_id,
                                        p_document_type        => 'BOL',
                                        p_document_sub_type    => v_ship_method_code,
                                        p_ledger_id            => v_set_of_books_id,
                                        p_consolidate_option   => 'BOTH',
                                        x_document_number      => l_document_number);
      print_debug ('print_bol wsh_document_pvt.create_document x_return_status = ' || x_return_status);
      print_debug ('print_bol wsh_document_pvt.create_document x_msg_count = ' || x_msg_count);
      print_debug ('print_bol wsh_document_pvt.create_document x_return_msg = ' || x_return_msg);
      print_debug ('print_bol l_document_number = ' || l_document_number);

      IF (x_return_status = 'S') THEN
         IF fnd_request.set_print_options (printer => p_printer, copies => 1) THEN
            xml_layout :=
               fnd_request.add_layout ('WSH',
                                       'XXPWC_BOL',
                                       'en',
                                       'US',
                                       'PDF');
            x_request_id :=
               fnd_request.submit_request (application   => 'WSH',
                                           program       => 'WSHRDBOL',
                                           description   => NULL,
                                           start_time    => NULL,
                                           sub_request   => FALSE,
                                           argument1     => p_org_id,
                                           argument2     => NULL,
                                           argument3     => NULL,
                                           argument4     => NULL,
                                           argument5     => p_delivery_id,
                                           argument6     => p_trip_id,
                                           argument7     => NULL,
                                           argument8     => 'D',
                                           argument9     => 'MSTK');
            print_debug ('print_bol x_request_id = ' || x_request_id);
            COMMIT;
            x_return_status := 'S';
         ELSE
            print_debug ('print_BOL error setting printer options');
            x_request_id := 0;
            x_return_status := 'E';
            x_return_msg := 'Error setting printer options.';
         END IF;
      END IF;                                                                        --IF (x_return_status = 'S') THEN
   EXCEPTION
      WHEN OTHERS THEN
         print_debug ('print_bol error ' || SQLERRM);
         x_return_status := 'E';
         x_return_msg := SQLERRM;
         x_msg_count := 1;
         x_request_id := 0;
   END print_bol;

   --
   PROCEDURE pick_release (x_return_status          OUT VARCHAR2,
                           x_msg_data               OUT VARCHAR2,
                           p_delivery_id         IN     NUMBER,
                           p_trip_id             IN     NUMBER,  -- KP
                           p_stage_locator       IN     VARCHAR2,
                           p_printer_name        IN     VARCHAR2,
                           p_organization_id     IN     NUMBER,
                           p_exclude_fg            IN   VARCHAR2,
                           x_request_id             OUT NUMBER,
                           x_part_label_req_id      OUT NUMBER)
   AS
      v_rule_name             VARCHAR2 (20);
      v_rule_id               NUMBER;
      p_pick_rec              wsh_picking_batches_pub.batch_info_rec;
      x_batch_id              NUMBER;
      x_msg_count             NUMBER;
      x_msg_details           VARCHAR2 (32500);
      x_msg_summary           VARCHAR2 (32500);

      v_where_tab             xxfox_label_pkg.where_tab;
      v_label_return_status   VARCHAR2 (1);
      v_label_msg_data        VARCHAR2 (32500);
      v_label_req_id          NUMBER;

    CURSOR c_delids(p_trip NUMBER)  ---------- KP
      IS
      SELECT DISTINCT wda.delivery_id
        FROM wsh_delivery_details wdd
           , wsh_delivery_assignments wda
           , wsh_delivery_legs wdl
           , wsh_trip_stops wts
       WHERE wdd.source_code = 'OE'
         AND wdd.delivery_detail_id = wda.delivery_detail_id
         AND wda.delivery_id = wdl.delivery_id(+)
         AND wdl.pick_up_stop_id = wts.stop_id(+)
         --AND wdd.organization_id = :parameter.organization_id
         AND wts.trip_id = p_trip
         AND wda.delivery_id IS NOT NULL
       ORDER BY 1;




   BEGIN
      x_return_status := fnd_api.g_ret_sts_success;
      apps.fnd_file.put_line (fnd_file.LOG,
                              '********************Pick Release Submission Starts******************** ');

      BEGIN
         SELECT wpr.name, wpr.picking_rule_id
           INTO v_rule_name, v_rule_id
           FROM wsh_picking_rules wpr, mtl_parameters mp
          WHERE     wpr.organization_id = mp.organization_id
                AND wpr.name = mp.organization_code
                AND mp.organization_id = p_organization_id;
      EXCEPTION
         WHEN OTHERS THEN
            x_return_status := 'E';
            x_msg_data := 'Error deriving default picking rule: ' || SQLERRM;
            RETURN;
      END;

      apps.fnd_file.put_line (fnd_file.LOG,'Pick rule name and ID : '||v_rule_name||' -  '||v_rule_id);
  ------------------------------KP
      IF p_trip_id IS NOT NULL THEN

          p_pick_rec.trip_id     := p_trip_id;
          apps.fnd_file.put_line (fnd_file.LOG,'Trip id is not null and taken trip id'||p_pick_rec.trip_id);
      ELSE
          p_pick_rec.delivery_id := p_delivery_id;
          apps.fnd_file.put_line (fnd_file.LOG,'Trip id null and taken delivery id'||p_pick_rec.delivery_id);
      END IF;
 -------------------------------KP



      p_pick_rec.backorders_only_flag := 'I';

      IF p_stage_locator IS NOT NULL THEN
         BEGIN
            SELECT subinventory_code, inventory_location_id
              INTO p_pick_rec.default_stage_subinventory, p_pick_rec.default_stage_locator_id
              FROM mtl_item_locations_kfv
             WHERE concatenated_segments = p_stage_locator AND organization_id = p_organization_id;
         EXCEPTION
            WHEN OTHERS THEN
               x_return_status := 'E';
               x_msg_data := 'Error deriving default stage locator: ' || SQLERRM;
         END;
      END IF;

      apps.fnd_file.put_line (fnd_file.LOG,'Sub inventory code:'||p_pick_rec.default_stage_subinventory|| ' - '||'inventory_location_id:'||p_pick_rec.default_stage_locator_id);
      apps.fnd_file.put_line (fnd_file.LOG,'Calling API wsh_picking_batches_pub.create_batch');
      wsh_picking_batches_pub.create_batch (p_api_version     => '1.0',
                                            p_init_msg_list   => fnd_api.g_true,
                                            p_commit          => fnd_api.g_false,
                                            x_return_status   => x_return_status,
                                            x_msg_count       => x_msg_count,
                                            x_msg_data        => x_msg_data,
                                            p_rule_id         => v_rule_id,
                                            p_rule_name       => v_rule_name,
                                            p_batch_rec       => p_pick_rec,
                                            p_batch_prefix    => 'A',
                                            x_batch_id        => x_batch_id);

      IF x_return_status = 'S' THEN
         apps.fnd_file.put_line (
            fnd_file.LOG,
            'Delivery: ' || p_delivery_id || ' create picking batch complete, batch_id: ' || x_batch_id);

         apps.fnd_file.put_line (fnd_file.LOG,'Calling API wsh_picking_batches_pub.release_batch');
         wsh_picking_batches_pub.release_batch (p_api_version     => '1.0',
                                                p_init_msg_list   => fnd_api.g_true,
                                                p_commit          => fnd_api.g_false,
                                                x_return_status   => x_return_status,
                                                x_msg_count       => x_msg_count,
                                                x_msg_data        => x_msg_data,
                                                p_batch_id        => x_batch_id,
                                                p_release_mode    => 'CONCURRENT',
                                                x_request_id      => x_request_id);
         apps.fnd_file.put_line (fnd_file.LOG,'Completed Calling API wsh_picking_batches_pub.release_batch'||x_request_id);
         IF x_return_status <> fnd_api.g_ret_sts_success THEN
            wsh_util_core.get_messages ('Y',
                                        x_msg_summary,
                                        x_msg_details,
                                        x_msg_count);

            IF x_msg_count > 1 THEN
               x_msg_data := SUBSTR (x_msg_details, 1, 250);
            ELSE
               x_msg_data := SUBSTR (x_msg_details, 1, 250);
            END IF;


            x_return_status := 'S';
           else
                    if(p_trip_id is not null) then
                    UPDATE WSH_TRIPS
                           SET ATTRIBUTE9 = p_stage_locator
                          WHERE trip_id = p_trip_id;
                   end if;
         END IF;                                                                         --release_batch return_status

         --added by Mangesh for part label start ---

         IF (p_trip_id IS NOT NULL) THEN          -----------------KP

            FOR c_trip_deliv IN c_delids(p_trip_id)
            LOOP
               apps.fnd_file.put_line (fnd_file.LOG,'Calling API FOX Packing Workbench Part Label for delivery id'||c_trip_deliv.delivery_id);
               v_label_req_id := fnd_request.submit_request ('XXFOX',
                                                             'XXFOXPICKPARTLBL',
                                                              NULL,
                                                              NULL,
                                                              NULL,                                                                  --TRUE,
                                                              p_printer_name,
                                                              1,
                                                              p_organization_id,
                                                              c_trip_deliv.delivery_id,
                                                              x_request_id,
                                                              p_exclude_fg);

                x_part_label_req_id := v_label_req_id;
                --added by Mangesh for part label end ---
                apps.fnd_file.put_line (fnd_file.LOG, 'FOX Packing Workbench Part Label - Program submitted for Delivery: ' || c_trip_deliv.delivery_id||' and Request id is:'||x_request_id);
             END LOOP;
         ELSIF (p_trip_id IS NULL) AND (p_delivery_id IS NOT NULL) THEN
               v_label_req_id := fnd_request.submit_request ('XXFOX',
                                                             'XXFOXPICKPARTLBL',
                                                              NULL,
                                                              NULL,
                                                              NULL,                                                                  --TRUE,
                                                              p_printer_name,
                                                              1,
                                                              p_organization_id,
                                                              p_delivery_id,
                                                              x_request_id,
                                                              p_exclude_fg);
                x_part_label_req_id := v_label_req_id;
                --added by Mangesh for part label end ---
                apps.fnd_file.put_line (fnd_file.LOG, 'FOX Packing Workbench Part Label - Program submitted for Delivery: ' || p_delivery_id);
          END IF; ---------------------KP
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         x_return_status := fnd_api.g_ret_sts_error;
         x_msg_data := 'Error in pick release: ' || SQLERRM;
         apps.fnd_file.put_line (fnd_file.LOG, x_msg_data);
   END pick_release;

   --
   PROCEDURE update_delivery_api (p_delivery_id     IN     NUMBER,
                                  x_return_status   IN OUT VARCHAR2,
                                  x_msg_data        IN OUT VARCHAR2)
   IS
      l_del_rec               wsh_deliveries_pub.delivery_pub_rec_type;
      l_delivery_id           NUMBER;
      l_delivery_number       VARCHAR2 (30);
      l_delivery_id_out       NUMBER;
      l_delivery_number_out   VARCHAR2 (30);
      l_tot_pallet_gross_wt   NUMBER;
      l_tot_pallet_net_wt     NUMBER;
      l_return_status         VARCHAR2 (1);
      l_msg_count             NUMBER;
      l_msg_data              VARCHAR2 (2000);
   BEGIN
      --     DBMS_output.put_line('Pos 1:');
      x_return_status := 'S';

      BEGIN
         SELECT SUM (pallet_gross_wt), SUM (pallet_net_wt)
           INTO l_tot_pallet_gross_wt, l_tot_pallet_net_wt
           FROM (  SELECT MAX (NVL (pallet_gross_wt, 0)) pallet_gross_wt,
                          MAX (NVL (pallet_net_wt, 0)) pallet_net_wt,
                          pallet_number
                     FROM xxfox_audit_lines
                    WHERE delivery_id = p_delivery_id
                 GROUP BY pallet_number);

         l_del_rec.delivery_id := p_delivery_id;
         l_del_rec.gross_weight := l_tot_pallet_gross_wt;
         l_del_rec.net_weight := l_tot_pallet_net_wt;
      EXCEPTION
         WHEN OTHERS THEN
            l_tot_pallet_gross_wt := 0;
            l_tot_pallet_net_wt := 0;
            x_msg_data := 'Error getting pallet wt for delivery id :' || p_delivery_id || ' Error :' || SQLERRM;
            x_return_status := 'E';
            RETURN;
      END;

      -- DBMS_output.put_line('Pos 2:');

      IF l_del_rec.gross_weight IS NOT NULL AND l_del_rec.net_weight IS NOT NULL THEN
         wsh_deliveries_pub.create_update_delivery (p_api_version_number   => 1.0,
                                                    p_init_msg_list        => fnd_api.g_true,
                                                    x_return_status        => l_return_status,
                                                    x_msg_count            => l_msg_count,
                                                    x_msg_data             => l_msg_data,
                                                    p_action_code          => 'UPDATE',
                                                    p_delivery_info        => l_del_rec,
                                                    p_delivery_name        => NULL,              -- p_delivery_number,
                                                    x_delivery_id          => l_delivery_id_out,
                                                    x_name                 => l_delivery_number_out);

         dbms_output.put_line ('Pos 5:' || l_return_status || ':' || l_delivery_id);

         IF (l_return_status <> wsh_util_core.g_ret_sts_success) THEN
            x_msg_data := 'Error in updating delivery :' || p_delivery_id || ' Error :' || l_msg_data;
            x_return_status := 'E';
         ELSE
            x_return_status := 'S';
         END IF;
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         x_return_status := 'E';
         x_msg_data := 'Error in procedure update_delivery_api :' || SQLERRM;
   END update_delivery_api;

   PROCEDURE update_serial_in_stg (p_delivery_id          IN NUMBER,
                                   p_delivery_detail_id   IN NUMBER,
                                   p_inventory_item_id    IN NUMBER,
                                   x_return_status        OUT VARCHAR2,
                                   x_msg_data             OUT VARCHAR2
                                   )
   IS
      CURSOR c_ser (p_delivery_detail_id IN NUMBER)
      IS
         SELECT fm_serial_number
           FROM wsh_serial_numbers
          WHERE delivery_detail_id = p_delivery_detail_id;
   BEGIN
      x_return_status := 'S';
      FOR r_ser IN c_ser (p_delivery_detail_id) LOOP
         UPDATE xxfox_audit_references
            SET upload_flag = 'Y'
          WHERE serial_number = r_ser.fm_serial_number
            AND upload_flag = 'N'
            AND delivery_id = p_delivery_id
            AND inventory_item_id = p_inventory_item_id;
      END LOOP;
   EXCEPTION
      WHEN OTHERS THEN
         x_return_status := 'E';
         x_msg_data := 'Error in procedure update_serial_in_stg :'||sqlerrm;
   END update_serial_in_stg;


      -- trip_id logic added by Raj N for PCN#194 on 12/21/2017
   PROCEDURE complete_delivery (x_return_status OUT VARCHAR2, x_msg_data OUT VARCHAR2, p_delivery_id IN NUMBER, p_trip_id IN NUMBER)
   AS

   CURSOR cur_del_trip (p_delivery_id IN NUMBER, p_trip_id IN NUMBER)
   IS
   SELECT wnd.delivery_id
     FROM wsh_new_deliveries wnd
    WHERE p_delivery_id IS NOT NULL AND wnd.delivery_id = p_delivery_id
   UNION
   SELECT wnd.delivery_id
     FROM wsh_trip_stops wts, wsh_delivery_legs wdl, wsh_new_deliveries wnd
    WHERE wnd.delivery_id = wdl.delivery_id AND wdl.pick_up_stop_id = wts.stop_id
      AND wts.trip_id = p_trip_id;

      x_msg_count              NUMBER;
      x_msg_details            VARCHAR2 (32500);
      x_msg_summary            VARCHAR2 (32500);
      p_tabofdeldets           wsh_delivery_details_pub.id_tab_type;
      l_msg                    VARCHAR2 (4000);
      l_action_code            VARCHAR2 (15);
      l_delivery_id            NUMBER;
      l_delivery_name          VARCHAR2 (30);
      l_asg_trip_id            NUMBER;
      l_asg_trip_name          VARCHAR2 (30);
      l_asg_pickup_stop_id     NUMBER;
      l_asg_pickup_loc_id      NUMBER;
      l_asg_pickup_loc_code    VARCHAR2 (30);
      l_asg_pickup_arr_date    DATE;
      l_asg_pickup_dep_date    DATE;
      l_asg_dropoff_stop_id    NUMBER;
      l_asg_dropoff_loc_id     NUMBER;
      l_asg_dropoff_loc_code   VARCHAR2 (30);
      l_asg_dropoff_arr_date   DATE;
      l_asg_dropoff_dep_date   DATE;
      l_sc_action_flag         VARCHAR2 (10);
      l_sc_intransit_flag      VARCHAR2 (10);
      l_sc_close_trip_flag     VARCHAR2 (10);
      l_sc_create_bol_flag     VARCHAR2 (10);
      l_sc_stage_del_flag      VARCHAR2 (10);
      l_sc_trip_ship_method    VARCHAR2 (30);
      l_sc_actual_dep_date     VARCHAR2 (30);
      l_sc_report_set_id       NUMBER;
      l_sc_report_set_name     VARCHAR2 (60);
      l_wv_override_flag       VARCHAR2 (10);
      x_trip_id                VARCHAR2 (30);
      x_trip_name              VARCHAR2 (30);
      v_ser_no_tbl             dbms_sql.varchar2_table;
      l_changedattrtabtype     wsh_delivery_details_pub.changedattributetabtype;
      l_serial_range_tab       wsh_glbl_var_strct_grp.ddserialrangetabtype;
      v_rec_ct                 NUMBER := 0;
      l_cnt                    NUMBER := 0;
      l_container_flag         VARCHAR2 (3) := 'Y';
      v_err_flag               VARCHAR2 (1);

      sn_count number;
      v_ser_no_count number;
      sn_range number;
      v_picked_qty number;
   BEGIN
      x_return_status := fnd_api.g_ret_sts_success;
      apps.fnd_file.put_line (fnd_file.LOG, '********************Delivery Completion Starts******************** ');
      p_tabofdeldets.delete;

      FOR rec_del_trip IN cur_del_trip (p_delivery_id, p_trip_id) LOOP -- Added by Raj N 12/21/2017

      SELECT wdd.delivery_detail_id
        BULK COLLECT INTO p_tabofdeldets
        FROM wsh_delivery_details wdd, wsh_delivery_assignments wda
       WHERE     wdd.delivery_detail_id = wda.delivery_detail_id
             AND wda.delivery_id = rec_del_trip.delivery_id -- p_delivery_id
             AND wdd.released_status IN ('R', 'B')
             AND wdd.source_code = 'OE';

      IF p_tabofdeldets.COUNT > 0 THEN
         wsh_delivery_details_pub.detail_to_delivery (p_api_version        => 1.0,
                                                      p_init_msg_list      => fnd_api.g_true,
                                                      p_commit             => fnd_api.g_false,
                                                      p_validation_level   => fnd_api.g_valid_level_full,
                                                      x_return_status      => x_return_status,
                                                      x_msg_count          => x_msg_count,
                                                      x_msg_data           => x_msg_data,
                                                      p_tabofdeldets       => p_tabofdeldets,
                                                      p_action             => 'UNASSIGN',
                                                      p_delivery_id        => rec_del_trip.delivery_id, --p_delivery_id,
                                                      p_delivery_name      => TO_CHAR (rec_del_trip.delivery_id));--TO_CHAR (p_delivery_id));

         IF x_return_status <> fnd_api.g_ret_sts_success THEN
            wsh_util_core.get_messages ('Y',
                                        x_msg_summary,
                                        x_msg_details,
                                        x_msg_count);

            IF x_msg_count > 1 THEN
               x_msg_data := SUBSTR (x_msg_details, 1, 250);
            ELSE
               x_msg_data := SUBSTR (x_msg_details, 1, 250);
            END IF;

            ROLLBACK;
            print_debug (
               'Error unassigning lines from delivery: ' || rec_del_trip.delivery_id || '-' || SUBSTR (x_msg_details, 1, 2500));
            apps.fnd_file.put_line (
               fnd_file.LOG,
               'Error unassigning lines from delivery: ' || rec_del_trip.delivery_id || '-' || SUBSTR (x_msg_details, 1, 2500));
            RETURN;
         END IF;                                                                     --unassign delivery return_status
      END IF;

      apps.fnd_file.put_line (fnd_file.LOG, 'Plan delivery: ' || rec_del_trip.delivery_id);
      l_action_code := 'PLAN';
      l_delivery_id := rec_del_trip.delivery_id;
      wsh_deliveries_pub.delivery_action (p_api_version_number     => 1.0,
                                          p_init_msg_list          => fnd_api.g_false,
                                          x_return_status          => x_return_status,
                                          x_msg_count              => x_msg_count,
                                          x_msg_data               => x_msg_data,
                                          p_action_code            => l_action_code,
                                          p_delivery_id            => l_delivery_id,
                                          p_delivery_name          => l_delivery_name,
                                          p_asg_trip_id            => l_asg_trip_id,  -- p_trip_id
                                          p_asg_trip_name          => l_asg_trip_name,
                                          p_asg_pickup_stop_id     => l_asg_pickup_stop_id,
                                          p_asg_pickup_loc_id      => l_asg_pickup_loc_id,
                                          p_asg_pickup_loc_code    => l_asg_pickup_loc_code,
                                          p_asg_pickup_arr_date    => l_asg_pickup_arr_date,
                                          p_asg_pickup_dep_date    => l_asg_pickup_dep_date,
                                          p_asg_dropoff_stop_id    => l_asg_dropoff_stop_id,
                                          p_asg_dropoff_loc_id     => l_asg_dropoff_loc_id,
                                          p_asg_dropoff_loc_code   => l_asg_dropoff_loc_code,
                                          p_asg_dropoff_arr_date   => l_asg_dropoff_arr_date,
                                          p_asg_dropoff_dep_date   => l_asg_dropoff_dep_date,
                                          p_sc_action_flag         => l_sc_action_flag,
                                          p_sc_intransit_flag      => l_sc_intransit_flag,
                                          p_sc_close_trip_flag     => l_sc_close_trip_flag,
                                          p_sc_create_bol_flag     => l_sc_create_bol_flag,
                                          p_sc_stage_del_flag      => l_sc_stage_del_flag,
                                          p_sc_trip_ship_method    => l_sc_trip_ship_method,
                                          p_sc_actual_dep_date     => l_sc_actual_dep_date,
                                          p_sc_report_set_id       => l_sc_report_set_id,
                                          p_sc_report_set_name     => l_sc_report_set_name,
                                          p_wv_override_flag       => l_wv_override_flag,
                                          x_trip_id                => x_trip_id,
                                          x_trip_name              => x_trip_name);

      IF x_return_status <> 'S' THEN
         x_msg_data := '';

         FOR i IN 1 .. x_msg_count LOOP
            l_msg := apps.fnd_msg_pub.get (p_msg_index => i, p_encoded => apps.fnd_api.g_false);
            x_msg_data := x_msg_data || 'Msg ' || i || ': ' || l_msg || CHR (10);
         END LOOP;

         apps.fnd_file.put_line (fnd_file.LOG,
                                 'Error in plan delivery: ' || rec_del_trip.delivery_id || ', ' || SUBSTR (x_msg_data, 1, 3000));
         x_msg_data := 'Error in plan delivery: ' || rec_del_trip.delivery_id || ', ' || SUBSTR (x_msg_data, 1, 200);
         ROLLBACK;
         RETURN;
      END IF;

      v_err_flag := 'N';
  END LOOP;
/*
for rec_ol in (      SELECT distinct wdd.source_line_id
               FROM wsh_delivery_details wdd, wsh_delivery_assignments wda
              WHERE     wdd.source_code = 'OE'
                    AND wdd.delivery_detail_id = wda.delivery_detail_id
                    AND wda.delivery_id = p_delivery_id
                    AND wdd.released_status = 'Y' ) loop
*/

for rec_ol in ( SELECT distinct wdd.source_line_id, wda.delivery_id
                  FROM wsh_delivery_details wdd, wsh_delivery_assignments wda
                 WHERE wdd.source_code = 'OE'
                   AND wdd.released_status = 'Y'
                   AND wdd.delivery_detail_id = wda.delivery_detail_id
                   AND p_delivery_id IS NOT NULL AND wda.delivery_id = p_delivery_id
                UNION
                SELECT distinct wdd.source_line_id, wda.delivery_id
                  FROM wsh_delivery_details wdd, wsh_delivery_assignments wda, wsh_trip_stops wts, wsh_delivery_legs wdl
                 WHERE wdd.source_code = 'OE'
                   AND wdd.released_status = 'Y'
                   AND wdd.delivery_detail_id = wda.delivery_detail_id
                   AND wda.delivery_id = wdl.delivery_id
                   AND wdl.pick_up_stop_id = wts.stop_id
                   AND p_trip_id IS NOT NULL AND wts.trip_id = p_trip_id
                  ) loop

             sn_count := 1;

      FOR rec_glb1
         IN (SELECT wdd.delivery_detail_id,
                    wdd.inventory_item_id,
                    wdd.picked_quantity,
                    msi.segment1 item
               FROM wsh_delivery_details wdd, wsh_delivery_assignments wda, mtl_system_items_b msi
              WHERE     wdd.source_code = 'OE'
                    AND wdd.delivery_detail_id = wda.delivery_detail_id
                    AND wdd.inventory_item_id = msi.inventory_item_id
                    AND wdd.organization_id = msi.organization_id
                    and wdd.source_line_id = rec_ol.source_line_id
                    AND wda.delivery_id = rec_ol.delivery_id -- p_delivery_id
                    AND wdd.released_status = 'Y'
                    AND EXISTS
                           (SELECT 1
                              FROM xxfox_audit_references audit_ref
                             WHERE audit_ref.inventory_item_id = wdd.inventory_item_id
                               AND audit_ref.reference_type IN ('M', 'S')
                               AND ((audit_ref.delivery_id = p_delivery_id and p_delivery_id is not null)
                                 OR (audit_ref.trip_id = p_trip_id and p_trip_id is not null)
                                   )
                               )
                ) LOOP

         IF l_changedattrtabtype.COUNT > 0 THEN
            l_changedattrtabtype.delete;
         END IF;

         v_rec_ct := 1;
         l_changedattrtabtype (v_rec_ct).delivery_detail_id := rec_glb1.delivery_detail_id;
         l_changedattrtabtype (v_rec_ct).shipped_quantity := rec_glb1.picked_quantity;
         v_ser_no_tbl.delete;

                begin
                SELECT count(serial_number) into v_ser_no_count
                   FROM xxfox.xxfox_audit_references
                  WHERE reference_type IN ('M', 'S')
                    AND inventory_item_id = rec_glb1.inventory_item_id
                    AND upload_flag = 'N'
                   -- AND delivery_id = p_delivery_id
                    AND ((delivery_id = p_delivery_id and p_delivery_id is not null)
                      OR (trip_id = p_trip_id and p_trip_id is not null)
                        );
                exception
                when others then
                v_ser_no_count := 0;
                end;
         begin
         SELECT              sum(wdd.picked_quantity) into v_picked_qty
                       FROM wsh_delivery_details wdd, wsh_delivery_assignments wda
                      WHERE     wdd.source_code = 'OE'
                            AND wdd.delivery_detail_id = wda.delivery_detail_id
                            AND wda.delivery_id = rec_ol.delivery_id  -- p_delivery_id
                            and wdd.source_line_id = rec_ol.source_line_id
                            and wdd.inventory_item_id = rec_glb1.inventory_item_id
                            AND wdd.released_status = 'Y';
                            exception
                            when others then
                            v_picked_qty := 0;
                            end;

         IF v_ser_no_COUNT = v_picked_qty THEN
            IF l_serial_range_tab.COUNT > 0 THEN
               l_serial_range_tab.delete;
            END IF;

         sn_range := 1;
         FOR sn_rec in (select serial_number
                          from (select rownum recid, serial_number
                                  from ( SELECT serial_number
                                           FROM xxfox.xxfox_audit_references
                                          WHERE reference_type IN ('M', 'S')
                                            AND inventory_item_id = rec_glb1.inventory_item_id
                                            AND upload_flag = 'N'
                                            AND ((delivery_id = p_delivery_id AND p_delivery_id IS NOT NULL)
                                              OR (trip_id = p_trip_id AND p_trip_id IS NOT NULL)
                                                )
                                          order by 1
                                       )
                               )
                         where 1=1
                           and recid between sn_count and (sn_count + rec_glb1.picked_quantity - 1)
                         ) LOOP

                l_serial_range_tab (sn_range).delivery_detail_id := rec_glb1.delivery_detail_id;
                l_serial_range_tab (sn_range).from_serial_number := sn_rec.serial_number;
                l_serial_range_tab (sn_range).to_serial_number := sn_rec.serial_number;
                l_serial_range_tab (sn_range).quantity := 1;
                       sn_range := sn_range + 1;

            END LOOP;
        sn_count := sn_count + rec_glb1.picked_quantity;

            wsh_delivery_details_pub.update_shipping_attributes (p_api_version_number   => 1.0,
                                                                 p_init_msg_list        => fnd_api.g_true,
                                                                 p_commit               => fnd_api.g_false,
                                                                 x_return_status        => x_return_status,
                                                                 x_msg_count            => x_msg_count,
                                                                 x_msg_data             => x_msg_data,
                                                                 p_changed_attributes   => l_changedattrtabtype,
                                                                 p_source_code          => 'OE',
                                                                 p_container_flag       => l_container_flag,
                                                                 p_serial_range_tab     => l_serial_range_tab);

            IF x_return_status <> 'S' THEN
               v_err_flag := 'Y';
               x_msg_data := '';

               FOR i IN 1 .. x_msg_count LOOP
                  l_msg := apps.fnd_msg_pub.get (p_msg_index => i, p_encoded => apps.fnd_api.g_false);
                  x_msg_data := x_msg_data || 'Msg ' || i || ': ' || l_msg || CHR (10);
               END LOOP;

               apps.fnd_file.put_line (
                  fnd_file.LOG,
                     'Error updating serial numbers, delivery: '
                  || p_delivery_id
                  || ', item: '
                  || rec_glb1.item
                  || ', '
                  || SUBSTR (x_msg_data, 1, 3000));
               x_msg_data :=
                     'Error updating serial numbers, delivery: '
                  || p_delivery_id
                  || ', item: '
                  || rec_glb1.item
                  || ', '
                  || SUBSTR (x_msg_data, 1, 200);
               ROLLBACK;
               RETURN;
            END IF;
         ELSE
            v_err_flag := 'Y';
            x_return_status := 'E';
            x_msg_data :=
                  'Scanned serials does not match picked quantity, delivery: '
               || p_delivery_id
               || ', item: '
               || rec_glb1.item;
            ROLLBACK;
            RETURN;
         END IF;
      END LOOP;
      END LOOP; --rec_ol

      IF v_err_flag = 'N' THEN
         SELECT COUNT (1)
           INTO l_cnt
           FROM xxfox_audit_lines
          WHERE delivery_id = p_delivery_id AND pallet_gross_wt IS NOT NULL;

         /*IF l_cnt > 0 THEN
            v_rec_ct := v_rec_ct + 1;
            update_delivery_api (p_delivery_id, x_return_status, x_msg_data);

            IF x_return_status = 'E' THEN
               v_err_flag := 'Y';
            END IF;
         END IF;*/
      END IF;

      IF v_err_flag = 'N' AND v_rec_ct > 0 THEN
         COMMIT;
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         x_return_status := fnd_api.g_ret_sts_error;
         x_msg_data := 'Error in complete_delivery: ' || SQLERRM;
         apps.fnd_file.put_line (fnd_file.LOG, x_msg_data);
   END complete_delivery;

   --
   PROCEDURE print_pick_slip (p_organization_id   IN     NUMBER,
                              p_delivery_id       IN     NUMBER,
                              p_printer           IN     VARCHAR2,
                              x_request_id           OUT NUMBER,
                              x_msg_count            OUT NUMBER,
                              x_return_status        OUT VARCHAR2,
                              x_return_msg           OUT VARCHAR2)
   AS
      add_printer   BOOLEAN;
      xml_layout    BOOLEAN;
      v_printer     VARCHAR2 (250) := fnd_profile.VALUE ('PRINTER');
   BEGIN
      print_debug ('print_pick_slip p_delivery_id = ' || p_delivery_id);

      IF fnd_request.set_print_options (printer => NVL (v_printer, p_printer), copies => 1) THEN
         /*add_printer := fnd_request.add_printer (printer => p_printer
                                                ,copies  => 1);*/
         xml_layout :=
            fnd_request.add_layout ('XXFOX',
                                    'XXFOX_WSHRDPIK',
                                    'en',
                                    'US',
                                    'PDF');
         x_request_id :=
            fnd_request.submit_request (application   => 'XXFOX',
                                        program       => 'XXFOX_WSHRDPIK',
                                        description   => NULL,
                                        start_time    => NULL,
                                        sub_request   => FALSE,
                                        argument1     => p_delivery_id,
                                        argument2     => NULL,
                                        argument3     => NULL,
                                        argument4     => NULL,
                                        argument5     => NULL,
                                        argument6     => NULL,
                                        argument7     => NULL,
                                        argument8     => NULL,
                                        argument9     => NULL,
                                        argument10    => p_organization_id,
                                        argument11    => NULL,
                                        argument12    => 'U',
                                        argument13    => NULL,
                                        argument14    => NULL,
                                        argument15    => 'D',
                                        argument16    => 'MSTK',
                                        argument17    => 'MTLL',
                                        argument18    => NULL);
         print_debug ('print_pick_slip request_id = ' || x_request_id);
         COMMIT;
         x_return_status := 'S';
      ELSE
         print_debug ('print_pick_slip error setting printer options');
         x_request_id := 0;
         x_return_status := 'E';
         x_return_msg := 'Error setting printer options.';
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         print_debug ('Error printing pick slip: ' || SQLERRM);
         x_return_status := 'E';
         x_return_msg := 'Error printing pick slip: ' || SQLERRM;
         x_msg_count := 1;
         x_request_id := 0;
   END print_pick_slip;


   PROCEDURE print_packing_slip (p_organization_id   IN     NUMBER,
                                 p_trip_id       IN     NUMBER,
                                 x_request_id           OUT NUMBER,
                                 x_return_status        OUT VARCHAR2,
                                 x_return_msg           OUT VARCHAR2)
   IS

      xml_layout           BOOLEAN;
      v_pack_slip_no       VARCHAR2 (50);
      v_pickup_loc_id      NUMBER;
      v_inv_org_id         NUMBER;
      v_set_of_books_id    NUMBER;
      x_document_number    NUMBER;
      x_msg_count          NUMBER;
       x_msg_data          VARCHAR2(2000);
       l_msg               VARCHAR2(2000);


    CURSOR c_del_ids (p_trip NUMBER) IS
        SELECT DISTINCT wda.delivery_id
          FROM wsh_delivery_details wdd
              , wsh_delivery_assignments wda
              , wsh_delivery_legs wdl
              , wsh_trip_stops wts
         WHERE wdd.source_code = 'OE'
           AND wdd.delivery_detail_id = wda.delivery_detail_id
           AND wda.delivery_id = wdl.delivery_id(+)
           AND wdl.pick_up_stop_id = wts.stop_id(+)
           --AND wdd.organization_id = p_organization_id
           AND wts.trip_id = p_trip
           AND wda.delivery_id IS NOT NULL
           ORDER BY 1;

   BEGIN
      print_debug ('print_packing_slip for Trip id = ' || p_trip_id);

         BEGIN
             FOR c_delivery_ids IN c_del_ids(p_trip_id) LOOP
                 v_pack_slip_no  := NULL;
                 v_pickup_loc_id := 0;
                 v_inv_org_id    := 0;

                 SELECT  wdi.sequence_number, wnd.initial_pickup_location_id, wnd.organization_id
                   INTO  v_pack_slip_no, v_pickup_loc_id, v_inv_org_id
                  FROM wsh_new_deliveries wnd, wsh_document_instances wdi
                 WHERE wnd.delivery_id = wdi.entity_id(+)
                   AND wdi.entity_name(+) = 'WSH_NEW_DELIVERIES'
                   AND wdi.document_type(+) = 'PACK_TYPE'
                   AND wnd.delivery_id = c_delivery_ids.delivery_id;

                 IF v_pack_slip_no IS NULL THEN
                   -- derive set_of_books_id
                    SELECT hoi.org_information1
                      INTO v_set_of_books_id
                     FROM hr_organization_information hoi
                    WHERE hoi.organization_id = v_inv_org_id
                      AND hoi.org_information_context = 'Accounting Information';

                    wsh_document_pvt.create_document (p_api_version          => 1.0,
                                              p_init_msg_list        => 'F',
                                              p_commit               => NULL,
                                              p_validation_level     => NULL,
                                              x_return_status        => x_return_status,
                                              x_msg_count            => x_msg_count,
                                              x_msg_data             => x_msg_data,
                                              p_entity_name          => 'WSH_NEW_DELIVERIES',
                                              p_entity_id            => c_delivery_ids.delivery_id,
                                              p_application_id       => 665,
                                              p_location_id          => v_pickup_loc_id,
                                              p_document_type        => 'PACK_TYPE',
                                              p_document_sub_type    => 'SALES_ORDER',
                                              p_ledger_id            => v_set_of_books_id,
                                              p_consolidate_option   => 'BOTH',
                                              x_document_number      => x_document_number);

                     print_debug ('print_pack_slip wsh_document_pvt.create_document x_return_status = ' || x_return_status);
                     print_debug ('print_pack_slip wsh_document_pvt.create_document x_msg_count = ' || x_msg_count);
                     print_debug ('print_pack_slip wsh_document_pvt.create_document x_return_msg = ' || x_msg_data);
                     print_debug ('print_pack_slip l_document_number = ' || x_document_number);

                     IF x_return_status <> fnd_api.g_ret_sts_success THEN
                         x_msg_data := '';
                         FOR i IN 1 .. x_msg_count LOOP
                             l_msg := apps.fnd_msg_pub.get (p_msg_index => i, p_encoded => apps.fnd_api.g_false);
                             x_msg_data := x_msg_data || 'Msg ' || i || ': ' || l_msg || CHR (10);
                         END LOOP;

                         x_msg_data :=
                                      'Error generating pack slip no for Delivery: '
                                  || c_delivery_ids.delivery_id
                                   || ' '
                                  || SUBSTR (x_msg_data, 1, 100);
                                  ROLLBACK;
                                  RETURN;
                     END IF;
                 END IF;
             END LOOP;
         EXCEPTION
             WHEN OTHERS THEN
                     print_debug ('Error generating packing slip : ');
         END;

         xml_layout :=
            fnd_request.add_layout ('XXFOX',
                                    'XXFOX_PACK_REP',
                                    'en',
                                    'US',
                                    'PDF');
         x_request_id :=
            fnd_request.submit_request (application   => 'XXFOX',
                                        program       => 'XXFOX_PACK_REP',
                                        description   => NULL,
                                        start_time    => NULL,
                                        sub_request   => FALSE,
                                        argument1     => p_organization_id,
                                        argument2     => p_trip_id,
                                        argument3     => NULL,
                                        argument4     => 'N',
                                        argument5     => 'D',
                                        argument6     => 'DRAFT',
                                        argument7     => 'INV',
                                        argument8     => NULL,
                                        argument9     => NULL,
                                        argument10    => NULL,
                                        argument11    => 2,
                                        argument12    => 'Y'
                                       );
         print_debug ('print_packing_slip Request ID = ' || x_request_id);
         COMMIT;
         x_return_status := 'S';

   EXCEPTION
      WHEN OTHERS THEN
         print_debug ('print_packing_slip error: ' || SQLERRM);
         x_return_status := 'E';
         x_return_msg := 'print_packing_slip error: ' || SQLERRM;
         x_request_id := 0;
   END print_packing_slip;






   --
   PROCEDURE update_ship_method (p_delivery_id        IN     NUMBER,
                                 p_ship_method_code   IN     VARCHAR2,
                                 x_msg_count             OUT NUMBER,
                                 x_return_status         OUT VARCHAR2,
                                 x_return_msg            OUT VARCHAR2)
   IS
      l_return_status   VARCHAR2 (1);
      l_msg_count       NUMBER;
      l_msg_data        VARCHAR2 (2000);
      l_delv_rec        wsh_deliveries_pub.delivery_pub_rec_type;
      l_delv_name       VARCHAR2 (240);
      l_delv_id         NUMBER;
      l_msg_details     VARCHAR2 (4000);
   BEGIN
      l_delv_rec.delivery_id := p_delivery_id;
      l_delv_rec.ship_method_code := p_ship_method_code;

      wsh_deliveries_pub.create_update_delivery (p_api_version_number   => 1.0,
                                                 p_init_msg_list        => fnd_api.g_true,
                                                 x_return_status        => l_return_status,
                                                 x_msg_count            => l_msg_count,
                                                 x_msg_data             => l_msg_data,
                                                 p_action_code          => 'UPDATE',
                                                 p_delivery_info        => l_delv_rec,
                                                 p_delivery_name        => fnd_api.g_miss_char,
                                                 x_delivery_id          => l_delv_id,
                                                 x_name                 => l_delv_name);

      dbms_output.put_line ('Status :' || l_return_status);
      x_return_status := 'S';
      x_return_msg := NULL;

      IF (l_return_status <> wsh_util_core.g_ret_sts_success) THEN
         wsh_util_core.get_messages ('Y',
                                     l_msg_data,
                                     l_msg_details,
                                     l_msg_count);
         dbms_output.put_line ('Messgae :' || l_msg_details);
         x_return_msg := l_msg_details;
         x_return_status := 'E';
      END IF;
   EXCEPTION
      WHEN OTHERS THEN
         x_return_msg := 'Error while updating ship method :' || SQLERRM;
         x_return_status := 'E';
   END;

   PROCEDURE create_update_freight1 (
      x_return_status1          OUT     VARCHAR2,
      x_msg_data1               OUT     VARCHAR2,
      p_trip_id1                 IN     NUMBER,  --KP
      p_freight_cost_id1         IN     NUMBER,
      p_freight_cost_type_id1   IN     NUMBER,
      p_unit_amount1            IN     NUMBER,
      p_action                  IN     VARCHAR2
   )
   IS

p_freight_rec          WSH_FREIGHT_COSTS_PUB.PubFreightCostRecType;
v_freight_cost_id      NUMBER;
v_freight_cost_type_id NUMBER;
v_unit_amount          NUMBER;
x_ret_status           VARCHAR2(5);
x_message_count        NUMBER;
x_message_data         VARCHAR2(2000);
x_frig_cost_id         NUMBER;
x_msg_count             NUMBER;
x_msg_details           VARCHAR2 (32500);
x_msg_summary           VARCHAR2 (32500);
x_msg_data              VARCHAR2 (32500);
BEGIN


      IF p_action = 'C' THEN
             p_freight_rec.FREIGHT_COST_TYPE_ID := p_freight_cost_type_id1;
             p_freight_rec.UNIT_AMOUNT          := p_unit_amount1;
             p_freight_rec.TRIP_ID              := p_trip_id1;

             --DBMS_OUTPUT.put_line('CREATING... ');
             --DBMS_OUTPUT.put_line('p_freight_cost_type_id1 - '||p_freight_cost_type_id1);
             --DBMS_OUTPUT.put_line('p_unit_amount1 - '||p_unit_amount1);
             --DBMS_OUTPUT.put_line('p_trip_id1 - '||p_trip_id1);


             APPS.WSH_FREIGHT_COSTS_PUB.Create_Update_Freight_Costs ( p_api_version_number  => '1.0',
                                                                      p_init_msg_list       => 'T', --APPS.fnd_api.g_true,
                                                                      p_commit              => 'F' ,--APPS.fnd_api.g_false,
                                                                      x_return_status       => x_return_status1,
                                                                      x_msg_count           => x_message_count,
                                                                      x_msg_data            => x_message_data,
                                                                      p_pub_freight_costs      => p_freight_rec,
                                                                      p_action_code         => 'CREATE',
                                                                      x_freight_cost_id     => x_frig_cost_id
                                                                     );
             --DBMS_OUTPUT.put_line('x_return_status1 - '||x_return_status1);


          IF x_return_status1 <> 'S' THEN
            wsh_util_core.get_messages ('Y',
                                        x_msg_summary,
                                        x_msg_details,
                                        x_message_count);

            IF x_message_count > 1 THEN
               x_msg_data1 := SUBSTR (x_msg_details, 1, 250);
            ELSE
               x_msg_data1 := SUBSTR (x_msg_details, 1, 250);
            END IF;

         END IF;
          COMMIT;
      ELSIF p_action = 'U' THEN
             p_freight_rec.FREIGHT_COST_ID       := p_freight_cost_id1;
             p_freight_rec.FREIGHT_COST_TYPE_ID  := p_freight_cost_type_id1;
             p_freight_rec.UNIT_AMOUNT           := p_unit_amount1;
             p_freight_rec.TRIP_ID               := p_trip_id1;

              --DBMS_OUTPUT.put_line('UPDATING ');
              --DBMS_OUTPUT.put_line('p_freight_cost_id1 - '||p_freight_cost_id1);
             -- DBMS_OUTPUT.put_line('p_freight_cost_type_id1 - '||p_freight_cost_type_id1);
             --DBMS_OUTPUT.put_line('p_unit_amount1 - '||p_freight_cost_type_id1);
             --DBMS_OUTPUT.put_line('p_trip_id1 - '||p_trip_id1);


             APPS.WSH_FREIGHT_COSTS_PUB.Create_Update_Freight_Costs ( p_api_version_number  => '1.0',
                                                                      p_init_msg_list       => 'T', --fnd_api.g_true,
                                                                      p_commit              => 'F', --fnd_api.g_false,
                                                                      x_return_status       => x_return_status1,
                                                                      x_msg_count           => x_message_count,
                                                                      x_msg_data            => x_message_data,
                                                                      p_pub_freight_costs   => p_freight_rec,
                                                                      p_action_code         => 'UPDATE' ,
                                                                      x_freight_cost_id     => x_frig_cost_id
                                                                    );
           --DBMS_OUTPUT.put_line('x_return_status1 - '||x_return_status1);

            IF x_return_status1 <>'S' THEN
            wsh_util_core.get_messages ('Y',
                                        x_msg_summary,
                                        x_msg_details,
                                        x_message_count);

              IF x_message_count > 1 THEN
               x_msg_data1 := SUBSTR (x_msg_details, 1, 250);
              ELSE
               x_msg_data1 := SUBSTR (x_msg_details, 1, 250);
              END IF;
            END IF;
        COMMIT;
      END IF;
     COMMIT;

END create_update_freight1;


FUNCTION xxfox_find_part_number  (p_item_id IN NUMBER) RETURN VARCHAR2 IS
v_part_number VARCHAR2(60);
 BEGIN
      IF p_item_id IS NOT NULL THEN
        SELECT segment1
        INTO v_part_number
        FROM mtl_system_items_b
        WHERE inventory_item_id = p_item_id
        AND organization_id = 83;
      END IF;
      RETURN v_part_number;
END xxfox_find_part_number;

FUNCTION xxfox_sscc_label_data(p_trip NUMBER, p_val VARCHAR2) RETURN VARCHAR2
IS

v_ord_num  NUMBER;
v_item     VARCHAR2(150);
v_po       VARCHAR2(150);
v_ship_from  VARCHAR2(2000);
v_ship_to    VARCHAR2(2000);
v_postalcode VARCHAR2(100);
v_shipmethod VARCHAR2(300);
v_bol     VARCHAR2(2000);
V_MAX_CARTON NUMBER;

BEGIN
/*
   BEGIN
    SELECT
          substr(order_numbers,1,instr(order_numbers, ',',1)-1),inventory_item_id
     INTO v_ord_num,
          v_item
     from xxfox_audit_lines
    where trip_id = p_trip
     and pallet_number = p_pallet_no
     and box_number = p_box_no
     and rownum = 1;
   EXCEPTION
      WHEN OTHERS THEN
          v_ord_num := NULL;
          v_item    := NULL;
   END;
  */

   BEGIN

        SELECT      (SELECT    hl_ship.address1 ||Decode(hl_ship.address2,NULL,' ', chr(10))
                               ||hl_ship.address2 ||Decode(hl_ship.address3,NULL,' ',chr(10))
                               ||hl_ship.address3 ||Decode(hl_ship.address4,NULL,' ',chr(10))
                               ||hl_ship.address4 ||Decode(hl_ship.city,NULL,' ',chr(10))
                               ||hl_ship.city     ||Decode(hl_ship.state,NULL,' ',',')
                               ||hl_ship.state    ||Decode(hl_ship.postal_code,' ',',')
                               ||hl_ship.COUNTRY  ||Decode(hl_ship.COUNTRY,' ',',')
                               ||hl_ship.postal_code
                         FROM hz_locations hl_ship WHERE location_id = wdd.ship_from_location_id),
                     (SELECT    hl_ship.address1 ||Decode(hl_ship.address2,NULL,' ', chr(10))
                               ||hl_ship.address2 ||Decode(hl_ship.address3,NULL,' ',chr(10))
                               ||hl_ship.address3 ||Decode(hl_ship.address4,NULL,' ',chr(10))
                               ||hl_ship.address4 ||Decode(hl_ship.city,NULL,' ',chr(10))
                               ||hl_ship.city     ||Decode(hl_ship.state,NULL,' ',',')
                               ||hl_ship.state    ||Decode(hl_ship.postal_code,' ',',')
                               ||hl_ship.COUNTRY  ||Decode(hl_ship.COUNTRY,' ',',')
                               ||hl_ship.postal_code
                         FROM hz_locations hl_ship WHERE location_id = wdd.ship_to_location_id),
                     (SELECT   hl_ship.postal_code FROM hz_locations hl_ship WHERE location_id = wdd.ship_to_location_id) ,
                      --wdd.ship_method_code,
                      wc.carrier_name,
                      OOH.CUST_PO_NUMBER
         INTO v_ship_from ,
              v_ship_to,
              v_postalcode,
              v_shipmethod,
              v_po
         FROM wsh_delivery_details wdd,
                         wsh_delivery_assignments wda,wsh_new_deliveries wnd,
                            wsh_delivery_legs wdl
                          , wsh_trip_stops wts,
                         OE_ORDER_HEADERS_ALL OOH,
                         OE_ORDER_LINES_ALL OOL, wsh_carrier_services wcs, wsh_carriers_v wc
        WHERE WDA.DELIVERY_DETAIL_ID=WDD.DELIVERY_DETAIL_ID
                     AND OOL.LINE_ID = WDD.SOURCE_LINE_ID
                     AND OOH.HEADER_ID = OOL.HEADER_ID
                     AND wda.delivery_id = wdl.delivery_id(+)
                     AND wdl.pick_up_stop_id = wts.stop_id(+)
                     AND wts.trip_id = p_trip
                     and wnd.delivery_id = wda.delivery_id
                     and wnd.ship_method_code = wcs.ship_method_code
                     and wcs.carrier_id = wc.carrier_id
                     --AND WDD.INVENTORY_ITEM_ID = NVL(v_item,WDD.INVENTORY_ITEM_ID)
                     --AND OOH.order_number = NVL(v_ord_num,OOH.order_number)
                     AND ROWNUM=1;
                    -- group by wts.trip_id,wda.delivery_id,WDD.INVENTORY_ITEM_ID,OOH.ORDER_NUMBER ,
                     --      OOL.LINE_NUMBER ,OOL.SHIPMENT_NUMBER,OOH.CUST_PO_NUMBER ;

   EXCEPTION
      WHEN OTHERS THEN
              v_ship_from := NULL;
              v_ship_to   := NULL;
              v_postalcode := NULL;
              v_shipmethod  := NULL;
              v_po          := NULL;
   END;


   BEGIN
      SELECT attribute7
        INTO v_bol
        FROM wsh_trips
       WHERE trip_id = p_trip;
   EXCEPTION
      WHEN OTHERS THEN
         v_bol := NULL;
   END;


   if(p_val  = 'PCARTON') THEN
    BEGIN
    SELECT MAX(PALLET_NUMBER)
    INTO V_MAX_CARTON
    FROM XXFOX_AUDIT_LINES
    WHERE TRIP_ID = P_TRIP;
    EXCEPTION
    WHEN OTHERS THEN
        RETURN NULL;
    END;

        RETURN (' OF '||V_MAX_CARTON);

   ELSIF (P_VAL = 'CCARTON') THEN
    BEGIN
    SELECT MAX(BOX_NUMBER)
    INTO V_MAX_CARTON
    FROM XXFOX_AUDIT_LINES
    WHERE TRIP_ID = P_TRIP
    AND PALLET_NUMBER IS NULL;
    EXCEPTION
    WHEN OTHERS THEN
        RETURN NULL;
    END;

        RETURN (' OF '||V_MAX_CARTON);


   END IF;

   IF (p_val = 'SF') THEN -- ship from
      RETURN v_ship_from;
   ELSIF (p_val = 'ST') THEN -- ship to
       RETURN v_ship_to;
   ELSIF (p_val = 'PC') THEN  --Postal code
       RETURN v_postalcode;
   ELSIF (p_val = 'SM') THEN  -- shipping method
        RETURN v_shipmethod;
   ELSIF (p_val = 'PO') THEN   -- po
        RETURN v_po;
   ELSIF (p_val = 'BOL') THEN   -- Bill of lading
       RETURN v_bol;
   else
    return null;
   END IF;

EXCEPTION
   WHEN OTHERS THEN
      NULL;
END;


FUNCTION xxfox_find_part_number1(p_item_id IN NUMBER) RETURN VARCHAR2 IS
v_part_number VARCHAR2(60);
 BEGIN
      IF p_item_id IS NOT NULL THEN
        SELECT segment1
        INTO v_part_number
        FROM mtl_system_items_b
        WHERE inventory_item_id = p_item_id
        AND organization_id = 83;
      END IF;
      RETURN v_part_number;
END xxfox_find_part_number1;

END xxfox_packwb_pkg;

EXIT; 
/
