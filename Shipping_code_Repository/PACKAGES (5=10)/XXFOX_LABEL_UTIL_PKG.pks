CREATE OR REPLACE PACKAGE APPS.XXFOX_LABEL_UTIL_PKG AUTHID CURRENT_USER AS
/* $Header: XXFOX_ORDSTATUS_PK.pks 1.4 2015/06/11 15:33:48PST Development  $
REM +==========================================================================+
REM |     Copyright (c) 2015 DAZ Systems, Inc .                                |
REM |                  El Segundo, CA 90505                                    |
REM |                All rights reserved                                       |
REM +==========================================================================+
REM
REM NAME
REM  XXFOX_LABEL_UTIL_PKG.pks
REM
REM PROGRAM TYPE
REM  PL/SQL Package Spec
REM
REM PURPOSE
REM  Label Printing Program
REM
REM ===========================================================================
REM  Date       Author            Activity
REM ===========================================================================
REM  15DEC2015  Mangesh V. Raut   Created
REM ===========================================================================
*/
P_DELIVERY_ID        VARCHAR2(240);
P_TRIP_ID            VARCHAR2(240); --Added by awaiz on 15.feb.2018
P_PICK_SLIP_NUM_LOW  VARCHAR2(240);
P_PICK_SLIP_NUM_HIGH VARCHAR2(240);
P_ORDER_TYPE_ID      VARCHAR2(240);
P_ORDER_NUM_LOW      VARCHAR2(240);
P_ORDER_NUM_HIGH     VARCHAR2(240);
P_MOVE_ORDER_LOW     VARCHAR2(240);
P_MOVE_ORDER_HIGH    VARCHAR2(240);
P_FREIGHT_CODE       VARCHAR2(240);
P_ORGANIZATION_ID    VARCHAR2(240);
P_CUSTOMER_ID        VARCHAR2(240);
P_PICK_STATUS        VARCHAR2(240);
P_DETAIL_DATE_LOW    VARCHAR2(240);
P_DETAIL_DATE_HIGH   VARCHAR2(240);
P_ITEM_DISPLAY       VARCHAR2(240);
P_ITEM_FLEX_CODE     VARCHAR2(240);
P_LOCATOR_FLEX_CODE  VARCHAR2(240);
P_PRINTER_NAME       VARCHAR2(240);
P_PALLET_NUMBER      VARCHAR2(240);

PROCEDURE receipt_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_ship_line_id    IN     NUMBER,
    p_quantity        IN     NUMBER
   );

PROCEDURE part_label_api
   (p_organization_id IN     NUMBER,
    p_delivery_number IN     VARCHAR2,
    p_part_number     IN     VARCHAR2,
    p_printer_name    IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    x_return_status   IN OUT VARCHAR2,
    x_msg_data        IN OUT VARCHAR2
   );

PROCEDURE part_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_delivery_number IN     VARCHAR2,
    p_part_number     IN     VARCHAR2
   );


PROCEDURE inventory_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_item_number     IN     VARCHAR2,
    p_quantity        IN     NUMBER
   );

PROCEDURE box_label_api
   (p_organization_id IN     NUMBER,
    p_delivery_number IN     VARCHAR2,
    p_box_number      IN     NUMBER,
    p_printer_name    IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    x_return_status   IN OUT VARCHAR2,
    x_msg_data        IN OUT VARCHAR2
   );

PROCEDURE box_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_delivery_number IN     VARCHAR2,
    p_box_number      IN     NUMBER
   );

PROCEDURE pallet_label_api
   (p_organization_id IN     NUMBER,
    p_delivery_number IN     VARCHAR2,
    p_pallet_number   IN     NUMBER,
    p_printer_name    IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    x_return_status   IN OUT VARCHAR2,
    x_msg_data        IN OUT VARCHAR2
   );

PROCEDURE pallet_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_delivery_number IN     VARCHAR2,
    p_pallet_number   IN     NUMBER
   );



PROCEDURE service_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_service_request IN     VARCHAR2
   );


PROCEDURE wip_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_job_number      IN     VARCHAR2,
    p_quantity        IN     NUMBER
   );

PROCEDURE kitting_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_tansaction_id   IN     VARCHAR2,
    p_quantity        IN     NUMBER
   );

PROCEDURE part_request_indvidual_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_tansaction_id   IN     VARCHAR2,
    p_quantity        IN     NUMBER
   );

PROCEDURE shipping_label_api
   (p_organization_id IN     NUMBER,
    --p_delivery_number IN     VARCHAR2,
    p_trip_id    in number,
    p_printer_name    IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    x_return_status   IN OUT VARCHAR2,
    x_msg_data        IN OUT VARCHAR2
   );

PROCEDURE shipping_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    --p_delivery_number IN     VARCHAR2
    p_trip_id    in number
   );

FUNCTION pick_release_label
   (--p_delivery_id        VARCHAR2, --commented by Awaiz on 15.feb.2018
    p_trip_id            VARCHAR2, --added by Awaiz on 15.feb.2018
    p_pick_slip_num_low  VARCHAR2,
    p_pick_slip_num_high VARCHAR2,
    p_order_type_id      VARCHAR2,
    p_order_num_low      VARCHAR2,
    p_order_num_high     VARCHAR2,
    p_move_order_low     VARCHAR2,
    p_move_order_high    VARCHAR2,
    p_freight_code       VARCHAR2,
    p_organization_id    VARCHAR2,
    p_customer_id        VARCHAR2,
    p_pick_status        VARCHAR2,
    p_detail_date_low    VARCHAR2,
    p_detail_date_high   VARCHAR2,
    p_item_display       VARCHAR2,
    p_item_flex_code     VARCHAR2,
    p_locator_flex_code  VARCHAR2,
    p_printer_name       VARCHAR2
   ) RETURN BOOLEAN;

FUNCTION aft_mkt_shiping_label
   (--p_delivery_id        VARCHAR2,
    p_trip_id        varchar2,
    p_pick_slip_num_low  VARCHAR2,
    p_pick_slip_num_high VARCHAR2,
    p_order_type_id      VARCHAR2,
    p_order_num_low      VARCHAR2,
    p_order_num_high     VARCHAR2,
    p_move_order_low     VARCHAR2,
    p_move_order_high    VARCHAR2,
    p_freight_code       VARCHAR2,
    p_organization_id    VARCHAR2,
    p_customer_id        VARCHAR2,
    p_pick_status        VARCHAR2,
    p_detail_date_low    VARCHAR2,
    p_detail_date_high   VARCHAR2,
    p_item_display       VARCHAR2,
    p_item_flex_code     VARCHAR2,
    p_locator_flex_code  VARCHAR2,
    p_printer_name       VARCHAR2
   ) RETURN BOOLEAN;

PROCEDURE pick_rel_part_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_delivery_id     IN     VARCHAR2,
    p_pick_rel_req_id IN     NUMBER,
    p_exclude_fg      IN     VARCHAR2
   );

PROCEDURE wip_non_job_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_item_number     IN     VARCHAR2,
    p_quantity        IN     NUMBER
   );

FUNCTION get_item_cross_ref
   (p_cross_ref_type       IN     VARCHAR2,
    p_inventory_item_id    IN     NUMBER,
    p_organization_id      IN     NUMBER
   ) RETURN VARCHAR2;

PROCEDURE shop_box_tag
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_organization_id    IN  NUMBER,
    p_job_name                IN  VARCHAR2,
    p_qty_print          IN  NUMBER,
    p_job_release_date   IN  DATE
   );

--FOX_DAZ_P2
--Procedure for Kanban Label
PROCEDURE kanban_label
   (errbuf                 OUT VARCHAR2,
    retcode                OUT NUMBER,
    p_organization_id   IN     NUMBER,
    p_printer           IN     VARCHAR2,
    p_no_of_copies      IN     NUMBER,
    p_item              IN     VARCHAR2,
    p_kanban_cnum_from  IN     VARCHAR2,
    p_kanban_cnum_to    IN     VARCHAR2
    --p_kanban_size       IN     NUMBER
   );
--FOX_DAZ_P2

PROCEDURE shop_box_tag_label_wv1
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_inv_org_id         IN  NUMBER,
    p_job_number         IN  VARCHAR2,
    p_qty_to_print       IN  NUMBER,
    p_job_release_date   IN  VARCHAR2
   );
  function get_shop_box_tag_op_details(p_wip_entity_id number, p_op_rec in number) return varchar2;
END XXFOX_LABEL_UTIL_PKG;
/
