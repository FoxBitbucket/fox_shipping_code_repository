CREATE OR REPLACE PACKAGE BODY APPS.XXFOX_SHIPPING_INTERFACE_PKG AS
g_freight_cost_type_id   CONSTANT NUMBER := 141;
g_api_version            CONSTANT NUMBER := 1.0;
g_init_message_list      CONSTANT VARCHAR2(30) := NULL;
PROCEDURE update_delivery_api
   (p_track_rec          IN     track_rec,
    x_return_status      IN OUT VARCHAR2,
    x_msg_data           IN OUT VARCHAR2
   )
IS

l_del_rec           WSH_DELIVERIES_PUB.Delivery_Pub_Rec_Type;
l_delivery_id       NUMBER;
l_delivery_number   VARCHAR2(30);
l_delivery_id_out       NUMBER;
l_delivery_number_out   VARCHAR2(30);
l_dlv_gross_weight  NUMBER;
l_dlv_waybill       VARCHAR2(30);
l_dlv_add_ship_info VARCHAR2(500);
l_dlv_status_code   VARCHAR2(30);

l_freight_cost_id   NUMBER;
l_dlv_freight_cost  NUMBER;
l_action_code       VARCHAR2(30);
l_freight_cost      wsh_freight_costs_pub.pubfreightcostrectype;

l_return_status     VARCHAR2(1);
l_msg_count         NUMBER;
l_msg_data          VARCHAR2(2000);

l_trip_id number;
l_trip_weight number;
l_pallet_count number;
l_box_count number;

BEGIN
DBMS_output.put_line('Pos 1:');

-- check if entered number is trip or delivery.
	begin
	select trip_id
	into l_trip_id
	from wsh_trips
	where to_char(trip_id) = p_track_rec.delivery_number;
	exception
	when others then
		/*begin
		select delivery_id
		into l_delivery_id
		from wsh_new_deliveries
		where name = p_track_rec.delivery_number
        		;
        	exception
        	when others then
		x_msg_data      := 'Error Validating Delivery Number :'||p_track_rec.delivery_number||' Error :'||SQLERRM;
		            x_return_status := wsh_util_core.g_ret_sts_error;
		            RETURN;
    		END;
    		*/
    		null;
    	end;	





if (l_trip_id is null) then
    BEGIN
        SELECT NVL(gross_weight,0), waybill, additional_shipment_info, status_code, delivery_id
        INTO   l_dlv_gross_weight, l_dlv_waybill, l_dlv_add_ship_info, l_dlv_status_code, l_delivery_id
        FROM   wsh_new_deliveries
        WHERE  name = p_track_rec.delivery_number
        ;
    EXCEPTION
        WHEN OTHERS THEN
            x_msg_data      := 'Error Validating Delivery Number :'||p_track_rec.delivery_number||' Error :'||SQLERRM;
            x_return_status := wsh_util_core.g_ret_sts_error;
            RETURN;
    END;

DBMS_output.put_line('Pos 2:');

    IF l_dlv_status_code = 'CL' THEN
        x_return_status := fnd_api.g_ret_sts_success;
        x_msg_data      := 'Cannot update colsed delivery';
        RETURN;
    END IF;

DBMS_output.put_line('Pos 3:');

    IF p_track_rec.void_indicator = 'Y' THEN
        l_del_rec.waybill      := NULL;
        l_del_rec.gross_weight := NULL;
        l_del_rec.additional_shipment_info := NULL;
    ELSE
        l_del_rec.gross_weight := l_dlv_gross_weight + NVL(p_track_rec.weight,0);
        l_del_rec.waybill      := p_track_rec.lead_track_number;

        IF l_dlv_add_ship_info IS NULL THEN
            l_del_rec.additional_shipment_info := p_track_rec.tracking_number||'('||NVL(p_track_rec.weight,0)||')';
        ELSE
            l_del_rec.additional_shipment_info := l_dlv_add_ship_info||','||CHR(13)||p_track_rec.tracking_number||'('||NVL(p_track_rec.weight,0)||')';
        END IF;
    END IF;

DBMS_output.put_line('Pos 4:');


    WSH_DELIVERIES_PUB.Create_Update_Delivery
       (p_api_version_number => g_api_version,
        p_init_msg_list      => FND_API.G_TRUE,
        x_return_status      => l_return_status,
        x_msg_count          => l_msg_count,
        x_msg_data           => l_msg_data,
        p_action_code        => 'UPDATE',
        p_delivery_info      => l_del_rec,
        p_delivery_name      => p_track_rec.delivery_number,
        x_delivery_id        => l_delivery_id_out,
        x_name               => l_delivery_number_out
       );

DBMS_output.put_line('Pos 5:'||l_return_status||':'||l_delivery_id);
    IF (l_return_status <> wsh_util_core.g_ret_sts_success) THEN
        x_msg_data      := 'Error in Validating Delivery Number :'||p_track_rec.delivery_number||' Error :'||SQLERRM;
        x_return_status := wsh_util_core.g_ret_sts_error;
        RETURN;
    END IF;

DBMS_output.put_line('Pos 6:');

    IF p_track_rec.tracking_number = p_track_rec.lead_track_number AND p_track_rec.freight_cost <> 0 THEN
        IF p_track_rec.void_indicator = 'N' THEN
            l_action_code := 'CREATE';

            l_freight_cost.delivery_id      :=  l_delivery_id;
            l_freight_cost.freight_cost_type_id := g_freight_cost_type_id;
            l_freight_cost.currency_code    := 'USD';
            l_freight_cost.unit_amount      := p_track_rec.freight_cost;

DBMS_output.put_line('Pos 7:');

            wsh_freight_costs_pub.create_update_freight_costs
               (p_api_version_number => g_api_version,
                p_init_msg_list      => FND_API.G_TRUE,
                p_commit             => FND_API.G_TRUE,
                x_return_status      => l_return_status,
                x_msg_count          => l_msg_count,
                x_msg_data           => l_msg_data,
                p_pub_freight_costs  => l_freight_cost,
                p_action_code        => l_action_code,
                x_freight_cost_id    => l_freight_cost_id
               );
DBMS_output.put_line('Pos 8:'||l_return_status||':'||l_freight_cost_id);

            IF (l_return_status <> wsh_util_core.g_ret_sts_success) THEN
                x_msg_data      := 'Error in creating freight cost for delivery Number :'||p_track_rec.delivery_number;
                x_return_status := wsh_util_core.g_ret_sts_error;
                RETURN;
            END IF;
            x_return_status := wsh_util_core.g_ret_sts_success;
            RETURN;
        ELSIF p_track_rec.void_indicator = 'Y' THEN
            BEGIN
DBMS_output.put_line('Pos 9:');
                SELECT freight_cost_id,unit_amount
                INTO   l_freight_cost_id, l_dlv_freight_cost
                FROM   wsh_freight_costs
                WHERE  freight_cost_type_id = g_freight_cost_type_id
                AND    delivery_id = l_delivery_id
                ;

                l_freight_cost.freight_cost_id  := l_freight_cost_id;
                l_freight_cost.delivery_id      := l_delivery_id;
DBMS_output.put_line('Pos 10:');
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    x_return_status := wsh_util_core.g_ret_sts_success;
                    RETURN;
                WHEN OTHERS THEN
                    x_msg_data      := 'Error while validating cost record :'||p_track_rec.delivery_number||' Error :'||SQLERRM;
                    x_return_status := wsh_util_core.g_ret_sts_error;
                    RETURN;
            END;

DBMS_output.put_line('Pos 11:');
            wsh_freight_costs_pub.delete_freight_costs
               (p_api_version_number => g_api_version,
                p_init_msg_list      => FND_API.G_TRUE,
                p_commit             => FND_API.G_FALSE,
                x_return_status      => l_return_status,
                x_msg_count          => l_msg_count,
                x_msg_data           => l_msg_data,
                p_pub_freight_costs  => l_freight_cost
               );
DBMS_output.put_line('Pos 12:'||l_return_status);
            IF (l_return_status <> wsh_util_core.g_ret_sts_success) THEN
                x_msg_data      := 'Error in deleting freight cost for delivery Number :'||p_track_rec.delivery_number;
                x_return_status := wsh_util_core.g_ret_sts_error;
                RETURN;
            END IF;
            x_return_status := wsh_util_core.g_ret_sts_success;
        ELSE
            x_msg_data      := 'Invalid void indivator value:'||p_track_rec.void_indicator;
            x_return_status := wsh_util_core.g_ret_sts_error;
            RETURN;
        END IF;
    END IF;
else -- for trip id

for rec in (select delivery_id,name delivery_number, status_code, additional_shipment_info from WSH_TRIP_DELIVERIES_V
		where trip_id  = l_trip_id
		and status_code != 'CL') loop



DBMS_output.put_line('Pos 2:');


    IF p_track_rec.void_indicator = 'Y' THEN
        l_del_rec.waybill      := NULL;
        l_del_rec.gross_weight := NULL;
        l_del_rec.additional_shipment_info := NULL;
    ELSE
        --l_del_rec.gross_weight := l_dlv_gross_weight + NVL(p_track_rec.weight,0);
        l_del_rec.waybill      := p_track_rec.lead_track_number;

        IF rec.additional_shipment_info IS NULL THEN
            l_del_rec.additional_shipment_info := p_track_rec.tracking_number||'('||NVL(p_track_rec.weight,0)||')';
        ELSE
            l_del_rec.additional_shipment_info := l_dlv_add_ship_info||','||CHR(13)||p_track_rec.tracking_number||'('||NVL(p_track_rec.weight,0)||')';
        END IF;
    END IF;

DBMS_output.put_line('Pos 4:');


    WSH_DELIVERIES_PUB.Create_Update_Delivery
       (p_api_version_number => g_api_version,
        p_init_msg_list      => FND_API.G_TRUE,
        x_return_status      => l_return_status,
        x_msg_count          => l_msg_count,
        x_msg_data           => l_msg_data,
        p_action_code        => 'UPDATE',
        p_delivery_info      => l_del_rec,
        p_delivery_name      => rec.delivery_number,
        x_delivery_id        => l_delivery_id_out,
        x_name               => l_delivery_number_out
       );

DBMS_output.put_line('Pos 5:'||l_return_status||':'||l_delivery_id);
    IF (l_return_status <> wsh_util_core.g_ret_sts_success) THEN
        x_msg_data      := 'Error in Validating Delivery Number :'||rec.delivery_number||' Error :'||SQLERRM;
        x_return_status := wsh_util_core.g_ret_sts_error;
        RETURN;
    END IF;

DBMS_output.put_line('Pos 6:');


end loop;


    IF p_track_rec.tracking_number = p_track_rec.lead_track_number  THEN
    
    -- update of weights into the audit tables
    		begin
    		select sum(weights)
    		into l_trip_weight
    		from xxfox_shipping_interface_stg
    		where delivery_number = p_track_rec.delivery_number
    		--and lead_track_number = p_track_rec.lead_track_number
    		and void_indicator = p_track_rec.void_indicator;
    		exception
    		when others then
    		l_trip_weight := 0;
    		end;
    		
    		
    		if (l_trip_weight != 0) then
    		
    		begin
    		select count(distinct pallet_number)
    		into l_pallet_count
    		from xxfox_audit_lines
    		where trip_id = l_trip_id
    		and pallet_number is not null;
    		
    		
    		if(l_pallet_count = 0) then
    			begin
		    			select count(distinct box_number)
					    		into l_box_count
					    		from xxfox_audit_lines
					    		where trip_id = l_trip_id
		    		and pallet_number is null
		    		and box_number is not null;
		    		
		    		
		    		exception
		    		when others then
		    			l_box_count := 0;
		    			end;
    		
    		
    		end if;
    		
    		exception
    		when others then
    			l_pallet_count := 0;
    			l_box_count := 0;
    		end;
    			
    		
    		if(l_pallet_count != 0) then
    			update xxfox_audit_lines
    			set pallet_gross_wt = l_trip_weight/l_pallet_count,
    			pallet_net_wt =  l_trip_weight/l_pallet_count
    			where trip_id = l_trip_id
    			and pallet_number is not null;
    			
    		
    		elsif (l_box_count != 0) then
    			update xxfox_audit_lines
			set pallet_gross_wt = l_trip_weight/l_box_count,
    			pallet_net_wt = l_trip_weight/l_box_count
    			where trip_id = l_trip_id
			and pallet_number is  null
			and box_number is not null;
    			
    		end if;
    		
    		end if;
    
    if p_track_rec.freight_cost <> 0 then
    
        IF p_track_rec.void_indicator = 'N' THEN
            l_action_code := 'CREATE';

            l_freight_cost.trip_id      :=  l_trip_id;
            l_freight_cost.freight_cost_type_id := g_freight_cost_type_id;
            l_freight_cost.currency_code    := 'USD';
            l_freight_cost.unit_amount      := p_track_rec.freight_cost;

DBMS_output.put_line('Pos 7:');

            wsh_freight_costs_pub.create_update_freight_costs
               (p_api_version_number => g_api_version,
                p_init_msg_list      => FND_API.G_TRUE,
                p_commit             => FND_API.G_TRUE,
                x_return_status      => l_return_status,
                x_msg_count          => l_msg_count,
                x_msg_data           => l_msg_data,
                p_pub_freight_costs  => l_freight_cost,
                p_action_code        => l_action_code,
                x_freight_cost_id    => l_freight_cost_id
               );
DBMS_output.put_line('Pos 8:'||l_return_status||':'||l_freight_cost_id);

            IF (l_return_status <> wsh_util_core.g_ret_sts_success) THEN
                x_msg_data      := 'Error in creating freight cost for trip :'||p_track_rec.delivery_number||' '||substr(l_msg_Data,1,180);
                x_return_status := wsh_util_core.g_ret_sts_error;
                RETURN;
            END IF;
            x_return_status := wsh_util_core.g_ret_sts_success;
            RETURN;
        ELSIF p_track_rec.void_indicator = 'Y' THEN
            BEGIN
DBMS_output.put_line('Pos 9:');
                SELECT freight_cost_id,unit_amount
                INTO   l_freight_cost_id, l_dlv_freight_cost
                FROM   wsh_freight_costs
                WHERE  freight_cost_type_id = g_freight_cost_type_id
                AND    trip_id = l_trip_id
                ;

                l_freight_cost.freight_cost_id  := l_freight_cost_id;
                l_freight_cost.trip_id      := l_trip_id;
DBMS_output.put_line('Pos 10:');
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    x_return_status := wsh_util_core.g_ret_sts_success;
                    RETURN;
                WHEN OTHERS THEN
                    x_msg_data      := 'Error while validating cost record :'||p_track_rec.delivery_number||' Error :'||SQLERRM;
                    x_return_status := wsh_util_core.g_ret_sts_error;
                    RETURN;
            END;

DBMS_output.put_line('Pos 11:');
            wsh_freight_costs_pub.delete_freight_costs
               (p_api_version_number => g_api_version,
                p_init_msg_list      => FND_API.G_TRUE,
                p_commit             => FND_API.G_FALSE,
                x_return_status      => l_return_status,
                x_msg_count          => l_msg_count,
                x_msg_data           => l_msg_data,
                p_pub_freight_costs  => l_freight_cost
               );
DBMS_output.put_line('Pos 12:'||l_return_status);
            IF (l_return_status <> wsh_util_core.g_ret_sts_success) THEN
                x_msg_data      := 'Error in deleting freight cost for trip :'||p_track_rec.delivery_number;
                x_return_status := wsh_util_core.g_ret_sts_error;
                RETURN;
            END IF;
            x_return_status := wsh_util_core.g_ret_sts_success;
        ELSE
            x_msg_data      := 'Invalid void indivator value:'||p_track_rec.void_indicator;
            x_return_status := wsh_util_core.g_ret_sts_error;
            RETURN;
        END IF;
       end if; 
    END IF;


end if;
    
    x_return_status := wsh_util_core.g_ret_sts_success;
EXCEPTION
    WHEN OTHERS THEN
        x_return_status := wsh_util_core.g_ret_sts_error;
        x_msg_data      := 'Error in procedure update_delivery_api :'||SQLERRM;

END update_delivery_api;

PROCEDURE UPDATE_DELIVERY
   (errbuf          OUT VARCHAR2,
    retcode         OUT NUMBER
   )
IS

CURSOR c_pending_records IS
    SELECT a.rowid row_id,b.delivery_number, a.tracking_number, a.lead_track_number, a.weights, a.freight_cost, a.package_id, a.void_indicator
    FROM   xxfox_shipping_interface_stg a, xxfox_shipping_interface_stg b
    WHERE  a.interface_status is null
    AND    a.lead_track_number = b.tracking_number
    AND    a.void_indicator = b.void_indicator
    --AND    b.delivery_number = '8101'
    --AND    a.lead_track_number = '1Z598A8X0356224353'
    --AND    a.void_indicator = 'N'
    --AND    a.package_id = 2
    ORDER BY b.delivery_number, a.creation_date, a.void_indicator, a.lead_track_number, a.package_id
    ;

TYPE pending_lst IS TABLE OF c_pending_records%ROWTYPE INDEX BY BINARY_INTEGER;
l_tab  pending_lst;

l_track         track_rec;
l_empty_track   track_rec;
l_lead_void_status VARCHAR2(1);
l_return_status VARCHAR2(1);
l_msg_data      VARCHAR2(2000);

e_skip_record   EXCEPTION;
BEGIN

    OPEN  c_pending_records;
    FETCH c_pending_records BULK COLLECT INTO l_tab;
    CLOSE c_pending_records;

    IF l_tab.COUNT > 0 THEN
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'=======================================================================================');
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Delivery                      : Tracking Number               : Void : Pkg ID : Status ');
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'=======================================================================================');
        FOR i IN l_tab.FIRST..l_tab.LAST LOOP
        BEGIN
            l_return_status := NULL;
            l_msg_data      := NULL;

            IF l_tab(i).void_indicator = 'Y' AND l_tab(i).package_id != 0 THEN
                BEGIN
                    SELECT interface_status
                    INTO   l_lead_void_status
                    FROM   xxfox_shipping_interface_stg
                    WHERE  tracking_number = l_tab(i).lead_track_number
                    AND    package_id = 0
                    AND    void_indicator = 'Y'
                    ;
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                        NULL;
                    WHEN OTHERS THEN
                        l_return_status := wsh_util_core.g_ret_sts_success;
                        l_msg_data      := 'Error while validating lead tracking number :'||SQLERRM;
                        RAISE e_skip_record;
                END;
                IF l_lead_void_status = 'X' THEN
                    l_return_status := wsh_util_core.g_ret_sts_success;
                    GOTO VOIDED;
                END IF;
            END IF;

            l_track := l_empty_track;
            l_track.delivery_number := l_tab(i).delivery_number;
            l_track.tracking_number := l_tab(i).tracking_number;
            l_track.lead_track_number := l_tab(i).lead_track_number;
            l_track.package_id      := l_tab(i).package_id;
            l_track.weight          := l_tab(i).weights;
            l_track.freight_cost    := l_tab(i).freight_cost;
            l_track.void_indicator  := l_tab(i).void_indicator;
    dbms_output.put_line('main 10:');
            update_delivery_api
               (p_track_rec     => l_track,
                x_return_status => l_return_status,
                x_msg_data      => l_msg_data
               );
    dbms_output.put_line('main 10:'||l_return_status||':'||l_msg_data);
            <<VOIDED>>
            IF l_return_status = wsh_util_core.g_ret_sts_success AND l_tab(i).void_indicator = 'Y' THEN
                UPDATE xxfox_shipping_interface_stg
                SET    interface_status = 'X'
                WHERE  lead_track_number = l_tab(i).tracking_number
                ;
            ELSE
                IF l_return_status = wsh_util_core.g_ret_sts_error THEN
                    ROLLBACK;
                END IF;
                UPDATE xxfox_shipping_interface_stg
                SET    interface_status = l_return_status
                       ,status_msg = l_msg_data
                WHERE  rowid = l_tab(i).row_id
                ;
            END IF;
        EXCEPTION
            WHEN OTHERS THEN
                l_msg_data := SQLERRM;
                l_return_status := wsh_util_core.g_ret_sts_error;
                UPDATE xxfox_shipping_interface_stg
                SET    interface_status = l_return_status
                       ,status_msg = l_msg_data
                WHERE  rowid = l_tab(i).row_id
                ;
        END;
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,RPAD(l_tab(i).delivery_number,30)||'  '||RPAD(l_tab(i).tracking_number,30)||'  '||RPAD(l_tab(i).void_indicator,5)||'  '||RPAD(l_tab(i).package_id,7)||'  '||l_return_status);

        COMMIT;
        END LOOP;
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'======================================End Of List======================================');
    ELSE
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'No Record to process');
    END IF;
    retcode := 0;
EXCEPTION
    WHEN OTHERS THEN
         l_msg_data := SQLERRM;
         ROLLBACK;
         l_msg_data := 'Error in procedure UPDATE_DELIVERY'||l_msg_data;
         errbuf  := l_msg_data;
         retcode := 2;
END UPDATE_DELIVERY;

FUNCTION get_CUST_UPS_ACCOUNT
   (p_site_use_id    IN    NUMBER
   ) RETURN VARCHAR2
IS

CURSOR c_ups_account Is
    SELECT c.attribute2 freight_accts, instr(c.attribute2, 'UPS '), instr(c.attribute2, ',',instr(c.attribute2, 'UPS '))
    FROM   hz_cust_site_uses_all a,hz_cust_acct_sites_all b, hz_cust_accounts c
    WHERE  a.site_use_id = p_site_use_id
    AND    a.cust_acct_site_id = b.cust_acct_site_id
    AND    b.cust_account_id = c.cust_account_id
    ;
l_freight_accounts         VARCHAR2(240);
l_ups_start_pos            NUMBER;
l_next_freight_acct_pos    NUMBER;
l_ups_account              VARCHAR2(240);
BEGIN
    OPEN  c_ups_account;
    FETCH c_ups_account
    INTO  l_freight_accounts, l_ups_start_pos, l_next_freight_acct_pos;
    CLOSE c_ups_account;

    IF l_freight_accounts IS NOT NULL THEN
        IF l_ups_start_pos > 0 AND l_next_freight_acct_pos = 0 THEN
            l_ups_account := SUBSTR(l_freight_accounts, l_ups_start_pos+4);
        ELSIF l_ups_start_pos > 0 AND l_next_freight_acct_pos > 0 THEN
            l_ups_account := SUBSTR(l_freight_accounts, l_ups_start_pos+4,l_next_freight_acct_pos-1);
        END IF;
    END IF;

    RETURN l_ups_account;
END get_CUST_UPS_ACCOUNT;

FUNCTION get_CUST_acct_UPS_ACCOUNT
   (p_cust_acct_id    IN    NUMBER
   ) RETURN VARCHAR2
IS

CURSOR c_ups_account Is
    SELECT c.attribute2 freight_accts, instr(c.attribute2, 'UPS '), instr(c.attribute2, ',',instr(c.attribute2, 'UPS '))
    FROM   hz_cust_accounts c
    WHERE  c.cust_account_id = p_cust_acct_id
    ;
l_freight_accounts         VARCHAR2(240);
l_ups_start_pos            NUMBER;
l_next_freight_acct_pos    NUMBER;
l_ups_account              VARCHAR2(240);
BEGIN
    OPEN  c_ups_account;
    FETCH c_ups_account
    INTO  l_freight_accounts, l_ups_start_pos, l_next_freight_acct_pos;
    CLOSE c_ups_account;

    IF l_freight_accounts IS NOT NULL THEN
        IF l_ups_start_pos > 0 AND l_next_freight_acct_pos = 0 THEN
            l_ups_account := SUBSTR(l_freight_accounts, l_ups_start_pos+4);
        ELSIF l_ups_start_pos > 0 AND l_next_freight_acct_pos > 0 THEN
            l_ups_account := SUBSTR(l_freight_accounts, l_ups_start_pos+4,l_next_freight_acct_pos-1);
        END IF;
    END IF;

    RETURN l_ups_account;
END get_CUST_acct_UPS_ACCOUNT;

FUNCTION get_CUST_acct_FEDEX_ACCOUNT
   (p_cust_acct_id    IN    NUMBER, p_delivery_id IN NUMBER, p_number in NUMBER
   ) RETURN VARCHAR2
IS

CURSOR c_fedex_account Is
    SELECT   regexp_replace(c.attribute2, '[^0-9]', '')  freight_accts
    FROM   hz_cust_accounts c
    WHERE  c.cust_account_id = p_cust_acct_id
    AND upper(c.attribute2) like '%FEDEX%'
    ;
l_freight_accounts         VARCHAR2(240);
l_fedex_account              VARCHAR2(240);
BEGIN
  BEGIN
	SELECT 	distinct regexp_replace(oh.shipping_instructions, '[^0-9]', '')
	INTO 	l_fedex_account
	FROM 	apps.wsh_new_deliveries wnd, apps.wsh_delivery_assignments wda, apps.wsh_delivery_details wdd, apps.oe_order_headers_all oh
	WHERE	wnd.delivery_id = wda.delivery_id
	and wda.delivery_detail_id = wdd.delivery_detail_id
	and wdd.source_header_id = oh.header_id
	and upper(oh.shipping_instructions) like '%FEDEX%'
	and wda.delivery_id = p_delivery_id;

   EXCEPTION
	WHEN NO_DATA_FOUND THEN
		l_fedex_account := NULL;
	WHEN OTHERS THEN
		l_fedex_account := NULL;
   END;

    IF l_fedex_account is NULL THEN
    	OPEN  c_fedex_account;
    	FETCH c_fedex_account
    	INTO  l_fedex_account;
    	CLOSE c_fedex_account;
    END IF;
    IF l_fedex_account is not NULL then

     	IF p_number =2 THEN
    		RETURN l_fedex_account;
    	ELSE
		RETURN 'FedEx '||l_fedex_account;
    	END IF;
    ELSE
		RETURN NULL;
    END IF;

EXCEPTION
	WHEN NO_DATA_FOUND THEN
		return NULL;
	WHEN OTHERS THEN
		return NULL;
END get_CUST_acct_FEDEX_ACCOUNT;

FUNCTION get_trip_weight
   (p_trip_id    IN    NUMBER
   ) RETURN number
IS
v_weight number;
begin
v_weight := 0;
for rec in (select distinct pallet_number refnum, pallet_gross_wt, pallet_net_wt from xxfox_audit_lines
where pallet_number is not null
and trip_id = p_trip_id
union
select distinct box_number refnum, pallet_gross_wt, pallet_net_wt from xxfox_audit_lines
where pallet_number is null
and trip_id = p_trip_id
) loop
v_weight := v_weight + rec.pallet_gross_wt;
end loop;


return(v_weight);
exception
when others then
return null;
end get_trip_Weight;

END XXFOX_SHIPPING_INTERFACE_PKG;

EXIT;

/
