CREATE OR REPLACE PACKAGE BODY APPS.XXFOX_LABEL_UTIL_PKG AS
/* $Header: XXFOX_ORDSTATUS_PK.pks 1.4 2015/06/11 15:33:48PST Development  $
REM +==========================================================================+
REM |     Copyright (c) 2015 DAZ Systems, Inc .                                |
REM |                  El Segundo, CA 90505                                    |
REM |                All rights reserved                                       |
REM +==========================================================================+
REM
REM NAME
REM  XXFOX_LABEL_UTIL_PKG.pkb
REM
REM PROGRAM TYPE
REM  PL/SQL Package Spec
REM
REM PURPOSE
REM  Label Printing Program
REM
REM ===========================================================================
REM  Date       Author            Activity
REM ===========================================================================
REM  15DEC2015  Mangesh V. Raut   Created
REM  15MAR2015  Mangesh V. Raut   Changed FUNCTION aft_mkt_shiping_label
REM                               Commented 'DEALER' condition.
REM ===========================================================================
*/
G_PROFILE_PRINT_MODE NUMBER;
G_PROFILE_OUT_DIR    VARCHAR2(200);

G_DEFAULT_XML_ENCODING CONSTANT VARCHAR2(50) := 'UTF-8';

PROCEDURE receipt_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_ship_line_id    IN     NUMBER,
    p_quantity        IN     NUMBER
   )
IS
l_return_status   VARCHAR2(1);
l_msg_data        VARCHAR2(4000);
l_where_tab       XXFOX_LABEL_PKG.where_tab;
e_receipt_label   EXCEPTION;
BEGIN


    l_where_tab(1).column_name := 'SHIPMENT_LINE_ID';
    l_where_tab(1).type := 'N';
    l_where_tab(1).value := p_ship_line_id;

    XXFOX_LABEL_PKG.process_label
       (p_organization_id => p_organization_id,
        p_label_format  => 'Receiving Labels',
        p_printer_name  => p_printer,
        p_no_of_copies  => p_no_of_copies,
        p_where_tab     => l_where_tab,
        p_qty_override  => p_quantity,
        x_return_status => l_return_status,
        x_msg_data      => l_msg_data
       );


    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_receipt_label;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_receipt_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure receipt_label :'||SQLERRM;
        retcode := 2;
END receipt_label;

PROCEDURE inventory_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_item_number     IN     VARCHAR2,
    p_quantity        IN     NUMBER
   )
IS
l_return_status   VARCHAR2(1);
l_msg_data        VARCHAR2(4000);
l_where_tab       XXFOX_LABEL_PKG.where_tab;
e_inventory_label   EXCEPTION;
BEGIN

    l_where_tab(1).column_name := 'ORGANIZATION_ID';
    l_where_tab(1).type := 'N';
    l_where_tab(1).value := p_organization_id;

    l_where_tab(2).column_name := 'ITEM_NUMBER';
    l_where_tab(2).type := 'V';
    l_where_tab(2).value := p_item_number;

    XXFOX_LABEL_PKG.process_label
       (p_organization_id => p_organization_id,
        p_label_format  => 'Inventory Label',
        p_printer_name  => p_printer,
        p_no_of_copies  => p_no_of_copies,
        p_where_tab     => l_where_tab,
        p_qty_override  => p_quantity,
        x_return_status => l_return_status,
        x_msg_data      => l_msg_data
       );

    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_inventory_label;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_inventory_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure inventory_label :'||SQLERRM;
        retcode := 2;
END inventory_label;

/* Part Label */
PROCEDURE part_label_api
   (p_organization_id IN     NUMBER,
    p_delivery_number IN     VARCHAR2,
    p_part_number     IN     VARCHAR2,
    p_printer_name    IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    x_return_status   IN OUT VARCHAR2,
    x_msg_data        IN OUT VARCHAR2
   )
IS

l_where_tab       XXFOX_LABEL_PKG.where_tab;

e_part_label_api  EXCEPTION;
BEGIN

    l_where_tab(1).column_name := 'DELIVERY_NUMBER';
    l_where_tab(1).type := 'V';
    l_where_tab(1).value := p_delivery_number;

    IF p_part_number IS NOT NULL THEN
        l_where_tab(2).column_name := 'ITEM_NUMBER';
        l_where_tab(2).type := 'V';
        l_where_tab(2).value := p_part_number;
    END IF;

    XXFOX_LABEL_PKG.process_label
       (p_organization_id => p_organization_id,
        p_label_format    => 'AW Parts Label',
        p_printer_name    => p_printer_name,
        p_no_of_copies    => p_no_of_copies,
        p_where_tab       => l_where_tab,
        p_qty_override    => null,
        x_return_status   => x_return_status,
        x_msg_data        => x_msg_data
       );


    IF x_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_part_label_api;
    END IF;

    x_return_status := fnd_api.g_ret_sts_success;
EXCEPTION
    WHEN e_part_label_api THEN
        x_return_status := fnd_api.g_ret_sts_error;
    WHEN OTHERS THEN
        x_msg_data := 'Error in procedure part_label_api :'||SQLERRM;
        x_return_status := fnd_api.g_ret_sts_error;
END part_label_api;


PROCEDURE part_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_delivery_number IN     VARCHAR2,
    p_part_number     IN     VARCHAR2
   )
IS
l_return_status   VARCHAR2(1);
l_msg_data        VARCHAR2(4000);
e_part_label      EXCEPTION;
BEGIN

    part_label_api
       (p_organization_id => p_organization_id,
        p_delivery_number => p_delivery_number,
        p_part_number     => p_part_number,
        p_printer_name    => p_printer,
        p_no_of_copies    => p_no_of_copies,
        x_return_status   => l_return_status,
        x_msg_data        => l_msg_data
       );

    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_part_label;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_part_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure part_label :'||SQLERRM;
        retcode := 2;
END part_label;

/* Pick Release Parts Label */
PROCEDURE pick_rel_part_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_delivery_id     IN     VARCHAR2,
    p_pick_rel_req_id IN     NUMBER,
    p_exclude_fg      IN     VARCHAR2
   )
IS
l_return_status   VARCHAR2(1);
l_msg_data        VARCHAR2(4000);
l_where_tab       XXFOX_LABEL_PKG.where_tab;

l_request_id        NUMBER;
l_phase             VARCHAR2(100);
l_status            VARCHAR2(100);
l_dev_phase         VARCHAR2(100);
l_dev_status        VARCHAR2(100);
l_conc_message      VARCHAR2(2000);
l_boolean           BOOLEAN;
l_interval          NUMBER;

e_pick_rel_part_label     EXCEPTION;

BEGIN

        FND_FILE.PUT_LINE(FND_FILE.log,'p_exclude_fg :'||p_exclude_fg);
    IF p_pick_rel_req_id IS NOT NULL THEN

        l_phase := 'START';

        WHILE upper(l_phase) != 'COMPLETED'  LOOP
            dbms_lock.sleep(NVL(l_interval,1));
            l_request_id := p_pick_rel_req_id;
            l_boolean := FND_CONCURRENT.GET_REQUEST_STATUS
                            (l_request_id,
                             'WSH',
                             'WSHPSGL',
                             l_phase,
                             l_status,
                             l_dev_phase,
                             l_dev_status,
                             l_conc_message
                           );
            l_interval := 5;
        END LOOP;
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Pick Selection List Generation Request ID :'||p_pick_rel_req_id);
        FND_FILE.PUT_LINE(FND_FILE.OUTPUT,'Status :'||l_phase||':'||l_status||':'||l_conc_message);


    END IF;

    l_where_tab(1).column_name := 'DELIVERY_ID';
    l_where_tab(1).type := 'N';
    l_where_tab(1).value := p_delivery_id;

    l_where_tab(2).column_name := 'RELEASED_STATUS';
    l_where_tab(2).type := 'V';
    l_where_tab(2).value := 'S';

    IF ( p_exclude_fg = 'Y') THEN
       l_where_tab(3).column_name := 'FG_YN';
       l_where_tab(3).type := 'V';
       l_where_tab(3).value := 'N';
    END IF;

    XXFOX_LABEL_PKG.process_label
       (p_organization_id => p_organization_id,
        p_label_format    => 'AW Parts Label',
        p_printer_name    => p_printer,
        p_no_of_copies    => p_no_of_copies,
        p_where_tab       => l_where_tab,
        p_qty_override    => null,
        x_return_status   => l_return_status,
        x_msg_data        => l_msg_data
       );


    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_pick_rel_part_label;
    END IF;

--    x_return_status := fnd_api.g_ret_sts_success;

    retcode := 0;
EXCEPTION
    WHEN e_pick_rel_part_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure pick_rel_part_label :'||SQLERRM;
        retcode := 2;
END pick_rel_part_label;

/****box label *******/

PROCEDURE box_label_api
   (p_organization_id IN     NUMBER,
    p_delivery_number IN     VARCHAR2,
    p_box_number      IN     NUMBER,
    p_printer_name    IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    x_return_status   IN OUT VARCHAR2,
    x_msg_data        IN OUT VARCHAR2
   )
IS

l_where_tab       XXFOX_LABEL_PKG.where_tab;

e_box_label_api  EXCEPTION;
BEGIN


    l_where_tab(1).column_name := 'DELIVERY_NUMBER';
    l_where_tab(1).type := 'V';
    l_where_tab(1).value := p_delivery_number;

    IF p_box_number IS NOT NULL THEN
        l_where_tab(2).column_name := 'BOX_NUMBER';
        l_where_tab(2).type := 'N';
        l_where_tab(2).value := p_box_number;
    END IF;

    XXFOX_LABEL_PKG.process_label
       (p_organization_id => p_organization_id,
        p_label_format    => 'AW Box Label',
        p_printer_name    => p_printer_name,
        p_no_of_copies    => p_no_of_copies,
        p_where_tab       => l_where_tab,
        p_qty_override    => null,
        x_return_status   => x_return_status,
        x_msg_data        => x_msg_data
       );

    IF x_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_box_label_api;
    END IF;

    x_return_status := fnd_api.g_ret_sts_success;
EXCEPTION
    WHEN e_box_label_api THEN
        x_return_status := fnd_api.g_ret_sts_error;
    WHEN OTHERS THEN
        x_msg_data := 'Error in procedure box_label_api :'||SQLERRM;
        x_return_status := fnd_api.g_ret_sts_error;
END box_label_api;

PROCEDURE box_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_delivery_number IN     VARCHAR2,
    p_box_number      IN     NUMBER
   )
IS
l_return_status   VARCHAR2(1);
l_msg_data        VARCHAR2(4000);
e_box_label       EXCEPTION;
BEGIN

    box_label_api
       (p_organization_id => p_organization_id,
        p_delivery_number => p_delivery_number,
        p_box_number      => p_box_number,
        p_printer_name    => p_printer,
        p_no_of_copies    => p_no_of_copies,
        x_return_status   => l_return_status,
        x_msg_data        => l_msg_data
       );

    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_box_label;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_box_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure box_label :'||SQLERRM;
        retcode := 2;
END box_label;

/****pallet label *******/

PROCEDURE pallet_label_api
   (p_organization_id IN     NUMBER,
    p_delivery_number IN     VARCHAR2,
    p_pallet_number   IN     NUMBER,
    p_printer_name    IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    x_return_status   IN OUT VARCHAR2,
    x_msg_data        IN OUT VARCHAR2
   )
IS

l_where_tab       XXFOX_LABEL_PKG.where_tab;

e_pallet_label_api  EXCEPTION;
BEGIN


    l_where_tab(1).column_name := 'DELIVERY_NUMBER';
    l_where_tab(1).type := 'V';
    l_where_tab(1).value := p_delivery_number;

    IF p_pallet_number IS NOT NULL THEN
        l_where_tab(2).column_name := 'PALLET_NUMBER';
        l_where_tab(2).type := 'N';
        l_where_tab(2).value := p_pallet_number;
    END IF;

    XXFOX_LABEL_PKG.process_label
       (p_organization_id => p_organization_id,
        p_label_format    => 'AW Pallet Label',
        p_printer_name    => p_printer_name,
        p_no_of_copies    => p_no_of_copies,
        p_where_tab       => l_where_tab,
        p_qty_override    => null,
        x_return_status   => x_return_status,
        x_msg_data        => x_msg_data
       );
    x_return_status := fnd_api.g_ret_sts_success;
EXCEPTION
    WHEN e_pallet_label_api THEN
        x_return_status := fnd_api.g_ret_sts_error;
    WHEN OTHERS THEN
        x_msg_data := 'Error in procedure pallet_label_api :'||SQLERRM;
        x_return_status := fnd_api.g_ret_sts_error;
END pallet_label_api;

PROCEDURE pallet_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_delivery_number IN     VARCHAR2,
    p_pallet_number   IN     NUMBER
   )
IS
l_return_status   VARCHAR2(1);
l_msg_data        VARCHAR2(4000);
e_pallet_label    EXCEPTION;
BEGIN

    pallet_label_api
       (p_organization_id => p_organization_id,
        p_delivery_number => p_delivery_number,
        p_pallet_number   => p_pallet_number,
        p_printer_name    => p_printer,
        p_no_of_copies    => p_no_of_copies,
        x_return_status   => l_return_status,
        x_msg_data        => l_msg_data
       );

    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_pallet_label;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_pallet_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure pallet_label :'||SQLERRM;
        retcode := 2;
END pallet_label;



PROCEDURE service_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_service_request IN     VARCHAR2
   )
IS
l_return_status   VARCHAR2(1);
l_msg_data        VARCHAR2(4000);
l_where_tab       XXFOX_LABEL_PKG.where_tab;
e_service_label       EXCEPTION;
BEGIN

    l_where_tab(1).column_name := 'INCIDENT_ID';
    l_where_tab(1).type := 'N';
    l_where_tab(1).value := p_service_request;

    XXFOX_LABEL_PKG.process_label
       (p_organization_id => NULL,
        p_label_format  => 'Service Label',
        p_printer_name  => p_printer,
        p_no_of_copies  => p_no_of_copies,
        p_where_tab     => l_where_tab,
        p_qty_override  => null,
        x_return_status => l_return_status,
        x_msg_data      => l_msg_data
       );

    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_service_label;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_service_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure service_label :'||SQLERRM;
        retcode := 2;
END service_label;


PROCEDURE wip_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_job_number      IN     VARCHAR2,
    p_quantity        IN     NUMBER
   )
IS

CURSOR c_format IS
    SELECT  format
    FROM    xxfox_label_wip_v
    WHERE   job_name = p_job_number
    ;

l_format          VARCHAR2(30);
l_label_format    VARCHAR2(240);
l_return_status   VARCHAR2(1);
l_msg_data        VARCHAR2(4000);
l_where_tab       XXFOX_LABEL_PKG.where_tab;
e_wip_label       EXCEPTION;
BEGIN


    OPEN  c_format;
    FETCH c_format INTO l_format;
    CLOSE c_format;

    l_where_tab(1).column_name := 'JOB_NAME';
    l_where_tab(1).type := 'V';
    l_where_tab(1).value := p_job_number;

    IF l_format = 'WIPCOMPLETION.zpl' THEN
        l_label_format := 'Job Completion Label - Manual';
    ELSE
        l_label_format := 'Job Completion Shock Label - Manual';
    END IF;

    XXFOX_LABEL_PKG.process_label
       (p_organization_id => null,
        p_label_format  => l_label_format,
        p_printer_name  => p_printer,
        p_no_of_copies  => p_no_of_copies,
        p_where_tab     => l_where_tab,
        p_qty_override  => p_quantity,
        x_return_status => l_return_status,
        x_msg_data      => l_msg_data
       );

    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_wip_label;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_wip_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure wip_label :'||SQLERRM;
        retcode := 2;
END wip_label;

PROCEDURE kitting_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_tansaction_id   IN     VARCHAR2,
    p_quantity        IN     NUMBER
   )
IS
l_return_status   VARCHAR2(1);
l_msg_data        VARCHAR2(4000);
l_where_tab       XXFOX_LABEL_PKG.where_tab;
e_kitting_label   EXCEPTION;
BEGIN

    l_where_tab(1).column_name := 'TRANSACTION_ID';
    l_where_tab(1).type := 'N';
    l_where_tab(1).value := p_tansaction_id;

    XXFOX_LABEL_PKG.process_label
       (p_organization_id => p_organization_id,
        p_label_format    => 'Kitting Label',
        p_printer_name    => p_printer,
        p_no_of_copies    => p_no_of_copies,
        p_where_tab       => l_where_tab,
        p_qty_override    => p_quantity,
        x_return_status   => l_return_status,
        x_msg_data        => l_msg_data
       );

    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_kitting_label;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_kitting_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure kitting_label :'||SQLERRM;
        retcode := 2;
END kitting_label;


PROCEDURE part_request_indvidual_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_tansaction_id   IN     VARCHAR2,
    p_quantity        IN     NUMBER
   )
IS
l_return_status   VARCHAR2(1);
l_msg_data        VARCHAR2(4000);
l_where_tab       XXFOX_LABEL_PKG.where_tab;
e_part_request_indvidual_label   EXCEPTION;
BEGIN

    l_where_tab(1).column_name := 'TRANSACTION_TEMP_ID';
    l_where_tab(1).type := 'N';
    l_where_tab(1).value := p_tansaction_id;

    XXFOX_LABEL_PKG.process_label
       (p_organization_id => p_organization_id,
        p_label_format    => 'Part Request Individual Label',
        p_printer_name    => p_printer,
        p_no_of_copies    => p_no_of_copies,
        p_where_tab       => l_where_tab,
        p_qty_override    => p_quantity,
        x_return_status   => l_return_status,
        x_msg_data        => l_msg_data
       );

    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_part_request_indvidual_label;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_part_request_indvidual_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure part_request_indvidual_label :'||SQLERRM;
        retcode := 2;
END part_request_indvidual_label;

/* Ship Label */
PROCEDURE shipping_label_api
   (p_organization_id IN     NUMBER,
    --p_delivery_number IN     VARCHAR2,
    p_trip_id    in number,
    p_printer_name    IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    x_return_status   IN OUT VARCHAR2,
    x_msg_data        IN OUT VARCHAR2
   )
IS

l_where_tab       XXFOX_LABEL_PKG.where_tab;

e_ship_label_api  EXCEPTION;
BEGIN

    --l_where_tab(1).column_name := 'DELIVERY_NUMBER';
    --l_where_tab(1).type := 'V';
    --l_where_tab(1).value := p_delivery_number;

    l_where_tab(1).column_name := 'TRIP_ID';
    l_where_tab(1).type := 'N';
    l_where_tab(1).value := p_trip_id;



    XXFOX_LABEL_PKG.process_label
       (p_organization_id => p_organization_id,
        p_label_format    => 'Shipping Label',
        p_printer_name    => p_printer_name,
        p_no_of_copies    => p_no_of_copies,
        p_where_tab       => l_where_tab,
        p_qty_override    => NULL,
        x_return_status   => x_return_status,
        x_msg_data        => x_msg_data
       );
    x_return_status := fnd_api.g_ret_sts_success;
EXCEPTION
    WHEN e_ship_label_api THEN
        x_return_status := fnd_api.g_ret_sts_error;
    WHEN OTHERS THEN
        x_msg_data := 'Error in procedure ship_label_api :'||SQLERRM;
        x_return_status := fnd_api.g_ret_sts_error;
END shipping_label_api;

PROCEDURE SHIPPING_LABEL
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    --p_delivery_number IN     VARCHAR2
    p_trip_id    in number
   )
IS
l_return_status   VARCHAR2(1);
l_msg_data        VARCHAR2(4000);
e_ship_label       EXCEPTION;
BEGIN

    shipping_label_api
       (p_organization_id => p_organization_id,
        --p_delivery_number => p_delivery_number,
        p_trip_id => p_trip_id,
        p_printer_name    => p_printer,
        p_no_of_copies    => p_no_of_copies,
        x_return_status   => l_return_status,
        x_msg_data        => l_msg_data
       );

    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_ship_label;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_ship_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure ship_label :'||SQLERRM;
        retcode := 2;
END shipping_label;

/******Pick release Part Label ************/
FUNCTION pick_release_label
   (--p_delivery_id        VARCHAR2, --commented by awaiz on 15.feb.2018
    p_trip_id            VARCHAR2, --added by awaiz on 15.feb.2018
    p_pick_slip_num_low  VARCHAR2,
    p_pick_slip_num_high VARCHAR2,
    p_order_type_id      VARCHAR2,
    p_order_num_low      VARCHAR2,
    p_order_num_high     VARCHAR2,
    p_move_order_low     VARCHAR2,
    p_move_order_high    VARCHAR2,
    p_freight_code       VARCHAR2,
    p_organization_id    VARCHAR2,
    p_customer_id        VARCHAR2,
    p_pick_status        VARCHAR2,
    p_detail_date_low    VARCHAR2,
    p_detail_date_high   VARCHAR2,
    p_item_display       VARCHAR2,
    p_item_flex_code     VARCHAR2,
    p_locator_flex_code  VARCHAR2,
    p_printer_name       VARCHAR2
   ) RETURN BOOLEAN
IS
errbuf               VARCHAR(2000);
retcode              NUMBER;

l_return_status      VARCHAR2(1);
l_msg_data           VARCHAR2(4000);


l_detail_date_low    DATE;
l_detail_date_high   DATE;
l_where_tab          XXFOX_LABEL_PKG.where_tab;

CURSOR cur_item IS
    SELECT wt.trip_id  --wnd.delivery_id,
          ,(SELECT   mci.customer_item_number
            FROM    mtl_customer_item_xrefs mcix,
                    mtl_customer_items mci
            WHERE  1 = 1
                AND mcix.inventory_item_id = wdd.inventory_item_id--
                AND mcix.master_organization_id = 83
                AND mcix.customer_item_id = mci.customer_item_id
                AND mcix.inactive_flag = 'N'
                AND mcix.preference_number = (SELECT MIN (a.preference_number)
                                           FROM mtl_customer_item_xrefs a
                                          WHERE a.customer_item_id = mcix.customer_item_id)
                AND mci.customer_id = wnd.customer_id --
            ) CUSTOMER_PART_NUMBER --Added by Awaiz on 16.FEB.2018
           ,pap.full_name requestor, msi.segment1 item_number, msi.description item_description, mtrl.organization_id, wdd.inventory_item_id
           ,TO_CHAR(prh.approved_date, 'MM/DD/YYYY') request_date, TO_CHAR(prh.approved_date, 'HH:MI:SS AM') request_time
           ,prl.destination_subinventory subinv, sum(wdd.requested_quantity) qty
           ,(SELECT concatenated_segments
             FROM   MTL_ITEM_LOCATIONS_kfv
             WHERE  inventory_location_id = decode(msi.reservable_type,2,wdd.locator_id,wpsv.locator_id)
            ) from_locator
    FROM   oe_order_headers_all oh, oe_order_lines_all ol, mtl_txn_request_lines mtrl, mtl_txn_request_headers mtrh, mtl_material_transactions_temp wpsv
           , wsh_delivery_details wdd,wsh_delivery_assignments_v wda, wsh_new_deliveries wnd
           , po_requisition_lines_all prl, po_requisition_headers_all prh, per_all_people_f pap
           , mtl_system_items_vl msi
           ,wsh_delivery_legs wdl --added by awaiz on 15.FEB.2018
           ,wsh_trip_stops wtsp--added by awaiz on 15.FEB.2018
           ,wsh_trip_stops wtsd --added by awaiz on 15.FEB.2018
           ,wsh_trips wt --added by awaiz on 15.FEB.2018
    WHERE  oh.source_document_type_id = 10
    AND    oh.header_id= ol.header_id
    AND    wpsv.move_order_line_id = mtrl.line_id
    AND    wpsv.move_order_line_id = mtrl.line_id
    AND    mtrl.header_id = mtrh.header_id
    AND    mtrl.line_id = wdd.move_order_line_id
    AND    wdd.delivery_detail_id = wda.delivery_detail_id
    AND    wda.delivery_id = wnd.delivery_id(+)
    AND    (wnd.delivery_type IS NULL OR wnd.delivery_type = 'STANDARD')
    AND    wdd.source_line_id = ol.line_id
    AND    wdd.source_header_id = ol.header_id
    AND    wdd.released_status='S'
    AND    wdd.source_code = 'OE'
    AND    wdd.container_flag IN ('N', 'Y')
    AND    ol.source_document_type_id = 10
    AND    ol.source_document_line_id = prl.requisition_line_id
    AND    ol.source_document_id = prh.requisition_header_id
    AND    prh.preparer_id = pap.person_id
    AND    SYSDATE BETWEEN pap.effective_start_date ANd pap.effective_end_date
    AND    wdd.inventory_item_id = msi.inventory_item_id
    AND    wdd.organization_id = msi.organization_id
    AND    (p_pick_status = 'A' OR (p_pick_status ='U' AND wdd.released_status = 'S' ) OR (p_pick_status='P' AND wdd.released_status <> 'S' ))
    AND    wpsv.pick_slip_number BETWEEN NVL(p_pick_slip_num_low,wpsv.pick_slip_number) AND NVL(p_pick_slip_num_high,wpsv.pick_slip_number)
    AND    wdd.source_header_type_id  = NVL(p_order_type_id,wdd.source_header_type_id)
    AND    to_number(wdd.source_header_number) BETWEEN  NVL(p_order_num_low,to_number(wdd.source_header_number)) AND NVL(p_order_num_high,to_number(wdd.source_header_number))
    AND    mtrh.request_number BETWEEN NVL(p_move_order_low,mtrh.request_number) AND NVL(p_move_order_high,mtrh.request_number)
    AND    NVL(wdd.ship_method_code,'X') = NVL(p_freight_code,NVL(wdd.ship_method_code,'X'))
    AND    mtrl.organization_id = NVL(p_organization_id,mtrl.organization_id)
    AND    wdd.customer_id = NVL(p_customer_id,wdd.customer_id)
    AND    mtrl.pick_slip_date BETWEEN NVL(l_detail_date_low,mtrl.pick_slip_date)  AND NVL (l_detail_date_high,mtrl.pick_slip_date)
    AND    wdl.delivery_id (+) = wnd.delivery_id  --added by awaiz on 15.FEB.2018
    AND    wtsp.stop_id (+) = wdl.pick_up_stop_id --added by awaiz on 15.FEB.2018
    AND    wtsd.stop_id (+) = wdl.drop_off_stop_id --added by awaiz on 15.FEB.2018
    AND    wt.trip_id (+) = wtsp.trip_id --added by awaiz on 15.FEB.2018
    AND    wt.trip_id = NVL(p_trip_id,wt.trip_id)--added by awaiz on 15.FEB.2018
    --GROUP BY wnd.delivery_id, --Commented by Awaiz on 15.FEB.2018
    GROUP BY wt.trip_id,wdd.inventory_item_id,wnd.customer_id,pap.full_name, msi.segment1, msi.description, mtrl.organization_id, wdd.inventory_item_id, prh.approved_date, prl.destination_subinventory
           ,msi.reservable_type,wdd.locator_id,wpsv.locator_id
      ORDER BY wt.trip_id, 11 --ADDED BY AWAIZ ON 15.FEB.2018
    --ORDER BY wnd.delivery_id, 11  --Commented by Awaiz on 15.FEB.2018
    ;

e_pick_release_label_api  EXCEPTION;
BEGIN

dbms_output.put_line('Part 1');

    l_detail_date_low  :=FND_DATE.CANONICAL_TO_DATE(p_detail_date_low);
    l_detail_date_high :=FND_DATE.CANONICAL_TO_DATE(p_detail_date_high);



    FOR i IN cur_item LOOP
      NULL;
      --+---------------------------------------------------------------------------
        --Commented BY Pratap Tummalapalli on 02-FEB-2018 For BT#32660
        -- Dated:15.FEB.2018, Earlier this code was in commented mode, now based on instructions
        -- from onsite team this was uncommented by Awaiz on 15.FEB.2018
      --+---------------------------------------------------------------------------
        --l_where_tab(1).column_name := 'DELIVERY_ID';--COMMENTED BY AWAIZ ON 15.FEB.2018
        l_where_tab(1).column_name := 'TRIP_ID'; --ADDED BY AWAIZ ON 15.FEB.2018
        l_where_tab(1).type := 'N';
        --l_where_tab(1).value := i.delivery_id; --COMMENTED BY AWAIZ ON 15.FEB.2018
        l_where_tab(1).value := i.trip_id; --ADDED BY AWAIZ ON 15.FEB.2018

        l_where_tab(2).column_name := 'ITEM_NUMBER';
        l_where_tab(2).type := 'V';
        l_where_tab(2).value := i.item_number;

        l_where_tab(3).column_name := 'RELEASED_STATUS';
        l_where_tab(3).type := 'V';
        l_where_tab(3).value := 'S';

        XXFOX_LABEL_PKG.process_label
           (p_organization_id => i.organization_id,
            p_label_format  => 'ISO Part Label',
            p_printer_name  => p_printer_name,
            p_no_of_copies  => 1,
            p_where_tab     => l_where_tab,
            p_qty_override  => null,
            x_return_status => l_return_status,
            x_msg_data      => l_msg_data
           );

        IF l_return_status != fnd_api.g_ret_sts_success THEN
            errbuf := l_msg_data;
            RAISE e_pick_release_label_api;
        END IF;


    END LOOP;
    retcode := 0;
    RETURN TRUE;
EXCEPTION
    WHEN e_pick_release_label_api THEN
        retcode := 2;
        fnd_file.put_line (fnd_file.log, errbuf);
        RETURN FALSE;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure pick_release_label :'||SQLERRM;
        retcode := 2;
        fnd_file.put_line (fnd_file.log, errbuf);
        RETURN FALSE;
END pick_release_label;

/******shiping_label ************/
FUNCTION aft_mkt_shiping_label
   (--p_delivery_id        VARCHAR2,
   p_trip_id        varchar2,
    p_pick_slip_num_low  VARCHAR2,
    p_pick_slip_num_high VARCHAR2,
    p_order_type_id      VARCHAR2,
    p_order_num_low      VARCHAR2,
    p_order_num_high     VARCHAR2,
    p_move_order_low     VARCHAR2,
    p_move_order_high    VARCHAR2,
    p_freight_code       VARCHAR2,
    p_organization_id    VARCHAR2,
    p_customer_id        VARCHAR2,
    p_pick_status        VARCHAR2,
    p_detail_date_low    VARCHAR2,
    p_detail_date_high   VARCHAR2,
    p_item_display       VARCHAR2,
    p_item_flex_code     VARCHAR2,
    p_locator_flex_code  VARCHAR2,
    p_printer_name       VARCHAR2
   ) RETURN BOOLEAN
IS
errbuf               VARCHAR(2000);
retcode              NUMBER;

l_return_status      VARCHAR2(1);
l_msg_data           VARCHAR2(4000);


l_detail_date_low    DATE;
l_detail_date_high   DATE;
l_where_tab          XXFOX_LABEL_PKG.where_tab;

CURSOR c_aft_mkt_cust IS
    SELECT --wnd.delivery_id, mtrl.organization_id, oh.header_id
    distinct wt.trip_id trip_id, mtrl.organization_id, wnd.delivery_id delivery_id
    FROM   oe_order_headers_all oh, oe_order_lines_all ol, mtl_txn_request_lines mtrl, mtl_txn_request_headers mtrh, mtl_material_transactions_temp wpsv
           , wsh_delivery_details wdd,wsh_delivery_assignments_v wda, wsh_new_deliveries wnd, hz_cust_accounts ca
           , wsh_trips wt, wsh_trip_stops wts, wsh_delivery_legs wdl
    WHERE  oh.header_id= ol.header_id
    AND    wpsv.move_order_line_id = mtrl.line_id
    AND    wpsv.move_order_line_id = mtrl.line_id
    AND    mtrl.header_id = mtrh.header_id
    AND    mtrl.line_id = wdd.move_order_line_id
    AND    wdd.delivery_detail_id = wda.delivery_detail_id
    AND    wda.delivery_id = wnd.delivery_id
    AND    (wnd.delivery_type IS NULL OR wnd.delivery_type = 'STANDARD')
    AND    wdd.source_line_id = ol.line_id
    AND    wdd.source_header_id = ol.header_id
    AND    wdd.released_status='S'
    AND    wdd.source_code = 'OE'
    AND    wdd.container_flag IN ('N', 'Y')
    AND    wnd.customer_id = ca.cust_account_id
    AND wts.stop_id (+) = wdl.pick_up_stop_id
                       AND wdl.delivery_id (+) = wnd.delivery_id
       AND wts.trip_id = wt.trip_id (+)
       --and wt.trip_id = nvl(p_trip_id,wt.trip_id)
       and (p_trip_id is null or wt.trip_id = p_trip_id)
    --AND    ca.sales_channel_code = 'DEALER' --commented as per trouble ticket #
    AND    (p_pick_status = 'A' OR (p_pick_status ='U' AND wdd.released_status = 'S' ) OR (p_pick_status='P' AND wdd.released_status <> 'S' ))
    AND    wpsv.pick_slip_number BETWEEN NVL(p_pick_slip_num_low,wpsv.pick_slip_number) AND NVL(p_pick_slip_num_high,wpsv.pick_slip_number)
    AND    wdd.source_header_type_id  = NVL(p_order_type_id,wdd.source_header_type_id)
    AND    to_number(wdd.source_header_number) BETWEEN  NVL(p_order_num_low,to_number(wdd.source_header_number)) AND NVL(p_order_num_high,to_number(wdd.source_header_number))
    AND    mtrh.request_number BETWEEN NVL(p_move_order_low,mtrh.request_number) AND NVL(p_move_order_high,mtrh.request_number)
    AND    NVL(wdd.ship_method_code,'X') = NVL(p_freight_code,NVL(wdd.ship_method_code,'X'))
    AND    mtrl.organization_id = NVL(p_organization_id,mtrl.organization_id)
    AND    wdd.customer_id = NVL(p_customer_id,wdd.customer_id)
    ;

e_aft_mkt_shiping_label_api  EXCEPTION;
BEGIN

dbms_output.put_line('Part 1');

    l_detail_date_low  :=FND_DATE.CANONICAL_TO_DATE(p_detail_date_low);
    l_detail_date_high :=FND_DATE.CANONICAL_TO_DATE(p_detail_date_high);



    FOR i IN c_aft_mkt_cust LOOP
    
    l_where_tab.delete;

fnd_file.put_line (fnd_file.log, 'Trip: '||i.trip_id||', Delivery: '||i.delivery_id);

        --l_where_tab(1).column_name := 'DELIVERY_ID';
        --l_where_tab(1).type := 'N';
        --l_where_tab(1).value := i.delivery_id;
        if (i.trip_id is not null) then

        l_where_tab(1).column_name := 'TRIP_ID';
        l_where_tab(1).type := 'N';
        
        
        l_where_tab(1).value := i.trip_id;
        
        else
        l_where_tab(1).column_name := 'DELIVERY_ID';
        l_where_tab(1).type := 'N';
        l_where_tab(1).value := i.delivery_id;
        end if;

        --l_where_tab(2).column_name := 'SALES_ORDER_HEADER_ID';
        --l_where_tab(2).type := 'N';
        --l_where_tab(2).value := i.header_id;

        XXFOX_LABEL_PKG.process_label
           (p_organization_id => i.organization_id,
            p_label_format  => 'Shipping Label',
            p_printer_name  => p_printer_name,
            p_no_of_copies  => 1,
            p_where_tab     => l_where_tab,
            p_qty_override  => null,
            x_return_status => l_return_status,
            x_msg_data      => l_msg_data
           );

        IF l_return_status != fnd_api.g_ret_sts_success THEN
            errbuf := l_msg_data;
            RAISE e_aft_mkt_shiping_label_api;
        END IF;

if(i.trip_id is null) then
    l_where_tab(1).column_name := 'DELIVERY_ID';
    l_where_tab(1).type := 'N';
    l_where_tab(1).value := i.delivery_id;

    l_where_tab(2).column_name := 'RELEASED_STATUS';
    l_where_tab(2).type := 'V';
    l_where_tab(2).value := 'S';


    XXFOX_LABEL_PKG.process_label
       (p_organization_id => i.organization_id,
        p_label_format    => 'AW Parts Label',
        p_printer_name    => p_printer_name,
        p_no_of_copies    => 1,
        p_where_tab       => l_where_tab,
        p_qty_override    => null,
        x_return_status   => l_return_status,
        x_msg_data        => l_msg_data
       );


    IF l_return_status != fnd_api.g_ret_sts_success THEN
    errbuf:='Error in AW Parts Label call: '||l_msg_data;
        RAISE e_aft_mkt_shiping_label_api;
    END IF;

end if;

    END LOOP;
    retcode := 0;
    RETURN TRUE;
EXCEPTION
    WHEN e_aft_mkt_shiping_label_api THEN
        retcode := 2;
        fnd_file.put_line (fnd_file.log, errbuf);
        RETURN FALSE;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure aft_mkt_shiping_label :'||SQLERRM;
        retcode := 2;
        fnd_file.put_line (fnd_file.log, errbuf);
        RETURN FALSE;
END aft_mkt_shiping_label;


PROCEDURE wip_non_job_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_item_number     IN     VARCHAR2,
    p_quantity        IN     NUMBER
   )
IS

CURSOR c_format IS
    SELECT  format
    FROM    xxfox_label_non_job_wip_v
    WHERE   assembly_item_name = p_item_number
    AND     organization_id = p_organization_id
    ;

l_format          VARCHAR2(30);
l_label_format    VARCHAR2(240);
l_return_status   VARCHAR2(1);
l_msg_data        VARCHAR2(4000);
l_where_tab       XXFOX_LABEL_PKG.where_tab;
e_wip_label       EXCEPTION;
BEGIN


    OPEN  c_format;
    FETCH c_format INTO l_format;
    CLOSE c_format;

    l_where_tab(1).column_name := 'ASSEMBLY_ITEM_NAME';
    l_where_tab(1).type := 'V';
    l_where_tab(1).value := p_item_number;


    l_where_tab(2).column_name := 'ORGANIZATION_ID';
    l_where_tab(2).type := 'N';
    l_where_tab(2).value := p_organization_id;

    IF l_format = 'WIPCOMPLETION.zpl' THEN
        l_label_format := 'Non Job WIP Label - Manual';
    ELSE
        l_label_format := 'Non Job WIP Shock Label - Manual';
    END IF;

    XXFOX_LABEL_PKG.process_label
       (p_organization_id => null,
        p_label_format  => l_label_format,
        p_printer_name  => p_printer,
        p_no_of_copies  => p_no_of_copies,
        p_where_tab     => l_where_tab,
        p_qty_override  => p_quantity,
        x_return_status => l_return_status,
        x_msg_data      => l_msg_data
       );

    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_wip_label;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_wip_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure wip_label :'||SQLERRM;
        retcode := 2;
END wip_non_job_label;

FUNCTION get_item_cross_ref
   (p_cross_ref_type       IN     VARCHAR2,
    p_inventory_item_id    IN     NUMBER,
    p_organization_id      IN     NUMBER
   ) RETURN VARCHAR2
IS
CURSOR c_item_ref IS
    SELECT cross_reference
    FROM   mtl_cross_references_vl
    WHERE  cross_reference_type = p_cross_ref_type
    AND    inventory_item_id    = p_inventory_item_id
    AND    ((org_independent_flag = 'N' AND organization_id = p_organization_id)
            OR
            (org_independent_flag = 'Y' AND organization_id IS NULL)
           )
    ORDER BY CASE WHEN org_independent_flag = 'N' AND organization_id = p_organization_id THEN 1
                  WHEN org_independent_flag = 'Y' AND organization_id IS NULL THEN 2
                  ELSE 3
             END
    ;

l_cross_ref    VARCHAR2(200);
BEGIN
    OPEN  c_item_ref;
    FETCH c_item_ref INTO l_cross_ref;
    CLOSE c_item_ref;

    RETURN l_cross_ref;
EXCEPTION
    WHEN OTHERS THEN
        RETURN l_cross_ref;
END get_item_cross_ref;

PROCEDURE traveler_label
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_printer         IN     VARCHAR2,
    p_no_of_copies    IN     NUMBER,
    p_organization_id IN     NUMBER,
    p_job_number      IN     VARCHAR2,
    p_quantity        IN     NUMBER
   )
IS

CURSOR c_format IS
    SELECT  wip_entity_id
    FROM    XXFOX_WIP_JOBTRAVEL_LABEL_V
    WHERE   wip_entity_name = p_job_number
and organization_id = p_organization_id ;

CURSOR c_printer
    IS
    SELECT printer_name
    FROM  wsh_report_printers wrp
    WHERE wrp.enabled_flag = 'Y'
    AND NVL(wrp.default_printer_flag, 'X')= 'Y'
    AND concurrent_program_id = 9981
    AND level_type_id = '10001';


l_printer         VARCHAR2(100);
l_wip_entity_id         wip_entities.wip_entity_id%TYPE;
l_label_format    VARCHAR2(240);
l_return_status   VARCHAR2(1);
l_msg_data        VARCHAR2(4000);
l_where_tab       XXFOX_LABEL_PKG.where_tab;
e_wip_label       EXCEPTION;
BEGIN


    OPEN  c_format;
    FETCH c_format INTO l_wip_entity_id;
    CLOSE c_format;

        l_where_tab(1).column_name := 'WIP_ENTITY_ID';
        l_where_tab(1).type := 'N';
        l_where_tab(1).value := l_wip_entity_id;


    l_label_format := 'FOX Job Traveler Label';

    IF p_printer is Not null THEN
    l_printer := p_printer;
     ELSE
         OPEN c_printer;
     FETCH c_printer INTO l_printer;
     CLOSE c_printer;

     END IF;

    XXFOX_LABEL_PKG.process_label
       (p_organization_id => p_organization_id,
        p_label_format  => l_label_format,
        p_printer_name  => l_printer,
        p_no_of_copies  => p_no_of_copies,
        p_where_tab     => l_where_tab,
        p_qty_override  => p_quantity,
        x_return_status => l_return_status,
        x_msg_data      => l_msg_data
       );

    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_wip_label;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_wip_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure wip_label :'||SQLERRM;
        retcode := 2;
END traveler_label;

PROCEDURE shop_box_tag
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_organization_id    IN  NUMBER,
    p_job_name           IN  VARCHAR2,
    p_qty_print          IN  NUMBER,
    p_job_release_date   IN  DATE
   )
IS
l_return_status     VARCHAR2(1);
l_msg_data          VARCHAR2(4000);
l_where_tab         XXFOX_LABEL_PKG.where_tab;
l_move_order_type   NUMBER;
l_job_name          VARCHAR2 (240);
e_shop_box_label    EXCEPTION;

BEGIN
   l_where_tab(1).column_name := 'ORGANIZATION_ID';
   l_where_tab(1).type := 'N';
   l_where_tab(1).value := p_organization_id;

   l_where_tab (1).column_name := 'JOB_NAME';
   l_where_tab (1).TYPE := 'V';
   l_where_tab (1).VALUE := p_job_name;

   XXFOX_LABEL_PKG.process_label (p_organization_id   => p_organization_id,
                                  p_label_format      => 'Shop Box Tag',
                                  p_printer_name      => NULL,
                                  p_no_of_copies      => p_qty_print, --Default 1
                                  p_where_tab         => l_where_tab,
                                  p_qty_override      => NULL,
                                  x_return_status     => l_return_status,
                                  x_msg_data          => l_msg_data);


    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_shop_box_label;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_shop_box_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure shop_box_tag :'||SQLERRM;
        retcode := 2;
END shop_box_tag;

----FOX_DAZ_P2
--Procedure for Kanban Label
PROCEDURE kanban_label
   (errbuf                 OUT VARCHAR2,
    retcode                OUT NUMBER,
    p_organization_id   IN     NUMBER,
    p_printer           IN     VARCHAR2,
    p_no_of_copies      IN     NUMBER,
    p_item              IN     VARCHAR2,
    p_kanban_cnum_from  IN     VARCHAR2,
    p_kanban_cnum_to    IN     VARCHAR2
    --p_kanban_size       IN     NUMBER
    )
IS
l_return_status   VARCHAR2(1);
l_msg_data        VARCHAR2(4000);
l_where_tab       XXFOX_LABEL_PKG.where_tab;
e_kanban_label    EXCEPTION;
BEGIN

    l_where_tab(1).column_name := 'ORGANIZATION_ID';
    l_where_tab(1).type := 'N';
    l_where_tab(1).value := p_organization_id;


    /*l_where_tab(3).column_name := 'ITEM';
    l_where_tab(3).type := 'R';
    l_where_tab(3).value := ' BETWEEN '||''''||p_item_from||''''||' AND '||''''||p_item_to||'''';*/

    l_where_tab(2).column_name := 'KANBAN_CARD_NUMBER';
    l_where_tab(2).type := 'R';
    l_where_tab(2).value := ' BETWEEN '||''''||p_kanban_cnum_from||''''||' AND '||''''||p_kanban_cnum_to||'''' ;

   /*
    l_where_tab(3).column_name := 'KANBAN_SIZE';
    l_where_tab(3).type := 'N';
    l_where_tab(3).value := p_kanban_size;
    */
    -- Commented above code because no restriction to select data from xxfox_kanban_label_v
    -- on kanban size and we are printing the size in XML for that Kanban Cards.

    IF p_item IS NOT NULL THEN

      l_where_tab(3).column_name := 'ITEM';
    l_where_tab(3).type := 'V';
    l_where_tab(3).value := p_item;

    END IF;


    /*l_where_tab(5).column_name := 'KANBAN_RESOURCE';
    l_where_tab(5).type := 'R';
    l_where_tab(5).value := ' BETWEEN '||''''||p_kanban_reso_from||''''||' AND '||''''||p_kanban_reso_to||''''   ;

    /*l_where_tab(6).column_name := 'KANBAN_CELL';
    l_where_tab(6).type := 'R';
    l_where_tab(6).value := ' BETWEEN '||''''||p_kanban_cell_from||''''||' AND '||''''||p_kanban_cell_to||'''' ;

    /*l_where_tab(7).column_name := 'CARD_STATUS';
    l_where_tab(7).type := 'V';
    l_where_tab(7).value := p_card_status;

    l_where_tab(8).column_name := 'SUPPLY_TYPE';
    l_where_tab(8).type := 'V';
    l_where_tab(8).value := p_supply_type;

    l_where_tab(9).column_name := 'SUBINVENTORY';
    l_where_tab(9).type := 'V';
    l_where_tab(9).value := p_supply_subinv;

    l_where_tab(10).column_name := 'SUPPLY_STATUS';
    l_where_tab(10).type := 'V';
    l_where_tab(10).value := p_supply_status;*/

    XXFOX_LABEL_PKG.process_label
       (p_organization_id => p_organization_id,
        p_label_format    => 'FOX Kanban Format',
        p_printer_name    => p_printer,
        p_no_of_copies    => p_no_of_copies,
        p_where_tab       => l_where_tab,
        p_qty_override    => NULL,
        x_return_status   => l_return_status,
        x_msg_data        => l_msg_data
       );

    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_kanban_label;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_kanban_label THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure kanban_label :'||SQLERRM;
        retcode := 2;
END kanban_label;

PROCEDURE shop_box_tag_label_wv1
   (errbuf               OUT VARCHAR,
    retcode              OUT NUMBER,
    p_inv_org_id         IN  NUMBER,
    p_job_number         IN  VARCHAR2,
    p_qty_to_print       IN  NUMBER,
    p_job_release_date   IN  VARCHAR2
   )
IS
l_return_status     VARCHAR2(1);
l_msg_data          VARCHAR2(4000);
l_org_id            NUMBER;
l_where_tab         XXFOX_LABEL_PKG.where_tab;
e_shop_box_tag_label_wv1 EXCEPTION;

BEGIN
   select organization_id
   into l_org_id
   from mtl_parameters
   where organization_code = 'SV1';
   
   l_where_tab(1).column_name := 'ORGANIZATION_ID';
   l_where_tab(1).type := 'N';
   l_where_tab(1).value := l_org_id;

   l_where_tab (2).column_name := 'JOB_NAME';
   l_where_tab (2).TYPE := 'V';
   l_where_tab (2).VALUE := p_job_number;

   XXFOX_LABEL_PKG.process_label (p_organization_id   => p_organization_id,
                                  p_label_format      => 'FOX Shop Box Tag WV1',
                                  p_printer_name      => fnd_profile.value('PRINTER'),
                                  p_no_of_copies      => p_qty_to_print, --Default 1
                                  p_where_tab         => l_where_tab,
                                  p_qty_override      => NULL,
                                  x_return_status     => l_return_status,
                                  x_msg_data          => l_msg_data);


    IF l_return_status != fnd_api.g_ret_sts_success THEN
        RAISE e_shop_box_tag_label_wv1;
    END IF;
    retcode := 0;
EXCEPTION
    WHEN e_shop_box_tag_label_wv1 THEN
        retcode := 2;
        errbuf  := l_msg_data;
    WHEN OTHERS THEN
        errbuf := 'Error in procedure shop_box_tag_label_wv1 :'||SQLERRM;
        retcode := 2;
END shop_box_tag_label_wv1;

function get_shop_box_tag_op_details(p_wip_entity_id number, p_op_rec in number) return varchar2
is
 v_ret varchar2(255);
begin
    select ret into v_ret from
    (select wo.operation_seq_num || '~' || bso.operation_code || '~'|| bso.operation_description || '~'|| br.resource_code ret, rownum op_rec
      from wip_operations wo, wip_operation_resources wor, bom_resources br, bom_standard_operations bso
     where wo.wip_entity_id = wor.wip_entity_id
       and wo.organization_id = wor.organization_id
       and wo.operation_seq_num = wor.operation_seq_num
       and wo.standard_operation_id = bso.standard_operation_id (+)
       and wor.scheduled_flag = 1
       AND wor.resource_id = br.resource_id
       and wo.wip_entity_id = p_wip_entity_id
       order by wo.operation_seq_num )
   where op_rec = p_op_rec;
   return v_ret;
end;
----FOX_DAZ_P2

END XXFOX_LABEL_UTIL_PKG;
/
