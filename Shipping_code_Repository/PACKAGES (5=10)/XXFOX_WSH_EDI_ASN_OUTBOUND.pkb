CREATE OR REPLACE PACKAGE BODY XXFOX_WSH_EDI_ASN_OUTBOUND_PKG AS
--
-- $Id$
--
/*
   -- =========================================================
   -- Copyright (c) 2012 FOX RIDE Inc.
   -- All rights reserved.
   -- =========================================================
   -- NAME: XXFOX_WSH_EDI_ASN_OUTBOUND_PKG
   -- Design R�f�rence : 
   -- PROGRAM TYPE: Package
   -- PURPOSE: This script will used to generate a data file(Outbound) as per format given by FOX RIDE
   -- PARAMETERS:
   -- CALLS:
   -- NOTES:
   -- CHANGE HISTORY:
   -- =========================================================
   -- Date         Author             Activity
   -- =========================================================
   -- 07-MAR-2018 Mohammad.Awaizsiddiq  Initial Creation
   -- =========================================================
   */

    PROCEDURE main_call (
        errbuf       OUT VARCHAR2,
        retcode      OUT VARCHAR2,
        p_trip_id    IN VARCHAR2
        
    ) IS
   
   
   --+--------------------------------------------------+--
   -- Cursor Declaration for 1st 2 Header Lines
   --+--------------------------------------------------+--  

        CURSOR c_rec_0010_0020 IS
            SELECT
                wdl.delivery_id,
                wnd.organization_id,
/*--Record -0010--*/
                wts.trip_id,
                TO_CHAR(wnd.initial_pickup_date,'MMDDYYYY HH24MISS ') ship_date,
                hao.name org_name,
                mp.organization_code org_code,
                hp.party_name customer_name,
                hcas.ece_tp_location_code customer_code,
                TO_CHAR(wnd.initial_pickup_date,'MMDDYYYY HH24MISS ') actual_ship_date,--delivery date
/*--Record -0020--*/
                nvl(
                    wt.attribute7,
                    wbr.bill_of_lading_number
                ) bol,
                wnd.waybill tracking_num,
                doc.sequence_number pack_slip_num,
                wc.scac_code,
                initcap(wnd.mode_of_transport) transport_method,--not yet confirmed
                wc.freight_code carrier_name
            FROM
                wsh_delivery_legs wdl,
                wsh_trip_stops wts,
                wsh_new_deliveries wnd,
                wsh_trips wt,
                mtl_parameters mp,
                hr_all_organization_units hao,
                hz_locations hl,
                hz_party_sites hps,
                hz_cust_acct_sites_all hcas,
                hz_cust_accounts hca,
                hz_parties hp,
                wsh_bols_rd_v wbr,
                wsh_document_instances doc,
                wsh_carriers wc
            WHERE
                    1 = 1
                AND
                    wts.stop_id = wdl.pick_up_stop_id
                AND
                    wdl.delivery_id = wnd.delivery_id
                AND
                    wts.trip_id = wt.trip_id
                AND
                    wt.trip_id = p_trip_id--38891 
                AND
                    mp.organization_id = wnd.organization_id
                AND
                    hao.organization_id = wnd.organization_id
                AND
                    hl.location_id = wnd.ultimate_dropoff_location_id
                AND
                    hl.location_id = hps.location_id
                AND
                    hps.party_site_id = hcas.party_site_id
                AND
                    hcas.cust_account_id = hca.cust_account_id
                AND
                    hp.party_id = hca.party_id
                AND
                    wnd.delivery_id = wbr.delivery_id (+)
                AND
                    doc.entity_id (+) = wnd.delivery_id
                AND
                    doc.entity_name (+) = 'WSH_NEW_DELIVERIES'
                AND
                    doc.document_type (+) = 'PACK_TYPE'
                AND
                    wc.carrier_id = wt.carrier_id
                AND
                    ROWNUM < 2;
--cursor 0025  distinct SSCC CODE
        CURSOR c_rec_0025 ( p_trip_id IN NUMBER ) IS
            SELECT DISTINCT
                po_number,
                trip_id
            FROM
                xxfox_audit_data_for_edi_tbl
            WHERE
                trip_id = p_trip_id
            ORDER BY po_number ASC;

--cursor 0030  distinct SSCC CODE

        CURSOR c_rec_0030 ( p_po_number IN VARCHAR2 ) IS
            SELECT DISTINCT
                sscc_code,
                pallet_number,
                box_number
            FROM
                xxfox_audit_data_for_edi_tbl
            WHERE
                po_number = p_po_number
            ORDER BY po_number ASC;
   
--cursor 0040

        CURSOR c_rec_0040 ( p_sscc_code IN VARCHAR2 ) IS
            SELECT DISTINCT
                inventory_item_id,
                order_number,
                SUM(consumed_qty) qty
            FROM
                xxfox_audit_data_for_edi_tbl
            WHERE
                sscc_code = p_sscc_code
            GROUP BY inventory_item_id,order_number
            ;   
   --+--------------------------------------------------+--
   -- VARIABLE DECLARATIONS
   --+--------------------------------------------------+--

        x_file_name              VARCHAR2(500) := NULL;
        x_file_path              VARCHAR2(1000) := NULL;
        x_record_0010            VARCHAR2(100) := '0010';
        x_record_0020            VARCHAR2(100) := '0020';
        x_record_0025            VARCHAR2(100) := '0025';
        x_record_0030            VARCHAR2(100) := '0030';
        x_record_0040            VARCHAR2(100) := '0040';
        x_ship_date_timestamp    VARCHAR2(100) := NULL;
        x_actual_ship_date       VARCHAR2(100) := NULL;
        x_tracking_num           VARCHAR2(1000) := NULL;
        x_transport_method       VARCHAR2(1000) := NULL;
        x_bol                    VARCHAR2(1000) := NULL;
        x_pallet_number          VARCHAR2(1000) := NULL;
        x_box_number             VARCHAR2(1000) := NULL;
        x_organization_id        NUMBER := 0;
        x_customer_id            NUMBER := 0;
        x_customer_part_number   VARCHAR2(1000) := NULL;
        x_consumed_qty           VARCHAR2(1000) := NULL;
        x_vendor_item_number     VARCHAR2(1000) := NULL;
        x_line_0010              VARCHAR2(4000) := NULL;
        x_line_0020              VARCHAR2(4000) := NULL;
        x_line_0025              VARCHAR2(4000) := NULL;
        x_line_0030              VARCHAR2(4000) := NULL;
        x_line_0040              VARCHAR2(4000) := NULL;
        x_file_id                utl_file.file_type;
        x_count                  NUMBER DEFAULT 0;
        x_rec_count              NUMBER DEFAULT 0;
    BEGIN
        fnd_file.put_line(
            fnd_file.log,
            ' Program Started '
             || TO_CHAR(SYSDATE,'YYYYMONDD  HH24:MI:SS')
        );

        fnd_file.put_line(fnd_file.log,' ---------------------------------------------------------------------------');
        fnd_file.put_line(fnd_file.log,' Input Parameters  :~ ');
        fnd_file.put_line(
            fnd_file.log,
            ' p_trip_id     :~ ' || p_trip_id
        );
        fnd_file.put_line(fnd_file.log,' ---------------------------------------------------------------------------');                                     

  --+---------------------------------
  --+ DERIVING FILE NAME 
  --+---------------------------------
        BEGIN
            SELECT
                'DSNO_CUSTOM_'
                 || XXFOX_EDI_ASN_FILENAME_S.NEXTVAL
                 || '.'
                 || 'dat'
            INTO
                x_file_name
            FROM
                sys.dual;

        END;

        fnd_file.put_line(
            fnd_file.log,
            ' FILE NAME    :~ ' || x_file_name
        ); 


  --+-------------------------------------------------------+--
  --+ Deriving directory path to generate the .dat file
  --+-------------------------------------------------------+--
/*
  BEGIN
  SELECT directory_path INTO x_file_path FROM all_directories  WHERE directory_name = 'EDI_OUT';
  END;
*/


/*--------------------------------------------------------------
  Setting Directory Path to Generate the Output File 
----------------------------------------------------------------*/
        x_file_path := 'ODPDIR';--Used for Testing
        fnd_file.put_line(
            fnd_file.log,
            ' FILE PATH    :~ ' || x_file_path
        );
        
/*--------------------------------------------------------------
  OPENIG FILE TO WRITE THE DATA 
----------------------------------------------------------------*/        
        x_file_id := utl_file.fopen(x_file_path,x_file_name,'W');
        
        
        FOR i_rec_0010_0020 IN c_rec_0010_0020 LOOP
        
            x_count := x_count + 1;
            x_rec_count := x_rec_count + 1;
            x_organization_id := i_rec_0010_0020.organization_id;
            
            BEGIN
                SELECT
                    substr(
                        i_rec_0010_0020.ship_date,
                        5,
                        4
                    )
                     || substr(
                        i_rec_0010_0020.ship_date,
                        1,
                        2
                    )
                     || substr(
                        i_rec_0010_0020.ship_date,
                        3,
                        2
                    )
                     || substr(
                        i_rec_0010_0020.ship_date,
                        9,
                        7
                    ) ship_date_timestamp,
                    substr(
                        i_rec_0010_0020.ship_date,
                        5,
                        4
                    )
                     || substr(
                        i_rec_0010_0020.ship_date,
                        1,
                        2
                    )
                     || substr(
                        i_rec_0010_0020.ship_date,
                        3,
                        2
                    ) actual_ship_date
                INTO
                    x_ship_date_timestamp,x_actual_ship_date
                FROM
                    sys.dual;

            END;

            x_bol := i_rec_0010_0020.bol;           


            x_tracking_num:=i_rec_0010_0020.tracking_num;
            
            IF x_tracking_num IS NOT NULL
            THEN
                x_transport_method:='P';
            ELSE
                x_transport_method:='M';
            END IF;

/*--------------------------------------------------------------
  Writing RECORDS 0010 and 0020
----------------------------------------------------------------*/
            IF
                x_count = 1
            THEN
                utl_file.put_line(
                    x_file_id,
                    x_record_0010
                     || '~'
                     || i_rec_0010_0020.trip_id
                     || '~'
                     || x_ship_date_timestamp
                     || '~'
                     || i_rec_0010_0020.org_name
                     || '~'
                     || i_rec_0010_0020.org_code
                     || '~'
                     || i_rec_0010_0020.customer_name
                     || '~'
                     || i_rec_0010_0020.customer_code
                     || '~'
                     || x_actual_ship_date
                ); --HEADER LINE 1

                utl_file.put_line(
                    x_file_id,
                    x_record_0020
                     || '~'
                     || x_bol
                     || '~'
                     || i_rec_0010_0020.tracking_num
                     || '~'
                     || i_rec_0010_0020.pack_slip_num
                     || '~'
                     || i_rec_0010_0020.scac_code
                     || '~'
                     || x_transport_method
                     || '~'
                     || i_rec_0010_0020.carrier_name
                ); --HEADER LINE 2


            x_line_0010 := x_record_0010
                         || '~'
                         || i_rec_0010_0020.trip_id
                         || '~'
                         || x_ship_date_timestamp
                         || '~'
                         || i_rec_0010_0020.org_name
                         || '~'
                         || i_rec_0010_0020.org_code
                         || '~'
                         || i_rec_0010_0020.customer_name
                         || '~'
                         || i_rec_0010_0020.customer_code
                         || '~'
                         || x_actual_ship_date;

            x_line_0020 := x_record_0020
                         || '~'
                         || x_bol
                         || '~'
                         || i_rec_0010_0020.tracking_num
                         || '~'
                         || i_rec_0010_0020.pack_slip_num
                         || '~'
                         || i_rec_0010_0020.scac_code
                         || '~'
                         || i_rec_0010_0020.transport_method
                         || '~'
                         || i_rec_0010_0020.carrier_name;
             
                fnd_file.put_line(fnd_file.log,x_line_0010);
                fnd_file.put_line(fnd_file.log,x_line_0020);
                
            END IF;
/*--------------------------------------------------------------
  Writing RECORDS 0025
----------------------------------------------------------------*/
            FOR i_rec_0025 IN c_rec_0025(p_trip_id) LOOP
            
                x_rec_count := x_rec_count + 1;
                
                utl_file.put_line(
                    x_file_id,
                    x_record_0020
                     || '~'
                     || i_rec_0025.po_number
                );
                
                x_line_0025 := x_record_0025
                             || '~'
                             || i_rec_0025.po_number;
                 
                fnd_file.put_line(fnd_file.log,x_line_0025);
/*--------------------------------------------------------------
  Writing RECORDS 0030
----------------------------------------------------------------*/                
                
                FOR i_rec_0030 IN c_rec_0030(i_rec_0025.po_number) LOOP
                    
                    x_rec_count := x_rec_count + 1;                
                    x_pallet_number := i_rec_0030.pallet_number;
                    x_box_number := i_rec_0030.box_number;
                    
                    IF
                        x_pallet_number IS NOT NULL 
                    THEN
                        x_pallet_number := 'T';
                    ELSE
                        x_pallet_number := 'P';
                    END IF;

                    utl_file.put_line(
                        x_file_id,
                        x_record_0030
                         || '~'
                         || x_pallet_number
                         || '~'
                         || i_rec_0030.sscc_code
                    );

                    x_line_0030 := x_record_0030
                                 || '~'
                                 || x_pallet_number
                                 || '~'
                                 || i_rec_0030.sscc_code;
                    fnd_file.put_line(fnd_file.log,x_line_0030);
                    
/*--------------------------------------------------------------
  Writing RECORDS 0040
----------------------------------------------------------------*/                        
                    FOR i_rec_0040 IN c_rec_0040(i_rec_0030.sscc_code) LOOP

                    x_rec_count := x_rec_count + 1;    
                    x_consumed_qty:=i_rec_0040.QTY;
                    
                    --+----------------------------
                    -- Deriving Vendor Item Number
                    --+----------------------------
                        BEGIN
                            SELECT
                                segment1
                            INTO
                                x_vendor_item_number
                            FROM
                                mtl_system_items_b
                            WHERE
                                    inventory_item_id = i_rec_0040.inventory_item_id
                                AND
                                    organization_id = x_organization_id
                                AND
                                    ROWNUM < 2;
                        EXCEPTION
                            WHEN OTHERS THEN
                                NULL;
                                 fnd_file.put_line(fnd_file.log,'Exception While Deriving VendorItemNumber :~'||SQLCODE||'~'||SQLERRM);                                    

                        END;

                    --+----------------------------
                    -- Deriving Customer ID
                    --+----------------------------
                        BEGIN
                            SELECT
                                wnd.customer_id
                            INTO
                                x_customer_id
                            FROM
                                oe_order_headers_all ooh,
                                wsh_delivery_details wnd
                            WHERE
                                    1 = 1
                                AND
                                    ooh.header_id = wnd.source_header_id
                                AND
                                    wnd.inventory_item_id = i_rec_0040.inventory_item_id
                                AND
                                    ooh.order_number = i_rec_0040.order_number
                                AND
                                    ROWNUM < 2;
                         EXCEPTION
                            WHEN OTHERS THEN
                                NULL;
                                 fnd_file.put_line(fnd_file.log,'Exception While Deriving CustomerID :~'||SQLCODE||'~'||SQLERRM);                                    

                        END;
                    --+----------------------------
                    -- Deriving Customer Part Number
                    --+----------------------------
                        BEGIN
                            SELECT
                                mci.customer_item_number
                            INTO
                                x_customer_part_number
                            FROM
                                mtl_customer_item_xrefs mcix,
                                mtl_customer_items mci
                            WHERE
                                    1 = 1
                                AND
                                    mcix.inventory_item_id = i_rec_0040.inventory_item_id
                                AND
                                    mcix.master_organization_id = 83
                                AND
                                    mcix.customer_item_id = mci.customer_item_id
                                AND
                                    mcix.inactive_flag = 'N'
                                AND
                                    mcix.preference_number = (
                                        SELECT
                                            MIN(a.preference_number)
                                        FROM
                                            mtl_customer_item_xrefs a
                                        WHERE
                                            a.customer_item_id = mcix.customer_item_id
                                    )
                                AND
                                    mci.customer_id = x_customer_id;

                        EXCEPTION
                            WHEN OTHERS THEN
                                NULL;
                                 fnd_file.put_line(fnd_file.log,'Exception While Deriving CustomerPart# :~'||SQLCODE||'~'||SQLERRM);
                        END;

                        utl_file.put_line(
                            x_file_id,
                            x_record_0040
                             || '~'
                             || x_customer_part_number
                             || '~'
                             || x_vendor_item_number
                             || '~'
                             || x_consumed_qty
                        );

                        x_line_0040 := x_record_0040
                                     || '~'
                                     || x_customer_part_number
                                     || '~'
                                     || x_vendor_item_number
                                     || '~'
                                     || x_consumed_qty;
                                     
                        fnd_file.put_line(fnd_file.log,x_line_0040);
                        
                    END LOOP; --i_rec_0040

                END LOOP;--i_rec_0030

            END LOOP;--i_rec_0025

        END LOOP;--i_rec_0010_0020

/*--------------------------------------------------------------
  Closing the File
----------------------------------------------------------------*/
        utl_file.fclose(x_file_id);
        
        
        fnd_file.put_line(
            fnd_file.output,
            'No of Records transfered to the data file  :' ||  x_rec_count 
        );
        fnd_file.put_line(fnd_file.output,' ');
        fnd_file.put_line(
            fnd_file.output,
            'Submitted User name                        :' || fnd_global.user_name
        );
        fnd_file.put_line(fnd_file.output,' ');
        fnd_file.put_line(
            fnd_file.output,
            'Submitted Responsibility name              :' ||fnd_global.resp_name
        );
        fnd_file.put_line(fnd_file.output,' ');
        fnd_file.put_line(
            fnd_file.output,
            'Submission Date                            :' || TO_CHAR(SYSDATE,'YYYYMMDD  HH24:MI:SS')
        );

    EXCEPTION
        WHEN utl_file.invalid_operation THEN
            fnd_file.put_line(fnd_file.log,'invalid operation');
            utl_file.fclose_all;
        WHEN utl_file.invalid_path THEN
            fnd_file.put_line(fnd_file.log,'invalid path');
            utl_file.fclose_all;
        WHEN utl_file.invalid_mode THEN
            fnd_file.put_line(fnd_file.log,'invalid mode');
            utl_file.fclose_all;
        WHEN utl_file.invalid_filehandle THEN
            fnd_file.put_line(fnd_file.log,'invalid filehandle');
            utl_file.fclose_all;
        WHEN utl_file.read_error THEN
            fnd_file.put_line(fnd_file.log,'read error');
            utl_file.fclose_all;
        WHEN utl_file.internal_error THEN
            fnd_file.put_line(fnd_file.log,'internal error');
            utl_file.fclose_all;
        WHEN OTHERS THEN
            fnd_file.put_line(fnd_file.log,'other error');
            utl_file.fclose_all;
    END main_call;

END XXFOX_WSH_EDI_ASN_OUTBOUND_PKG;
/

EXIT;