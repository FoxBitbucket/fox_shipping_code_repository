CREATE OR REPLACE PACKAGE APPS.XXFOX_WSHRDPAK_XMLP_PKG
   AUTHID CURRENT_USER
AS
   /*REM +==========================================================================+
         REM |     Copyright (c) 2009 DAZ Systems, Inc .                                |
         REM |                  El Segundo, CA 90505                                    |
         REM |                All rights reserved                                       |
         REM +==========================================================================+
         REM
         REM NAME
         REM XXFOX_WSHRDPAK_XMLP_PKG
         REM
         REM PROGRAM TYPE
         REM PACKAGE
         REM
         REM PURPOSE
         REM Packing Slip Report
         REM
         REM HISTORY
         REM ===========================================================================
         REM  Date          Author                        Activity
         REM ===========================================================================
         REM  18/08/2015   Siva Mandapalli               Created
         REM  15/12/2015   Siva Mandapalli                  Modified - Added bursting process
         REM  21/03/2016   Sriram Natarajan              Added new function for gross_wt and net_wt
         REM ===========================================================================*/

   P_CONC_REQUEST_ID         NUMBER;
   P_DELIVERY_ID             NUMBER;
   P_TRIP_ID                 NUMBER;
   P_DELIVERY_DATE_HIGH      DATE;
   P_DELIVERY_DATE_LOW       DATE;
   P_PRINT_CUST_ITEM         VARCHAR2 (1);
   P_ITEM_DISPLAY            VARCHAR2 (1);
   P_PRINT_MODE              VARCHAR2 (30);
   P_SORT                    VARCHAR2 (30);
   P_FREIGHT_CODE            VARCHAR2 (30);
   P_ORGANIZATION_ID         NUMBER;
   P_ITEM_FLEX               VARCHAR2 (999) := ' ';
   P_QUANTITY_PRECISION      NUMBER;
   P_ORDER_BY                VARCHAR2 (999);
   P_DISPLAY_UNSHIPPED       VARCHAR2 (1);
   P_ORGANIZATIONID          VARCHAR2 (40);
   P_ORGANIZATIONID_1        VARCHAR2 (40) := ' ';
   P_ENTITY_NAME_WND         VARCHAR2 (32767);
   P_DOC_TYPE                VARCHAR2 (32767);
   P_FUNC_NAME               VARCHAR2 (32767);
   P_ENTITY_NAME_WDD         VARCHAR2 (32767);
   P_ENTITY_NAME_OEH         VARCHAR2 (32767);
   P_ENTITY_NAME_OED         VARCHAR2 (32767);
   P_CUSTOMER_ITEM_NUMBER    VARCHAR2 (50) := ' ';
   P_TAX_VAT_FLAG            NUMBER;
   CP_INTERNAL_SALES_ORDER   VARCHAR2 (40);
   CP_WAREHOUSE_NAME         VARCHAR2 (240);
   CP_DRAFT_OR_FINAL         VARCHAR2 (80);
   CP_PRINT_DATE             DATE;
   CP_RLM_PRINT_CUM_DATA     VARCHAR2 (3) := 'N';
   CP_SOURCE_CODE            VARCHAR2 (30);
   CP_BILL_TO_CONTACT_ID     NUMBER;
   CP_SHIP_TO_CONTACT_ID     NUMBER;
   CP_BILL_ADDRESS_LINE_1    VARCHAR2 (240);
   CP_BILL_ADDRESS_LINE_2    VARCHAR2 (240);
   CP_BILL_ADDRESS_LINE_3    VARCHAR2 (240);
   CP_BILL_TOWN_OR_CITY      VARCHAR2 (60);
   CP_BILL_REGION            VARCHAR2 (120);
   CP_BILL_POSTAL_CODE       VARCHAR2 (60);
   CP_BILL_COUNTRY           VARCHAR2 (80);
   CP_BILL_ADDRESS_LINE_4    VARCHAR2 (240);
   --STANDALONE CHANGES
   P_STANDALONE              VARCHAR2 (1);
   g_process_flag            VARCHAR2 (1) := 'Y';
   g_email_address   VARCHAR2 (3000) := NULL;

   CP_DELIVERY_ID            NUMBER;
   CP_TRIP_ID                 NUMBER;

   FUNCTION AFTERPFORM
      RETURN BOOLEAN;

   FUNCTION AFTERREPORT
      RETURN BOOLEAN;

   FUNCTION BeforeReport(p_tripid NUMBER)
      RETURN BOOLEAN;

   FUNCTION CF_ITEM_NUMFORMULA (C_ITEM_FLEX     IN VARCHAR2,
                                C_INV_ITEM_ID   IN NUMBER,
                                C_DEL_ORG_ID    IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION CF_FREIGHT_CARRIERFORMULA (C_SHIP_METHOD      IN VARCHAR2,
                                       C_DEL_ORG_ID       IN NUMBER,
                                       C_Q2_DELIVERY_ID   IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION CF_FREIGHT_TERMSFORMULA (C_FREIGHT_TERMS_CODE IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION CF_FOBFORMULA (C_FOB_CODE IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION CF_CUST_ITEM_NUMFORMULA (C_CUSTOMER_ITEM_ID IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION CF_FROM_CITY_STATE_ZIPFORMULA (C_FROM_CITY          IN VARCHAR2,
                                           C_FROM_POSTAL_CODE   IN VARCHAR2,
                                           C_FROM_REGION        IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION CF_TO_CITY_STATE_ZIPFORMULA (C_TO_CITY          IN VARCHAR2,
                                         C_TO_POSTAL_CODE   IN VARCHAR2,
                                         C_TO_REGION        IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION CF_CUM_QTYFORMULA (C_DEL_CUSTOMER_ID   IN NUMBER,
                               C_CUSTOMER_ID       IN NUMBER,
                               C_SRC_LINE_ID       IN NUMBER)
      RETURN NUMBER;

   FUNCTION CF_UNSHIPPED_QTYFORMULA (C_SRC_LINE_ID IN NUMBER)
      RETURN NUMBER;

   FUNCTION CF_FROM_ADDR_2FORMULA (C_FROM_ADDRESS_2         IN VARCHAR2,
                                   C_FROM_ADDRESS_3         IN VARCHAR2,
                                   CF_FROM_CITY_STATE_ZIP   IN VARCHAR2,
                                   C_FROM_COUNTRY           IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION CF_FROM_ADDR3FORMULA (C_FROM_ADDRESS_2         IN VARCHAR2,
                                  C_FROM_ADDRESS_3         IN VARCHAR2,
                                  CF_FROM_CITY_STATE_ZIP   IN VARCHAR2,
                                  C_FROM_COUNTRY           IN VARCHAR2)
      RETURN VARCHAR2;

   FUNCTION CF_FROM_CITYFORMULA (C_FROM_ADDRESS_2         IN VARCHAR2,
                                 C_FROM_ADDRESS_3         IN VARCHAR2,
                                 CF_FROM_CITY_STATE_ZIP   IN VARCHAR2,
                                 C_FROM_COUNTRY           IN VARCHAR2)
      RETURN CHAR;

   FUNCTION CF_TO_ADDR_2FORMULA (C_TO_ADDRESS_2         IN VARCHAR2,
                                 C_TO_ADDRESS_3         IN VARCHAR2,
                                 C_TO_ADDRESS_4         IN VARCHAR2,
                                 CF_TO_CITY_STATE_ZIP   IN VARCHAR2,
                                 C_TO_COUNTRY           IN VARCHAR2)
      RETURN CHAR;

   FUNCTION CF_TO_ADDR_3FORMULA (C_TO_ADDRESS_2         IN VARCHAR2,
                                 C_TO_ADDRESS_3         IN VARCHAR2,
                                 C_TO_ADDRESS_4         IN VARCHAR2,
                                 CF_TO_CITY_STATE_ZIP   IN VARCHAR2,
                                 C_TO_COUNTRY           IN VARCHAR2)
      RETURN CHAR;

   FUNCTION CF_TO_CITYFORMULA (C_TO_ADDRESS_2         IN VARCHAR2,
                               C_TO_ADDRESS_3         IN VARCHAR2,
                               CF_TO_CITY_STATE_ZIP   IN VARCHAR2,
                               C_TO_COUNTRY           IN VARCHAR2,
                               C_TO_ADDRESS_4         IN VARCHAR2)
      RETURN CHAR;

   FUNCTION CF_BILL_ADDR_2FORMULA (CF_BILL_CITY_STATE_ZIP IN VARCHAR2)
      RETURN CHAR;

   FUNCTION CF_BILL_ADDR_3FORMULA (CF_BILL_CITY_STATE_ZIP IN VARCHAR2)
      RETURN CHAR;

   FUNCTION CF_BILL_CITYFORMULA (CF_BILL_CITY_STATE_ZIP IN VARCHAR2)
      RETURN CHAR;

   FUNCTION CF_UNSHIP_ITEM_NAMEFORMULA (BO_INVENTORY_ITEM_ID   IN NUMBER,
                                        BO_ORGANIZATION_ID     IN NUMBER)
      RETURN CHAR;

   FUNCTION CF_NUM_OF_LPNSFORMULA (C_Q1_DELIVERY_ID   IN NUMBER,
                                   NUM_LPN            IN NUMBER)
      RETURN NUMBER;

   FUNCTION CF_BILL_TO_LOC1FORMULA (CF_OE_LINE_ID   IN NUMBER,
                                    F_OE_LINE_ID    IN NUMBER)
      RETURN NUMBER;

   FUNCTION CF_OE_LINE_ID1FORMULA
      RETURN NUMBER;

   FUNCTION CF_BILL_TO_CONTACT1FORMULA
      RETURN CHAR;

   FUNCTION CF_SHIP_TO_CONTACT1FORMULA
      RETURN CHAR;

   FUNCTION F_BILL_TO_CUST_NAME1FORMULA (F_OE_LINE_ID IN NUMBER)
      RETURN CHAR;

   FUNCTION CF_LINE_TAX_CODE1FORMULA (F_OE_LINE_ID IN NUMBER)
      RETURN CHAR;

   FUNCTION F_SHIP_TO_CUST_NAME1FORMULA (F_SHIP_TO_SITE_USE_ID IN NUMBER)
      RETURN CHAR;

   FUNCTION F_SHIP_TO_SITE_USE_ID1FORMULA (F_DEL_DETAIL_ID IN NUMBER)
      RETURN NUMBER;

   FUNCTION F_BILL_TO_LOC_ID1FORMULA (F_OE_LINE_ID IN NUMBER)
      RETURN NUMBER;

   FUNCTION F_OE_LINE_ID1FORMULA
      RETURN NUMBER;

   FUNCTION F_DEL_DETAIL_ID1FORMULA
      RETURN NUMBER;

   FUNCTION CF_TO_ADDR_4FORMULA (CF_TO_CITY_STATE_ZIP   IN VARCHAR2,
                                 C_TO_COUNTRY           IN VARCHAR2,
                                 C_TO_ADDRESS_2         IN VARCHAR2,
                                 C_TO_ADDRESS_3         IN VARCHAR2,
                                 C_TO_ADDRESS_4         IN VARCHAR2)
      RETURN CHAR;

   FUNCTION CF_BILL_ADDR_4FORMULA (CF_BILL_CITY_STATE_ZIP IN VARCHAR2)
      RETURN CHAR;

   FUNCTION CF_REQUESTOR_NAMEFORMULA (ATTACH_ORDER_ID IN NUMBER)
      RETURN CHAR;

   FUNCTION CF_ITEM_DESCRIPTIONFORMULA (C_ITEM_DESCRIPTION   IN VARCHAR2,
                                        C_INV_ITEM_ID        IN NUMBER,
                                        C_ORGANIZATION_ID    IN NUMBER)
      RETURN CHAR;

   FUNCTION CF_ITEM_DISPLAYFORMULA
      RETURN CHAR;

   FUNCTION CF_DISPLAY_UNSHIPPEDFORMULA
      RETURN CHAR;

   FUNCTION CF_PRINT_CUST_ITEMFORMULA
      RETURN CHAR;

   FUNCTION CF_CARRIER_ADDRFORMULA (WND_CARRIER_ID     IN NUMBER,
                                    C_DEL_ORG_ID       IN NUMBER,
                                    C_Q2_DELIVERY_ID   IN NUMBER)
      RETURN CHAR;

   FUNCTION CF_CARRIER_ADDRESS1FORMULA (TRIP_CARRIER_ID      IN NUMBER,
                                        C_TRIP_DELIVERY_ID   IN NUMBER)
      RETURN CHAR;

   FUNCTION CF_VAT_REG_NUMFORMULA (C_Q2_DELIVERY_ID   IN NUMBER,
                                   C_DEL_ORG_ID       IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION CP_INTERNAL_SALES_ORDER_P
      RETURN VARCHAR2;

   FUNCTION CP_WAREHOUSE_NAME_P
      RETURN VARCHAR2;

   FUNCTION CP_DRAFT_OR_FINAL_P
      RETURN VARCHAR2;

   FUNCTION CP_PRINT_DATE_P
      RETURN DATE;

   FUNCTION CP_RLM_PRINT_CUM_DATA_P
      RETURN VARCHAR2;

   FUNCTION CP_SOURCE_CODE_P
      RETURN VARCHAR2;

   FUNCTION CP_BILL_TO_CONTACT_ID_P
      RETURN NUMBER;

   FUNCTION CP_SHIP_TO_CONTACT_ID_P
      RETURN NUMBER;

   FUNCTION CP_BILL_ADDRESS_LINE_1_P
      RETURN VARCHAR2;

   FUNCTION CP_BILL_ADDRESS_LINE_2_P
      RETURN VARCHAR2;

   FUNCTION CP_BILL_ADDRESS_LINE_3_P
      RETURN VARCHAR2;

   FUNCTION CP_BILL_TOWN_OR_CITY_P
      RETURN VARCHAR2;

   FUNCTION CP_BILL_REGION_P
      RETURN VARCHAR2;

   FUNCTION CP_BILL_POSTAL_CODE_P
      RETURN VARCHAR2;

   FUNCTION CP_BILL_COUNTRY_P
      RETURN VARCHAR2;

   FUNCTION CP_BILL_ADDRESS_LINE_4_P
      RETURN VARCHAR2;

   FUNCTION Address_New (cf_bill_to_loc IN number)
      RETURN VARCHAR2;

   FUNCTION BOX_PALLET_QTY (P_DELIVERY_ID         IN NUMBER,
                            P_INVENTORY_ITEM_ID   IN NUMBER,
                            P_PALLET_NUMBER       IN NUMBER,
                            P_BOX_NUMBER          IN number)
      RETURN NUMBER;

   FUNCTION CUST_PO_NUMBER (P_DELIVERY_ID IN NUMBER, P_ITEM_ID IN NUMBER,P_TRIP_ID  IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION SHIPPED_QTY (P_DELIVERY_DETAIL_ID IN NUMBER, P_ITEM_ID IN NUMBER)
      RETURN NUMBER;

   FUNCTION REQ_QTY (P_DELIVERY_DETAIL_ID IN NUMBER, P_ITEM_ID IN NUMBER)
      RETURN NUMBER;

   FUNCTION ORDER_LINE_SHIP (P_DELIVERY_DETAIL_ID   IN NUMBER,
                             P_ITEM_ID              IN NUMBER,
                             P_TRIP_ID              IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION SALES_REP (P_DELIVERY_ID IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION PACKING_INSTRUCTIONS (P_DELIVERY_ID IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION NW (P_DELIVERY_ID IN NUMBER)
      RETURN NUMBER;

   FUNCTION GW (P_DELIVERY_ID IN NUMBER)
      RETURN NUMBER;

   FUNCTION ITEM_BOX_QTY (P_DELID    IN NUMBER,
                          P_BOXNO    IN NUMBER,
                          P_PALL_NO  IN NUMBER,
                          P_LINEID   IN NUMBER,
                          P_ITEMID   IN NUMBER,
                          P_TRIP_ID  IN NUMBER)
      RETURN NUMBER;

   FUNCTION ITEM_PALL_QTY (P_DELID    IN NUMBER,
                          P_PALL_NO  IN NUMBER,
                          P_LINEID   IN NUMBER,
                          P_ITEMID   IN NUMBER,
                          P_TRIP_ID  IN NUMBER
                          )
      RETURN NUMBER;

   FUNCTION BILL_TELEPHONE (P_CUSTOMER_ID IN number)
      RETURN char;

   FUNCTION BILL_FAX (P_CUSTOMER_ID IN number)
      RETURN char;

   FUNCTION SHIP_TELEPHONE (P_CUSTOMER_ID IN number)
      RETURN char;

   FUNCTION SHIP_FAX (P_CUSTOMER_ID IN number)
      RETURN char;

   FUNCTION SHIP_FROM_ADDRESS (P_DELIVERY_ID IN number)
      RETURN char;

   FUNCTION CF_BILL_CITY_STATE_ZIPFORMULA (
      CP_BILL_TOWN_OR_CITY   IN varchar2,
      CP_BILL_POSTAL_CODE    IN varchar2,
      CP_BILL_REGION         IN varchar2
   )
      RETURN VARCHAR2;

   FUNCTION ITEM_PALLET_QTY (P_DELIVERY_ID    IN NUMBER,
                             P_PALLET_NUMER   IN NUMBER,
                             P_ITEM_ID        IN NUMBER,
                             P_TRIP_ID  IN NUMBER)
      RETURN NUMBER;

   FUNCTION ITEM_BOX_QUANTITY (P_DELIVERY_ID    IN NUMBER,
                               P_PALLET_NUMER   IN NUMBER,
                               P_BOX_NUMER      IN NUMBER,
                               P_ITEM_ID        IN NUMBER,
                               P_TRIP_ID  IN NUMBER)
      RETURN NUMBER;

   FUNCTION ITEM_COO (P_ORG_ID IN NUMBER, P_ITEM_ID IN NUMBER)
      RETURN VARCHAR2;

          FUNCTION TO_RECIPIENTS
      RETURN VARCHAR2;

   FUNCTION get_gross_weight(p_delivery_id in number)
      return number;

   FUNCTION get_net_weight(p_delivery_id in number)
      return number;

   FUNCTION get_weight_uom(p_delivery_id in number)
      return varchar2;

   FUNCTION CF_CUSTOMER_ITEM (p_customer_id         IN    NUMBER,
                              p_inventory_item_id   IN    NUMBER)
        RETURN VARCHAR2;

   FUNCTION TRIP_DEL_NAMES (P_DELIVERY_ID IN NUMBER, P_TRIP_ID IN NUMBER)
      RETURN VARCHAR2;

   FUNCTION TRIP_PACK_SLIP_NUM (P_DELIVERY_ID IN NUMBER, P_TRIP_ID IN NUMBER)
      RETURN VARCHAR2;
	
	FUNCTION XXFOX_GET_TRIP_WEIGHT(p_trip_id NUMBER , p_requested_wt  VARCHAR2 )  RETURN NUMBER;

END XXFOX_WSHRDPAK_XMLP_PKG;
/
EXIT;