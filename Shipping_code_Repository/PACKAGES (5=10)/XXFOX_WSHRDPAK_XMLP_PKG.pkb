CREATE OR REPLACE PACKAGE BODY APPS.XXFOX_WSHRDPAK_XMLP_PKG
AS
   /*REM +==========================================================================+
         REM |     Copyright (c) 2009 DAZ Systems, Inc .                                |
         REM |                  El Segundo, CA 90505                                    |
         REM |                All rights reserved                                       |
         REM +==========================================================================+
         REM
         REM NAME
         REM XXFOX_WSHRDPAK_XMLP_PKG
         REM
         REM PROGRAM TYPE
         REM PACKAGE
         REM
         REM PURPOSE
         REM Packing Slip Report
         REM
         REM HISTORY
         REM ===========================================================================
         REM  Date          Author                        Activity
         REM ===========================================================================
         REM  18/08/2015   Siva Mandapalli               Created
         REM  21/03/2016   Sriram Natarajan              Added new function for gross_wt and net_wt
         REM ===========================================================================*/

   FUNCTION SHIP_FROM_ADDRESS (P_DELIVERY_ID IN number)
      RETURN char
   AS
      V_SHIP_FROM_ADDRESS   VARCHAR2 (500) := NULL;
   BEGIN
      SELECT   DECODE (hl.description,
                       NULL, NULL,
                       hl.description || ' ' || CHR (10))
               || DECODE (hl.address_line_1,
                          NULL, NULL,
                          hl.address_line_1 || ' ' || CHR (10))
               || DECODE (hl.address_line_2,
                          NULL, NULL,
                          hl.address_line_2 || CHR (10))
               || DECODE (hl.address_line_3,
                          NULL, NULL,
                          hl.address_line_3 || ' ' || CHR (10))
               || DECODE (hl.town_or_city, NULL, NULL, hl.town_or_city)
               || ', '
               || DECODE (hl.REGION_2, NULL, NULL, hl.REGION_2)
               || ' '
               || DECODE (hl.postal_code,
                          NULL, NULL,
                          hl.postal_code || ' ' || CHR (10))
               || DECODE (ftt.territory_short_name,
                          NULL, NULL,
                          ftt.territory_short_name)
        INTO   V_SHIP_FROM_ADDRESS
        FROM   mtl_secondary_inventories msi,
               hr_organization_units hou,
               hr_locations hL,
               fnd_territories_vl ftt
       WHERE       1 = 1
               AND hl.country = ftt.territory_code
               AND HOU.LOCATION_ID = hL.LOCATION_ID
               AND MSI.ORGANIZATION_ID = HOU.ORGANIZATION_ID
               AND MSI.SECONDARY_INVENTORY_NAME IN
                        (SELECT   subinventory
                           FROM   wsh_delivery_details wdd,
                                  wsh_delivery_assignments wda
                          WHERE   WDA.DELIVERY_DETAIL_ID =
                                     WDD.DELIVERY_DETAIL_ID
                                  AND WDA.DELIVERY_ID = P_DELIVERY_ID
                                  AND ROWNUM = 1);

      RETURN V_SHIP_FROM_ADDRESS;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION NW (P_DELIVERY_ID IN NUMBER)
      RETURN NUMBER
   AS
      V_NW_PLT   NUMBER := 0;
      V_NW_TOT   NUMBER := 0;
   BEGIN
      FOR r_rec IN (SELECT   DISTINCT pallet_number
                      FROM   xxfox_audit_lines
                     WHERE   delivery_id = P_DELIVERY_ID)
      LOOP
         BEGIN
            V_NW_PLT := 0;

            SELECT   NVL (pallet_net_wt, 0)
              INTO   V_NW_PLT
              FROM   xxfox_audit_lines
             WHERE   delivery_id = P_DELIVERY_ID
                     AND pallet_number = r_rec.pallet_number
                     AND (NVL (pallet_net_wt, 0) <> 0
                          OR NVL (pallet_gross_wt, 0) <> 0)
                     AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               V_NW_PLT := 0;
         END;
         V_NW_TOT := V_NW_TOT + V_NW_PLT;
      END LOOP;

      RETURN V_NW_TOT;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION GW (P_DELIVERY_ID IN NUMBER)
      RETURN NUMBER
   AS
      V_GW_PLT   NUMBER := 0;
      V_GW_TOT   NUMBER := 0;
   BEGIN
      FOR r_rec IN (SELECT   DISTINCT pallet_number
                      FROM   xxfox_audit_lines
                     WHERE   delivery_id = P_DELIVERY_ID)
      LOOP
         BEGIN
            V_GW_PLT := 0;

            SELECT   NVL (pallet_gross_wt, 0)
              INTO   V_GW_PLT
              FROM   xxfox_audit_lines
             WHERE   delivery_id = P_DELIVERY_ID
                     AND pallet_number = r_rec.pallet_number
                     AND (NVL (pallet_net_wt, 0) <> 0
                          OR NVL (pallet_gross_wt, 0) <> 0)
                     AND ROWNUM = 1;
         EXCEPTION
            WHEN OTHERS
            THEN
               V_GW_PLT := 0;
         END;
         V_GW_TOT := V_GW_TOT + V_GW_PLT;
      END LOOP;

      RETURN V_GW_TOT;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION SALES_REP (P_DELIVERY_ID IN NUMBER)
      RETURN VARCHAR2
   AS
      V_SALES_REP   VARCHAR2 (100) := NULL;
   BEGIN
      SELECT   SR.NAME
        INTO   V_SALES_REP
        FROM   ra_salesreps SR, oe_order_headers_all H
       WHERE   h.salesrep_id = sr.salesrep_id(+)
               AND H.HEADER_ID =
                     (SELECT   SOURCE_HEADER_ID
                        FROM   wsh_delivery_details WDD,
                               wsh_delivery_assignments wda
                       WHERE   WDD.DELIVERY_DETAIL_ID =
                                  WDA.DELIVERY_DETAIL_ID
                               AND wda.DELIVERY_ID = P_DELIVERY_ID
                               AND ROWNUM = 1);

      RETURN V_SALES_REP;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION PACKING_INSTRUCTIONS (P_DELIVERY_ID IN NUMBER)
      RETURN VARCHAR2
   AS
      V_PACKING_INSTRUCTIONS   VARCHAR2 (9000) := NULL;
   BEGIN
      SELECT   PACKING_INSTRUCTIONS
        INTO   V_PACKING_INSTRUCTIONS
        FROM   oe_order_headers_all H
       WHERE   H.HEADER_ID =
                  (SELECT   SOURCE_HEADER_ID
                     FROM   wsh_delivery_details WDD,
                            wsh_delivery_assignments wda
                    WHERE   WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                            AND wda.DELIVERY_ID = P_DELIVERY_ID
                            AND ROWNUM = 1);

      RETURN V_PACKING_INSTRUCTIONS;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION BILL_TELEPHONE (P_CUSTOMER_ID IN number)
      RETURN char
   IS
      l_phone   varchar2 (30);
   BEGIN
      SELECT   NVL (
                  RAW_PHONE_NUMBER,
                  DECODE (PHONE_COUNTRY_CODE,
                          NULL, NULL,
                          '(' || PHONE_COUNTRY_CODE || ') ')
                  || DECODE (PHONE_AREA_CODE,
                             NULL, NULL,
                             PHONE_AREA_CODE || ' ')
                  || PHONE_NUMBER
               )
        INTO   l_phone
        FROM   hz_contact_points
       WHERE   PHONE_LINE_TYPE = 'GEN'
               AND OWNER_TABLE_ID IN
                        (SELECT   MIN (owner_table_id)
                           FROM   HZ_CONTACT_POINTS HCP,
                                  HZ_RELATIONSHIPS HR,
                                  HZ_PARTIES HP,
                                  HZ_CUST_ACCOUNTS HCA,
                                  hz_party_sites hps,
                                  hz_cust_acct_sites_all hcas,
                                  hz_cust_site_uses_all hcasu
                          WHERE       HCA.CUST_ACCOUNT_ID = P_CUSTOMER_ID
                                  AND HCA.PARTY_ID = HP.PARTY_ID
                                  AND HP.PARTY_ID = HR.OBJECT_ID
                                  AND HR.OBJECT_TYPE = 'ORGANIZATION'
                                  AND HR.RELATIONSHIP_CODE = 'CONTACT_OF'
                                  AND HR.PARTY_ID = HCP.OWNER_TABLE_ID
                                  AND CONTACT_POINT_TYPE = 'PHONE'
                                  AND hp.party_id = hps.party_id
                                  AND hps.PARTY_SITE_ID = hcas.party_site_id
                                  AND hps.party_site_id = hcas.party_site_id
                                  AND hcas.CUST_ACCT_SITE_ID =
                                        hcasu.CUST_ACCT_SITE_ID
                                  AND hcp.application_id = 660
                                  AND hcasu.site_use_code = 'BILL_TO')
               AND CONTACT_POINT_TYPE = 'PHONE'
               AND PRIMARY_FLAG = 'Y'
               AND ROWNUM = 1
               AND application_id = 660;

      RETURN l_phone;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_phone := NULL;
         RETURN l_phone;
   END;

   FUNCTION BILL_FAX (P_CUSTOMER_ID IN number)
      RETURN char
   IS
      l_phone   varchar2 (30);
   BEGIN
      SELECT   NVL (
                  RAW_PHONE_NUMBER,
                  DECODE (PHONE_COUNTRY_CODE,
                          NULL, NULL,
                          '(' || PHONE_COUNTRY_CODE || ') ')
                  || DECODE (PHONE_AREA_CODE,
                             NULL, NULL,
                             PHONE_AREA_CODE || ' ')
                  || PHONE_NUMBER
               )
        INTO   l_phone
        FROM   hz_contact_points
       WHERE   PHONE_LINE_TYPE = 'FAX'
               AND OWNER_TABLE_ID IN
                        (SELECT   MIN (owner_table_id)
                           FROM   HZ_CONTACT_POINTS HCP,
                                  HZ_RELATIONSHIPS HR,
                                  HZ_PARTIES HP,
                                  HZ_CUST_ACCOUNTS HCA,
                                  hz_party_sites hps,
                                  hz_cust_acct_sites_all hcas,
                                  hz_cust_site_uses_all hcasu
                          WHERE       HCA.CUST_ACCOUNT_ID = P_CUSTOMER_ID
                                  AND HCA.PARTY_ID = HP.PARTY_ID
                                  AND HP.PARTY_ID = HR.OBJECT_ID
                                  AND HR.OBJECT_TYPE = 'ORGANIZATION'
                                  AND HR.RELATIONSHIP_CODE = 'CONTACT_OF'
                                  AND HR.PARTY_ID = HCP.OWNER_TABLE_ID
                                  AND CONTACT_POINT_TYPE = 'FAX'
                                  AND hp.party_id = hps.party_id
                                  AND hps.PARTY_SITE_ID = hcas.party_site_id
                                  AND hps.party_site_id = hcas.party_site_id
                                  AND hcas.CUST_ACCT_SITE_ID =
                                        hcasu.CUST_ACCT_SITE_ID
                                  AND hcp.application_id = 660
                                  AND hcasu.site_use_code = 'BILL_TO')
               AND CONTACT_POINT_TYPE = 'FAX'
               AND PRIMARY_FLAG = 'Y'
               AND ROWNUM = 1
               AND application_id = 660;

      RETURN l_phone;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_phone := NULL;
         RETURN l_phone;
   END;

   FUNCTION SHIP_TELEPHONE (P_CUSTOMER_ID IN number)
      RETURN char
   IS
      l_phone   varchar2 (30);
   BEGIN
      SELECT   NVL (
                  RAW_PHONE_NUMBER,
                  DECODE (PHONE_COUNTRY_CODE,
                          NULL, NULL,
                          '(' || PHONE_COUNTRY_CODE || ') ')
                  || DECODE (PHONE_AREA_CODE,
                             NULL, NULL,
                             PHONE_AREA_CODE || ' ')
                  || PHONE_NUMBER
               )
        INTO   l_phone
        FROM   hz_contact_points
       WHERE   PHONE_LINE_TYPE = 'GEN'
               AND OWNER_TABLE_ID IN
                        (SELECT   MIN (owner_table_id)
                           FROM   HZ_CONTACT_POINTS HCP,
                                  HZ_RELATIONSHIPS HR,
                                  HZ_PARTIES HP,
                                  HZ_CUST_ACCOUNTS HCA,
                                  hz_party_sites hps,
                                  hz_cust_acct_sites_all hcas,
                                  hz_cust_site_uses_all hcasu
                          WHERE       HCA.CUST_ACCOUNT_ID = P_CUSTOMER_ID
                                  AND HCA.PARTY_ID = HP.PARTY_ID
                                  AND HP.PARTY_ID = HR.OBJECT_ID
                                  AND HR.OBJECT_TYPE = 'ORGANIZATION'
                                  AND HR.RELATIONSHIP_CODE = 'CONTACT_OF'
                                  AND HR.PARTY_ID = HCP.OWNER_TABLE_ID
                                  AND CONTACT_POINT_TYPE = 'PHONE'
                                  AND hp.party_id = hps.party_id
                                  AND hps.PARTY_SITE_ID = hcas.party_site_id
                                  AND hps.party_site_id = hcas.party_site_id
                                  AND hcas.CUST_ACCT_SITE_ID =
                                        hcasu.CUST_ACCT_SITE_ID
                                  AND hcp.application_id = 660
                                  AND hcasu.site_use_code = 'SHIP_TO')
               AND CONTACT_POINT_TYPE = 'PHONE'
               AND PRIMARY_FLAG = 'Y'
               AND ROWNUM = 1
               AND application_id = 660;

      RETURN l_phone;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_phone := NULL;
         RETURN l_phone;
   END;

   FUNCTION SHIP_FAX (P_CUSTOMER_ID IN number)
      RETURN char
   IS
      l_phone   varchar2 (30);
   BEGIN
      SELECT   NVL (
                  RAW_PHONE_NUMBER,
                  DECODE (PHONE_COUNTRY_CODE,
                          NULL, NULL,
                          '(' || PHONE_COUNTRY_CODE || ') ')
                  || DECODE (PHONE_AREA_CODE,
                             NULL, NULL,
                             PHONE_AREA_CODE || ' ')
                  || PHONE_NUMBER
               )
        INTO   l_phone
        FROM   hz_contact_points
       WHERE   PHONE_LINE_TYPE = 'FAX'
               AND OWNER_TABLE_ID IN
                        (SELECT   MIN (owner_table_id)
                           FROM   HZ_CONTACT_POINTS HCP,
                                  HZ_RELATIONSHIPS HR,
                                  HZ_PARTIES HP,
                                  HZ_CUST_ACCOUNTS HCA,
                                  hz_party_sites hps,
                                  hz_cust_acct_sites_all hcas,
                                  hz_cust_site_uses_all hcasu
                          WHERE       HCA.CUST_ACCOUNT_ID = P_CUSTOMER_ID
                                  AND HCA.PARTY_ID = HP.PARTY_ID
                                  AND HP.PARTY_ID = HR.OBJECT_ID
                                  AND HR.OBJECT_TYPE = 'ORGANIZATION'
                                  AND HR.RELATIONSHIP_CODE = 'CONTACT_OF'
                                  AND HR.PARTY_ID = HCP.OWNER_TABLE_ID
                                  AND CONTACT_POINT_TYPE = 'FAX'
                                  AND hp.party_id = hps.party_id
                                  AND hps.PARTY_SITE_ID = hcas.party_site_id
                                  AND hps.party_site_id = hcas.party_site_id
                                  AND hcas.CUST_ACCT_SITE_ID =
                                        hcasu.CUST_ACCT_SITE_ID
                                  AND hcp.application_id = 660
                                  AND hcasu.site_use_code = 'SHIP_TO')
               AND CONTACT_POINT_TYPE = 'FAX'
               AND PRIMARY_FLAG = 'Y'
               AND ROWNUM = 1
               AND application_id = 660;

      RETURN l_phone;
   EXCEPTION
      WHEN OTHERS
      THEN
         l_phone := NULL;
         RETURN l_phone;
   END;

   FUNCTION ITEM_BOX_QTY (P_DELID    IN NUMBER,
                          P_BOXNO    IN NUMBER,
                          P_PALL_NO  IN NUMBER,
                          P_LINEID   IN NUMBER,
                          P_ITEMID   IN NUMBER,
                          P_TRIP_ID  IN NUMBER)
      RETURN NUMBER
   IS
      V_MO_COUNT         NUMBER := 0;
      V_MB_COUNT         NUMBER := 0;
      V_QTY              NUMBER := 0;
      V_LQTY             NUMBER := 0;
      V_CUM_BOXQTY       NUMBER := 0;
      V_PRE_CUM_BOXQTY   NUMBER := 0;
      V_PRE_CUM_LQTY     NUMBER := 0;
      V_CUM_LQTY         NUMBER := 0;
      V_AVAIL_QTY        NUMBER := 0;
   BEGIN
      DBMS_OUTPUT.PUT_LINE ('P_DELID- ' || P_DELID);
      DBMS_OUTPUT.PUT_LINE ('P_BOXNO- ' || P_BOXNO);
      DBMS_OUTPUT.PUT_LINE ('P_LINEID- ' || P_LINEID);
      DBMS_OUTPUT.PUT_LINE ('P_ITEMID- ' || P_ITEMID);
      DBMS_OUTPUT.PUT_LINE ('P_TRIP_ID- ' || P_TRIP_ID);

      IF P_PALL_NO IS NOT NULL AND P_BOXNO IS NULL THEN
        RETURN ITEM_PALL_QTY (P_DELID,P_PALL_NO,P_LINEID,P_ITEMID,P_TRIP_ID);

      ELSE
      BEGIN
         SELECT   COUNT (DISTINCT BOX_NUMBER)
           INTO   V_MB_COUNT
           FROM   XXFOX_AUDIT_LINES
          WHERE   DELIVERY_ID = NVL(P_DELID,DELIVERY_ID) AND INVENTORY_ITEM_ID = P_ITEMID
                  AND TRIP_ID = NVL(P_TRIP_ID,TRIP_ID);
      EXCEPTION
         WHEN OTHERS
         THEN
            V_MB_COUNT := 1;
      END;

      DBMS_OUTPUT.PUT_LINE ('V_MB_COUNT- ' || V_MB_COUNT);

      BEGIN
         SELECT   COUNT (DISTINCT SOURCE_LINE_ID)
           INTO   V_MO_COUNT
           FROM   WSH_DELIVERY_DETAILS WDD, WSH_DELIVERY_ASSIGNMENTS WDA, wsh_delivery_legs wdl,wsh_trip_stops wts
          WHERE       WDD.SOURCE_CODE = 'OE'
                  AND WDD.INVENTORY_ITEM_ID = P_ITEMID
                  AND WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                  AND WDA.DELIVERY_ID = NVL(P_DELID,WDA.DELIVERY_ID)
                  AND WDA.delivery_id     = wdl.delivery_id(+)
                  AND wdl.pick_up_stop_id = wts.stop_id(+)
                  AND NVL(wts.trip_id,-1) = NVL(P_TRIP_ID,NVL(wts.trip_id,-1));
      EXCEPTION
         WHEN OTHERS
         THEN
            V_MO_COUNT := 1;
      END;
      DBMS_OUTPUT.PUT_LINE ('V_MO_COUNT- ' || V_MO_COUNT);
      BEGIN
         IF V_MO_COUNT <= 1 AND V_MB_COUNT <= 1
         THEN
            BEGIN
               SELECT   NVL (SUM (AUDIT_QUANTITY), 0)
                 INTO   V_QTY
                 FROM   XXFOX_AUDIT_LINES
                WHERE   DELIVERY_ID = NVL(P_DELID,DELIVERY_ID)
                        AND INVENTORY_ITEM_ID = P_ITEMID
                        AND TRIP_ID = NVL(P_TRIP_ID,TRIP_ID);
            EXCEPTION
               WHEN OTHERS
               THEN
                  V_QTY := 0;
            END;
            DBMS_OUTPUT.PUT_LINE ('V_MO_COUNT <=1 AND V_MB_COUNT <= 1');
         ELSIF V_MO_COUNT > 1 AND V_MB_COUNT <= 1
         THEN
            BEGIN
               SELECT   NVL (SUM (NVL (WDD.PICKED_QUANTITY, 0)), 0)
                 INTO   V_QTY
                 FROM   WSH_DELIVERY_DETAILS WDD,
                        WSH_DELIVERY_ASSIGNMENTS WDA,
                        wsh_delivery_legs wdl,wsh_trip_stops wts
                WHERE       WDD.SOURCE_CODE = 'OE'
                        AND WDD.SOURCE_LINE_ID = P_LINEID
                        AND WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                        AND WDA.DELIVERY_ID = NVL(P_DELID,WDA.DELIVERY_ID)
                        AND WDD.INVENTORY_ITEM_ID = P_ITEMID
                        AND WDA.delivery_id     = wdl.delivery_id(+)
                        AND wdl.pick_up_stop_id = wts.stop_id(+)
                        AND NVL(wts.trip_id,-1) = NVL(P_TRIP_ID,NVL(wts.trip_id,-1));
            EXCEPTION
               WHEN OTHERS
               THEN
                  V_QTY := 0;
            END;
            DBMS_OUTPUT.PUT_LINE ('V_MO_COUNT >1 AND V_MB_COUNT <= 1');
         ELSIF V_MO_COUNT <= 1 AND V_MB_COUNT > 1
         THEN
            BEGIN
               SELECT   NVL (SUM (AUDIT_QUANTITY), 0)
                 INTO   V_QTY
                 FROM   XXFOX_AUDIT_LINES
                WHERE       DELIVERY_ID = NVL(P_DELID,DELIVERY_ID)
                        AND INVENTORY_ITEM_ID = P_ITEMID
                        AND BOX_NUMBER = P_BOXNO
                        AND TRIP_ID = NVL(P_TRIP_ID,TRIP_ID);
            EXCEPTION
               WHEN OTHERS
               THEN
                  V_QTY := 0;
            END;
            DBMS_OUTPUT.PUT_LINE ('V_MO_COUNT <=1 AND V_MB_COUNT > 1');
         ELSE
            BEGIN
               V_CUM_BOXQTY := 0;
               V_PRE_CUM_BOXQTY := 0;
               V_LQTY := 0;
               V_PRE_CUM_LQTY := 0;
               V_CUM_LQTY := 0;
               V_AVAIL_QTY := 0;
               BEGIN
                  SELECT   NVL (SUM (AUDIT_QUANTITY), 0)
                    INTO   V_CUM_BOXQTY
                    FROM   XXFOX_AUDIT_LINES
                   WHERE       DELIVERY_ID = NVL(P_DELID,DELIVERY_ID)
                           AND INVENTORY_ITEM_ID = P_ITEMID
                           AND BOX_NUMBER <= P_BOXNO
                           AND TRIP_ID = NVL(P_TRIP_ID,TRIP_ID);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_CUM_BOXQTY := 0;
               END;
               DBMS_OUTPUT.PUT_LINE ('V_CUM_BOXQTY ' || V_CUM_BOXQTY);
               BEGIN
                  SELECT   NVL (SUM (AUDIT_QUANTITY), 0)
                    INTO   V_PRE_CUM_BOXQTY
                    FROM   XXFOX_AUDIT_LINES
                   WHERE       DELIVERY_ID = NVL(P_DELID,DELIVERY_ID)
                           AND INVENTORY_ITEM_ID = P_ITEMID
                           AND BOX_NUMBER < P_BOXNO
                           AND TRIP_ID = NVL(P_TRIP_ID,TRIP_ID);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_PRE_CUM_BOXQTY := 0;
               END;
               DBMS_OUTPUT.PUT_LINE ('V_PRE_CUM_BOXQTY ' || V_PRE_CUM_BOXQTY);
               BEGIN
                  SELECT   NVL (SUM (NVL (WDD.PICKED_QUANTITY, 0)), 0)
                    INTO   V_LQTY
                    FROM   WSH_DELIVERY_DETAILS WDD,
                           WSH_DELIVERY_ASSIGNMENTS WDA,
                           wsh_delivery_legs wdl,wsh_trip_stops wts
                   WHERE   WDD.SOURCE_CODE = 'OE'
                           AND WDD.SOURCE_LINE_ID = P_LINEID
                           AND WDD.DELIVERY_DETAIL_ID =  WDA.DELIVERY_DETAIL_ID
                           AND WDA.DELIVERY_ID = NVL(P_DELID,WDA.DELIVERY_ID)
                           AND WDD.INVENTORY_ITEM_ID = P_ITEMID
                           AND WDA.delivery_id     = wdl.delivery_id(+)
                           AND wdl.pick_up_stop_id = wts.stop_id(+)
                           AND NVL(wts.trip_id,-1) = NVL(P_TRIP_ID,NVL(wts.trip_id,-1));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_LQTY := 0;
               END;
               DBMS_OUTPUT.PUT_LINE ('V_LQTY ' || V_LQTY);
               BEGIN
                  SELECT   NVL (SUM (NVL (WDD.PICKED_QUANTITY, 0)), 0)
                    INTO   V_PRE_CUM_LQTY
                    FROM   WSH_DELIVERY_DETAILS WDD,
                           WSH_DELIVERY_ASSIGNMENTS WDA,
                           wsh_delivery_legs wdl,wsh_trip_stops wts
                   WHERE       WDD.SOURCE_CODE = 'OE'
                           AND WDD.SOURCE_LINE_ID < P_LINEID
                           AND WDD.INVENTORY_ITEM_ID = P_ITEMID
                           AND WDD.DELIVERY_DETAIL_ID =  WDA.DELIVERY_DETAIL_ID
                           AND WDA.DELIVERY_ID = NVL(P_DELID,WDA.DELIVERY_ID)
                           AND WDA.delivery_id     = wdl.delivery_id(+)
                           AND wdl.pick_up_stop_id = wts.stop_id(+)
                           AND NVL(wts.trip_id,-1) = NVL(P_TRIP_ID,NVL(wts.trip_id,-1));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_PRE_CUM_LQTY := 0;
               END;
               DBMS_OUTPUT.PUT_LINE ('V_PRE_CUM_LQTY ' || V_PRE_CUM_LQTY);
               BEGIN
                  SELECT   NVL (SUM (NVL (WDD.PICKED_QUANTITY, 0)), 0)
                    INTO   V_CUM_LQTY
                    FROM   WSH_DELIVERY_DETAILS WDD,
                           WSH_DELIVERY_ASSIGNMENTS WDA,
                           wsh_delivery_legs wdl,wsh_trip_stops wts
                   WHERE       WDD.SOURCE_CODE = 'OE'
                           AND WDD.SOURCE_LINE_ID <= P_LINEID
                           AND WDD.INVENTORY_ITEM_ID = P_ITEMID
                           AND WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                           AND WDA.DELIVERY_ID = NVL(P_DELID,WDA.DELIVERY_ID)
                           AND WDA.delivery_id     = wdl.delivery_id(+)
                           AND wdl.pick_up_stop_id = wts.stop_id(+)
                           AND NVL(wts.trip_id,-1) = NVL(P_TRIP_ID,NVL(wts.trip_id,-1));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_CUM_LQTY := 0;
               END;
               DBMS_OUTPUT.PUT_LINE ('V_CUM_LQTY ' || V_CUM_LQTY);
               BEGIN
                  IF V_CUM_LQTY > V_PRE_CUM_BOXQTY
                  THEN
                     V_AVAIL_QTY := V_CUM_BOXQTY - V_PRE_CUM_LQTY;
                  ELSE
                     V_AVAIL_QTY := 0;
                  END IF;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_AVAIL_QTY := 0;
               END;

               IF V_AVAIL_QTY > V_LQTY
               THEN
                  V_QTY := V_LQTY;
               ELSIF V_AVAIL_QTY > 0
               THEN
                  V_QTY := V_AVAIL_QTY;
               ELSE
                  V_QTY := 0;
               END IF;

               DBMS_OUTPUT.PUT_LINE ('V_AVAIL_QTY ' || V_AVAIL_QTY);
            EXCEPTION
               WHEN OTHERS
               THEN
                  V_QTY := 0;
            END;
         END IF;

         DBMS_OUTPUT.PUT_LINE ('V_QTY ' || V_QTY);

         RETURN V_QTY;
      EXCEPTION
         WHEN OTHERS
         THEN
            RETURN 0;
      END;
      END IF;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END ITEM_BOX_QTY;

-- New pallet logic
   FUNCTION ITEM_PALL_QTY (P_DELID    IN NUMBER,
                          P_PALL_NO  IN NUMBER,
                          P_LINEID   IN NUMBER,
                          P_ITEMID   IN NUMBER,
                          P_TRIP_ID  IN NUMBER
                          )
      RETURN NUMBER
   IS
      V_MO_COUNT         NUMBER := 0;
      V_MB_COUNT         NUMBER := 0;
      V_QTY              NUMBER := 0;
      V_LQTY             NUMBER := 0;
      V_CUM_BOXQTY       NUMBER := 0;
      V_PRE_CUM_BOXQTY   NUMBER := 0;
      V_PRE_CUM_LQTY     NUMBER := 0;
      V_CUM_LQTY         NUMBER := 0;
      V_AVAIL_QTY        NUMBER := 0;
   BEGIN
      DBMS_OUTPUT.PUT_LINE ('P_DELID- ' || P_DELID);
      DBMS_OUTPUT.PUT_LINE ('P_PALL_NO- ' || P_PALL_NO);
      DBMS_OUTPUT.PUT_LINE ('P_LINEID- ' || P_LINEID);
      DBMS_OUTPUT.PUT_LINE ('P_ITEMID- ' || P_ITEMID);
      DBMS_OUTPUT.PUT_LINE ('P_TRIP_ID- ' || P_TRIP_ID);

      BEGIN
         SELECT   COUNT (DISTINCT PALLET_NUMBER)
           INTO   V_MB_COUNT
           FROM   XXFOX_AUDIT_LINES
          WHERE   DELIVERY_ID = NVL(P_DELID,DELIVERY_ID) AND INVENTORY_ITEM_ID = P_ITEMID
                  AND TRIP_ID = NVL(P_TRIP_ID,TRIP_ID);
      EXCEPTION
         WHEN OTHERS
         THEN
            V_MB_COUNT := 1;
      END;

      DBMS_OUTPUT.PUT_LINE ('V_MB_COUNT- ' || V_MB_COUNT);

      BEGIN
         SELECT   COUNT (DISTINCT SOURCE_LINE_ID)
           INTO   V_MO_COUNT
           FROM   WSH_DELIVERY_DETAILS WDD, WSH_DELIVERY_ASSIGNMENTS WDA, wsh_delivery_legs wdl,wsh_trip_stops wts
          WHERE       WDD.SOURCE_CODE = 'OE'
                  AND WDD.INVENTORY_ITEM_ID = P_ITEMID
                  AND WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                  AND WDA.DELIVERY_ID = NVL(P_DELID,WDA.DELIVERY_ID)
                  AND WDA.delivery_id     = wdl.delivery_id(+)
                  AND wdl.pick_up_stop_id = wts.stop_id(+)
                  AND NVL(wts.trip_id,-1) = NVL(P_TRIP_ID,NVL(wts.trip_id,-1));
      EXCEPTION
         WHEN OTHERS
         THEN
            V_MO_COUNT := 1;
      END;
      DBMS_OUTPUT.PUT_LINE ('V_MO_COUNT- ' || V_MO_COUNT);
      BEGIN
         IF V_MO_COUNT <= 1 AND V_MB_COUNT <= 1
         THEN
            BEGIN
               SELECT   NVL (SUM (AUDIT_QUANTITY), 0)
                 INTO   V_QTY
                 FROM   XXFOX_AUDIT_LINES
                WHERE   DELIVERY_ID = NVL(P_DELID,DELIVERY_ID)
                        AND INVENTORY_ITEM_ID = P_ITEMID
                        AND TRIP_ID = NVL(P_TRIP_ID,TRIP_ID);
            EXCEPTION
               WHEN OTHERS
               THEN
                  V_QTY := 0;
            END;
            DBMS_OUTPUT.PUT_LINE ('V_MO_COUNT <=1 AND V_MB_COUNT <= 1');
         ELSIF V_MO_COUNT > 1 AND V_MB_COUNT <= 1
         THEN
            BEGIN
               SELECT   NVL (SUM (NVL (WDD.PICKED_QUANTITY, 0)), 0)
                 INTO   V_QTY
                 FROM   WSH_DELIVERY_DETAILS WDD,
                        WSH_DELIVERY_ASSIGNMENTS WDA,
                        wsh_delivery_legs wdl,wsh_trip_stops wts
                WHERE       WDD.SOURCE_CODE = 'OE'
                        AND WDD.SOURCE_LINE_ID = P_LINEID
                        AND WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                        AND WDA.DELIVERY_ID = NVL(P_DELID,WDA.DELIVERY_ID)
                        AND WDD.INVENTORY_ITEM_ID = P_ITEMID
                        AND WDA.delivery_id     = wdl.delivery_id(+)
                        AND wdl.pick_up_stop_id = wts.stop_id(+)
                        AND NVL(wts.trip_id,-1) = NVL(P_TRIP_ID,NVL(wts.trip_id,-1));
            EXCEPTION
               WHEN OTHERS
               THEN
                  V_QTY := 0;
            END;
            DBMS_OUTPUT.PUT_LINE ('V_MO_COUNT >1 AND V_MB_COUNT <= 1');
         ELSIF V_MO_COUNT <= 1 AND V_MB_COUNT > 1
         THEN
            BEGIN
               SELECT   NVL (SUM (AUDIT_QUANTITY), 0)
                 INTO   V_QTY
                 FROM   XXFOX_AUDIT_LINES
                WHERE       DELIVERY_ID = NVL(P_DELID,DELIVERY_ID)
                        AND INVENTORY_ITEM_ID = P_ITEMID
                        AND PALLET_NUMBER = P_PALL_NO
                        AND TRIP_ID = NVL(P_TRIP_ID,TRIP_ID);
            EXCEPTION
               WHEN OTHERS
               THEN
                  V_QTY := 0;
            END;
            DBMS_OUTPUT.PUT_LINE ('V_MO_COUNT <=1 AND V_MB_COUNT > 1');
         ELSE
            BEGIN
               V_CUM_BOXQTY := 0;
               V_PRE_CUM_BOXQTY := 0;
               V_LQTY := 0;
               V_PRE_CUM_LQTY := 0;
               V_CUM_LQTY := 0;
               V_AVAIL_QTY := 0;
               BEGIN
                  SELECT   NVL (SUM (AUDIT_QUANTITY), 0)
                    INTO   V_CUM_BOXQTY
                    FROM   XXFOX_AUDIT_LINES
                   WHERE       DELIVERY_ID = NVL(P_DELID,DELIVERY_ID)
                           AND INVENTORY_ITEM_ID = P_ITEMID
                           AND PALLET_NUMBER <= P_PALL_NO
                           AND TRIP_ID = NVL(P_TRIP_ID,TRIP_ID);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_CUM_BOXQTY := 0;
               END;
               DBMS_OUTPUT.PUT_LINE ('V_CUM_BOXQTY ' || V_CUM_BOXQTY);
               BEGIN
                  SELECT   NVL (SUM (AUDIT_QUANTITY), 0)
                    INTO   V_PRE_CUM_BOXQTY
                    FROM   XXFOX_AUDIT_LINES
                   WHERE       DELIVERY_ID = NVL(P_DELID,DELIVERY_ID)
                           AND INVENTORY_ITEM_ID = P_ITEMID
                           AND PALLET_NUMBER < P_PALL_NO
                           AND TRIP_ID = NVL(P_TRIP_ID,TRIP_ID);
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_PRE_CUM_BOXQTY := 0;
               END;
               DBMS_OUTPUT.PUT_LINE ('V_PRE_CUM_BOXQTY ' || V_PRE_CUM_BOXQTY);
               BEGIN
                  SELECT   NVL (SUM (NVL (WDD.PICKED_QUANTITY, 0)), 0)
                    INTO   V_LQTY
                    FROM   WSH_DELIVERY_DETAILS WDD,
                           WSH_DELIVERY_ASSIGNMENTS WDA,
                           wsh_delivery_legs wdl,wsh_trip_stops wts
                   WHERE   WDD.SOURCE_CODE = 'OE'
                           AND WDD.SOURCE_LINE_ID = P_LINEID
                           AND WDD.DELIVERY_DETAIL_ID =  WDA.DELIVERY_DETAIL_ID
                           AND WDA.DELIVERY_ID = NVL(P_DELID,WDA.DELIVERY_ID)
                           AND WDD.INVENTORY_ITEM_ID = P_ITEMID
                           AND WDA.delivery_id     = wdl.delivery_id(+)
                           AND wdl.pick_up_stop_id = wts.stop_id(+)
                           AND NVL(wts.trip_id,-1) = NVL(P_TRIP_ID,NVL(wts.trip_id,-1));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_LQTY := 0;
               END;
               DBMS_OUTPUT.PUT_LINE ('V_LQTY ' || V_LQTY);
               BEGIN
                  SELECT   NVL (SUM (NVL (WDD.PICKED_QUANTITY, 0)), 0)
                    INTO   V_PRE_CUM_LQTY
                    FROM   WSH_DELIVERY_DETAILS WDD,
                           WSH_DELIVERY_ASSIGNMENTS WDA,
                           wsh_delivery_legs wdl,wsh_trip_stops wts
                   WHERE       WDD.SOURCE_CODE = 'OE'
                           AND WDD.SOURCE_LINE_ID < P_LINEID
                           AND WDD.INVENTORY_ITEM_ID = P_ITEMID
                           AND WDD.DELIVERY_DETAIL_ID =  WDA.DELIVERY_DETAIL_ID
                           AND WDA.DELIVERY_ID = NVL(P_DELID,WDA.DELIVERY_ID)
                           AND WDA.delivery_id     = wdl.delivery_id(+)
                           AND wdl.pick_up_stop_id = wts.stop_id(+)
                           AND NVL(wts.trip_id,-1) = NVL(P_TRIP_ID,NVL(wts.trip_id,-1));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_PRE_CUM_LQTY := 0;
               END;
               DBMS_OUTPUT.PUT_LINE ('V_PRE_CUM_LQTY ' || V_PRE_CUM_LQTY);
               BEGIN
                  SELECT   NVL (SUM (NVL (WDD.PICKED_QUANTITY, 0)), 0)
                    INTO   V_CUM_LQTY
                    FROM   WSH_DELIVERY_DETAILS WDD,
                           WSH_DELIVERY_ASSIGNMENTS WDA,
                           wsh_delivery_legs wdl,wsh_trip_stops wts
                   WHERE       WDD.SOURCE_CODE = 'OE'
                           AND WDD.SOURCE_LINE_ID <= P_LINEID
                           AND WDD.INVENTORY_ITEM_ID = P_ITEMID
                           AND WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                           AND WDA.DELIVERY_ID = NVL(P_DELID,WDA.DELIVERY_ID)
                           AND WDA.delivery_id     = wdl.delivery_id(+)
                           AND wdl.pick_up_stop_id = wts.stop_id(+)
                           AND NVL(wts.trip_id,-1) = NVL(P_TRIP_ID,NVL(wts.trip_id,-1));
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_CUM_LQTY := 0;
               END;
               DBMS_OUTPUT.PUT_LINE ('V_CUM_LQTY ' || V_CUM_LQTY);
               BEGIN
                  IF V_CUM_LQTY > V_PRE_CUM_BOXQTY
                  THEN
                     V_AVAIL_QTY := V_CUM_BOXQTY - V_PRE_CUM_LQTY;
                  ELSE
                     V_AVAIL_QTY := 0;
                  END IF;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     V_AVAIL_QTY := 0;
               END;

               IF V_AVAIL_QTY > V_LQTY
               THEN
                  V_QTY := V_LQTY;
               ELSIF V_AVAIL_QTY > 0
               THEN
                  V_QTY := V_AVAIL_QTY;
               ELSE
                  V_QTY := 0;
               END IF;

               DBMS_OUTPUT.PUT_LINE ('V_AVAIL_QTY ' || V_AVAIL_QTY);
            EXCEPTION
               WHEN OTHERS
               THEN
                  V_QTY := 0;
            END;
         END IF;

         DBMS_OUTPUT.PUT_LINE ('V_QTY ' || V_QTY);

         RETURN V_QTY;
      EXCEPTION
         WHEN OTHERS
         THEN
            RETURN 0;
      END;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END ITEM_PALL_QTY;
-- END OF new pallet logic

   FUNCTION CUST_PO_NUMBER (P_DELIVERY_ID IN NUMBER, P_ITEM_ID IN NUMBER, P_TRIP_ID  IN NUMBER)
      RETURN VARCHAR2
   AS
      V_STRING   VARCHAR2 (500) := 'A';
   BEGIN
      FOR REC
      IN (  SELECT   DISTINCT OOH.CUST_PO_NUMBER, WDD.SOURCE_HEADER_ID
              FROM   WSH_DELIVERY_DETAILS WDD,
                     WSH_DELIVERY_ASSIGNMENTS WDA,
                     OE_ORDER_HEADERS_ALL OOH,
                     wsh_delivery_legs wdl,
                     wsh_trip_stops wts
             WHERE       WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                     AND WDA.DELIVERY_ID = NVL(P_DELIVERY_ID,WDA.DELIVERY_ID)
                     AND WDD.INVENTORY_ITEM_ID = P_ITEM_ID
                     AND OOH.HEADER_ID = WDD.SOURCE_HEADER_ID
                     AND WDA.delivery_id     = wdl.delivery_id(+)
                     AND wdl.pick_up_stop_id = wts.stop_id(+)
                     AND NVL(wts.trip_id,-1) = NVL(P_TRIP_ID,NVL(wts.trip_id,-1))
          ORDER BY   WDD.SOURCE_HEADER_ID)
      LOOP
         IF V_STRING = 'A'
         THEN
            V_STRING := NULL;

            V_STRING := REC.CUST_PO_NUMBER;
         ELSE
            V_STRING := V_STRING || ', ' || REC.CUST_PO_NUMBER;
         END IF;
      END LOOP;

      RETURN V_STRING;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION SHIPPED_QTY (P_DELIVERY_DETAIL_ID IN NUMBER, P_ITEM_ID IN NUMBER)
      RETURN NUMBER
   AS
      V_SHIPPED_QTY   NUMBER := 0;
   BEGIN
      SELECT   NVL (SUM (PICKED_QUANTITY), 0)
        INTO   V_SHIPPED_QTY
        FROM   WSH_DELIVERY_DETAILS WDD, wsh_delivery_assignments wda
       WHERE       WDD.DELIVERY_DETAIL_ID = wda.DELIVERY_DETAIL_ID
               AND WDA.DELIVERY_ID = P_DELIVERY_DETAIL_ID
               AND WDD.INVENTORY_ITEM_ID = P_ITEM_ID;


      RETURN V_SHIPPED_QTY;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION REQ_QTY (P_DELIVERY_DETAIL_ID IN NUMBER, P_ITEM_ID IN NUMBER)
      RETURN NUMBER
   AS
      V_REQ_QTY   NUMBER := 0;
   BEGIN
      SELECT   NVL (SUM (REQUESTED_QUANTITY), 0)
        INTO   V_REQ_QTY
        FROM   WSH_DELIVERY_DETAILS WDD, wsh_delivery_assignments wda
       WHERE       WDD.DELIVERY_DETAIL_ID = wda.DELIVERY_DETAIL_ID
               AND WDA.DELIVERY_ID = P_DELIVERY_DETAIL_ID
               AND WDD.INVENTORY_ITEM_ID = P_ITEM_ID;

      RETURN V_REQ_QTY;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION ORDER_LINE_SHIP (P_DELIVERY_DETAIL_ID   IN NUMBER,
                             P_ITEM_ID              IN NUMBER,
                             P_TRIP_ID              IN NUMBER)
      RETURN VARCHAR2
   AS
      V_STRING   VARCHAR2 (500) := 'A';
   BEGIN
      FOR REC
      IN (  SELECT   DISTINCT
                     OOH.ORDER_NUMBER, OOL.LINE_NUMBER, OOL.SHIPMENT_NUMBER
              FROM   WSH_DELIVERY_DETAILS WDD,
                     WSH_DELIVERY_ASSIGNMENTS WDA,
                     OE_ORDER_HEADERS_ALL OOH,
                     OE_ORDER_LINES_ALL OOL,
                     wsh_delivery_legs wdl,
                     wsh_trip_stops wts
             WHERE       WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                     AND WDA.DELIVERY_ID =  nvl(P_DELIVERY_DETAIL_ID,WDA.DELIVERY_ID)
                     AND WDD.INVENTORY_ITEM_ID = P_ITEM_ID
                     AND WDD.SOURCE_LINE_ID = OOL.LINE_ID
                     AND OOH.HEADER_ID = OOL.HEADER_ID
                     AND WDA.delivery_id     = wdl.delivery_id(+)
                     AND wdl.pick_up_stop_id = wts.stop_id(+)
                     AND NVL(wts.trip_id,-1) = NVL(P_TRIP_ID,NVL(wts.trip_id,-1))
          ORDER BY   OOH.ORDER_NUMBER, OOL.LINE_NUMBER, OOL.SHIPMENT_NUMBER)
      LOOP
         IF V_STRING = 'A'
         THEN
            V_STRING := NULL;

            V_STRING :=
                  REC.ORDER_NUMBER
               || ' / '
               || REC.LINE_NUMBER
               || ' / '
               || REC.SHIPMENT_NUMBER;
         ELSE
            V_STRING :=
                  V_STRING
               || ', '
               || REC.ORDER_NUMBER
               || ' / '
               || REC.LINE_NUMBER
               || ' / '
               || REC.SHIPMENT_NUMBER;
         END IF;
      END LOOP;

      RETURN V_STRING;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END;

   FUNCTION BOX_PALLET_QTY (P_DELIVERY_ID         IN NUMBER,
                            P_INVENTORY_ITEM_ID   IN NUMBER,
                            P_PALLET_NUMBER       IN NUMBER,
                            P_BOX_NUMBER          IN number)
      RETURN NUMBER
   AS
      V_QTY   NUMBER := 0;
   BEGIN
      SELECT   NVL (SUM (AUDIT_QUANTITY), 0)
        INTO   V_QTY
        FROM   XXFOX_AUDIT_LINES
       WHERE       DELIVERY_ID = P_DELIVERY_ID
               AND INVENTORY_ITEM_ID = P_INVENTORY_ITEM_ID
               AND BOX_NUMBER = P_BOX_NUMBER
               AND PALLET_NUMBER = P_PALLET_NUMBER;

      RETURN V_QTY;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION AFTERPFORM
      RETURN BOOLEAN
   IS
   BEGIN
      --STANDALONE CHANGES
      IF WMS_DEPLOY.WMS_DEPLOYMENT_MODE = 'D'
      THEN
         P_STANDALONE := 'Y';
      ELSE
         P_STANDALONE := 'N';
      END IF;

      BEGIN
       SELECT del.delivery_id,
            tdv.TRIP_ID
        INTO CP_DELIVERY_ID,CP_TRIP_ID
        FROM wsh_new_deliveries del ,
          wsh_document_instances doc ,
          WSH_TRIP_DELIVERIES_V tdv
        WHERE doc.entity_id  =del.delivery_id
        AND doc.entity_name  = 'WSH_NEW_DELIVERIES'
        AND doc.document_type= 'PACK_TYPE'
        AND del.delivery_id   = NVL(P_DELIVERY_ID,del.delivery_id)
        AND del.delivery_type = 'STANDARD'
        AND tdv.delivery_id   (+) = del.delivery_id
        --AND tdv.TRIP_ID       = NVL(P_TRIP_ID,tdv.TRIP_ID)
        and (p_trip_id is null or tdv.trip_id = p_trip_id)
        AND NVL(del.confirm_date,sysdate) BETWEEN NVL(to_date(P_DELIVERY_DATE_LOW,'dd-mon-yyyy'),NVL(del.confirm_date,(sysdate-1)))
        AND NVL((to_date(P_DELIVERY_DATE_HIGH,'dd-mon-yyyy') + (86399/86400)), NVL(del.confirm_date,(sysdate+1)))
        AND ROWNUM < 2;

        fnd_file.put_line (fnd_file.LOG,
                            'Get CP_DELIVERY_ID, CP_TRIP_ID: ' || CP_DELIVERY_ID||'-'||CP_TRIP_ID);

      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                            'Exception at CP_DELIVERY_ID, CP_TRIP_ID: ' || CP_DELIVERY_ID||'-'||CP_TRIP_ID);

      END;


      RETURN (TRUE);
   END AFTERPFORM;


   FUNCTION TO_RECIPIENTS
      RETURN VARCHAR2 AS
      l_customer_id NUMBER:= NULL;
      l_email_address varchar2(1000):=NULL;
       BEGIN

         SELECT   d.CUSTOMER_ID
           INTO   l_customer_id
           FROM   wsh_new_deliveries d,
                  WSH_TRIP_DELIVERIES_V tdv
          WHERE   1=1
                  AND d.delivery_id = NVL(P_DELIVERY_ID,d.delivery_id)
                  AND tdv.delivery_id (+) =d.delivery_id
                  --AND tdv.TRIP_ID = NVL(P_TRIP_ID,tdv.TRIP_ID)
                  and (p_trip_id is null or tdv.trip_id = p_trip_id)
                  AND ROWNUM = 1;

         l_email_address :=
            XXFOX_BURSTING_EMAILS_PKG.MAIN (p_customer_id    => l_customer_id,
                                            p_cust_site_id   => NULL,
                                            P_EMAIL_ADD      => NULL,
                                            p_report_name    => 'PACK');

         fnd_file.put_line (fnd_file.LOG,
                            'EMAIL_ADDRESS: ' || l_email_address);

         IF l_email_address IS NOT NULL
         THEN
            g_process_flag := 'Y';
         ELSE
            g_process_flag := 'N';
         END IF;
         RETURN l_email_address;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG,
                            'Exception at Pulling Email address: ' || sqlerrm);
                            RETURN NULL;
      END;



   FUNCTION AFTERREPORT
      RETURN BOOLEAN
   IS
      L_RETURN_STATUS   VARCHAR2 (5);
      L_MSG_COUNT       NUMBER;
      L_MSG_DATA        VARCHAR2 (3000);
      l_req_id          NUMBER;
      l_customer_id     NUMBER;
   BEGIN


      BEGIN
         IF P_PRINT_MODE = 'FINAL'
         THEN
            WSH_DOCUMENT_PVT.SET_FINAL_PRINT_DATE (1.0,
                                                   NULL,
                                                   'T',
                                                   NULL,
                                                   L_RETURN_STATUS,
                                                   L_MSG_COUNT,
                                                   L_MSG_DATA,
                                                   CP_DELIVERY_ID,
                                                   'PACK_TYPE',
                                                   CP_PRINT_DATE);

            IF L_RETURN_STATUS <> 'S'
            THEN
               /*SRW.MESSAGE(1
                                ,'Fatal error encountered when trying to set final print date.')*/
               NULL;
               /*RAISE SRW.PROGRAM_ABORT*/
               RAISE_APPLICATION_ERROR (-20101, NULL);
            END IF;
         END IF;
      END;
      BEGIN
         /*SRW.USER_EXIT('FND SRWEXIT')*/
         NULL;
      EXCEPTION
         WHEN                                        /*SRW.USER_EXIT_FAILURE*/
             OTHERS
         THEN
            /*SRW.MESSAGE(1
                       ,'Failed in SRWEXIT')*/
            NULL;
            RAISE;
      END;
      --Bursting process


      BEGIN
         IF g_process_flag = 'Y'
         THEN
            l_req_id :=
               fnd_request.submit_request ('XDO',
                                           'XDOBURSTREP',
                                           NULL,
                                           SYSDATE,
                                           FALSE,
                                           'Y',
                                           FND_GLOBAL.CONC_REQUEST_ID,
                                           'N');

            IF l_req_id > 0
            THEN
               fnd_file.put_line (
                  fnd_file.LOG,
                  'Bursting Request Id submitted: ' || l_req_id
               );
            ELSE
               fnd_file.put_line (
                  fnd_file.LOG,
                  'Bursting Request Id could not be submitted.'
               );
            END IF;
         ELSE
            fnd_file.put_line (fnd_file.LOG, 'No Records');
         END IF;
      EXCEPTION
         WHEN OTHERS
         THEN
            fnd_file.put_line (fnd_file.LOG, 'Exception - '||SQLERRM);
      END;


      RETURN (TRUE);
   EXCEPTION
      WHEN OTHERS
      THEN
      fnd_file.put_line (fnd_file.LOG, 'After Report Exception - '||SQLERRM);
         RETURN (TRUE);
   END AFTERREPORT;

   FUNCTION CF_ITEM_NUMFORMULA (C_ITEM_FLEX     IN VARCHAR2,
                                C_INV_ITEM_ID   IN NUMBER,
                                C_DEL_ORG_ID    IN NUMBER)
      RETURN VARCHAR2
   IS
      L_ITEM_FLEX   VARCHAR2 (2000);
   BEGIN
      /*SRW.REFERENCE(C_ITEM_FLEX)*/
      NULL;
      -- LSP PROJECT : passing p_remove_client_code as 'Y'
      L_ITEM_FLEX :=
         WSH_UTIL_CORE.GET_ITEM_NAME (
            p_item_id              => C_INV_ITEM_ID,
            p_organization_id      => NVL (C_DEL_ORG_ID, P_ORGANIZATION_ID),
            p_remove_client_code   => 'Y'
         );
      RETURN (L_ITEM_FLEX);
   EXCEPTION
      WHEN                                           /*SRW.USER_EXIT_FAILURE*/
          OTHERS
      THEN
         RETURN ('User Exit Failure');
   END CF_ITEM_NUMFORMULA;

   FUNCTION CF_FREIGHT_CARRIERFORMULA (C_SHIP_METHOD      IN VARCHAR2,
                                       C_DEL_ORG_ID       IN NUMBER,
                                       C_Q2_DELIVERY_ID   IN NUMBER)
      RETURN VARCHAR2
   IS
      L_CARRIER            VARCHAR2 (35);
      L_COUNT              NUMBER;
      L_SHIP_METHOD_CODE   WSH_TRIPS.SHIP_METHOD_CODE%TYPE;
      L_TRIP_PRESENT       BOOLEAN := FALSE;
   BEGIN
      /*SRW.REFERENCE(C_SHIP_METHOD)*/
      NULL;
      /*SRW.REFERENCE(C_DEL_ORG_ID)*/
      NULL;
      /*SRW.REFERENCE(P_ORGANIZATION_ID)*/
      NULL;
      /*SRW.REFERENCE(C_Q2_DELIVERY_ID)*/
      NULL;
      BEGIN
         L_SHIP_METHOD_CODE := NULL;

         SELECT   WT.SHIP_METHOD_CODE
           INTO   L_SHIP_METHOD_CODE
           FROM   WSH_TRIPS WT,
                  WSH_TRIP_STOPS WTS,
                  WSH_DELIVERY_ASSIGNMENTS_V WDA,
                  WSH_DELIVERY_LEGS WDL
          WHERE       WTS.STOP_ID = WDL.PICK_UP_STOP_ID
                  AND WDA.DELIVERY_ID = C_Q2_DELIVERY_ID
                  AND WDA.DELIVERY_ID = WDL.DELIVERY_ID
                  AND WTS.TRIP_ID = WT.TRIP_ID
                  AND ROWNUM = 1;

         IF SQL%NOTFOUND
         THEN
            NULL;
         END IF;
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            L_SHIP_METHOD_CODE := NULL;
      END;

      IF L_SHIP_METHOD_CODE IS NULL
      THEN
         L_SHIP_METHOD_CODE := C_SHIP_METHOD;
      ELSE
         L_TRIP_PRESENT := TRUE;
      END IF;

      IF L_SHIP_METHOD_CODE IS NOT NULL
      THEN
         SELECT   COUNT(NVL (SUBSTRB (A.SHIP_METHOD_MEANING, 1, 35),
                             L_SHIP_METHOD_CODE))
           INTO   L_COUNT
           FROM   WSH_CARRIER_SERVICES A, WSH_ORG_CARRIER_SERVICES B
          WHERE   A.SHIP_METHOD_CODE = L_SHIP_METHOD_CODE
                  AND A.CARRIER_SERVICE_ID = B.CARRIER_SERVICE_ID
                  AND B.ORGANIZATION_ID =
                        NVL (C_DEL_ORG_ID, P_ORGANIZATION_ID);

         IF NVL (L_COUNT, 0) <> 1
         THEN
            L_CARRIER := '';
         ELSE
            SELECT   NVL (SUBSTRB (A.SHIP_METHOD_MEANING, 1, 35),
                          L_SHIP_METHOD_CODE)
              INTO   L_CARRIER
              FROM   WSH_CARRIER_SERVICES A, WSH_ORG_CARRIER_SERVICES B
             WHERE   A.SHIP_METHOD_CODE = L_SHIP_METHOD_CODE
                     AND A.CARRIER_SERVICE_ID = B.CARRIER_SERVICE_ID
                     AND B.ORGANIZATION_ID =
                           NVL (C_DEL_ORG_ID, P_ORGANIZATION_ID);
         END IF;
      ELSE
         L_CARRIER := '';
      END IF;

      IF (L_TRIP_PRESENT AND L_CARRIER IS NULL)
      THEN
         L_CARRIER := L_SHIP_METHOD_CODE;
      END IF;

      RETURN (L_CARRIER);
   END CF_FREIGHT_CARRIERFORMULA;

   FUNCTION CF_FREIGHT_TERMSFORMULA (C_FREIGHT_TERMS_CODE IN VARCHAR2)
      RETURN VARCHAR2
   IS
      L_FREIGHT_TERMS   VARCHAR2 (80);
   BEGIN
      /*SRW.REFERENCE(C_FREIGHT_TERMS_CODE)*/
      NULL;

      IF CP_SOURCE_CODE = 'OE'
      THEN
         IF C_FREIGHT_TERMS_CODE IS NOT NULL
         THEN
            SELECT   FV.FREIGHT_TERMS
              INTO   L_FREIGHT_TERMS
              FROM   OE_FRGHT_TERMS_ACTIVE_V FV
             WHERE   FV.FREIGHT_TERMS_CODE = C_FREIGHT_TERMS_CODE;
         ELSE
            L_FREIGHT_TERMS := '';
         END IF;
      ELSIF CP_SOURCE_CODE = 'OKE'
      THEN
         L_FREIGHT_TERMS := '';
      END IF;

      RETURN (L_FREIGHT_TERMS);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         /*SRW.MESSAGE(1
                    ,'Freight terms lookups not found.')*/
         NULL;
         RETURN (NULL);
      WHEN OTHERS
      THEN
         RAISE;
   END CF_FREIGHT_TERMSFORMULA;

   FUNCTION CF_FOBFORMULA (C_FOB_CODE IN VARCHAR2)
      RETURN VARCHAR2
   IS
      L_FOB   VARCHAR2 (80);
   BEGIN
      /*SRW.REFERENCE(C_FOB_CODE)*/
      NULL;

      IF CP_SOURCE_CODE = 'OE'
      THEN
         IF C_FOB_CODE IS NOT NULL
         THEN
            SELECT   FOB
              INTO   L_FOB
              FROM   OE_FOBS_ACTIVE_V FV
             WHERE   FV.FOB_CODE = C_FOB_CODE;
         ELSE
            L_FOB := '';
         END IF;
      ELSIF CP_SOURCE_CODE = 'OKE'
      THEN
         L_FOB := '';
      END IF;

      RETURN (L_FOB);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         /*SRW.MESSAGE(1
                    ,'FOB lookups not found.')*/
         NULL;
         RETURN (NULL);
      WHEN OTHERS
      THEN
         RAISE;
   END CF_FOBFORMULA;

   FUNCTION CF_CUST_ITEM_NUMFORMULA (C_CUSTOMER_ITEM_ID IN NUMBER)
      RETURN VARCHAR2
   IS
      L_CUST_ITEM_NUMBER   VARCHAR2 (50);
   BEGIN
      /*SRW.REFERENCE(C_CUSTOMER_ITEM_ID)*/
      NULL;

      IF C_CUSTOMER_ITEM_ID IS NOT NULL
      THEN
         SELECT   CUSTOMER_ITEM_NUMBER
           INTO   L_CUST_ITEM_NUMBER
           FROM   MTL_CUSTOMER_ITEMS MCI
          WHERE   MCI.CUSTOMER_ITEM_ID = C_CUSTOMER_ITEM_ID;
      ELSE
         L_CUST_ITEM_NUMBER := '';
      END IF;

      RETURN (L_CUST_ITEM_NUMBER);
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN ('');
   END CF_CUST_ITEM_NUMFORMULA;

   FUNCTION CF_FROM_CITY_STATE_ZIPFORMULA (C_FROM_CITY          IN VARCHAR2,
                                           C_FROM_POSTAL_CODE   IN VARCHAR2,
                                           C_FROM_REGION        IN VARCHAR2)
      RETURN VARCHAR2
   IS
      CITY_REGION_POSTAL   VARCHAR2 (190);
   BEGIN
      /*SRW.REFERENCE(C_FROM_CITY)*/
      NULL;
      /*SRW.REFERENCE(C_FROM_POSTAL_CODE)*/
      NULL;
      /*SRW.REFERENCE(C_FROM_REGION)*/
      NULL;
      CITY_REGION_POSTAL :=
         WSH_UTIL_CORE.CITY_REGION_POSTAL (C_FROM_CITY,
                                           C_FROM_REGION,
                                           C_FROM_POSTAL_CODE);

      IF (CITY_REGION_POSTAL IS NULL)
      THEN
         CITY_REGION_POSTAL :=
            C_FROM_CITY || ', ' || C_FROM_REGION || ' ' || C_FROM_POSTAL_CODE;
      END IF;

      RETURN (CITY_REGION_POSTAL);
   END CF_FROM_CITY_STATE_ZIPFORMULA;

   FUNCTION CF_TO_CITY_STATE_ZIPFORMULA (C_TO_CITY          IN VARCHAR2,
                                         C_TO_POSTAL_CODE   IN VARCHAR2,
                                         C_TO_REGION        IN VARCHAR2)
      RETURN VARCHAR2
   IS
      CITY_REGION_POSTAL   VARCHAR2 (190);
   BEGIN
      /*SRW.REFERENCE(C_TO_CITY)*/
      NULL;
      /*SRW.REFERENCE(C_TO_POSTAL_CODE)*/
      NULL;
      /*SRW.REFERENCE(C_TO_REGION)*/
      NULL;
      CITY_REGION_POSTAL :=
         WSH_UTIL_CORE.CITY_REGION_POSTAL (C_TO_CITY,
                                           C_TO_REGION,
                                           C_TO_POSTAL_CODE);
      RETURN (CITY_REGION_POSTAL);
   END CF_TO_CITY_STATE_ZIPFORMULA;

   FUNCTION CF_BILL_CITY_STATE_ZIPFORMULA (
      CP_BILL_TOWN_OR_CITY   IN varchar2,
      CP_BILL_POSTAL_CODE    IN varchar2,
      CP_BILL_REGION         IN varchar2
   )
      RETURN VARCHAR2
   IS
      CITY_REGION_POSTAL   VARCHAR2 (190);
   BEGIN
      /*SRW.REFERENCE(CP_BILL_TOWN_OR_CITY)*/
      NULL;
      /*SRW.REFERENCE(CP_BILL_POSTAL_CODE)*/
      NULL;
      /*SRW.REFERENCE(CP_BILL_REGION)*/
      NULL;
      CITY_REGION_POSTAL :=
         WSH_UTIL_CORE.CITY_REGION_POSTAL (CP_BILL_TOWN_OR_CITY,
                                           CP_BILL_REGION,
                                           CP_BILL_POSTAL_CODE);

      IF (CITY_REGION_POSTAL IS NULL)
      THEN
         CITY_REGION_POSTAL :=
               CP_BILL_TOWN_OR_CITY
            || ', '
            || CP_BILL_REGION
            || ' '
            || CP_BILL_POSTAL_CODE;
      END IF;

      RETURN (CITY_REGION_POSTAL);
   END CF_BILL_CITY_STATE_ZIPFORMULA;

   FUNCTION CF_CUM_QTYFORMULA (C_DEL_CUSTOMER_ID   IN NUMBER,
                               C_CUSTOMER_ID       IN NUMBER,
                               C_SRC_LINE_ID       IN NUMBER)
      RETURN NUMBER
   IS
      L_RESULT          NUMBER;
      L_RETURN_STATUS   VARCHAR2 (5);
      L_MSG_COUNT       NUMBER;
      L_MSG_DATA        VARCHAR2 (3000);
      L_CUSTOMER_ID     WSH_NEW_DELIVERIES.CUSTOMER_ID%TYPE;
   BEGIN
      /*SRW.REFERENCE(C_DEL_CUSTOMER_ID)*/
      NULL;
      /*SRW.REFERENCE(C_CUSTOMER_ID)*/
      NULL;
      /*SRW.REFERENCE(C_SRC_LINE_ID)*/
      NULL;
      L_CUSTOMER_ID := NVL (C_CUSTOMER_ID, C_DEL_CUSTOMER_ID);
      L_RESULT :=
         WSH_DOCUMENT_PVT.GET_CUMQTY (1.0,
                                      NULL,
                                      NULL,
                                      NULL,
                                      L_RETURN_STATUS,
                                      L_MSG_COUNT,
                                      L_MSG_DATA,
                                      L_CUSTOMER_ID,
                                      C_SRC_LINE_ID);
      RETURN ROUND (NVL (L_RESULT, 0), P_QUANTITY_PRECISION);
   END CF_CUM_QTYFORMULA;

   FUNCTION CF_UNSHIPPED_QTYFORMULA (C_SRC_LINE_ID IN NUMBER)
      RETURN NUMBER
   IS
      TOTAL_UNSHIPPED_QUANTITY   NUMBER;
      L_LINE_SET_ID              NUMBER;
      L_LINE_ID                  NUMBER;
   BEGIN
      IF CP_SOURCE_CODE = 'OE'
      THEN
         SELECT   NVL (L.LINE_SET_ID, -999), L.LINE_ID
           INTO   L_LINE_SET_ID, L_LINE_ID
           FROM   OE_ORDER_LINES_ALL L
          WHERE   L.LINE_ID = C_SRC_LINE_ID;

         SELECT   SUM (WDD.REQUESTED_QUANTITY)
                  - NVL (SUM (WDD.SHIPPED_QUANTITY), 0)
           INTO   TOTAL_UNSHIPPED_QUANTITY
           FROM   WSH_DELIVERY_DETAILS WDD, OE_ORDER_LINES_ALL L
          WHERE       WDD.SOURCE_LINE_ID = L.LINE_ID
                  AND WDD.SOURCE_CODE = 'OE'
                  AND WDD.CONTAINER_FLAG = 'N'
                  AND ( (L.LINE_SET_ID IS NOT NULL
                         AND L.LINE_SET_ID = L_LINE_SET_ID)
                       OR (L.LINE_ID = L_LINE_ID));
      ELSIF CP_SOURCE_CODE = 'OKE'
      THEN
         SELECT   SUM (WDD.REQUESTED_QUANTITY)
                  - NVL (SUM (WDD.SHIPPED_QUANTITY), 0)
           INTO   TOTAL_UNSHIPPED_QUANTITY
           FROM   WSH_DELIVERY_DETAILS WDD
          WHERE   WDD.SOURCE_LINE_ID = C_SRC_LINE_ID
                  AND WDD.SOURCE_CODE = 'OKE';
      END IF;

      RETURN TOTAL_UNSHIPPED_QUANTITY;
   END CF_UNSHIPPED_QTYFORMULA;

   FUNCTION CF_FROM_ADDR_2FORMULA (C_FROM_ADDRESS_2         IN VARCHAR2,
                                   C_FROM_ADDRESS_3         IN VARCHAR2,
                                   CF_FROM_CITY_STATE_ZIP   IN VARCHAR2,
                                   C_FROM_COUNTRY           IN VARCHAR2)
      RETURN VARCHAR2
   IS
   BEGIN
      /*SRW.REFERENCE(C_FROM_ADDRESS_2)*/
      NULL;
      /*SRW.REFERENCE(C_FROM_ADDRESS_3)*/
      NULL;
      /*SRW.REFERENCE(CF_FROM_CITY_STATE_ZIP)*/
      NULL;
      /*SRW.REFERENCE(C_FROM_COUNTRY)*/
      NULL;

      IF (C_FROM_ADDRESS_2 IS NOT NULL)
      THEN
         RETURN (C_FROM_ADDRESS_2);
      ELSIF (C_FROM_ADDRESS_2 IS NULL AND C_FROM_ADDRESS_3 IS NOT NULL)
      THEN
         RETURN (C_FROM_ADDRESS_3);
      ELSIF (C_FROM_ADDRESS_2 IS NULL AND C_FROM_ADDRESS_3 IS NULL)
      THEN
         RETURN (CF_FROM_CITY_STATE_ZIP || ', ' || C_FROM_COUNTRY);
      END IF;
   END CF_FROM_ADDR_2FORMULA;

   FUNCTION CF_FROM_ADDR3FORMULA (C_FROM_ADDRESS_2         IN VARCHAR2,
                                  C_FROM_ADDRESS_3         IN VARCHAR2,
                                  CF_FROM_CITY_STATE_ZIP   IN VARCHAR2,
                                  C_FROM_COUNTRY           IN VARCHAR2)
      RETURN VARCHAR2
   IS
   BEGIN
      /*SRW.REFERENCE(C_FROM_ADDRESS_2)*/
      NULL;
      /*SRW.REFERENCE(C_FROM_ADDRESS_3)*/
      NULL;
      /*SRW.REFERENCE(CF_FROM_CITY_STATE_ZIP)*/
      NULL;
      /*SRW.REFERENCE(C_FROM_COUNTRY)*/
      NULL;

      IF (C_FROM_ADDRESS_2 IS NOT NULL AND C_FROM_ADDRESS_3 IS NOT NULL)
      THEN
         RETURN (C_FROM_ADDRESS_3);
      ELSIF (C_FROM_ADDRESS_2 IS NOT NULL AND C_FROM_ADDRESS_3 IS NULL)
      THEN
         RETURN (CF_FROM_CITY_STATE_ZIP || ', ' || C_FROM_COUNTRY);
      ELSIF (C_FROM_ADDRESS_2 IS NULL AND C_FROM_ADDRESS_3 IS NOT NULL)
      THEN
         RETURN (CF_FROM_CITY_STATE_ZIP || ', ' || C_FROM_COUNTRY);
      ELSIF (C_FROM_ADDRESS_2 IS NULL AND C_FROM_ADDRESS_3 IS NULL)
      THEN
         RETURN ('                  ');
      END IF;
   END CF_FROM_ADDR3FORMULA;

   FUNCTION CF_FROM_CITYFORMULA (C_FROM_ADDRESS_2         IN VARCHAR2,
                                 C_FROM_ADDRESS_3         IN VARCHAR2,
                                 CF_FROM_CITY_STATE_ZIP   IN VARCHAR2,
                                 C_FROM_COUNTRY           IN VARCHAR2)
      RETURN CHAR
   IS
   BEGIN
      /*SRW.REFERENCE(C_FROM_ADDRESS_2)*/
      NULL;
      /*SRW.REFERENCE(C_FROM_ADDRESS_3)*/
      NULL;
      /*SRW.REFERENCE(CF_FROM_CITY_STATE_ZIP)*/
      NULL;
      /*SRW.REFERENCE(C_FROM_COUNTRY)*/
      NULL;

      IF (C_FROM_ADDRESS_2 IS NOT NULL AND C_FROM_ADDRESS_3 IS NOT NULL)
      THEN
         RETURN (CF_FROM_CITY_STATE_ZIP || ', ' || C_FROM_COUNTRY);
      ELSIF (C_FROM_ADDRESS_2 IS NULL OR C_FROM_ADDRESS_3 IS NULL)
      THEN
         RETURN ('                     ');
      END IF;
   END CF_FROM_CITYFORMULA;

   FUNCTION CF_TO_ADDR_2FORMULA (C_TO_ADDRESS_2         IN VARCHAR2,
                                 C_TO_ADDRESS_3         IN VARCHAR2,
                                 C_TO_ADDRESS_4         IN VARCHAR2,
                                 CF_TO_CITY_STATE_ZIP   IN VARCHAR2,
                                 C_TO_COUNTRY           IN VARCHAR2)
      RETURN CHAR
   IS
   BEGIN
      /*SRW.REFERENCE(C_TO_ADDRESS_2)*/
      NULL;
      /*SRW.REFERENCE(C_TO_ADDRESS_3)*/
      NULL;
      /*SRW.REFERENCE(C_TO_ADDRESS_4)*/
      NULL;
      /*SRW.REFERENCE(CF_TO_CITY_STATE_ZIP)*/
      NULL;
      /*SRW.REFERENCE(C_TO_COUNTRY)*/
      NULL;

      IF (C_TO_ADDRESS_2 IS NOT NULL)
      THEN
         RETURN (C_TO_ADDRESS_2);
      ELSIF (C_TO_ADDRESS_2 IS NULL AND C_TO_ADDRESS_3 IS NOT NULL)
      THEN
         RETURN (C_TO_ADDRESS_3);
      ELSIF (C_TO_ADDRESS_2 IS NULL AND C_TO_ADDRESS_3 IS NULL AND C_TO_ADDRESS_4 IS NOT NULL)
      THEN
         RETURN (C_TO_ADDRESS_4);
      ELSIF (C_TO_ADDRESS_2 IS NULL AND C_TO_ADDRESS_3 IS NULL AND C_TO_ADDRESS_4 IS NULL)
      THEN
         IF (CF_TO_CITY_STATE_ZIP IS NULL)
         THEN
            RETURN (C_TO_COUNTRY);
         ELSE
            RETURN (CF_TO_CITY_STATE_ZIP || ', ' || C_TO_COUNTRY);
         END IF;
      END IF;
   END CF_TO_ADDR_2FORMULA;

   FUNCTION CF_TO_ADDR_3FORMULA (C_TO_ADDRESS_2         IN VARCHAR2,
                                 C_TO_ADDRESS_3         IN VARCHAR2,
                                 C_TO_ADDRESS_4         IN VARCHAR2,
                                 CF_TO_CITY_STATE_ZIP   IN VARCHAR2,
                                 C_TO_COUNTRY           IN VARCHAR2)
      RETURN CHAR
   IS
      L_CF_TO_CITY_STATE_ZIP   VARCHAR2 (190);
   BEGIN
      /*SRW.REFERENCE(C_TO_ADDRESS_2)*/
      NULL;
      /*SRW.REFERENCE(C_TO_ADDRESS_3)*/
      NULL;
      /*SRW.REFERENCE(C_TO_ADDRESS_4)*/
      NULL;
      /*SRW.REFERENCE(CF_TO_CITY_STATE_ZIP)*/
      NULL;
      /*SRW.REFERENCE(C_TO_COUNTRY)*/
      NULL;

      IF (CF_TO_CITY_STATE_ZIP IS NULL)
      THEN
         L_CF_TO_CITY_STATE_ZIP := '';
      ELSE
         L_CF_TO_CITY_STATE_ZIP := CF_TO_CITY_STATE_ZIP || ', ';
      END IF;

      IF (    C_TO_ADDRESS_2 IS NOT NULL
          AND C_TO_ADDRESS_3 IS NOT NULL
          AND C_TO_ADDRESS_4 IS NOT NULL)
      THEN
         RETURN (C_TO_ADDRESS_3);
      ELSIF (C_TO_ADDRESS_2 IS NOT NULL AND C_TO_ADDRESS_3 IS NULL AND C_TO_ADDRESS_4 IS NULL)
      THEN
         RETURN (L_CF_TO_CITY_STATE_ZIP || C_TO_COUNTRY);
      ELSIF (C_TO_ADDRESS_2 IS NOT NULL AND C_TO_ADDRESS_3 IS NULL AND C_TO_ADDRESS_4 IS NOT NULL)
      THEN
         RETURN (C_TO_ADDRESS_4);
      ELSIF (C_TO_ADDRESS_2 IS NULL AND C_TO_ADDRESS_3 IS NOT NULL AND C_TO_ADDRESS_4 IS NOT NULL)
      THEN
         RETURN (C_TO_ADDRESS_4);
      ELSIF (C_TO_ADDRESS_2 IS NOT NULL AND C_TO_ADDRESS_3 IS NOT NULL AND C_TO_ADDRESS_4 IS NULL)
      THEN
         RETURN (C_TO_ADDRESS_3);
      ELSIF (C_TO_ADDRESS_2 IS NULL AND C_TO_ADDRESS_3 IS NOT NULL AND C_TO_ADDRESS_4 IS NULL)
      THEN
         RETURN (L_CF_TO_CITY_STATE_ZIP || C_TO_COUNTRY);
      ELSIF (C_TO_ADDRESS_2 IS NULL AND C_TO_ADDRESS_3 IS NULL AND C_TO_ADDRESS_4 IS NOT NULL)
      THEN
         RETURN (L_CF_TO_CITY_STATE_ZIP || C_TO_COUNTRY);
      ELSIF (C_TO_ADDRESS_2 IS NULL AND C_TO_ADDRESS_3 IS NULL AND C_TO_ADDRESS_4 IS NULL)
      THEN
         RETURN ('                  ');
      END IF;
   END CF_TO_ADDR_3FORMULA;

   FUNCTION CF_TO_CITYFORMULA (C_TO_ADDRESS_2         IN VARCHAR2,
                               C_TO_ADDRESS_3         IN VARCHAR2,
                               CF_TO_CITY_STATE_ZIP   IN VARCHAR2,
                               C_TO_COUNTRY           IN VARCHAR2,
                               C_TO_ADDRESS_4         IN VARCHAR2)
      RETURN CHAR
   IS
   BEGIN
      /*SRW.REFERENCE(C_TO_ADDRESS_2)*/
      NULL;
      /*SRW.REFERENCE(C_TO_ADDRESS_3)*/
      NULL;
      /*SRW.REFERENCE(CF_TO_CITY_STATE_ZIP)*/
      NULL;
      /*SRW.REFERENCE(C_TO_COUNTRY)*/
      NULL;
      /*SRW.REFERENCE(C_TO_ADDRESS_4)*/
      NULL;

      IF (    C_TO_ADDRESS_2 IS NOT NULL
          AND C_TO_ADDRESS_3 IS NOT NULL
          AND C_TO_ADDRESS_4 IS NOT NULL)
      THEN
         RETURN (CF_TO_CITY_STATE_ZIP || ', ' || C_TO_COUNTRY);
      ELSE
         RETURN ('                     ');
      END IF;
   END CF_TO_CITYFORMULA;

   FUNCTION CF_BILL_ADDR_2FORMULA (CF_BILL_CITY_STATE_ZIP IN VARCHAR2)
      RETURN CHAR
   IS
   BEGIN
      /*SRW.REFERENCE(CP_BILL_ADDRESS_LINE_2)*/
      NULL;
      /*SRW.REFERENCE(CP_BILL_ADDRESS_LINE_3)*/
      NULL;
      /*SRW.REFERENCE(CP_BILL_ADDRESS_LINE_4)*/
      NULL;
      /*SRW.REFERENCE(CF_BILL_CITY_STATE_ZIP)*/
      NULL;
      /*SRW.REFERENCE(CP_BILL_COUNTRY)*/
      NULL;

      IF (CP_BILL_ADDRESS_LINE_2 IS NOT NULL)
      THEN
         RETURN (CP_BILL_ADDRESS_LINE_2);
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NULL AND CP_BILL_ADDRESS_LINE_3 IS NOT NULL)
      THEN
         RETURN (CP_BILL_ADDRESS_LINE_3);
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NULL AND CP_BILL_ADDRESS_LINE_3 IS NULL AND CP_BILL_ADDRESS_LINE_4 IS NOT NULL)
      THEN
         RETURN (CP_BILL_ADDRESS_LINE_4);
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NULL AND CP_BILL_ADDRESS_LINE_3 IS NULL AND CP_BILL_ADDRESS_LINE_4 IS NULL)
      THEN
         RETURN (CF_BILL_CITY_STATE_ZIP || ', ' || CP_BILL_COUNTRY);
      END IF;
   END CF_BILL_ADDR_2FORMULA;

   FUNCTION CF_BILL_ADDR_3FORMULA (CF_BILL_CITY_STATE_ZIP IN VARCHAR2)
      RETURN CHAR
   IS
   BEGIN
      /*SRW.REFERENCE(CP_BILL_ADDRESS_LINE_2)*/
      NULL;
      /*SRW.REFERENCE(CP_BILL_ADDRESS_LINE_3)*/
      NULL;
      /*SRW.REFERENCE(CP_BILL_ADDRESS_LINE_4)*/
      NULL;
      /*SRW.REFERENCE(CF_BILL_CITY_STATE_ZIP)*/
      NULL;
      /*SRW.REFERENCE(CP_BILL_COUNTRY)*/
      NULL;

      IF (CP_BILL_ADDRESS_LINE_2 IS NOT NULL
          AND CP_BILL_ADDRESS_LINE_3 IS NOT NULL)
      THEN
         RETURN (CP_BILL_ADDRESS_LINE_3);
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NOT NULL AND CP_BILL_ADDRESS_LINE_3 IS NULL AND CP_BILL_ADDRESS_LINE_4 IS NOT NULL)
      THEN
         RETURN (CP_BILL_ADDRESS_LINE_4);
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NOT NULL AND CP_BILL_ADDRESS_LINE_3 IS NULL AND CP_BILL_ADDRESS_LINE_4 IS NULL)
      THEN
         RETURN (CF_BILL_CITY_STATE_ZIP || ', ' || CP_BILL_COUNTRY);
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NULL AND CP_BILL_ADDRESS_LINE_3 IS NOT NULL AND CP_BILL_ADDRESS_LINE_4 IS NOT NULL)
      THEN
         RETURN (CP_BILL_ADDRESS_LINE_4);
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NULL AND CP_BILL_ADDRESS_LINE_3 IS NULL AND CP_BILL_ADDRESS_LINE_4 IS NOT NULL)
      THEN
         RETURN (CF_BILL_CITY_STATE_ZIP || ', ' || CP_BILL_COUNTRY);
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NULL AND CP_BILL_ADDRESS_LINE_3 IS NOT NULL AND CP_BILL_ADDRESS_LINE_4 IS NULL)
      THEN
         RETURN (CF_BILL_CITY_STATE_ZIP || ', ' || CP_BILL_COUNTRY);
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NULL AND CP_BILL_ADDRESS_LINE_3 IS NULL AND CP_BILL_ADDRESS_LINE_4 IS NULL)
      THEN
         RETURN ('');
      END IF;
   END CF_BILL_ADDR_3FORMULA;

   FUNCTION CF_BILL_CITYFORMULA (CF_BILL_CITY_STATE_ZIP IN VARCHAR2)
      RETURN CHAR
   IS
   BEGIN
      /*SRW.REFERENCE(CP_BILL_ADDRESS_LINE_2)*/
      NULL;
      /*SRW.REFERENCE(CP_BILL_ADDRESS_LINE_3)*/
      NULL;
      /*SRW.REFERENCE(CP_BILL_ADDRESS_LINE_4)*/
      NULL;
      /*SRW.REFERENCE(CF_BILL_CITY_STATE_ZIP)*/
      NULL;
      /*SRW.REFERENCE(CP_BILL_COUNTRY)*/
      NULL;

      IF (    CP_BILL_ADDRESS_LINE_2 IS NOT NULL
          AND CP_BILL_ADDRESS_LINE_3 IS NOT NULL
          AND CP_BILL_ADDRESS_LINE_4 IS NOT NULL)
      THEN
         RETURN (CF_BILL_CITY_STATE_ZIP || ', ' || CP_BILL_COUNTRY);
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NULL OR CP_BILL_ADDRESS_LINE_3 IS NULL OR CP_BILL_ADDRESS_LINE_4 IS NULL)
      THEN
         RETURN ('');
      END IF;
   END CF_BILL_CITYFORMULA;

   FUNCTION CF_UNSHIP_ITEM_NAMEFORMULA (BO_INVENTORY_ITEM_ID   IN NUMBER,
                                        BO_ORGANIZATION_ID     IN NUMBER)
      RETURN CHAR
   IS
      L_BO_ITEM_FLEX   VARCHAR2 (2000);
   BEGIN
      /*SRW.REFERENCE(BO_INVENTORY_ITEM_ID)*/
      NULL;
      /*SRW.REFERENCE(BO_ORGANIZATION_ID)*/
      NULL;
      L_BO_ITEM_FLEX :=
         WSH_UTIL_CORE.GET_ITEM_NAME (
            BO_INVENTORY_ITEM_ID,
            NVL (BO_ORGANIZATION_ID, P_ORGANIZATION_ID)
         );
      RETURN (L_BO_ITEM_FLEX);
   EXCEPTION
      WHEN                                           /*SRW.USER_EXIT_FAILURE*/
          OTHERS
      THEN
         RETURN ('User Exit Failure');
   END CF_UNSHIP_ITEM_NAMEFORMULA;

   FUNCTION CF_NUM_OF_LPNSFORMULA (C_Q1_DELIVERY_ID   IN NUMBER,
                                   NUM_LPN            IN NUMBER)
      RETURN NUMBER
   IS
   BEGIN
      DECLARE
         CURSOR BOXES
         IS
            SELECT   COUNT ( * )
              FROM   WSH_DELIVERY_ASSIGNMENTS_V WDA, WSH_DELIVERY_DETAILS WDD
             WHERE       WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                     AND WDD.CONTAINER_FLAG = 'Y'
                     AND WDA.PARENT_DELIVERY_DETAIL_ID IS NULL
                     AND WDA.DELIVERY_ID IS NOT NULL
                     AND WDA.DELIVERY_ID = C_Q1_DELIVERY_ID;

         NUM_OF_BOXES   NUMBER;
      BEGIN
         IF (NUM_LPN IS NULL)
         THEN
            OPEN BOXES;

            FETCH BOXES INTO   NUM_OF_BOXES;

            CLOSE BOXES;

            RETURN (NUM_OF_BOXES);
         ELSE
            RETURN (NUM_LPN);
         END IF;
      END;
      RETURN NULL;
   END CF_NUM_OF_LPNSFORMULA;

   FUNCTION CF_BILL_TO_LOC1FORMULA (CF_OE_LINE_ID   IN NUMBER,
                                    F_OE_LINE_ID    IN NUMBER)
      RETURN NUMBER
   IS
   BEGIN
      DECLARE
         BILL_TO_LOC_ID   NUMBER;
      BEGIN
         /*SRW.REFERENCE(CF_OE_LINE_ID)*/
         NULL;

         IF CP_SOURCE_CODE = 'OE'
         THEN
            SELECT   PS.LOCATION_ID
              INTO   BILL_TO_LOC_ID
              FROM   HZ_PARTY_SITES PS,
                     HZ_CUST_ACCT_SITES_ALL CA,
                     HZ_CUST_SITE_USES_ALL SU,
                     OE_ORDER_LINES_ALL OLA
             WHERE       OLA.LINE_ID = CF_OE_LINE_ID
                     AND SU.SITE_USE_ID = OLA.INVOICE_TO_ORG_ID
                     AND SU.CUST_ACCT_SITE_ID = CA.CUST_ACCT_SITE_ID
                     AND CA.PARTY_SITE_ID = PS.PARTY_SITE_ID;

            IF SQL%NOTFOUND
            THEN
               RETURN (NULL);
            END IF;
         ELSIF CP_SOURCE_CODE = 'OKE'
         THEN
            BILL_TO_LOC_ID := OKE_DTS_WSH_PKG.BILL_TO_LOCATION (F_OE_LINE_ID);
         END IF;

         RETURN (BILL_TO_LOC_ID);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN (NULL);
         WHEN OTHERS
         THEN
            RAISE;
      END;
   END CF_BILL_TO_LOC1FORMULA;

   FUNCTION CF_OE_LINE_ID1FORMULA
      RETURN NUMBER
   IS
   BEGIN
      DECLARE
         OE_LINE_ID           NUMBER;
         SHP_TO_CONTACT_ID    NUMBER;
         BILL_TO_CONTACT_ID   NUMBER;
      BEGIN
         /*SRW.REFERENCE(P_DELIVERY_ID)*/
         NULL;

         IF CP_SOURCE_CODE = 'OE'
         THEN
            SELECT   WDD.SOURCE_LINE_ID,
                     OLA.SHIP_TO_CONTACT_ID,
                     OLA.INVOICE_TO_CONTACT_ID
              INTO   OE_LINE_ID, SHP_TO_CONTACT_ID, BILL_TO_CONTACT_ID
              FROM   WSH_DELIVERY_DETAILS WDD,
                     WSH_DELIVERY_ASSIGNMENTS_V WDA,
                     OE_ORDER_LINES_ALL OLA
             WHERE       WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                     AND NVL (WDD.CONTAINER_FLAG, 'N') = 'N'
                     AND WDA.DELIVERY_ID = CP_DELIVERY_ID
                     AND WDA.DELIVERY_ID IS NOT NULL
                     AND WDD.SOURCE_CODE = 'OE'
                     AND WDD.SOURCE_LINE_ID = OLA.LINE_ID
                     AND ROWNUM < 2;

            IF SQL%NOTFOUND
            THEN
               CP_SHIP_TO_CONTACT_ID := NULL;
               CP_BILL_TO_CONTACT_ID := NULL;
               RETURN (NULL);
            END IF;

            CP_SHIP_TO_CONTACT_ID := SHP_TO_CONTACT_ID;
            CP_BILL_TO_CONTACT_ID := BILL_TO_CONTACT_ID;
         ELSIF CP_SOURCE_CODE = 'OKE'
         THEN
            SELECT   WDD.SOURCE_LINE_ID
              INTO   OE_LINE_ID
              FROM   WSH_DELIVERY_DETAILS WDD,
                     WSH_DELIVERY_ASSIGNMENTS_V WDA,
                     OE_ORDER_LINES_ALL OLA
             WHERE       WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                     AND WDD.CONTAINER_FLAG = 'N'
                     AND WDA.DELIVERY_ID = CP_DELIVERY_ID
                     AND WDA.DELIVERY_ID IS NOT NULL
                     AND WDD.SOURCE_CODE = 'OKE'
                     AND ROWNUM < 2;

            CP_SHIP_TO_CONTACT_ID := NULL;
            CP_BILL_TO_CONTACT_ID := NULL;
         END IF;

         RETURN (OE_LINE_ID);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            CP_SHIP_TO_CONTACT_ID := NULL;
            CP_BILL_TO_CONTACT_ID := NULL;
            RETURN (NULL);
         WHEN OTHERS
         THEN
            RAISE;
      END;
   END CF_OE_LINE_ID1FORMULA;

   FUNCTION CF_BILL_TO_CONTACT1FORMULA
      RETURN CHAR
   IS
      CONTACT_NAME        HZ_PARTIES.PARTY_NAME%TYPE;
      L_PERSON_TITLE      HZ_PARTIES.PERSON_TITLE%TYPE;
      L_PERSON_TITLE_UP   HZ_PARTIES.PERSON_TITLE%TYPE;
      L_LOOKUP_TYPE       VARCHAR2 (20);
   BEGIN
      /*SRW.REFERENCE(CP_BILL_TO_CONTACT_ID)*/
      NULL;

      IF (CP_BILL_TO_CONTACT_ID IS NOT NULL)
      THEN
         SELECT   PARTY.PARTY_NAME,
                  NVL (PARTY.PERSON_PRE_NAME_ADJUNCT, PARTY.PERSON_TITLE)
                     TITLE
           INTO   CONTACT_NAME, L_PERSON_TITLE
           FROM   HZ_CUST_ACCOUNT_ROLES ACCT_ROLE,
                  HZ_PARTIES PARTY,
                  HZ_RELATIONSHIPS REL,
                  HZ_ORG_CONTACTS ORG_CONT,
                  HZ_PARTIES REL_PARTY
          WHERE       ACCT_ROLE.CUST_ACCOUNT_ROLE_ID = CP_BILL_TO_CONTACT_ID
                  AND ACCT_ROLE.PARTY_ID = REL.PARTY_ID
                  AND ACCT_ROLE.ROLE_TYPE = 'CONTACT'
                  AND REL.SUBJECT_TABLE_NAME = 'HZ_PARTIES'
                  AND REL.OBJECT_TABLE_NAME = 'HZ_PARTIES'
                  AND REL.DIRECTIONAL_FLAG = 'F'
                  AND ORG_CONT.PARTY_RELATIONSHIP_ID = REL.RELATIONSHIP_ID
                  AND REL.SUBJECT_ID = PARTY.PARTY_ID
                  AND REL.PARTY_ID = REL_PARTY.PARTY_ID;

         IF L_PERSON_TITLE IS NOT NULL
         THEN
            BEGIN
               L_LOOKUP_TYPE := 'RESPONSIBILITY';
               L_PERSON_TITLE_UP := UPPER (L_PERSON_TITLE);

               SELECT   MEANING || ' ' || CONTACT_NAME
                 INTO   CONTACT_NAME
                 FROM   AR_LOOKUPS
                WHERE   LOOKUP_CODE = L_PERSON_TITLE_UP
                        AND LOOKUP_TYPE = L_LOOKUP_TYPE;
            EXCEPTION
               WHEN OTHERS
               THEN
                  CONTACT_NAME := L_PERSON_TITLE || ' ' || CONTACT_NAME;
            END;
         END IF;
      ELSE
         CONTACT_NAME := '   ';
      END IF;

      RETURN (CONTACT_NAME);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         CONTACT_NAME := '   ';
         RETURN (CONTACT_NAME);
      WHEN OTHERS
      THEN
         RAISE;
   END CF_BILL_TO_CONTACT1FORMULA;

   FUNCTION CF_SHIP_TO_CONTACT1FORMULA
      RETURN CHAR
   IS
      CONTACT_NAME        HZ_PARTIES.PARTY_NAME%TYPE;
      L_PERSON_TITLE      HZ_PARTIES.PERSON_TITLE%TYPE;
      L_PERSON_TITLE_UP   HZ_PARTIES.PERSON_TITLE%TYPE;
      L_LOOKUP_TYPE       VARCHAR2 (20);
   BEGIN
      /*SRW.REFERENCE(CP_SHIP_TO_CONTACT_ID)*/
      NULL;

      IF (CP_SHIP_TO_CONTACT_ID IS NOT NULL)
      THEN
         SELECT   PARTY.PARTY_NAME,
                  NVL (PARTY.PERSON_PRE_NAME_ADJUNCT, PARTY.PERSON_TITLE)
                     TITLE
           INTO   CONTACT_NAME, L_PERSON_TITLE
           FROM   HZ_CUST_ACCOUNT_ROLES ACCT_ROLE,
                  HZ_PARTIES PARTY,
                  HZ_RELATIONSHIPS REL,
                  HZ_ORG_CONTACTS ORG_CONT,
                  HZ_PARTIES REL_PARTY
          WHERE       ACCT_ROLE.CUST_ACCOUNT_ROLE_ID = CP_SHIP_TO_CONTACT_ID
                  AND ACCT_ROLE.PARTY_ID = REL.PARTY_ID
                  AND ACCT_ROLE.ROLE_TYPE = 'CONTACT'
                  AND REL.SUBJECT_TABLE_NAME = 'HZ_PARTIES'
                  AND REL.OBJECT_TABLE_NAME = 'HZ_PARTIES'
                  AND REL.DIRECTIONAL_FLAG = 'F'
                  AND ORG_CONT.PARTY_RELATIONSHIP_ID = REL.RELATIONSHIP_ID
                  AND REL.SUBJECT_ID = PARTY.PARTY_ID
                  AND REL.PARTY_ID = REL_PARTY.PARTY_ID;

         IF L_PERSON_TITLE IS NOT NULL
         THEN
            BEGIN
               L_LOOKUP_TYPE := 'RESPONSIBILITY';
               L_PERSON_TITLE_UP := UPPER (L_PERSON_TITLE);

               SELECT   MEANING || ' ' || CONTACT_NAME
                 INTO   CONTACT_NAME
                 FROM   AR_LOOKUPS
                WHERE   LOOKUP_CODE = L_PERSON_TITLE_UP
                        AND LOOKUP_TYPE = L_LOOKUP_TYPE;
            EXCEPTION
               WHEN OTHERS
               THEN
                  CONTACT_NAME := L_PERSON_TITLE || ' ' || CONTACT_NAME;
            END;
         END IF;
      ELSE
         CONTACT_NAME := '   ';
      END IF;

      RETURN (CONTACT_NAME);
   EXCEPTION
      WHEN NO_DATA_FOUND
      THEN
         CONTACT_NAME := '   ';
         RETURN (CONTACT_NAME);
      WHEN OTHERS
      THEN
         RAISE;
   END CF_SHIP_TO_CONTACT1FORMULA;

   FUNCTION F_BILL_TO_CUST_NAME1FORMULA (F_OE_LINE_ID IN NUMBER)
      RETURN CHAR
   IS
   BEGIN
      DECLARE
         BILL_TO_CUST_NAME   HZ_PARTIES.PARTY_NAME%TYPE;
         L_PERSON_TITLE      HZ_PARTIES.PERSON_TITLE%TYPE;
         L_PERSON_TITLE_UP   HZ_PARTIES.PERSON_TITLE%TYPE;
         L_LOOKUP_TYPE       VARCHAR2 (20);
      BEGIN
         /*SRW.REFERENCE(F_OE_LINE_ID)*/
         NULL;

         IF CP_SOURCE_CODE = 'OE'
         THEN
            SELECT   HP.PARTY_NAME,
                     NVL (HP.PERSON_PRE_NAME_ADJUNCT, HP.PERSON_TITLE) TITLE
              INTO   BILL_TO_CUST_NAME, L_PERSON_TITLE
              FROM   HZ_PARTY_SITES PS,
                     HZ_CUST_ACCT_SITES_ALL CA,
                     HZ_CUST_SITE_USES_ALL SU,
                     HZ_PARTIES HP,
                     OE_ORDER_LINES_ALL OLA
             WHERE       OLA.LINE_ID = F_OE_LINE_ID
                     AND SU.SITE_USE_ID = OLA.INVOICE_TO_ORG_ID
                     AND SU.CUST_ACCT_SITE_ID = CA.CUST_ACCT_SITE_ID
                     AND CA.PARTY_SITE_ID = PS.PARTY_SITE_ID
                     AND HP.PARTY_ID = PS.PARTY_ID;

            IF L_PERSON_TITLE IS NOT NULL
            THEN
               BEGIN
                  L_LOOKUP_TYPE := 'RESPONSIBILITY';
                  L_PERSON_TITLE_UP := UPPER (L_PERSON_TITLE);

                  SELECT   MEANING || ' ' || BILL_TO_CUST_NAME
                    INTO   BILL_TO_CUST_NAME
                    FROM   AR_LOOKUPS
                   WHERE   LOOKUP_CODE = L_PERSON_TITLE_UP
                           AND LOOKUP_TYPE = L_LOOKUP_TYPE;
               EXCEPTION
                  WHEN OTHERS
                  THEN
                     BILL_TO_CUST_NAME :=
                        L_PERSON_TITLE || ' ' || BILL_TO_CUST_NAME;
               END;
            END IF;

            IF SQL%NOTFOUND
            THEN
               RETURN (NULL);
            END IF;
         ELSIF CP_SOURCE_CODE = 'OKE'
         THEN
            BILL_TO_CUST_NAME := '';
         END IF;

         RETURN (BILL_TO_CUST_NAME);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN (NULL);
         WHEN OTHERS
         THEN
            RAISE;
      END;
   END F_BILL_TO_CUST_NAME1FORMULA;

   FUNCTION CF_LINE_TAX_CODE1FORMULA (F_OE_LINE_ID IN NUMBER)
      RETURN CHAR
   IS
   BEGIN
      DECLARE
         L_LINE_TAX_CODE   OE_ORDER_LINES_ALL.TAX_CODE%TYPE;
      BEGIN
         /*SRW.REFERENCE(F_OE_LINE_ID)*/
         NULL;

         IF CP_SOURCE_CODE = 'OKE'
         THEN
            L_LINE_TAX_CODE := '';
         ELSIF CP_SOURCE_CODE = 'OE'
         THEN
            SELECT   TAX_CODE
              INTO   L_LINE_TAX_CODE
              FROM   OE_ORDER_LINES_ALL
             WHERE   LINE_ID = F_OE_LINE_ID;

            IF SQL%NOTFOUND
            THEN
               RETURN (NULL);
            END IF;
         END IF;

         RETURN (L_LINE_TAX_CODE);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN (NULL);
         WHEN OTHERS
         THEN
            RAISE;
      END;
   END CF_LINE_TAX_CODE1FORMULA;

   FUNCTION F_SHIP_TO_CUST_NAME1FORMULA (F_SHIP_TO_SITE_USE_ID IN NUMBER)
      RETURN CHAR
   IS
   BEGIN
      DECLARE
         SHIP_TO_CUST_NAME   HZ_PARTIES.PARTY_NAME%TYPE;
         L_PERSON_TITLE      HZ_PARTIES.PERSON_TITLE%TYPE;
         L_PERSON_TITLE_UP   HZ_PARTIES.PERSON_TITLE%TYPE;
         L_LOOKUP_TYPE       VARCHAR2 (20);
      BEGIN
         /*SRW.REFERENCE(F_SHIP_TO_SITE_USE_ID)*/
         NULL;

         SELECT   HP.PARTY_NAME,
                  NVL (HP.PERSON_PRE_NAME_ADJUNCT, HP.PERSON_TITLE) TITLE
           INTO   SHIP_TO_CUST_NAME, L_PERSON_TITLE
           FROM   HZ_PARTY_SITES PS,
                  HZ_CUST_ACCT_SITES_ALL CA,
                  HZ_CUST_SITE_USES_ALL SU,
                  HZ_PARTIES HP
          WHERE       SU.SITE_USE_ID = F_SHIP_TO_SITE_USE_ID
                  AND SU.CUST_ACCT_SITE_ID = CA.CUST_ACCT_SITE_ID
                  AND CA.PARTY_SITE_ID = PS.PARTY_SITE_ID
                  AND HP.PARTY_ID = PS.PARTY_ID;

         IF L_PERSON_TITLE IS NOT NULL
         THEN
            BEGIN
               L_LOOKUP_TYPE := 'RESPONSIBILITY';
               L_PERSON_TITLE_UP := UPPER (L_PERSON_TITLE);

               SELECT   MEANING || ' ' || SHIP_TO_CUST_NAME
                 INTO   SHIP_TO_CUST_NAME
                 FROM   AR_LOOKUPS
                WHERE   LOOKUP_CODE = L_PERSON_TITLE_UP
                        AND LOOKUP_TYPE = L_LOOKUP_TYPE;
            EXCEPTION
               WHEN OTHERS
               THEN
                  SHIP_TO_CUST_NAME :=
                     L_PERSON_TITLE || ' ' || SHIP_TO_CUST_NAME;
            END;
         END IF;

         IF SQL%NOTFOUND
         THEN
            RETURN (NULL);
         END IF;

         RETURN (SHIP_TO_CUST_NAME);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN (NULL);
         WHEN OTHERS
         THEN
            RAISE;
      END;
   END F_SHIP_TO_CUST_NAME1FORMULA;

   FUNCTION F_SHIP_TO_SITE_USE_ID1FORMULA (F_DEL_DETAIL_ID IN NUMBER)
      RETURN NUMBER
   IS
   BEGIN
      DECLARE
         SHP_TO_SITE_USE_ID   NUMBER;
      BEGIN
         /*SRW.REFERENCE(F_DEL_DETAIL_ID)*/
         NULL;

         IF CP_SOURCE_CODE = 'OE'
         THEN
            SELECT   SHIP_TO_SITE_USE_ID
              INTO   SHP_TO_SITE_USE_ID
              FROM   WSH_DELIVERY_DETAILS
             WHERE       DELIVERY_DETAIL_ID = F_DEL_DETAIL_ID
                     AND SOURCE_CODE = 'OE'
                     AND ROWNUM < 2;

            IF SQL%NOTFOUND
            THEN
               RETURN (NULL);
            END IF;
         ELSIF CP_SOURCE_CODE = 'OKE'
         THEN
            SELECT   SHIP_TO_SITE_USE_ID
              INTO   SHP_TO_SITE_USE_ID
              FROM   WSH_DELIVERY_DETAILS
             WHERE       DELIVERY_DETAIL_ID = F_DEL_DETAIL_ID
                     AND SOURCE_CODE = 'OKE'
                     AND ROWNUM < 2;
         END IF;

         RETURN (SHP_TO_SITE_USE_ID);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN (NULL);
         WHEN OTHERS
         THEN
            RAISE;
      END;
   END F_SHIP_TO_SITE_USE_ID1FORMULA;

   FUNCTION F_BILL_TO_LOC_ID1FORMULA (F_OE_LINE_ID IN NUMBER)
      RETURN NUMBER
   IS
   BEGIN
      DECLARE
         BILL_TO_LOC_ID   NUMBER;
      BEGIN
         /*SRW.REFERENCE(F_OE_LINE_ID)*/
         NULL;

         IF CP_SOURCE_CODE = 'OE'
         THEN
            SELECT   PS.LOCATION_ID
              INTO   BILL_TO_LOC_ID
              FROM   HZ_PARTY_SITES PS,
                     HZ_CUST_ACCT_SITES_ALL CA,
                     HZ_CUST_SITE_USES_ALL SU,
                     OE_ORDER_LINES_ALL OLA
             WHERE       OLA.LINE_ID = F_OE_LINE_ID
                     AND SU.SITE_USE_ID = OLA.INVOICE_TO_ORG_ID
                     AND SU.CUST_ACCT_SITE_ID = CA.CUST_ACCT_SITE_ID
                     AND CA.PARTY_SITE_ID = PS.PARTY_SITE_ID;

            IF SQL%NOTFOUND
            THEN
               RETURN (NULL);
            END IF;
         ELSIF CP_SOURCE_CODE = 'OKE'
         THEN
            BILL_TO_LOC_ID := OKE_DTS_WSH_PKG.BILL_TO_LOCATION (F_OE_LINE_ID);
         END IF;

         RETURN (BILL_TO_LOC_ID);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN (NULL);
         WHEN OTHERS
         THEN
            RAISE;
      END;
   END F_BILL_TO_LOC_ID1FORMULA;

   FUNCTION F_OE_LINE_ID1FORMULA
      RETURN NUMBER
   IS
   BEGIN
      DECLARE
         OE_LINE_ID           NUMBER;
         SHP_TO_CONTACT_ID    NUMBER;
         BILL_TO_CONTACT_ID   NUMBER;
      BEGIN
         /*SRW.REFERENCE(P_DELIVERY_ID)*/
         NULL;

         IF CP_SOURCE_CODE = 'OE'
         THEN
            SELECT   WDD.SOURCE_LINE_ID,
                     OLA.SHIP_TO_CONTACT_ID,
                     OLA.INVOICE_TO_CONTACT_ID
              INTO   OE_LINE_ID, SHP_TO_CONTACT_ID, BILL_TO_CONTACT_ID
              FROM   WSH_DELIVERY_DETAILS WDD,
                     WSH_DELIVERY_ASSIGNMENTS_V WDA,
                     OE_ORDER_LINES_ALL OLA
             WHERE       WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                     AND NVL (WDD.CONTAINER_FLAG, 'N') = 'N'
                     AND WDA.DELIVERY_ID = CP_DELIVERY_ID
                     AND WDA.DELIVERY_ID IS NOT NULL
                     AND WDD.SOURCE_CODE = 'OE'
                     AND WDD.SOURCE_LINE_ID = OLA.LINE_ID
                     AND ROWNUM < 2;

            IF SQL%NOTFOUND
            THEN
               CP_SHIP_TO_CONTACT_ID := NULL;
               CP_BILL_TO_CONTACT_ID := NULL;
               RETURN (NULL);
            END IF;

            CP_SHIP_TO_CONTACT_ID := SHP_TO_CONTACT_ID;
            CP_BILL_TO_CONTACT_ID := BILL_TO_CONTACT_ID;
         ELSIF CP_SOURCE_CODE = 'OKE'
         THEN
            SELECT   WDD.SOURCE_LINE_ID
              INTO   OE_LINE_ID
              FROM   WSH_DELIVERY_DETAILS WDD, WSH_DELIVERY_ASSIGNMENTS_V WDA
             WHERE       WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                     AND WDD.CONTAINER_FLAG = 'N'
                     AND WDA.DELIVERY_ID = CP_DELIVERY_ID
                     AND WDA.DELIVERY_ID IS NOT NULL
                     AND WDD.SOURCE_CODE = 'OKE'
                     AND ROWNUM < 2;

            CP_SHIP_TO_CONTACT_ID := NULL;
            CP_BILL_TO_CONTACT_ID := NULL;
         END IF;

         RETURN (OE_LINE_ID);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            CP_SHIP_TO_CONTACT_ID := NULL;
            CP_BILL_TO_CONTACT_ID := NULL;
            RETURN (NULL);
         WHEN OTHERS
         THEN
            RAISE;
      END;
   END F_OE_LINE_ID1FORMULA;

   FUNCTION F_DEL_DETAIL_ID1FORMULA
      RETURN NUMBER
   IS
   BEGIN
      DECLARE
         DEL_DTL_ID      NUMBER;
         L_COUNT         NUMBER := 0;
         L_SOURCE_CODE   VARCHAR2 (30);
      BEGIN
         /*SRW.REFERENCE(P_DELIVERY_ID)*/
         NULL;

           SELECT   COUNT (DISTINCT WDD.SOURCE_CODE)
             INTO   L_COUNT
             FROM   WSH_DELIVERY_DETAILS WDD, WSH_DELIVERY_ASSIGNMENTS_V WDA
            WHERE       WDA.DELIVERY_ID IS NOT NULL
                    AND WDA.DELIVERY_DETAIL_ID = WDD.DELIVERY_DETAIL_ID
                    AND WDA.DELIVERY_ID = CP_DELIVERY_ID
                    AND WDD.CONTAINER_FLAG = 'N'
         GROUP BY   WDD.SOURCE_CODE;

         IF L_COUNT > 1
         THEN
            NULL;
         ELSIF L_COUNT = 1
         THEN
            SELECT   WDD.SOURCE_CODE, WDD.DELIVERY_DETAIL_ID
              INTO   L_SOURCE_CODE, DEL_DTL_ID
              FROM   WSH_DELIVERY_DETAILS WDD, WSH_DELIVERY_ASSIGNMENTS_V WDA
             WHERE       WDD.DELIVERY_DETAIL_ID = WDA.DELIVERY_DETAIL_ID
                     AND WDD.CONTAINER_FLAG = 'N'
                     AND WDA.DELIVERY_ID = CP_DELIVERY_ID
                     AND WDA.DELIVERY_ID IS NOT NULL
                     AND ROWNUM < 2;

            CP_SOURCE_CODE := L_SOURCE_CODE;
         END IF;

         IF SQL%NOTFOUND
         THEN
            RETURN (NULL);
         END IF;

         RETURN (DEL_DTL_ID);
      EXCEPTION
         WHEN NO_DATA_FOUND
         THEN
            RETURN (NULL);
         WHEN OTHERS
         THEN
            RAISE;
      END;
   END F_DEL_DETAIL_ID1FORMULA;

   FUNCTION CF_TO_ADDR_4FORMULA (CF_TO_CITY_STATE_ZIP   IN VARCHAR2,
                                 C_TO_COUNTRY           IN VARCHAR2,
                                 C_TO_ADDRESS_2         IN VARCHAR2,
                                 C_TO_ADDRESS_3         IN VARCHAR2,
                                 C_TO_ADDRESS_4         IN VARCHAR2)
      RETURN CHAR
   IS
      L_CF_TO_CITY_STATE_ZIP   VARCHAR2 (190);
   BEGIN
      /*SRW.REFERENCE(CF_TO_CITY_STATE_ZIP)*/
      NULL;
      /*SRW.REFERENCE(C_TO_COUNTRY)*/
      NULL;
      /*SRW.REFERENCE(C_TO_ADDRESS_2)*/
      NULL;
      /*SRW.REFERENCE(C_TO_ADDRESS_3)*/
      NULL;

      IF (CF_TO_CITY_STATE_ZIP IS NULL)
      THEN
         L_CF_TO_CITY_STATE_ZIP := '';
      ELSE
         L_CF_TO_CITY_STATE_ZIP := CF_TO_CITY_STATE_ZIP || ', ';
      END IF;

      IF (    C_TO_ADDRESS_2 IS NOT NULL
          AND C_TO_ADDRESS_3 IS NOT NULL
          AND C_TO_ADDRESS_4 IS NOT NULL)
      THEN
         RETURN (C_TO_ADDRESS_4);
      ELSIF (C_TO_ADDRESS_2 IS NOT NULL AND C_TO_ADDRESS_3 IS NULL AND C_TO_ADDRESS_4 IS NULL)
      THEN
         RETURN ('       ');
      ELSIF (C_TO_ADDRESS_2 IS NOT NULL AND C_TO_ADDRESS_3 IS NULL AND C_TO_ADDRESS_4 IS NOT NULL)
      THEN
         RETURN (L_CF_TO_CITY_STATE_ZIP || C_TO_COUNTRY);
      ELSIF (C_TO_ADDRESS_2 IS NULL AND C_TO_ADDRESS_3 IS NOT NULL AND C_TO_ADDRESS_4 IS NOT NULL)
      THEN
         RETURN (L_CF_TO_CITY_STATE_ZIP || C_TO_COUNTRY);
      ELSIF (C_TO_ADDRESS_2 IS NOT NULL AND C_TO_ADDRESS_3 IS NOT NULL AND C_TO_ADDRESS_4 IS NULL)
      THEN
         RETURN (L_CF_TO_CITY_STATE_ZIP || C_TO_COUNTRY);
      ELSIF (C_TO_ADDRESS_2 IS NULL AND C_TO_ADDRESS_3 IS NOT NULL AND C_TO_ADDRESS_4 IS NULL)
      THEN
         RETURN ('         ');
      ELSIF (C_TO_ADDRESS_2 IS NULL AND C_TO_ADDRESS_3 IS NULL AND C_TO_ADDRESS_4 IS NOT NULL)
      THEN
         RETURN ('              ');
      ELSIF (C_TO_ADDRESS_2 IS NULL AND C_TO_ADDRESS_3 IS NULL AND C_TO_ADDRESS_4 IS NULL)
      THEN
         RETURN ('                  ');
      END IF;
   END CF_TO_ADDR_4FORMULA;

   FUNCTION CF_BILL_ADDR_4FORMULA (CF_BILL_CITY_STATE_ZIP IN VARCHAR2)
      RETURN CHAR
   IS
   BEGIN
      /*SRW.REFERENCE(CP_BILL_ADDRESS_LINE_2)*/
      NULL;
      /*SRW.REFERENCE(CP_BILL_ADDRESS_LINE_3)*/
      NULL;
      /*SRW.REFERENCE(CP_BILL_ADDRESS_LINE_4)*/
      NULL;
      /*SRW.REFERENCE(CF_BILL_CITY_STATE_ZIP)*/
      NULL;
      /*SRW.REFERENCE(CP_BILL_COUNTRY)*/
      NULL;

      IF (    CP_BILL_ADDRESS_LINE_2 IS NOT NULL
          AND CP_BILL_ADDRESS_LINE_3 IS NOT NULL
          AND CP_BILL_ADDRESS_LINE_4 IS NOT NULL)
      THEN
         RETURN (CP_BILL_ADDRESS_LINE_4);
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NOT NULL AND CP_BILL_ADDRESS_LINE_3 IS NULL AND CP_BILL_ADDRESS_LINE_4 IS NOT NULL)
      THEN
         RETURN (CF_BILL_CITY_STATE_ZIP || ', ' || CP_BILL_COUNTRY);
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NOT NULL AND CP_BILL_ADDRESS_LINE_3 IS NULL AND CP_BILL_ADDRESS_LINE_4 IS NULL)
      THEN
         RETURN ('');
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NOT NULL AND CP_BILL_ADDRESS_LINE_3 IS NOT NULL AND CP_BILL_ADDRESS_LINE_4 IS NULL)
      THEN
         RETURN (CF_BILL_CITY_STATE_ZIP || ', ' || CP_BILL_COUNTRY);
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NULL AND CP_BILL_ADDRESS_LINE_3 IS NOT NULL AND CP_BILL_ADDRESS_LINE_4 IS NOT NULL)
      THEN
         RETURN (CF_BILL_CITY_STATE_ZIP || ', ' || CP_BILL_COUNTRY);
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NULL AND CP_BILL_ADDRESS_LINE_3 IS NULL AND CP_BILL_ADDRESS_LINE_4 IS NOT NULL)
      THEN
         RETURN ('');
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NULL AND CP_BILL_ADDRESS_LINE_3 IS NOT NULL AND CP_BILL_ADDRESS_LINE_4 IS NULL)
      THEN
         RETURN ('');
      ELSIF (CP_BILL_ADDRESS_LINE_2 IS NULL AND CP_BILL_ADDRESS_LINE_3 IS NULL AND CP_BILL_ADDRESS_LINE_4 IS NULL)
      THEN
         RETURN ('');
      END IF;
   END CF_BILL_ADDR_4FORMULA;

   FUNCTION CF_REQUESTOR_NAMEFORMULA (ATTACH_ORDER_ID IN NUMBER)
      RETURN CHAR
   IS
      REQ_NAME       VARCHAR2 (240);
      ORDER_NUMBER   VARCHAR2 (40);
   BEGIN
      SELECT   TO_CHAR (BH.ORDER_NUMBER), RE.FULL_NAME
        INTO   ORDER_NUMBER, REQ_NAME
        FROM   OE_ORDER_HEADERS_ALL BH,
               PO_REQUISITION_HEADERS_ALL RH,
               HR_EMPLOYEES RE
       WHERE       RH.TYPE_LOOKUP_CODE = 'INTERNAL'
               AND RH.PREPARER_ID = RE.EMPLOYEE_ID
               AND BH.HEADER_ID = ATTACH_ORDER_ID
               AND BH.SOURCE_DOCUMENT_ID = RH.REQUISITION_HEADER_ID
               AND BH.ORDER_SOURCE_ID = 10;

      CP_INTERNAL_SALES_ORDER := ORDER_NUMBER;
      RETURN REQ_NAME;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END CF_REQUESTOR_NAMEFORMULA;

   FUNCTION CF_ITEM_DESCRIPTIONFORMULA (C_ITEM_DESCRIPTION   IN VARCHAR2,
                                        C_INV_ITEM_ID        IN NUMBER,
                                        C_ORGANIZATION_ID    IN NUMBER)
      RETURN CHAR
   IS
      CURSOR INVENTORY_LABEL (ID IN NUMBER, ORG_ID IN NUMBER)
      IS
         SELECT   DESCRIPTION
           FROM   MTL_SYSTEM_ITEMS_VL
          WHERE   INVENTORY_ITEM_ID = ID AND ORGANIZATION_ID = ORG_ID;

      L_ITEM_DESC   VARCHAR2 (250);
   BEGIN
      L_ITEM_DESC := C_ITEM_DESCRIPTION;

      IF (C_INV_ITEM_ID IS NOT NULL)
      THEN
      OPEN INVENTORY_LABEL(C_INV_ITEM_ID,C_ORGANIZATION_ID);

         FETCH INVENTORY_LABEL INTO   L_ITEM_DESC;

         IF (INVENTORY_LABEL%NOTFOUND)
         THEN
            L_ITEM_DESC := C_ITEM_DESCRIPTION;
         END IF;

         CLOSE INVENTORY_LABEL;
      END IF;

      RETURN L_ITEM_DESC;
   END CF_ITEM_DESCRIPTIONFORMULA;

   FUNCTION CF_ITEM_DISPLAYFORMULA
      RETURN CHAR
   IS
   BEGIN
      RETURN (P_ITEM_DISPLAY);
   END CF_ITEM_DISPLAYFORMULA;

   FUNCTION CF_DISPLAY_UNSHIPPEDFORMULA
      RETURN CHAR
   IS
   BEGIN
      --STANDALONE CHANGES
      IF P_STANDALONE = 'Y'
      THEN
         RETURN ('N');
      ELSE
         RETURN (p_display_unshipped);
      END IF;
   END CF_DISPLAY_UNSHIPPEDFORMULA;

   FUNCTION CF_PRINT_CUST_ITEMFORMULA
      RETURN CHAR
   IS
   BEGIN
      RETURN (P_PRINT_CUST_ITEM);
   END CF_PRINT_CUST_ITEMFORMULA;

   FUNCTION CF_CARRIER_ADDRFORMULA (WND_CARRIER_ID     IN NUMBER,
                                    C_DEL_ORG_ID       IN NUMBER,
                                    C_Q2_DELIVERY_ID   IN NUMBER)
      RETURN CHAR
   IS
      CURSOR C_CARRIER_ADDRESS (
         C_ORGANIZATION_ID   IN            NUMBER,
         C_CARRIER_ID        IN            NUMBER,
         C_DELIVERY_ID       IN            NUMBER
      )
      IS
         SELECT      LOC.ADDRESS1
                  || ', '
                  || LOC.ADDRESS2
                  || ', '
                  || LOC.ADDRESS3
                  || ', '
                  || LOC.ADDRESS4
                  || ', '
                  || LOC.CITY
                  || ', '
                  || LOC.STATE
                  || ', '
                  || LOC.COUNTRY
                  || ', '
                  || LOC.POSTAL_CODE
                     CARRIER_ADDR
           FROM   WSH_ORG_CARRIER_SITES ORG_SITES,
                  WSH_NEW_DELIVERIES DEL,
                  HZ_PARTY_SITES HZ_SITES,
                  HZ_LOCATIONS LOC
          WHERE       ORG_SITES.ORGANIZATION_ID = DEL.ORGANIZATION_ID
                  AND HZ_SITES.PARTY_SITE_ID = ORG_SITES.CARRIER_SITE_ID
                  AND HZ_SITES.PARTY_ID = C_CARRIER_ID
                  AND ORG_SITES.ENABLED_FLAG = 'Y'
                  AND LOC.LOCATION_ID = HZ_SITES.LOCATION_ID
                  AND DEL.DELIVERY_ID = C_DELIVERY_ID;

      L_CARRIER_ADDR   VARCHAR2 (2000);
   BEGIN
      IF (WND_CARRIER_ID IS NOT NULL)
      THEN
         FOR c_rec
         IN C_CARRIER_ADDRESS (c_organization_id   => c_del_org_id,
                               c_carrier_id        => wnd_carrier_id,
                               c_delivery_id       => c_q2_delivery_id)
         LOOP
            L_CARRIER_ADDR := C_REC.CARRIER_ADDR;
         END LOOP;
      ELSE
         L_CARRIER_ADDR := ' ';
      END IF;

      RETURN L_CARRIER_ADDR;
   END CF_CARRIER_ADDRFORMULA;

   FUNCTION CF_CARRIER_ADDRESS1FORMULA (TRIP_CARRIER_ID      IN NUMBER,
                                        C_TRIP_DELIVERY_ID   IN NUMBER)
      RETURN CHAR
   IS
      CURSOR C_CARRIER_ADDRESS (
         C_CARRIER_ID    IN            NUMBER,
         C_DELIVERY_ID   IN            NUMBER
      )
      IS
         SELECT      LOC.ADDRESS1
                  || ', '
                  || LOC.ADDRESS2
                  || ', '
                  || LOC.ADDRESS3
                  || ', '
                  || LOC.ADDRESS4
                  || ', '
                  || LOC.CITY
                  || ', '
                  || LOC.STATE
                  || ', '
                  || LOC.COUNTRY
                  || ', '
                  || LOC.POSTAL_CODE
                     CARRIER_ADDR
           FROM   WSH_ORG_CARRIER_SITES ORG_SITES,
                  WSH_NEW_DELIVERIES DEL,
                  HZ_PARTY_SITES HZ_SITES,
                  HZ_LOCATIONS LOC
          WHERE       ORG_SITES.ORGANIZATION_ID = DEL.ORGANIZATION_ID
                  AND HZ_SITES.PARTY_SITE_ID = ORG_SITES.CARRIER_SITE_ID
                  AND HZ_SITES.PARTY_ID = C_CARRIER_ID
                  AND ORG_SITES.ENABLED_FLAG = 'Y'
                  AND LOC.LOCATION_ID = HZ_SITES.LOCATION_ID
                  AND DEL.DELIVERY_ID = C_DELIVERY_ID;

      L_CARRIER_ADDR   VARCHAR2 (2000);

      CURSOR C_DEL_CARRIER (C_DELIVERY_ID IN NUMBER)
      IS
         SELECT   WND.CARRIER_ID DEL_CARRIER_ID
           FROM   WSH_NEW_DELIVERIES WND
          WHERE   WND.DELIVERY_ID = C_DELIVERY_ID;
   BEGIN
      IF (TRIP_CARRIER_ID IS NOT NULL)
      THEN
         FOR c_rec
         IN C_CARRIER_ADDRESS (c_carrier_id    => trip_carrier_id,
                               c_delivery_id   => c_trip_delivery_id)
         LOOP
            L_CARRIER_ADDR := C_REC.CARRIER_ADDR;
            EXIT;
         END LOOP;
      ELSE
         FOR c_rec IN C_DEL_CARRIER (c_delivery_id => c_trip_delivery_id)
         LOOP
            IF (C_REC.DEL_CARRIER_ID IS NOT NULL)
            THEN
               FOR c_rec1
               IN C_CARRIER_ADDRESS (c_carrier_id    => c_rec.del_carrier_id,
                                     c_delivery_id   => c_trip_delivery_id)
               LOOP
                  L_CARRIER_ADDR := C_REC1.CARRIER_ADDR;
               END LOOP;
            END IF;

            EXIT;
         END LOOP;

         IF (L_CARRIER_ADDR IS NULL)
         THEN
            L_CARRIER_ADDR := ' ';
         END IF;
      END IF;

      RETURN L_CARRIER_ADDR;
   END CF_CARRIER_ADDRESS1FORMULA;

   FUNCTION CF_VAT_REG_NUMFORMULA (C_Q2_DELIVERY_ID   IN NUMBER,
                                   C_DEL_ORG_ID       IN NUMBER)
      RETURN VARCHAR2
   IS
      L_VAT_REG_NUM           VARCHAR2 (60);
      L_TAX_REG_NUM           VARCHAR2 (60);
      L_RETURN_STATUS         VARCHAR2 (50);
      L_MSG_COUNT             NUMBER;
      L_MSG_DATA              VARCHAR2 (50);
      L_INV_LE_INFO           XLE_BUSINESSINFO_GRP.INV_ORG_REC_TYPE;
      L_LEGAL_ENTITY_ID       NUMBER;
      L_REGISTRATION_NUMBER   NUMBER;
      L_OPERATING_UNIT_ID     NUMBER;

      CURSOR TAX_REG_NUM_CSR (
         P_ORG_ID   IN            NUMBER
      )
      IS
         SELECT   RCODES.REPORTING_CODE_CHAR_VALUE
           FROM   ZX_REPORTING_TYPES_B RTYPES,
                  ZX_REPORT_CODES_ASSOC RCODES,
                  ZX_PARTY_TAX_PROFILE PTP
          WHERE       RCODES.ENTITY_CODE = 'ZX_PARTY_TAX_PROFILE'
                  AND RCODES.ENTITY_ID = PTP.PARTY_TAX_PROFILE_ID
                  AND RCODES.REPORTING_TYPE_ID = RTYPES.REPORTING_TYPE_ID
                  AND RTYPES.REPORTING_TYPE_CODE = 'AR-SYSTEM-PARAM-REG-NUM'
                  AND PTP.PARTY_TYPE_CODE = 'OU'
                  AND PTP.PARTY_ID = P_ORG_ID;
   BEGIN
      L_OPERATING_UNIT_ID :=
         WSH_UTIL_CORE.GET_OPERATINGUNIT_ID (C_Q2_DELIVERY_ID);
    OPEN TAX_REG_NUM_CSR(L_OPERATING_UNIT_ID);

      FETCH TAX_REG_NUM_CSR INTO   L_TAX_REG_NUM;

      IF TAX_REG_NUM_CSR%NOTFOUND OR L_TAX_REG_NUM IS NULL
      THEN
         CLOSE TAX_REG_NUM_CSR;

         XLE_BUSINESSINFO_GRP.GET_INVORG_INFO (
            X_RETURN_STATUS   => L_RETURN_STATUS,
            X_MSG_DATA        => L_MSG_DATA,
            P_INVORG_ID       => C_DEL_ORG_ID,
            P_LE_ID           => NULL,
            P_PARTY_ID        => NULL,
            X_INV_LE_INFO     => L_INV_LE_INFO
         );

         IF L_RETURN_STATUS <> 'S'
            OR L_INV_LE_INFO (1).LEGAL_ENTITY_ID IS NULL
         THEN
            L_VAT_REG_NUM := NULL;
         ELSE
            L_RETURN_STATUS := NULL;
            L_MSG_DATA := NULL;
            XLE_UTILITIES_GRP.GET_FP_VATREGISTRATION_LEID (
               P_API_VERSION           => 1.0,
               P_INIT_MSG_LIST         => 'TRUE',
               P_COMMIT                => NULL,
               P_EFFECTIVE_DATE        => SYSDATE,
               X_RETURN_STATUS         => L_RETURN_STATUS,
               X_MSG_COUNT             => L_MSG_COUNT,
               X_MSG_DATA              => L_MSG_DATA,
               P_LEGAL_ENTITY_ID       => L_INV_LE_INFO (1).LEGAL_ENTITY_ID,
               X_REGISTRATION_NUMBER   => L_REGISTRATION_NUMBER
            );
            L_VAT_REG_NUM := L_REGISTRATION_NUMBER;

            IF L_REGISTRATION_NUMBER IS NOT NULL
            THEN
               P_TAX_VAT_FLAG := 0;
            ELSE
               P_TAX_VAT_FLAG := 1;
            END IF;
         END IF;
      ELSE
         CLOSE TAX_REG_NUM_CSR;

         L_VAT_REG_NUM := L_TAX_REG_NUM;
         P_TAX_VAT_FLAG := 1;
      END IF;

      RETURN (L_VAT_REG_NUM);
   EXCEPTION
      WHEN OTHERS
      THEN
         IF TAX_REG_NUM_CSR%ISOPEN
         THEN
            CLOSE TAX_REG_NUM_CSR;
         END IF;
   END CF_VAT_REG_NUMFORMULA;

   FUNCTION CP_INTERNAL_SALES_ORDER_P
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN CP_INTERNAL_SALES_ORDER;
   END CP_INTERNAL_SALES_ORDER_P;

   FUNCTION CP_WAREHOUSE_NAME_P
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN CP_WAREHOUSE_NAME;
   END CP_WAREHOUSE_NAME_P;

   FUNCTION CP_DRAFT_OR_FINAL_P
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN CP_DRAFT_OR_FINAL;
   END CP_DRAFT_OR_FINAL_P;

   FUNCTION CP_PRINT_DATE_P
      RETURN DATE
   IS
   BEGIN
      RETURN CP_PRINT_DATE;
   END CP_PRINT_DATE_P;

   FUNCTION CP_RLM_PRINT_CUM_DATA_P
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN CP_RLM_PRINT_CUM_DATA;
   END CP_RLM_PRINT_CUM_DATA_P;

   FUNCTION CP_SOURCE_CODE_P
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN CP_SOURCE_CODE;
   END CP_SOURCE_CODE_P;

   FUNCTION CP_BILL_TO_CONTACT_ID_P
      RETURN NUMBER
   IS
   BEGIN
      RETURN CP_BILL_TO_CONTACT_ID;
   END CP_BILL_TO_CONTACT_ID_P;

   FUNCTION CP_SHIP_TO_CONTACT_ID_P
      RETURN NUMBER
   IS
   BEGIN
      RETURN CP_SHIP_TO_CONTACT_ID;
   END CP_SHIP_TO_CONTACT_ID_P;

   FUNCTION CP_BILL_ADDRESS_LINE_1_P
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN CP_BILL_ADDRESS_LINE_1;
   END CP_BILL_ADDRESS_LINE_1_P;

   FUNCTION CP_BILL_ADDRESS_LINE_2_P
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN CP_BILL_ADDRESS_LINE_2;
   END CP_BILL_ADDRESS_LINE_2_P;

   FUNCTION CP_BILL_ADDRESS_LINE_3_P
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN CP_BILL_ADDRESS_LINE_3;
   END CP_BILL_ADDRESS_LINE_3_P;

   FUNCTION CP_BILL_TOWN_OR_CITY_P
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN CP_BILL_TOWN_OR_CITY;
   END CP_BILL_TOWN_OR_CITY_P;

   FUNCTION CP_BILL_REGION_P
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN CP_BILL_REGION;
   END CP_BILL_REGION_P;

   FUNCTION CP_BILL_POSTAL_CODE_P
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN CP_BILL_POSTAL_CODE;
   END CP_BILL_POSTAL_CODE_P;

   FUNCTION CP_BILL_COUNTRY_P
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN CP_BILL_COUNTRY;
   END CP_BILL_COUNTRY_P;

   FUNCTION CP_BILL_ADDRESS_LINE_4_P
      RETURN VARCHAR2
   IS
   BEGIN
      RETURN CP_BILL_ADDRESS_LINE_4;
   END CP_BILL_ADDRESS_LINE_4_P;

   --ADDED
   FUNCTION BeforeReport(p_tripid NUMBER)
      RETURN boolean
   IS
      l_result          VARCHAR2 (1);
      l_return_status   VARCHAR2 (5);
      l_msg_count       NUMBER;
      l_msg_data        VARCHAR2 (3000);
      l_cum_profile     VARCHAR2 (5);

      CURSOR c_invitem(p_trip NUMBER) IS
        SELECT DISTINCT INVENTORY_ITEM_ID
        FROM xxfox_audit_lines xal
       WHERE trip_id = p_trip;

   BEGIN
      BEGIN
         l_result :=
            WSH_Document_PVT.is_final (1.0,
                                       NULL,
                                       NULL,
                                       NULL,
                                       l_return_status,
                                       l_msg_count,
                                       l_msg_data,
                                       cp_delivery_id,
                                       'PACK_TYPE');

         IF FND_API.to_boolean (l_result)
         THEN
            NULL;
         END IF;
      END;
      BEGIN
         l_cum_profile := FND_PROFILE.VALUE ('RLM_PRINT_CUM_DATA');

         IF l_cum_profile = 'Y'
         THEN
            cp_rlm_print_cum_data := 'Y';
         ELSE
            cp_rlm_print_cum_data := 'N';
         END IF;
      END;
      BEGIN
         IF p_print_mode IS NOT NULL
         THEN
            cp_draft_or_final :=
               WSH_UTIL_CORE.Get_Lookup_Meaning (
                  p_lookup_type   => 'PACK_MODE',
                  p_lookup_code   => p_print_mode
               );
         ELSE
            NULL;
         END IF;

         IF cp_delivery_id IS NOT NULL
         THEN
            SELECT   hr.name
              INTO   cp_warehouse_name
              FROM   hr_organization_units hr, wsh_new_deliveries del
             WHERE   del.organization_id = hr.organization_id
                     AND del.delivery_id = cp_delivery_id;
         ELSIF p_organization_id IS NOT NULL
         THEN
            SELECT   name
              INTO   cp_warehouse_name
              FROM   hr_organization_units
             WHERE   organization_id = p_organization_id;

            p_organizationid_1 :=
               'AND det.organization_id = :p_organization_id';
         ELSE
            NULL;
         END IF;

         cp_print_date := SYSDATE;
      EXCEPTION
         WHEN OTHERS
         THEN
            NULL;
      END;
      BEGIN
         IF p_sort = 'CUST'
         THEN
            IF p_print_cust_item = 'Y'
            THEN
               p_customer_item_number := 'mci.customer_item_number';
            ELSE
               IF p_item_display = 'D'
               THEN
                  p_customer_item_number := '''''';
                  p_item_flex := '''''';
               ELSE
                  p_customer_item_number := '''''';
               END IF;
            END IF;
         ELSE
            IF p_item_display = 'D'
            THEN
               p_customer_item_number := '''''';
               p_item_flex := '''''';
            ELSE
               p_customer_item_number := '''''';
            END IF;
         END IF;
      END;

      BEGIN

        FOR c_aud_descr IN c_invitem(p_tripid)
        LOOP
              APPS.xxfox_trip_audit_details_prc(p_tripid,c_aud_descr.inventory_item_id) ;
        END LOOP;
      EXCEPTION
          WHEN OTHERS THEN
             NULL;
      END;



      IF (p_item_display = 'B' OR p_item_display = 'F')
      THEN
         BEGIN
            NULL;
         END;
      END IF;

      RETURN (TRUE);
   END;

   FUNCTION Address_New (cf_bill_to_loc IN number)
      RETURN VARCHAR2
   IS
      l_location_id      NUMBER;
      l_address_line_1   hz_locations.address1%TYPE;
      l_address_line_2   hz_locations.address2%TYPE;
      l_address_line_3   hz_locations.address3%TYPE;
      l_address_line_4   hz_locations.address4%TYPE;
      l_town_or_city     hz_locations.city%TYPE;
      l_region           hz_locations.county%TYPE;
      l_postal_code      hz_locations.postal_code%TYPE;
      l_country          fnd_territories_tl.territory_short_name%TYPE;
   BEGIN
      SELECT   loc_bill.address1,
               loc_bill.address2,
               loc_bill.address3,
               loc_bill.address4,
               loc_bill.city,
               NVL (NVL (loc_bill.province, loc_bill.state), loc_bill.county),
               loc_bill.postal_code,
               terr_bill.territory_short_name
        INTO   l_address_line_1,
               l_address_line_2,
               l_address_line_3,
               l_address_line_4,
               l_town_or_city,
               l_region,
               l_postal_code,
               l_country
        FROM   hz_locations loc_bill, fnd_territories_tl terr_bill
       WHERE       loc_bill.country = terr_bill.territory_code(+)
               AND DECODE (loc_bill.country,
                           NULL, USERENV ('LANG'),
                           terr_bill.language) = USERENV ('LANG')
               AND loc_bill.location_id = cf_bill_to_loc;

      cp_bill_address_line_1 := l_address_line_1;
      cp_bill_address_line_2 := l_address_line_2;
      cp_bill_address_line_3 := l_address_line_3;
      cp_bill_address_line_4 := l_address_line_4;
      cp_bill_town_or_city := l_town_or_city;
      cp_bill_region := l_region;
      cp_bill_postal_code := l_postal_code;
      cp_bill_country := l_country;
      RETURN (' ');
   END Address_New;

   FUNCTION ITEM_PALLET_QTY (P_DELIVERY_ID    IN NUMBER,
                             P_PALLET_NUMER   IN NUMBER,
                             P_ITEM_ID        IN NUMBER,
                             P_TRIP_ID  IN NUMBER)
      RETURN NUMBER
   AS
      V_QTY   NUMBER := 0;
   BEGIN
      SELECT   NVL (SUM (AUDIT_QUANTITY), 0)
        INTO   V_QTY
        FROM   xxfox_audit_lines
       WHERE       delivery_id = NVL(P_DELIVERY_ID,delivery_id)
               AND pallet_number = P_PALLET_NUMER
               AND inventory_item_id = P_ITEM_ID
               AND TRIP_ID = NVL(P_TRIP_ID,TRIP_ID);

      RETURN V_QTY;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION ITEM_BOX_QUANTITY (P_DELIVERY_ID    IN NUMBER,
                               P_PALLET_NUMER   IN NUMBER,
                               P_BOX_NUMER      IN NUMBER,
                               P_ITEM_ID        IN NUMBER,
                               P_TRIP_ID  IN NUMBER)
      RETURN NUMBER
   AS
      V_QTY   NUMBER := 0;
   BEGIN
      SELECT   NVL (SUM (AUDIT_QUANTITY), 0)
        INTO   V_QTY
        FROM   xxfox_audit_lines
       WHERE       delivery_id = NVL(P_DELIVERY_ID,delivery_id)
               AND pallet_number = P_PALLET_NUMER
               AND box_number = P_BOX_NUMER
               AND inventory_item_id = P_ITEM_ID
               AND TRIP_ID = NVL(P_TRIP_ID,TRIP_ID);

      RETURN V_QTY;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN 0;
   END;

   FUNCTION ITEM_COO (P_ORG_ID IN NUMBER, P_ITEM_ID IN NUMBER)
      RETURN VARCHAR2
   AS
      V_XREF   VARCHAR2 (100) := NULL;
   BEGIN
      SELECT   SUBSTR (XREF.CROSS_REFERENCE, 1, 100)
        INTO   V_XREF
        FROM   MTL_CROSS_REFERENCES_B XREF
       WHERE       XREF.CROSS_REFERENCE_TYPE = 'COO'
               AND XREF.INVENTORY_ITEM_ID = P_ITEM_ID
               AND XREF.ORGANIZATION_ID = P_ORG_ID
               AND ROWNUM = 1;

      RETURN V_XREF;
   EXCEPTION
      WHEN OTHERS
      THEN
         BEGIN
            SELECT   SUBSTR (XREF.CROSS_REFERENCE, 1, 100)
              INTO   V_XREF
              FROM   MTL_CROSS_REFERENCES_B XREF
             WHERE       XREF.CROSS_REFERENCE_TYPE = 'COO'
                     AND XREF.INVENTORY_ITEM_ID = P_ITEM_ID
                     AND XREF.ORGANIZATION_ID IS NULL
                     AND ORG_INDEPENDENT_FLAG = 'Y'
                     AND ROWNUM = 1;

            RETURN V_XREF;
         EXCEPTION
            WHEN OTHERS
            THEN
               RETURN NULL;
         END;
   END;

   PROCEDURE get_weight(p_delivery_id in number, p_weight_type in varchar2, x_weight out number, x_weight_uom out varchar2)
   is

   cursor c_wt is
      select sum(pallet_gross_wt) pallet_gross_wt
            ,sum(pallet_net_wt) pallet_net_wt
            ,pallet_wt_uom
       from
            (
            select distinct    pallet_number
                       ,nvl(pallet_gross_wt,0) pallet_gross_wt
                       ,nvl(pallet_net_wt,0) pallet_net_wt
                       ,pallet_wt_uom pallet_wt_uom
                     from xxfox_audit_lines
                     where delivery_id=p_delivery_id
             )
        group by pallet_wt_uom;

       l_count   number := 0;
   begin

     for rec in c_wt
     loop
        l_count := l_count + 1;
         fnd_file.put_line (fnd_file.LOG,
                            'delivery# '||p_delivery_id||'  Net Wt: '|| rec.pallet_net_wt || ' Gross Wt: '||rec.pallet_gross_wt);

        if p_weight_type = 'GROSS' then
           x_weight := rec.pallet_gross_wt;
        elsif p_weight_type = 'NET' then
           x_weight := rec.pallet_net_wt;
        end if;

        x_weight_uom := rec.pallet_wt_uom;
     end loop;

     if l_count > 1 then
         fnd_file.put_line (fnd_file.LOG,'Error: multiple UOM found for Wt');
     end if;

     if l_count = 0 then
         fnd_file.put_line (fnd_file.LOG,'No data found in get_wt procedure...xxfox_audit_lines for delivery# '||p_delivery_id);
     end if;

   exception
      when others then
         fnd_file.put_line (fnd_file.LOG,'Exception in get_wt procedure, delivery# '||p_delivery_id||' sqlerr:'||sqlerrm);
   end get_weight;

   FUNCTION get_gross_weight(p_delivery_id in number)
      return number is
      l_gross_weight  number;
      l_uom varchar2(10);
   BEGIN
        get_weight(p_delivery_id,'GROSS',l_gross_weight,l_uom);
        return l_gross_weight;
   exception
      when others then
         fnd_file.put_line (fnd_file.LOG,'Exception in get_gross_wt function, delivery# '||p_delivery_id||' sqlerr:'||sqlerrm);
         return null;
   END get_gross_weight;

   FUNCTION get_net_weight(p_delivery_id in number)
      return number is
      l_net_weight  number;
      l_uom varchar2(10);
   BEGIN
        get_weight(p_delivery_id,'NET',l_net_weight,l_uom);
        return l_net_weight;
   exception
      when others then
         fnd_file.put_line (fnd_file.LOG,'Exception in get_net_weight function, delivery# '||p_delivery_id||' sqlerr:'||sqlerrm);
         return null;
   END get_net_weight;

   FUNCTION get_weight_uom(p_delivery_id in number)
      return varchar2 is
      l_weight  number;
      l_uom         varchar2(10);
   BEGIN
        get_weight(p_delivery_id,'GROSS',l_weight,l_uom);
        return l_uom;
   exception
      when others then
         fnd_file.put_line (fnd_file.LOG,'Exception in get_weight_uom function, delivery# '||p_delivery_id||' sqlerr:'||sqlerrm);
         return null;
   END get_weight_uom;

  FUNCTION CF_CUSTOMER_ITEM (p_customer_id          IN    NUMBER,
                             p_inventory_item_id    IN    NUMBER)
        RETURN VARCHAR2
  IS
    l_cust_item     mtl_customer_items.customer_item_number%type;
  BEGIN
       SELECT   mci.customer_item_number
         INTO   l_cust_item
         FROM   mtl_customer_item_xrefs mcix,
                mtl_customer_items mci
         WHERE  1 = 1
           AND mcix.inventory_item_id = p_inventory_item_id
           AND mcix.master_organization_id = 83
           AND mcix.customer_item_id = mci.customer_item_id
           AND mcix.inactive_flag = 'N'
           AND mcix.preference_number = (SELECT MIN (a.preference_number)
                                           FROM mtl_customer_item_xrefs a
                                          WHERE a.customer_item_id = mcix.customer_item_id)
          AND mci.customer_id = p_customer_id ;

        IF l_cust_item IS NOT NULL
        THEN
            l_cust_item := 'Customer Part : '||l_cust_item;
        ELSE
            l_cust_item := NULL;
        END IF;
            RETURN l_cust_item;
  EXCEPTION
        WHEN NO_DATA_FOUND THEN
        fnd_file.put_line(fnd_file.log, 'No customer item found for customer_id, item_id: '||p_customer_id||', '||p_inventory_item_id);
                RETURN NULL;
        WHEN TOO_MANY_ROWS THEN
        fnd_file.put_line(fnd_file.log, 'More than one cust item found for customer_id, item_id: '||p_customer_id||', '||p_inventory_item_id);
                RETURN NULL;
  END;

     FUNCTION TRIP_DEL_NAMES (P_DELIVERY_ID IN NUMBER, P_TRIP_ID IN NUMBER)
      RETURN VARCHAR2
   AS
      V_TRIP_DEL_NAMES   VARCHAR2 (6000) ;
   BEGIN

        SELECT  rtrim(rtrim(dbms_xmlgen.convert(extract(xmlagg(xmlelement(e, ''
          || tpdel.trip_del_name,',') ), '/E/text()').getClobVal(),1),','),',') trip_del_names
          INTO V_TRIP_DEL_NAMES
        FROM
            (select tdv.NAME trip_del_name
        from WSH_TRIP_DELIVERIES_V tdv
        where tdv.TRIP_ID = NVL(P_TRIP_ID,TRIP_ID)
        AND EXISTS (SELECT 1
        FROM WSH_TRIP_DELIVERIES_V wtd
        WHERE wtd.delivery_id = NVL(P_DELIVERY_ID,wtd.delivery_id)
        AND wtd.TRIP_ID       = NVL(P_TRIP_ID,wtd.TRIP_ID)
        AND wtd.TRIP_ID       = tdv.TRIP_ID)
        ORDER BY tdv.delivery_id
        ) tpdel;

      RETURN V_TRIP_DEL_NAMES;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END TRIP_DEL_NAMES;

 FUNCTION TRIP_PACK_SLIP_NUM (P_DELIVERY_ID IN NUMBER, P_TRIP_ID IN NUMBER)
      RETURN VARCHAR2
   AS
      V_TRIP_PACK_SLIP_NUM   VARCHAR2 (6000) ;
   BEGIN

        SELECT  rtrim(rtrim(dbms_xmlgen.convert(extract(xmlagg(xmlelement(e, ''
          || tppsnum.pack_slip_num,',') ), '/E/text()').getClobVal(),1),','),',') tp_pack_slip_num
          INTO V_TRIP_PACK_SLIP_NUM
        FROM
            (select doc.sequence_number pack_slip_num
        from WSH_TRIP_DELIVERIES_V tdv,
    wsh_document_instances doc
        where 1=1
    AND doc.entity_id=tdv.delivery_id
    AND doc.entity_name= 'WSH_NEW_DELIVERIES'
    AND doc.document_type='PACK_TYPE'
    AND tdv.TRIP_ID = NVL(P_TRIP_ID,TRIP_ID)
        AND EXISTS (SELECT 1
        FROM WSH_TRIP_DELIVERIES_V wtd
        WHERE wtd.delivery_id = NVL(P_DELIVERY_ID,wtd.delivery_id)
        AND wtd.TRIP_ID       = NVL(P_TRIP_ID,wtd.TRIP_ID)
        AND wtd.TRIP_ID       = tdv.TRIP_ID)
        ORDER BY tdv.delivery_id
        ) tppsnum;

      RETURN V_TRIP_PACK_SLIP_NUM;
   EXCEPTION
      WHEN OTHERS
      THEN
         RETURN NULL;
   END TRIP_PACK_SLIP_NUM;


FUNCTION XXFOX_GET_TRIP_WEIGHT(p_trip_id NUMBER , p_requested_wt  VARCHAR2 )  RETURN NUMBER
IS
   v_gross_wt   NUMBER;
   v_net_wt     NUMBER;
   v_del_grs_wt NUMBER;
   v_del_net_wt NUMBER;

   CURSOR c_deliv_ids (tripid  NUMBER)
   IS
      SELECT
            DISTINCT wda.delivery_id
        FROM wsh_delivery_details wdd
           , wsh_delivery_assignments wda
           , wsh_delivery_legs wdl
           , wsh_trip_stops wts
       WHERE wdd.source_code = 'OE'
         AND wdd.delivery_detail_id = wda.delivery_detail_id
         AND wda.delivery_id = wdl.delivery_id(+)
         AND wdl.pick_up_stop_id = wts.stop_id(+)
         AND wts.trip_id = tripid
         AND wda.delivery_id IS NOT NULL;
BEGIN
   BEGIN
       SELECT SUM(pallet_gross_wt),
              SUM(pallet_net_wt)
         INTO v_gross_wt,
              v_net_wt
         FROM(
                SELECT DISTINCT pallet_number,
                                NULL box_number,
                                pallet_gross_wt,
                                pallet_net_wt
                  FROM xxfox_audit_lines
                 WHERE trip_id =p_trip_id
                   AND pallet_number IS NOT NULL
               UNION
                SELECT DISTINCT NULL PALLET_NUMBER,
                                box_number,
                                pallet_gross_wt,
                                pallet_net_wt
                  FROM XXFOX_AUDIT_LINES
                 WHERE trip_id = p_trip_id
                   AND pallet_number IS NULL );
   EXCEPTION
       WHEN OTHERS THEN
          v_gross_wt := NULL;
          v_net_wt   := NULL;
   END;

   --DBMS_OUTPUT.put_line('Trip Gross Weight From Audit Tables :: '||NVL(v_gross_wt,0));
   --DBMS_OUTPUT.put_line('Trip Net Weight From Audit Tables :: '||NVL(v_net_wt,0));

   IF v_gross_wt IS NULL AND v_net_wt IS NULL THEN
      FOR c_deliveryids IN c_deliv_ids (p_trip_id) LOOP
         BEGIN
            SELECT NVL(gross_weight,0),
                   NVL(net_weight,0)
              INTO v_del_grs_wt,
                   v_del_net_wt
              FROM wsh_new_deliveries
             WHERE delivery_id = c_deliveryids.delivery_id;
         EXCEPTION
             WHEN OTHERS THEN
                  v_del_grs_wt := 0;
                  v_del_net_wt := 0;
         END;
         v_gross_wt := v_gross_wt + v_del_grs_wt;
         v_net_wt   := v_net_wt   + v_del_net_wt;
      END LOOP;
     --DBMS_OUTPUT.put_line('Trip Gross Weight From wsh_new_deliveries Table :: '||v_gross_wt);
     --DBMS_OUTPUT.put_line('Trip Net Weight From wsh_new_deliveries Table :: '||v_net_wt);
   END IF;

   IF p_requested_wt = 'GW' THEN
      RETURN v_gross_wt;
   ELSIF p_requested_wt = 'NW' THEN
      RETURN v_net_wt;
   ELSE
      RETURN NULL;
   END IF;
EXCEPTION
   WHEN OTHERS THEN
       RETURN NULL;
END XXFOX_GET_TRIP_WEIGHT;


END XXFOX_WSHRDPAK_XMLP_PKG;
/