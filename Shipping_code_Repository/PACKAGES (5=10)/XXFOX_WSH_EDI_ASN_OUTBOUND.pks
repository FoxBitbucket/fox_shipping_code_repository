CREATE OR REPLACE PACKAGE   XXFOX_WSH_EDI_ASN_OUTBOUND_PKG

AS
--
-- $Id$
--
/*
   -- =========================================================
   -- Copyright (c) 2012 FOX RIDE Inc.
   -- All rights reserved.
   -- =========================================================
   -- NAME: XXFOX_WSH_EDI_ASN_OUTBOUND_PKG
   -- Design R�f�rence : 
   -- PROGRAM TYPE: Package
   -- PURPOSE: This script will used to generate a data file(Outbound) as per format given by FOX RIDE
   -- PARAMETERS:
   -- CALLS:
   -- NOTES:
   -- CHANGE HISTORY:
   -- =========================================================
   -- Date         Author             Activity
   -- =========================================================
   -- 07-MAR-2018 Mohammad.Awaizsiddiq  Initial Creation
   -- =========================================================
   */
PROCEDURE main_call
    (
        errbuf          OUT     VARCHAR2,
        retcode         OUT     VARCHAR2,
        p_trip_id       IN      VARCHAR2
        )  ;

END XXFOX_WSH_EDI_ASN_OUTBOUND_PKG;
/

EXIT;