DECLARE

CURSOR c_documents IS
    SELECT distinct a.lookup_code document_id, b.attribute1 source
    FROM   fnd_lookup_values a, wms_label_formats b
    WHERE  a.lookup_type = 'WMS_LABEL_TYPE'
    AND    a.lookup_code IN (9995)
    AND    a.lookup_code = b.document_id
    AND    b.attribute1 is not null
    ;
CURSOR c_tab_col (i_document_id number, i_table_name VARCHAR2) IS
    SELECT column_name 
    FROM   all_tab_columns a
    WHERE  table_name = i_table_name
    AND    owner = 'APPS'
    AND    not exists (select field_name from WMS_LABEL_FIELDS where field_name = a.column_name and document_id = i_document_id)
    ORDER BY column_id
    ;

l_rowid     VARCHAR2(240);
BEGIN
    FOR i IN c_documents LOOP
    FOR j IN c_tab_col (i.document_id,i.source)  LOOP
        dbms_output.put_line('field :'||j.column_name);
        WMS_LABEL_FIELDS_PKG.INSERT_ROW
           (X_ROWID => l_rowid,
            X_LABEL_FIELD_ID => WMS_LABEL_FIELDS_s.nextval,
            X_DOCUMENT_ID    => i.document_id,
            X_FIELD_LIST_UPDATED_FLAG   => NULL,
            X_COLUMN_NAME   => 'sql_stmt',
            X_ATTRIBUTE_CATEGORY=> NULL,
            X_ATTRIBUTE1          => NULL,
            X_ATTRIBUTE2          => NULL,
            X_ATTRIBUTE3          => NULL,
            X_ATTRIBUTE4          => NULL,
            X_ATTRIBUTE5          => NULL,
            X_ATTRIBUTE6          => NULL,
            X_ATTRIBUTE7          => NULL,
            X_ATTRIBUTE8          => NULL,
            X_ATTRIBUTE9          => NULL,
            X_ATTRIBUTE10         => NULL,
            X_ATTRIBUTE11         => NULL,
            X_ATTRIBUTE12         => NULL,
            X_ATTRIBUTE13         => NULL,
            X_ATTRIBUTE14         => NULL,
            X_ATTRIBUTE15         => NULL,
            X_FIELD_NAME          => j.column_name,
            X_FIELD_DESCRIPTION   => j.column_name,
            X_LAST_UPDATE_DATE    => sysdate,
            X_LAST_UPDATED_BY     => 0,
            X_LAST_UPDATE_LOGIN   => null,
            X_CREATED_BY          => 0,
            X_CREATION_DATE       => sysdate,
            X_REQUEST_ID          => NULL,
            X_PROGRAM_APPLICATION_ID => NULL,
            X_PROGRAM_ID          => NULL,
            X_PROGRAM_UPDATE_DATE => NULL,
            X_WMS_EXCLUSIVE       => NULL,
            X_SQL_STMT => 'DUMMY'
            --X_SQL_STMT => 'select '||''''||'X'||''''||' from dual xlpr, wms_label_requests wlr where 1 = wlr.delivery_detail_id'
          );
    END LOOP;
    END LOOP;
    commit;
END;
/

exit;