CREATE OR REPLACE PROCEDURE APPS.xxfox_trip_audit_details_prc(p_trip_id NUMBER,p_itemid NUMBER)
IS

   v_local_audit_qty NUMBER :=0;
   v_description     VARCHAR2(2000);
   v_bal_audit_qty   NUMBER := 0;
   v_aureccnt         NUMBER := 0;
   v_po_num          VARCHAR2(100);
   v_ord_num         VARCHAR2(100);
   v_custatt         VARCHAR2(4) :='N';


  CURSOR c_ord_det(p_trip NUMBER,p_inv_item  NUMBER)
  IS
    SELECT       wts.trip_id,
                           wda.delivery_id,
                           OOH.ORDER_NUMBER ORDER_NUMBER,
                           OOL.LINE_NUMBER LINE_NUMBER ,
                           WDD.INVENTORY_ITEM_ID,
                           OOL.SHIPMENT_NUMBER,
                           OOH.CUST_PO_NUMBER,
                           decode(UPPER(OTL.NAME),'INTERNAL',ooh.ORIG_SYS_DOCUMENT_REF, null) req_num,
                           SUM(WDD.REQUESTED_QUANTITY) REQ_QTY,
                           SUM(NVL(WDD.PICKED_QUANTITY,0)) SHIPPED_QTY
                    FROM wsh_delivery_details wdd,
                         wsh_delivery_assignments wda,
                            wsh_delivery_legs wdl
                          , wsh_trip_stops wts,
                         OE_ORDER_HEADERS_ALL OOH,
                         OE_ORDER_LINES_ALL OOL, OE_TRANSACTION_TYPES_TL otl
                   WHERE WDA.DELIVERY_DETAIL_ID=WDD.DELIVERY_DETAIL_ID
                     AND OOL.LINE_ID = WDD.SOURCE_LINE_ID
                     AND OOH.HEADER_ID = OOL.HEADER_ID
                     AND wda.delivery_id = wdl.delivery_id(+)
                     AND wdl.pick_up_stop_id = wts.stop_id(+)
                     AND wts.trip_id = p_trip
                     AND WDD.INVENTORY_ITEM_ID = p_inv_item
                     AND OOH.ORDER_TYPE_ID = OTL.TRANSACTION_TYPE_ID
                         AND OTL.LANGUAGE = USERENV ('LANG')
                     group by wts.trip_id,wda.delivery_id,WDD.INVENTORY_ITEM_ID,OOH.ORDER_NUMBER ,
                           OOL.LINE_NUMBER ,OOL.SHIPMENT_NUMBER,OOH.CUST_PO_NUMBER, otl.name, ooh.orig_sys_document_ref
                     ORDER BY wts.trip_id,wda.delivery_id,OOH.ORDER_NUMBER ,OOL.LINE_NUMBER,WDD.INVENTORY_ITEM_ID;

   CURSOR c_aud_det(p_trip1 NUMBER,p_inv_item1 NUMBER)
   IS
      SELECT trip_id,
             inventory_item_id,
             pallet_number,
             box_number,
             SUM(audit_quantity) audit_quantity
       FROM xxfox_audit_lines
      WHERE trip_id = p_trip1
        AND inventory_item_id = p_inv_item1
        GROUP BY trip_id,
             inventory_item_id,
             pallet_number,
             box_number
      ORDER BY trip_id,
             inventory_item_id,
             pallet_number,
             box_number;



   CURSOR c_ord_details IS
     SELECT * FROM XXFOX_TRIP_AUDIT_DETAILS_TBL
     WHERE NVL(NEW_REQ_QUANTITY,0) > 0;


BEGIN

   DELETE FROM xxfox_trip_audit_details_tbl
   WHERE CREATION_DATE < SYSDATE - 1;
   COMMIT;
 
   DELETE FROM xxfox_trip_audit_details_tbl
   WHERE trip_id = p_trip_id ;
   COMMIT;

   FOR c_ord_det_1 IN c_ord_det(p_trip_id,p_itemid) LOOP
      BEGIN
          INSERT INTO xxfox_trip_audit_details_tbl
                      (  TRIP_ID ,
                         DELIVERY_ID,
                         ORDER_NUMBER,
                         LINE_NUMBER,
                         INVENTORY_ITEM_ID,
                         SHIPMENT_NUMBER,
                         CUST_PO_NUMBER ,
                         REQ_QUANTITY,
                         NEW_REQ_QUANTITY,
                         SHIPPED_QTY ,
                         NEW_AUDIT_QUANTITY,
                         req_num
                       )
              VALUES ( c_ord_det_1.trip_id,
                       c_ord_det_1.DELIVERY_ID,
                       c_ord_det_1.ORDER_NUMBER,
                       c_ord_det_1.LINE_NUMBER,
                       c_ord_det_1.INVENTORY_ITEM_ID,
                       c_ord_det_1.SHIPMENT_NUMBER,
                       c_ord_det_1.CUST_PO_NUMBER,
                       c_ord_det_1.REQ_QTY,
                       c_ord_det_1.REQ_QTY,
                       c_ord_det_1.SHIPPED_QTY ,
                       NULL,
                       c_ord_det_1.req_num
                       );
      EXCEPTION
         WHEN OTHERS THEN
              DBMS_OUTPUT.put_line('Order Details Insertion Error ..'||sqlcode||'-'||sqlerrm);
      END;
   END LOOP;

   v_bal_audit_qty   := NULL;
   v_local_audit_qty := NULL;

   FOR c_audit_details IN c_aud_det(p_trip_id,p_itemid) LOOP
     BEGIN

         -- v_local_audit_qty := v_bal_audit_qty + c_audit_details.audit_quantity ;
          v_description     := NULL;
          v_local_audit_qty := NULL;
          v_po_num          := NULL;
          v_ord_num         := NULL;


          FOR  c_order_details IN c_ord_details LOOP
             BEGIN
                IF nvl(v_local_audit_qty, c_audit_details.audit_quantity) <= nvl(c_order_details.new_req_quantity,c_order_details.new_audit_quantity) THEN
                    IF v_description IS NOT NULL THEN
                        if(c_order_details.req_num is not null) then
                        v_description := v_description||chr(10)||'IR#: '||c_order_details.req_num||' Order#:'||c_order_details.order_number||'  Line#:'||c_order_details.line_number||'  Requested:'||c_order_details.req_quantity||'  Shipped:'||c_order_details.shipped_qty||'  Pallet#:'||c_audit_details.pallet_number||' Box#:'||c_audit_details.box_number||' PO#'||NVL(c_order_details.CUST_PO_NUMBER,0)||' Qty:'||nvl(v_local_audit_qty,c_audit_details.audit_quantity);
                        else
                       v_description := v_description||chr(10)||'Order#:'||c_order_details.order_number||'  Line#:'||c_order_details.line_number||'  Requested:'||c_order_details.req_quantity||'  Shipped:'||c_order_details.shipped_qty||'  Pallet#:'||c_audit_details.pallet_number||' Box#:'||c_audit_details.box_number||' PO#'||NVL(c_order_details.CUST_PO_NUMBER,0)||' Qty:'||nvl(v_local_audit_qty,c_audit_details.audit_quantity);
                       end if;
                    ELSE
                        if(c_order_details.req_num is not null) then
                        v_description := 'IR#: '||c_order_details.req_num||' Order#:'||c_order_details.order_number||'  Line#:'||c_order_details.line_number||'  Requested:'||c_order_details.req_quantity||'  Shipped:'||c_order_details.shipped_qty||'  Pallet#:'||c_audit_details.pallet_number||' Box#:'||c_audit_details.box_number||' PO#'||NVL(c_order_details.CUST_PO_NUMBER,0)||' Qty:'||nvl(v_local_audit_qty,c_audit_details.audit_quantity);
                        else
                       v_description := 'Order#:'||c_order_details.order_number||'  Line#:'||c_order_details.line_number||'  Requested:'||c_order_details.req_quantity||'  Shipped:'||c_order_details.shipped_qty||'  Pallet#:'||c_audit_details.pallet_number||' Box#:'||c_audit_details.box_number||' PO#'||NVL(c_order_details.CUST_PO_NUMBER,0)||' Qty:'||nvl(v_local_audit_qty,c_audit_details.audit_quantity);
                        end if;
                    END IF;
                    -- Start Inserting into the EDI audit table
                    BEGIN

                         SELECT   NVL (HCA.ATTRIBUTE7, 'N')
                           INTO   v_custatt
                          FROM   HZ_CUST_ACCOUNTS HCA
                         WHERE   party_id IN
                                             (SELECT   hp_ship.party_id
                                               FROM   oe_order_headers_all ooh,
                                                       hz_cust_site_uses_all hcs_ship,
                                                       hz_cust_acct_sites_all hca_ship,
                                                       hz_party_sites hps_ship,
                                                       hz_parties hp_ship,
                                                       hz_locations hl_ship,
                                                       hz_cust_site_uses_all hcs_bill,
                                                       hz_cust_acct_sites_all hca_bill,
                                                       hz_party_sites hps_bill,
                                                       hz_parties hp_bill,
                                                       hz_locations hl_bill,
                                                       mtl_parameters mp
                                               WHERE       1 = 1
                                                 AND ooh.order_number = c_order_details.order_number
                                                 AND ooh.ship_to_org_id = hcs_ship.site_use_id
                                                 AND hcs_ship.cust_acct_site_id =
                                                       hca_ship.cust_acct_site_id
                                                 AND hca_ship.party_site_id = hps_ship.party_site_id
                                                 AND hps_ship.party_id = hp_ship.party_id
                                                 AND hps_ship.location_id = hl_ship.location_id
                                                 AND ooh.invoice_to_org_id = hcs_bill.site_use_id
                                                 AND hcs_bill.cust_acct_site_id = hca_bill.cust_acct_site_id
                                                 AND hca_bill.party_site_id = hps_bill.party_site_id
                                                 AND hps_bill.party_id = hp_bill.party_id
                                                 AND hps_bill.location_id = hl_bill.location_id
                                                 AND mp.organization_id(+) = ooh.ship_from_org_id);

                         IF v_custatt = 'Y' THEN
                                INSERT INTO XXFOX.XXFOX_AUDIT_DATA_FOR_EDI_TBL
                                       (
                                         TRIP_ID   ,
                                         INVENTORY_ITEM_ID  ,
                                         AUDIT_QUANTITY ,
                                         BOX_NUMBER  ,
                                         PALLET_NUMBER  ,
                                         SSCC_CODE  ,
                                         PO_NUMBER  ,
                                         ORDER_NUMBER  ,
                                         ORDER_LINE_NUMBER  ,
                                         CONSUMED_QTY
                                        )
                                 VALUES (
                                     c_audit_details.trip_id, --TRIP_ID
                                     c_audit_details.inventory_item_id, --INVENTORY_ITEM_ID
                                     c_audit_details.audit_quantity, --AUDIT_QUANTITY
                                     c_audit_details.box_number, --BOX_NUMBER
                                     c_audit_details.pallet_number, --PALLET_NUMBER
                                     NULL, --SSCC_CODE
                                     NVL(c_order_details.CUST_PO_NUMBER,0), --PO_NUMBER
                                     c_order_details.order_number, --ORDER_NUMBER
                                     c_order_details.line_number , --ORDER_LINE_NUMBER
                                     nvl(v_local_audit_qty,c_audit_details.audit_quantity) --CONSUMED_QTY
                                    );
                                 COMMIT;
                         END IF;
                    EXCEPTION
                       WHEN OTHERS THEN
                              NULL;
                    END;  -- Start Inserting into the EDI audit table

                    -- To get the values for po_number and order_numbers coloumns in XXFOX_AUDIT_LINES table.
                    v_po_num := v_po_num||c_order_details.CUST_PO_NUMBER||',';
                    v_ord_num := v_ord_num||c_order_details.order_number||',';


                    UPDATE xxfox_trip_audit_details_tbl
                      SET NEW_REQ_QUANTITY = (nvl(c_order_details.new_req_quantity,c_order_details.new_audit_quantity) - nvl(v_local_audit_qty, c_audit_details.audit_quantity)),
                          NEW_AUDIT_QUANTITY = nvl(v_local_audit_qty, c_audit_details.audit_quantity) + NVL(NEW_AUDIT_QUANTITY,0)
                    WHERE trip_id =  c_order_details.trip_id
                      AND inventory_item_id = c_order_details.inventory_item_id
                      AND order_number = c_order_details.order_number
                      AND line_number  = c_order_details.line_number
                      AND shipment_number = c_order_details.shipment_number ;
                      COMMIT;
                    EXIT;  --- Exiting
                 ELSIF nvl(v_local_audit_qty, c_audit_details.audit_quantity) > nvl(c_order_details.new_req_quantity,c_order_details.REQ_QUANTITY) THEN
                    IF v_description IS NOT NULL THEN
                       v_description := v_description||chr(10)||'Order#:'||c_order_details.order_number||'  Line#:'||c_order_details.line_number||'  Requested:'||c_order_details.req_quantity||'  Shipped:'||c_order_details.shipped_qty||'  Pallet#:'||c_audit_details.pallet_number||' Box#:'||c_audit_details.box_number||' PO#'||NVL(c_order_details.CUST_PO_NUMBER,0)||' Qty:'||nvl(c_order_details.new_req_quantity,c_order_details.REQ_QUANTITY);
                    ELSE
                      v_description := 'Order#:'||c_order_details.order_number||'  Line#:'||c_order_details.line_number||'  Requested:'||c_order_details.req_quantity||'  Shipped:'||c_order_details.shipped_qty||'  Pallet#:'||c_audit_details.pallet_number||' Box#:'||c_audit_details.box_number||' PO#'||NVL(c_order_details.CUST_PO_NUMBER,0)||' Qty:'||nvl(c_order_details.new_req_quantity,c_order_details.REQ_QUANTITY);
                    END IF;
                    -- Start Inserting into the EDI audit table
                    BEGIN
                         SELECT   NVL (HCA.ATTRIBUTE7, 'N')
                           INTO   v_custatt
                          FROM   HZ_CUST_ACCOUNTS HCA
                         WHERE   party_id IN
                                             (SELECT   hp_ship.party_id
                                               FROM   oe_order_headers_all ooh,
                                                       hz_cust_site_uses_all hcs_ship,
                                                       hz_cust_acct_sites_all hca_ship,
                                                       hz_party_sites hps_ship,
                                                       hz_parties hp_ship,
                                                       hz_locations hl_ship,
                                                       hz_cust_site_uses_all hcs_bill,
                                                       hz_cust_acct_sites_all hca_bill,
                                                       hz_party_sites hps_bill,
                                                       hz_parties hp_bill,
                                                       hz_locations hl_bill,
                                                       mtl_parameters mp
                                               WHERE       1 = 1
                                                 AND ooh.order_number = c_order_details.order_number
                                                 AND ooh.ship_to_org_id = hcs_ship.site_use_id
                                                 AND hcs_ship.cust_acct_site_id =
                                                       hca_ship.cust_acct_site_id
                                                 AND hca_ship.party_site_id = hps_ship.party_site_id
                                                 AND hps_ship.party_id = hp_ship.party_id
                                                 AND hps_ship.location_id = hl_ship.location_id
                                                 AND ooh.invoice_to_org_id = hcs_bill.site_use_id
                                                 AND hcs_bill.cust_acct_site_id = hca_bill.cust_acct_site_id
                                                 AND hca_bill.party_site_id = hps_bill.party_site_id
                                                 AND hps_bill.party_id = hp_bill.party_id
                                                 AND hps_bill.location_id = hl_bill.location_id
                                                 AND mp.organization_id(+) = ooh.ship_from_org_id);

                         IF v_custatt = 'Y' THEN
                                 INSERT INTO XXFOX.XXFOX_AUDIT_DATA_FOR_EDI_TBL
                                       (
                                         TRIP_ID   ,
                                         INVENTORY_ITEM_ID  ,
                                         AUDIT_QUANTITY ,
                                         BOX_NUMBER  ,
                                         PALLET_NUMBER  ,
                                         SSCC_CODE  ,
                                         PO_NUMBER  ,
                                         ORDER_NUMBER  ,
                                         ORDER_LINE_NUMBER  ,
                                         CONSUMED_QTY
                                        )
                                VALUES (
                                     c_audit_details.trip_id, --TRIP_ID
                                     c_audit_details.inventory_item_id, --INVENTORY_ITEM_ID
                                     c_audit_details.audit_quantity, --AUDIT_QUANTITY
                                     c_audit_details.box_number, --BOX_NUMBER
                                     c_audit_details.pallet_number, --PALLET_NUMBER
                                     NULL, --SSCC_CODE
                                     NVL(c_order_details.CUST_PO_NUMBER,0), --PO_NUMBER
                                     c_order_details.order_number, --ORDER_NUMBER
                                     c_order_details.line_number , --ORDER_LINE_NUMBER
                                     nvl(c_order_details.new_req_quantity,c_order_details.REQ_QUANTITY) --CONSUMED_QTY
                                    );
                                COMMIT;
                         END IF;
                    EXCEPTION
                       WHEN OTHERS THEN
                              NULL;
                    END;   -- End Inserting into the EDI audit table

                    -- To get the values for po_number and order_numbers coloumns in XXFOX_AUDIT_LINES table.
                    v_po_num := v_po_num||c_order_details.CUST_PO_NUMBER||',';
                    v_ord_num := v_ord_num||c_order_details.order_number||',';

                    v_local_audit_qty :=  nvl(v_local_audit_qty, c_audit_details.audit_quantity) - nvl(c_order_details.new_req_quantity,c_order_details.REQ_QUANTITY);

                    UPDATE xxfox_trip_audit_details_tbl
                      SET NEW_REQ_QUANTITY = NULL,
                          new_audit_quantity = nvl(c_order_details.new_req_quantity,c_order_details.REQ_QUANTITY) + NVL(new_audit_quantity,0)
                    WHERE trip_id =  c_order_details.trip_id
                      AND inventory_item_id = c_order_details.inventory_item_id
                      AND order_number = c_order_details.order_number
                      AND line_number  = c_order_details.line_number
                      AND shipment_number = c_order_details.shipment_number ;
                      COMMIT;
                 END IF;
             END;
          END LOOP;
        DBMS_OUTPUT.put_line('v_description'||v_description);

         UPDATE xxfox.xxfox_audit_lines
           SET description  =  v_description,
               po_number    =  v_po_num,
              order_numbers = v_ord_num
         WHERE trip_id           = c_audit_details.trip_id
           AND inventory_item_id = c_audit_details.inventory_item_id
           AND NVL(pallet_number,0)     = NVL(c_audit_details.pallet_number,0)
           AND NVL(box_number,0)        = NVL(c_audit_details.box_number,0);
         COMMIT;
     END;
   END LOOP;

EXCEPTION
   WHEN OTHERS THEN
      DBMS_OUTPUT.put_line(SQLERRM);
END;
/
EXIT;