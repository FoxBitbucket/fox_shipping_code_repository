#!/bin/ksh
CRNO=CR_newchanges
INSTALL_PATH=$PWD
echo "$INSTALL_PATH"
DATE_TIME=`date | awk {' print $1"_"$2"_"$3"_"$6"_"$4 '}`
LOG_FILE=$CRNO"$DATE_TIME".log
echo $LOG_FILE
echo
echo "***Current Date and Time****" > $LOG_FILE
echo $DATE_TIME >> $LOG_FILE
echo "----------------------------" >> $LOG_FILE
echo >> $LOG_FILE

echo "Enter Global User Name (Apps).......: "
read apps_user

echo "Enter Global User Password.(Apps)...: "
read -s APPS_PSWD
APPSID=APPS/$APPS_PSWD

echo "Enter SID...........................: "
read dbsid
echo "Enter Port number...: "
read DB_PORT
echo "Enter db host address...: "
read HOST
# --------------------------------------------------------
#  Check if database SID is entered else prompt to get it
# --------------------------------------------------------
while [ -z "$dbsid" ];
do
    echo "Please enter Database SID : "
    read dbsid
done
cd $INSTALL_PATH
#Change permissions for all files
chmod 777 *
cd $INSTALL_PATH


# -----------  TABLES ----------------
# XXFOX_TRIPS_SEL_TEMP
# Table for packing workbench - XXFOX_TRIPS_SEL_TEMP 
echo "Table for packing workbench - XXFOX_TRIPS_SEL_TEMP "
echo
if sqlplus -s $APPSID @XXFOX_TRIPS_SEL_TEMP.sql exit
then
    echo "Table for packing workbench - XXFOX_TRIPS_SEL_TEMP successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Table for packing workbench - XXFOX_TRIPS_SEL_TEMP unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi




#-----------  PACKAGES ----------------
# XXFOX_PACKWB_PKG
#Package Spec compiling - XXFOX_PACKWB_PKG.pks
if sqlplus -s $APPSID @XXFOX_PACKWB_PKG.pks exit
then
    echo "Compilation of package spec XXFOX_PACKWB_PKG.pks successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Compilation of package spec XXFOX_PACKWB_PKG.pks unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi


#Package Body compiling - XXFOX_PACKWB_PKG.pkb
if sqlplus -s $APPSID @XXFOX_PACKWB_PKG.pkb exit
then
    echo "Compilation of Package Body XXFOX_PACKWB_PKG.pkb successful">>$LOG_FILE
    echo " ">>$LOG_FILE
else
    echo "Compilation of Package Body XXFOX_PACKWB_PKG.pkb unsuccessful">>$LOG_FILE
    echo "Please check and rerun">>$LOG_FILE
    echo "Aborting......">>$LOG_FILE
    exit 1
fi



#-----------  FORMS (2)----------------
# XXFOX_WSHPACWB.fmb
# XXFOX_WSHAUDWB.fmb
# Renaming the existing forms 
echo "Renaming the existing forms"
cp $XXFOX_TOP/forms/US/XXFOX_WSHPACWB.fmb $XXFOX_TOP/forms/US/XXFOX_WSHPACWB.fmb$CRNO
cp $XXFOX_TOP/forms/US/XXFOX_WSHAUDWB.fmb $XXFOX_TOP/forms/US/XXFOX_WSHAUDWB.fmb$CRNO
# Copying the forms to AU_TOP
echo "Copying the forms to AU_TOP"
cp XXFOX_WSHPACWB.fmb $AU_TOP/forms/US/XXFOX_WSHPACWB.fmb
cp XXFOX_WSHAUDWB.fmb $AU_TOP/forms/US/XXFOX_WSHAUDWB.fmb
#Changing the current directory to AU_TOP and compiling the form
echo "Changing the current directory to AU_TOP"
cd $AU_TOP/forms/US
#Change permissions 
chmod 777 XXFOX_WSHPACWB.fmb
chmod 777 XXFOX_WSHAUDWB.fmb


# Compiling the form XXFOX_WSHPACWB.fmb
echo "Compiling the form XXFOX_WSHPACWB.fmb"
frmcmp_batch module=$AU_TOP/forms/US/XXFOX_WSHPACWB.fmb userid=$APPSID output_file=$AU_TOP/forms/US/XXFOX_WSHPACWB.fmx module_type=form batch=no compile_all=yes
echo "Moving the XXFOX_WSHPACWB.fmx to custom top $XXFOX_TOP/forms/US "
mv $AU_TOP/forms/US/XXFOX_WSHPACWB.fmx $XXFOX_TOP/forms/US
#Error log generation
echo "Error file generation  at XXFOX_TOP/forms/US/XXFOX_WSHPACWB.err"
cat $LOG_FILE >> $INSTALL_PATH/XXFOX_WSHPACWB$CRNO.log
cat $INSTALL_PATH/XXFOX_WSHPACWB$CRNO.log $XXFOX_TOP/forms/US/XXFOX_WSHPACWB.err >>$LOG_FILE
rm $INSTALL_PATH/XXFOX_WSHPACWB$CRNO.log

# Compiling the form XXFOX_WSHAUDWB.fmb
echo "Compiling the form XXFOX_WSHAUDWB.fmb"
frmcmp_batch module=$AU_TOP/forms/US/XXFOX_WSHAUDWB.fmb userid=$APPSID output_file=$AU_TOP/forms/US/XXFOX_WSHAUDWB.fmx module_type=form batch=no compile_all=yes
echo "Moving the XXFOX_WSHAUDWB.fmx to custom top $XXFOX_TOP/forms/US "
mv $AU_TOP/forms/US/XXFOX_WSHAUDWB.fmx $XXFOX_TOP/forms/US
#Error log generation
echo "Error file generation  at XXFOX_TOP/forms/US/XXFOX_WSHAUDWB.err"
cat $LOG_FILE >> $INSTALL_PATH/XXFOX_WSHAUDWB$CRNO.log
cat $INSTALL_PATH/XXFOX_WSHAUDWB$CRNO.log $XXFOX_TOP/forms/US/XXFOX_WSHAUDWB.err >>$LOG_FILE
rm $INSTALL_PATH/XXFOX_WSHAUDWB$CRNO.log


cd $INSTALL_PATH

echo " "
echo "Installation Completed..."
echo "Please check installation log file : "
echo "Installation Completed..."
echo " "

exit 0
